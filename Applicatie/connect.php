
<?php

class connect {

    /**
     * @var Database $instance Instance of the Database class
     */
    private static $instance = null;

    public static function getInstance() {
        if (!isset(static::$instance)) {
            static::$instance = new connect();
        }
        return static::$instance;
    }

    private $db;

    private $hostname = '127.0.0.1';
    private $username = 'SA';
    private $password = '7W69GLtK6T600uF5xgla9l1';
    private $hostname = 'localhost';
    private $username = 'sa';
    private $database = 'FactBasedAnalysis';

    /**
     * Database constructor.
     */
    private function __construct() {
        try {
            $this->db = new PDO("sqlsrv:Server=$this->hostname;Database=$this->database;ConnectionPooling=0",$this->username, $this->password);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Destructs the database instance
     */
    public function __destruct() {
        $this->db = null;
    }

    /**
     * @return PDO The database connection
     */
    public function getDatabase() {
        return $this->db;
    }
}
?>