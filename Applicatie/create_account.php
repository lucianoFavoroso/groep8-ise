<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 20/05/2019
 * Time: 19:12
 */
$page_title = "Registreren";
include "header.php";
?>

<div class="login">
<div class="title">
    <H1>Account aanmaken</H1>
</div>
<div class="loginform">
        <form role="form" action="./php/PersistenceLayer/CreateAccRepo.php" method="post">
            <div class="mb-4">
                <input type="text" class="form-control" id="inputUserName" name="userName" placeholder="Gebruikersnaam">
            </div>
            <div class="mb-4">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Wachtwoord">
            </div>
        <div class="mb-4">
            <input type="password" class="form-control" id="second" name="second" placeholder="Herhaal wachtwoord">
        </div>
            <div class="btn btn-lg btn-block">
                <input type="submit" value="Aanmaken" name="submit" class="btn btn-primary"/>
            </div>
    </form>
</div>
</div>

<?php
include "footer.php";
?>
