<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 12:30
 */
$page_title = "Attribuut";
include "php/PersistenceLayer/AttributeRepository.php";
include "header.php";
$repo = new AttributeRepository();

?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Attribuut</H1>
</div>

<div class="instructiontext">
    <p>Benoem een attribuut in het volgende feit</p>
    <?php
    $repo = new AttributeRepository();
    $verbalisatieID = $_GET['ID'];
    $facts = $repo->getVerbalisation($_GET['ID']);

    if($facts != null) {
        echo "<p>\"{$facts[0]['VERBALISATIE_ZIN']}\"</p>";
        foreach ($facts as $fact) {
            echo "<p>\"{$fact['SUBVERBALISATIE_ZIN']}\"</p>";
        }
    }
    ?></div>

<div class="analysis">
    <form action="php/PersistenceLayer/AttributeRepository.php" method="post">
        <table class="entityanalysis">
            <tr>
                <td>Entiteit</td>
                <td>
                    <select name="entiteiten">
                    <?php
                    $entities = $repo->getAllEntitiesInProject($_GET['Project']);
                    if($entities != null) {

                        foreach ($entities as $entity) {
                            echo "<option value={$entity['ENTITEIT_ID']} name = 'EntityName'>{$entity['ENTITEIT_NAAM']}</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Naam attribuut</td>
                <td>
                    <input type="text" class="form-control" id="inputEntityName" name="AttributeName" placeholder="Naam">
                </td>
            </tr>
            <tr>
                <td>Primairy identifier</td>
                <td><input type="checkbox" id="primairyIdentifier" name="pi"></td>
            </tr>
            <tr>
                <td>Mandatory</td>
                <td><input type="checkbox" id="mandatory" name="m"></td>
            </tr>
            <tr>
                <td>Attribuutindex</td>
                <td>
                    <input type="number" class="form-control" id="inputIndexStart" name="indexStart" placeholder="Index start">
                </td>
                <td>
                    <input type="number" class="form-control" id="inputIndexEind" name="indexEind" placeholder="Index eind">
                </td>
            </tr>
        </table>
        <input type="hidden" name="VerbalisatieID" value="<?php echo "{$_GET['ID']}"?>">
        <input type="hidden" name="ProjectID" value="<?php echo "{$_GET['Project']}"?>">
        <div class="btn btn-lg btn-block">
            <input type="submit" value="Opslaan" name="submit_attribute" class="btn btn-primary"/>
        </div>
    </form>
</div>

<?php
include "footer.php";
?>