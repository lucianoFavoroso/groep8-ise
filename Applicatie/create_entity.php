<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 12:30
 */
$page_title = "Entiteit";
include "php/PersistenceLayer/EntityRepository.php";
include "header.php";
?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Entiteit</H1>
</div>

<div class="instructiontext">
    <p>Selecteer de analyse-actie die je wilt uitvoeren voor het volgende feiten.</p>
    <?php
    $repo = new EntityRepository();
    $verbalisatieID = $_GET['ID'];
    $facts = $repo->getVerbalisation($_GET['ID']);

    if($facts != null) {
        echo "<p>\"{$facts[0]['VERBALISATIE_ZIN']}\"</p>";
        foreach ($facts as $fact) {
            echo "<p>\"{$fact['SUBVERBALISATIE_ZIN']}\"</p>";
        }
    }
    ?>
</div>

<div class="analysis">
    <form action="php/PersistenceLayer/EntityRepository.php" method="post">
        <table class="entityanalysis">
            <tr>
                <td>Naam entiteit</td>
                <td>
                    <div class="mb-4">
                        <input type="text" class="form-control" id="inputEntityName" name="EntityName" placeholder="Naam">
                    </div>
                </td>
            </tr>
            <tr>
                <td>MATCH</td>
                <td><input type="checkbox" id="isMatch" name="isMatch"></td>
                <input type="hidden" name="VerbalisatieID" value="<?php echo "{$_GET['ID']}"?>">
                <input type="hidden" name="ProjectID" value="<?php echo "{$_GET['Project']}"?>">
            </tr>
            <tr>
                <td>Is subtype</td>
                <td><input type="checkbox" id="isSubtype" name="isSubtype"></td>


            </tr>
        </table>
        <div class="btn btn-lg btn-block">
            <input type="submit" value="Opslaan" name="submit_entity" class="btn btn-primary"/>
        </div>
    </form>
</div>

<?php
include "footer.php";
?>
