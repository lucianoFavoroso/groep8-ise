<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 23/05/2019
 * Time: 14:39
 */
$page_title = "Verbalisatie toevoegen";
include "header.php";
include "php/PersistenceLayer/VerbalisatieRepo.php";

?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['ProjectID']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Verbalisatie toevoegen</H1>
</div>

<form action="php/PersistenceLayer/VerbalisatieRepo.php" method="post">
<div class="newverbalisation">
    <p>Vul hier je verbalisatie in</p>
<div class="inputverbalisations">
    <div class="mb-4">
        <input type="text" class="form-control" id="inputVerbalisation" name="verbalisation" placeholder="Eerste verbalisatie">
    </div>
    <div class="mb-4">
        <input type="text" class="form-control" id="inputVerbalisation2" name="verbalisation2" placeholder="Tweede verbalisatie">
    </div>
    <input type="hidden" class="form-control" id="ProjectID" name="ProjectID" value="<?php echo "{$_GET['ProjectID']}"?>">

</div>
    <div class="btn btn-lg btn-block">
        <input type="submit" value="Aanmaken" name="submit" class="btn btn-primary"/>
    </div>
</form>
</div>



<?php
include "footer.php";
?>

