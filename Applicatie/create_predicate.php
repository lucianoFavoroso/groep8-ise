<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 12:31
 */
$page_title = "Predicate";
include "header.php";
?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Opstellen predicate</H1>
</div>

<div class="instructiontext">
    <p>"Er is een fiets met ID 5."</p>
</div>

<form action="details_fact.php" method="post">
    <div class="attribuutInPredicate">
    <p>Attribuut</p>
    <select name="attributes">
        <option value="a1">attribuut 1</option>
        <option value="a2">attribuut 2</option>
    </select>
    <div class="col-2">
        <input type="text" class="form-control" id="inputIndexStart" name="indexStart" placeholder="Index start">
    </div>
    <div class="col-2">
        <input type="text" class="form-control" id="inputIndexEind" name="indexEind" placeholder="Index eind">
    </div>
    </div>
    <div class="btn btn-lg btn-block">
        <input type="submit" value="Opslaan" name="submit" class="btn btn-primary"/>
    </div>
</form>

<?php
include "footer.php";
?>
