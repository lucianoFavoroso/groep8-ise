<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 22/05/2019
 * Time: 10:19
 */
$page_title = "Nieuw project";
include "header.php";
?>

<div class="logoutbutton">
    <a href="login.php">
        <button class="btn btn-primary">Uitloggen</button>
    </a>
</div>

<div class="title">
    <H1>Nieuw project aanmaken</H1>
</div>
<div class="newproject">
<form class="projectname" method="post" action="./php/PersistenceLayer/CreateProjectRepo.php">
    <label for="inputNewProjectName"></label><input type="text" class="form-control" id="inputNewProjectName" name="projectName" placeholder="Naam nieuw project"/>

    <div class="btn btn-lg btn-block">
        <input type="submit" value="Aanmaken" name="submit" class="btn btn-primary"/>
    </div>
</form>
</div>


<?php
include "footer.php";
?>

