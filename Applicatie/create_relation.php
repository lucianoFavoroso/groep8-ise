<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 12:31
 */
$page_title = "Relatie";
include "header.php";
include "php/PersistenceLayer/RelationRepository.php";
?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Relatie</H1>
</div>

<div class="instructiontext">
    <p>Benoem de relatie in het volgende feit</p>
    <?php
    $repo = new RelationRepository();
    $verbalisatieID = $_GET['ID'];
    $facts = $repo->getVerbalisation($_GET['ID']);

    if($facts != null) {
        echo "<p>\"{$facts[0]['VERBALISATIE_ZIN']}\"</p>";
        foreach ($facts as $fact) {
            echo "<p>\"{$fact['SUBVERBALISATIE_ZIN']}\"</p>";
        }
    }
    ?>
</div>


<div class="relatie">
    <form action="php/PersistenceLayer/RelationRepository.php" method="POST">
        <input type="hidden" name="VerbalisatieID" value="<?php echo "{$_GET['ID']}"?>">
        <input type="hidden" name="ProjectID" value="<?php echo "{$_GET['Project']}"?>">
        <div class="relationname">
            <p>Relatienaam</p>
            <div class="col-5">
                <input type="text" class="form-control" id="inputRelationName" name="RelationName" placeholder="Relatienaam">
            </div>
        </div>

        <table class="relatieanalysis">
            <tr>
                <th>Primaire Entiteit</th>
                <th></th>
                <th>Secundaire Entiteit</th>
                <th></th>
            </tr>
            <tr>
                <td>Entiteit</td>
                <td>
                    <select name="entiteiten">
                        <?php
                        $entities = $repo->getAllEntitiesInProject((int)$_GET['Project']);
                        if($entities != null) {
                            foreach ($entities as $entity) {
                                echo "<option value={$entity['ENTITEIT_ID']} name='PrimairEntity' id='priEt'>{$entity['ENTITEIT_NAAM']}</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td>Entiteit</td>
                <td>
                    <select name="entiteitenSec">
                        <?php
                        $entities = $repo->getAllEntitiesInProject($_GET['Project']);
                        if($entities != null) {
                            foreach ($entities as $entity) {
                                echo "<option value={$entity['ENTITEIT_ID']} name='SecundaireEntity' id='secEt'>{$entity['ENTITEIT_NAAM']}</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Multipliciteit</td>
                <td>
                    <input type="text" class="form-control" id="inputMultipliciteitPrimair" name="MultipliciteitPrimair" placeholder="Multipliciteit">
                </td>
                <td>Multipliciteit</td>
                <td>
                    <input type="text" class="form-control" id="inputMultipliciteitSecundair" name="MultipliciteitSecundair" placeholder="Multipliciteit">
                </td>
            </tr>
            <tr>
                <td>Dependent</td>
                <td><input type="checkbox" name="dependentPrimair"></td>
                <td>Dependent</td>
                <td><input type="checkbox" name="dependentSecundair"></td>
            </tr>
            <tr>
                <td>Mandatory</td>
                <td><input type="checkbox" name=mandatoryPrimair"></td>
                <td>Mandatory</td>
                <td><input type="checkbox" name="mandatorySecundair"></td>
            </tr>

        </table>
        <div class="btn btn-lg btn-block">
            <input type="submit" value="Opslaan" name="submit_relation" class="btn btn-primary"/>
        </div>
    </form>
</div>

<?php
include "footer.php";
?>
