<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 09:47
 */
$page_title = "Subtype entiteit aanmaken";
include "header.php";
?>

<script>
    let int = 1;
    function addInputField() {
        let element = document.createElement('div');
        element.className = "attribute";
        element.innerHTML = '<input type="text" class="form-control col-sm-6" id="naamSubtype" name="subtype['+int+'][naam]" placeholder="Naam subtype"><label>Primairy identifier</label> <input type="checkbox" id="pi" name="subtype['+int+'][pi]"> <label>Mandatory</label> <input type="checkbox" id="mandatory" name="subtype['+int+'][man]">'.trim();
        document.getElementById("attributes").appendChild(element);
        int++;
    }
</script>
<div class="buttons">
    <div class="projectbutton">
        <a href="details_project.php?Project=<?php echo "{$_GET['ProjectID']}" ?>">
            <button class=" btn btn-primary">Project</button>
        </a>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Subtype-entiteit aanmaken</H1>
</div>
<form action="php/PersistenceLayer/EntityRepository.php" method="post">
    <input type="hidden" name="ProjectID" value="<?php echo "{$_GET['ProjectID']}"?>">
    <div class="namesubtype">
        <h3>Naam subtype-entiteit</h3>
        <div class="mb-4">
            <input type="text" class="form-control" id="naamSubtypegroep" name="naamSubtypeEntiteit" placeholder="Naam subtype-entiteit">
        </div>
    </div>

    <div class="attributessubtype">
        <h3>Attributen</h3>
        <button type="button" class="btn btn-primary" style="width:500px;" onclick="addInputField()">+</button>
        <hr>
        <div id="attributes">
            <div class="attribute">
                <input type="text" class="form-control col-sm-6" id="naamSubtype" name="subtype[0][naam]" placeholder="Naam subtype">
                <label>Primairy identifier</label>
                <input type="checkbox" id="pi" name="subtype[0][pi]">
                <label>Mandatory</label>
                <input type="checkbox" id="mandatory" name="subtype[0][man]">
            </div>
        </div>
    </div>

    <div class="btn btn-lg btn-block">
        <input type="submit" class="btn btn-primary" value="Subtype-entiteit aanmaken" name="submit_sub_entity"></input>
    </div>
</form>
<?php
include "footer.php";
?>
