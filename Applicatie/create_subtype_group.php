<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 01:17
 */
$page_title = "Subtypegroep";
include "header.php";

require_once "php/BusinessLayer/SubtypeGroupService.php";
require_once "php/PersistenceLayer/SubtypeGroupRepository.php";

$subtypeGroupRepository = new SubtypeGroupRepository();
$subtypeGroupService = new SubtypeGroupService($subtypeGroupRepository);

$projectId = htmlspecialchars($_GET["Project"]);
$superType = htmlspecialchars($_GET["SupertypeID"]);

if (isset($_POST['submit'])) {
    $test = $_POST;
    $naamSubtypeGroep = $_POST['naamSubtypegroep'];
    $subtypeType = $_POST['options'];
    $entiteiten = $_POST['entiteiten'];
    $subtypeGroupService->addSubtypeGroup($superType, $naamSubtypeGroep, $subtypeType, $entiteiten);
}
?>
<section class="mb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center buttons">
                <div class="projectbutton">
                    <?php
                    echo "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
                    ?>
                </div>
                <div class="logoutbutton">
                    <a href="login.php">
                        <button class="btn btn-primary">Uitloggen</button>
                    </a>
                </div>
            </div>
            <div class="col-lg-12 text-center p-3">
                <?php
                if (isset($test)) {
                    ?>
                    <div class="alert alert-success" role="alert">
                        Subtype toegevoegd!
                    </div>
                    <?php
                }
                ?>
                <h1>Subtypegroep</h1>
            </div>
            <div class="col-lg-12">
                <div class="row" id="SuperType">
                    <div class="col-6 align-self-center">
                        <label>Supertype:</label>
                    </div>
                    <div class="col-6 align-self-center">
                        <p>
                            <?php
                                echo $subtypeGroupService->getSupertypeNameFromIdHTML($superType);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <form role="form" method="post" action="">
                    <div class="row" id="SuperType">
                        <div class="col-6 align-self-center">
                            <label class="mb-0">Supertypegroepnaam:</label>
                        </div>
                        <div class="col-6">
                            <input type="text" class="form-control h-100" id="naamSubtypegroep" name="naamSubtypegroep"
                                   placeholder="Naam subtypegroep">
                        </div>
                    </div>
                    <hr>
                    <div class="row text-center">
                        <label class="col-3">
                            <img src="/Img/normal.png" class="w-100" name="options">
                            <input type="radio" name="options" value="normal" checked>
                        </label>
                        <label class="col-3">
                            <img src="/Img/complete.png" class="w-100">
                            <input type="radio" name="options" value="complete">
                        </label>
                        <label class="col-3">
                            <img src="/Img/mutually.png" class="w-100">
                            <input type="radio" name="options" value="mutually">
                        </label>
                        <label class="col-3">
                            <img src="/Img/completeMutually.png" class="w-100">
                            <input type="radio" name="options" value="completeMutually">
                        </label>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12 text-left align-self-center mb-3">
                            <label class="m-0">
                                Subtypen
                            </label>
                        </div>
                        <div class="col-12 controls">
                            <!--JS: dynamicFormField, Div entry copied
                                Create php function for select!(replace select for dynamic select)<div>-->
                            <div class="entry input-group col-xs-3 mb-3">
                                <?php
                                echo $subtypeGroupService->getSubtypesFromProjectIdHtml($projectId);
                                ?>
                                <span class="input-group-btn">
                                    <button class="btn btn-success btn-add" type="button">
                                        &#43;
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <button class="form-control btn btn-primary" name="submit" type="submit">Subtypegroep toevoegen
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php
include "footer.php";
?>
