<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 23/05/2019
 * Time: 14:04
 */
$page_title = "Projectdetails";
include "php/PersistenceLayer/DetailFactsRepo.php";
include "header.php";
?>

<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
<div class="logoutbutton">
    <a href="login.php">
        <button class="btn btn-primary">Uitloggen</button>
    </a>
</div>
</div>

<div class="title">

    <H1>Feitdetails</H1>
</div>

<div class="instructiontext">
    <p>Selecteer de analyse-actie die je wilt uitvoeren voor de volgende feiten.</p>
    <?php
    $repo = new DetailFactsRepo();
    $verbalisatieID = $_GET['ID'];
    $feiten = $repo->getSubVerbalisations($_GET['ID']);
    if($feiten != null) {

        echo "<p>\"{$feiten[0]['VERBALISATIE_ZIN']}\"</p>";
        foreach ($feiten as $item) {
            echo "<p>\"{$item['SUBVERBALISATIE_ZIN']}\"</p>";
        }
    }else{
        echo "<p>Er zijn geen sub verbalisatie</p>";
    }
    ?>
</div>

<div class="analysis">
    <p>Reeds geïdentificeerde onderdelen</p>
    <div class="analysisentity">
        <table class="analysisentitytable">
            <tr>
                <th>Entiteit</th>
                <th>Primairy identifier</th>
            </tr>
            <?php
            $gevondenEntiteit = $repo->getEntiteitenBijVerbalisatie($_GET['ID']);
            if($gevondenEntiteit != null) {
                foreach ($gevondenEntiteit as $entiteiten) {
                    if ($entiteiten['IS_PRIMARY_IDENTIFIER'] = 1) {
                        echo "            <tr>
                <td>{$entiteiten['ENTITEIT_NAAM']}</td>
                <td>{$entiteiten['ATTRIBUUT_NAAM']}</td>
            </tr>";
                    } else {
                        echo "<td>geen PI</td>";
                    }
                }
            }
            ?>
        </table>
    </div>
    <div class="analysisattribute">
        <table class="analysisattributetable">
            <tr>
                <th>Attribuut</th>
                <th>Entiteit</th>
            </tr>
            <tr>
                <?php
                if($gevondenEntiteit != null) {
                    foreach ($gevondenEntiteit as $entiteiten) {
                        echo "            <tr>
                <td>{$entiteiten['ATTRIBUUT_NAAM']}</td>
                <td>{$entiteiten['ENTITEIT_NAAM']}</td>
            </tr>";
                    }
                }
                ?>
            </tr>
        </table>
    </div>
    <div class="analysisrelation">
        <table class="analysisattributetable">
            <tr>
                <th>Relatie naam</th>
                <th>Van</th>
                <th>Naar</th>
                <th>Relatie type</th>
            </tr>
            <tr>
            <?php
            
            $relation = $repo->getRelationsByVerbalisation($verbalisatieID);
            echo "      <tr><td>{$relation[0]['RELATIE_NAAM']}</td>
                            <td>{$relation[0]['VAN']}</td>
                            <td>{$relation[0]['NAAR']}</td>
                        <td>{$relation[0]['CARDINALITEITSTYPE']}</td>
                </tr>";
            ?>
            </tr>
        </table>
    </div>
    <div class="analysispredicate">

    </div>
</div>

        <div class="factbuttons">
            <div class="btn btn-md btn-block">
                <?php
                echo "<a href=\"/create_entity.php?ID={$verbalisatieID}&Project={$_GET['Project']}\">
                    <button class=\"btn btn-primary\">Entiteit</button>
                </a>";
                ?>

            </div>
            <div class="btn btn-md btn-block">
                <?php
                echo "<a href=\"/create_attribute.php?ID={$verbalisatieID}&Project={$_GET['Project']}\">
                    <button class=\"btn btn-primary\">Attribuut</button>
                </a>";
                ?>
            </div>
            <div class="btn btn-md btn-block">
                <?php
                echo "<a href=\"/create_relation.php?ID={$verbalisatieID}&Project={$_GET['Project']}\">
                    <button class=\"btn btn-primary\">Relatie</button>
                </a>";
                ?>
            </div>
            <div class="btn btn-md btn-block">
                <?php
                echo "<a href=\"/create_predicate.php?ID={$verbalisatieID}&Project={$_GET['Project']}\">
                    <button class=\"btn btn-primary\">Predicate</button>
                </a>";
                ?>
            </div>
        </div>

<?php
include "footer.php";
?>
