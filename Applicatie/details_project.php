<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 22/05/2019
 * Time: 10:11
 */
$page_title = "Projectdetails";
include "header.php";
include "php/PersistenceLayer/VerbalisatieRepo.php";
$repo = new VerbalisatieRepo();

?>

    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>

    <div class="title">
        <H1>Projectdetails</H1>
    </div>

    <div class="projectdetails">
        <div class="factsmain col-8">
            <div class="col-12">
                <h2>Feiten</h2>
            </div>
            <div class="facts col-12">
                <?php
                $projects = $repo->getAllVerbalisatieInProject($_GET['Project']);
                if ($projects != null) {
                    foreach ($projects as $item) {
                        echo "<div class=\"fact\">
                <a href=\"details_fact.php?ID={$item['VERBALISATIE_ID']}&Project={$_GET['Project']}\">{$item['VERBALISATIE_ZIN']}</a></a>
            </div>";
                    }
                } else {
                    echo "<p>Geen verbalisaties gevonden</p>";
                }
                ?>
            </div>
            <div class="addfact col-12">
                <a href="create_fact.php?ProjectID=<?php echo "{$_GET['Project']}" ?>">
                    <button class="btn btn-primary">Feit toevoegen</button>
                </a>
            </div>
        </div>

        <div class="information col-4">
            <h2>Informatie</h2>
            <div class="projectbuttons">
                <div class="btn btn-md btn-block">
                    <a href="overview_entities.php?Project=<?php echo "{$_GET['Project']}"; ?>">
                        <button class="btn btn-primary">Entiteiten en subtype</button>
                    </a>
                </div>
                <div class="btn btn-md btn-block">
                    <a href="overview_business_rules.php?ProjectID=<?php echo "{$_GET['Project']}" ?>">
                        <button class="btn btn-primary">Business rules</button>
                    </a>
                </div>
                <div class="btn btn-md btn-block">

                    <a href="overview_project_history.php?Project=<?php echo "{$_GET['Project']}" ?>">
                        <button class="btn btn-primary">Geschiedenis</button>
                    </a>
                </div>
            </div>
            <div class="groupmembers">
                <h2>Groepsleden</h2>
                <ul>
                    <?php
                    $usersInProject = $repo->getProjectMembersForProject($_GET['Project']);
                    foreach ($usersInProject as $user) {
                        echo "<li>{$user['GEBRUIKERSNAAM']}</li>";
                    }
                    ?>
                </ul>
                <form action="php/PersistenceLayer/ProjectRepo.php" method="post">
                    <input type="text" id="username" name="username">
                    <input type="hidden" id="projectID" name="projectID" value="<?php echo "{$_GET['Project']}" ?>">
                    <a href="php/PersistenceLayer/ProjectRepo.php">
                        <button type="submit">Voeg lid toe</button>
                    </a>
                </form>

            </div>
        </div>
    </div>

<?php
include "footer.php";
?>