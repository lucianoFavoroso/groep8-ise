<?php
$page_title = "Inloggen";
include "header.php";

?>

<div class="login">
    <div class="title">
        <H1>Inloggen</H1>
    </div>
    <div class="loginform">
        <form role="form" method="post" action="./php/PersistenceLayer/InloggenRepo.php">
            <div class="mb-3">
                <input type="text" class="form-control" id="inputUserName" name="userName" placeholder="Gebruikersnaam">
            </div>
            <div class="mb-3">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Wachtwoord">
            </div>

            <div class="btn btn-lg btn-block">
                <a href="overview_projects.php">
                    <input type="submit" value="Inloggen" name="submit" class="btn btn-primary"/>
                </a>
            </div>
        </form>
        <form role="form" action="create_account.php">
            <div class="btn btn-lg btn-block">
                <a href="create_account.php">
                    <button class="btn btn-primary">Account aanmaken</button>
                </a>
            </div>
        </form>
    </div>
</div>
<?php
include "footer.php";
?>




