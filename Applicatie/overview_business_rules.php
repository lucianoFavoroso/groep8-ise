<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 16:21
 */
$page_title = "Business rules";
include "header.php";
include "php/PersistenceLayer/BusinessRuleRepository.php";
$repo = new BusinessRuleRepository();
?>

<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['ProjectID']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Business rules</H1>
</div>

<div class="businessrules">
    <ul>
        <?php
        $businessRules = $repo->getAllBusinessRules($_GET['ProjectID']);
        foreach ($businessRules as $businessRule){
            echo "<li>{$businessRule['BUSINESS_RULE_ZIN']}</li>";
        }
        ?>
    </ul>
</div>

<div class="newbusinessrules">
    <form action="php/PersistenceLayer/BusinessRuleRepository.php" method="post">
        <input type="hidden" name="ProjectID" value="<?php echo "{$_GET['ProjectID']}"?>">
        <input type="text" class="form-control" id="businessRuleName" name="businessRuleName" placeholder="Business rule naam">

        <div class="btn btn-lg btn-block">
            <input type="submit" value="Voeg toe" name="submit_Business_Rule" class="btn btn-primary"/>
        </div>
    </form>

</div>

<?php
include "footer.php";
?>
