<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 23/05/2019
 * Time: 14:08
 */
$page_title = "Entiteiten en subtypen";
include "header.php";
include "./PHP/PersistenceLayer/SubTypeRepo.php";

$Repo = new SubTypeRepo();
?>

<div class="buttons">
    <div class="projectbutton">

        <?php
        echo "<a href=\"details_project.php?Project={$_GET['Project']}\">
                <button class=\"btn btn-primary\">Project</button>
                </a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Overzicht entiteiten en subtypen</H1>
</div>

<<<<<<< HEAD
<table class="predicatetable">
    <tr>
        <td>Post</td>
        <td></td>
        <td><a href="create_subtype_group.php"><button class="btn btn-primary">subtype toevoegen</button></a></td>
    </tr>
    <tr>
        <td>Station</td>
        <td></td>
        <td><a href="create_subtype_group.php"><button class="btn btn-primary">subtype toevoegen</button></a></td>
    </tr>
    <tr>
        <td>Fiets</td>
        <td>
            <table>
                <td>
                    <tr>Fietssoorten</tr>
                    <tr>
                        <ul>
                            <li>mechanische fiets</li>
                            <li>Elektrische fiets</li>
                        </ul>
                    </tr>
                </td>
                <td>
                    <img class="subtypeimg" src="/Img/completeMutually.png">
                </td>
            </table>
        <td><a href="create_subtype_group.php"><button class="btn btn-primary">subtype toevoegen</button></a></td>
    </tr>
=======
<div class="row align-items-center justify-content-center">
    <div class="col-10">
        <table class="table table-striped table-hover">
            <tr>
                <th>
                    Entiteit
                </th>
                <th>
                    Subtypes
                </th>
                <th>
                </th>
            </tr>
>>>>>>> develop

            <?php
            $data = $Repo->getAllEntities($_GET['Project']);

            foreach ($data as $item) {
                if ($item['SUBTYPE_ID'] == null) {
                    echo "<tr>
                    <td>
                    {$item['ENTITEIT_NAAM']}";
                    echo "
                </td>";
                    $subEntiteiten = $Repo->getSubtypesWithAllEntities($item['ENTITEIT_ID']);
                    if ($subEntiteiten != null) {

                                echo "
                <td>";
                                if($subEntiteiten[0]['COMPLETE'] == 1 && $subEntiteiten[0]['MUTUALLY_EXCLUSIVE'] == 1){
                                    echo "<img src=\"Img/completeMutually.png\" alt=\"complete\" class=\"col-3\" style=\"margin: 0px;padding: 0px;\">";
                                }
                        if($subEntiteiten[0]['COMPLETE'] == 0 && $subEntiteiten[0]['MUTUALLY_EXCLUSIVE'] == 1){
                            echo "<img src=\"Img/mutually.png\" alt=\"complete\" class=\"col-3\" style=\"margin: 0px;padding: 0px;\">";
                        }
                        if($subEntiteiten[0]['COMPLETE'] == 1 && $subEntiteiten[0]['MUTUALLY_EXCLUSIVE'] == 0){
                            echo "<img src=\"Img/complete.png\" alt=\"complete\" class=\"col-3\" style=\"margin: 0px;padding: 0px;\">";
                        }
                        if($subEntiteiten[0]['COMPLETE'] == 0 && $subEntiteiten[0]['MUTUALLY_EXCLUSIVE'] == 0){
                            echo "<img src=\"Img/normal.png\" alt=\"complete\" class=\"col-3\" style=\"margin: 0px;padding: 0px;\">";
                        }
                                echo "
                    <ul class=\"list-group\">";
                    foreach ($subEntiteiten as $subEnt) {
                            if ($subEnt != null) {
                                echo "
                        <li class=\"list-inline-item\">{$subEnt['ENTITEIT_NAAM']}</li>
                        ";
                                echo "
                    </ul>
                </td>";
                            }
                        }
                    } else {
                        echo "<td>Er zijn geen subtypes gevonden bij deze entiteit.</td>";
                    }
                    echo "
                <td>
                    <span class=\"input-group-btn float-right col-2\">
                    <a href='create_subtype_group.php?Project={$_GET['Project']}&SupertypeID={$item['ENTITEIT_ID']}'>
                        <button class=\"btn btn-success btn-add\" type=\"button\">
                            &#43;
                        </button>
                     </a>
                    </span>
                </td>
            </tr>
                    ";
                }
            }
            ?>

<<<<<<< HEAD
<div class="btn btn-lg btn-block">
    <a href="create_subtype_entity.php?ProjectID=<?php echo "{$_GET['ProjectID']}" ?>">
        <button class="btn btn-primary">Subtype entiteit aanmaken</button>
    </a>
=======
        </table>
    </div>
>>>>>>> develop
</div>


<?php
include "footer.php";
?>
