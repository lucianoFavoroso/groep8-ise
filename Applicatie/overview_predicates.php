<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 23/05/2019
 * Time: 14:07
 */
$page_title = "Overzicht predicaten";
include "header.php";
//include "php/persistenceLayer/PredicateRepository.php"


//TODO dit komt straks uit de database query.
$keys = [
    1 => [
        "VERBALISATIE_ZIN" => "test",
        "predicate" => "test pred"
    ],
    2 => [
        "VERBALISATIE_ZIN" => "test",
        "predicate" => "test pred"
    ]

];
?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['ProjectID']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
    <div class="logoutbutton">
        <a href="login.php">
            <button class="btn btn-primary">Uitloggen</button>
        </a>
    </div>
</div>

<div class="title">
    <H1>Overzicht predicaten</H1>
</div>

<table class="predicatetable">
    <tr>
        <th>Feit</th>
        <th>Predicate</th>
    </tr>

    <?php foreach ($keys as $key => $value) { ?>
        <tr>
            <td><?php echo $value['VERBALISATIE_ZIN'] ?></td>
            <td><?php echo $value['predicate'] ?></td>
        </tr>
    <?php } ?>

</table>


<?php
include "footer.php";
?>
