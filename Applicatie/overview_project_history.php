<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 23/05/2019
 * Time: 14:05
 */
$page_title = "Nieuw project";
include "header.php";
include "php/PersistenceLayer/HistRepo.php";
?>


<div class="buttons">
    <div class="projectbutton">
        <?php
        echo  "<a href=\"details_project.php?Project={$_GET['Project']}\"><button class=\"btn btn-primary\">Project</button></a>";
        ?>
    </div>
<div class="logoutbutton">
    <a href="login.php">
        <button class="btn btn-primary">Uitloggen</button>
    </a>
</div>
</div>

<div class="row justify-content-center align-items-center">
    <div class="col-10 text-center">
        <h1>History</h1>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
        <table class="table table-striped table-hover">
            <tr>
                <th>Gebruiker</th>
                <th>ChangeTime</th>
                <th>New value</th>
                <th>Old value</th>
                <th>Tabel</th>
            </tr>
                <?php
                $repo = new HistRepo();
                $hist = $repo->getHistForProject($_GET['Project']);
                foreach ($hist as $item) {
                    echo "<tr>";
                    echo "<td>{$item['GEBRUIKERSNAAM']}</td>";
                    echo "<td>{$item['changeTime']}</td>";
                    echo "<td>{$item['newValue']}</td>";
                    echo "<td>{$item['oldValue']}</td>";
                    echo "<td>{$item['tableName']}</td>";
                    echo "</tr>";
                }
                ?>

        </table>
        </div>
    </div>
</div>







<?php
include "footer.php";
?>
