<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 21/05/2019
 * Time: 11:42
 */
$page_title = "Projects";
include "header.php";
include 'php/PersistenceLayer/ProjectRepo.php';
?>
<div class="section1">
<div class="logoutbutton">
    <a href="login.php">
        <button class="btn btn-primary">Uitloggen</button>
    </a>
</div>

<div class="title">
    <H1>Projecten</H1>
</div>

<div class="newbutton">
    <a href="create_project.php">
        <button class="btn btn-primary">+</button>
    </a>
</div>

<div class="projects">
    <?php
    $repo = new ProjectRepo();
    $userProjects = $repo->getProjectsForUser($_SESSION['Gebruikersnaam']);
    if($userProjects != null) {
        foreach ($userProjects as $userProject) {
            echo "
<div class=\"project\">
    <div class=\"card mb-4 shadow-sm\">
        <a href=\"details_project.php?Project={$userProject['PROJECT_ID']}\">
            <div class=\"card-header\">
<h4 class=\"my-0 font-weight-normal\">{$userProject['PROJECTNAAM']}</h4>
            </div>
        </a>
    </div>
    </div>";
        }
    }
    else{
        echo "Geen projecten";
    }?>
</div>

</div>

<?php
include "footer.php";
?>

