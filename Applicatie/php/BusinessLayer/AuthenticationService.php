<?php

class AuthenticationService
{
    /* @var UserRepository $userRepository */
    private $userRepository;
    private $errors = [];

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

    /*--LOGIN--*/
    function login($email, $password){
        $loggedIn = $this->userRepository->checkLogin($email, $password);
        if($loggedIn){
            $_SESSION['user'] = $loggedIn;
            header('Location: /index.php');
        }else{
            $this->errors[] = "Uw gebruikersnaam of wachtwoord is verkeerd!";
        }
    }

    function checkIfEmailExists($email){
        if($this->userRepository->getUserData($email) > 0){
            return true;
        }
        return false;
    }

    function logout(){
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
        header('Location: /index.php');
    }

    function hasErrors(){
        return $this->errors;
    }
}