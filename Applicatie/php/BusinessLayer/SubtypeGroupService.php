<?php

class SubtypeGroupService
{

    private $subtypeGroupRepository;

    public function __construct(SubtypeGroupRepository $subtypeGroupRepository)
    {
        $this->subtypeGroupRepository = $subtypeGroupRepository;
    }

    public function getSubtypesFromProjectIdHtml($project_id): string
    {
        $html = "<select class='form-control' name='entiteiten[]'>";
        foreach ($this->getSubtypes($project_id) as $entiteit) {
            $html .= "<option value='" . $entiteit['ENTITEIT_ID'] . "'>" . $entiteit['ENTITEIT_NAAM'] . "</option>";
        }
        $html .= "</select>";
        return $html;
    }

    public function getSupertypeNameFromIdHTML($subtype){
        return $this->getSupertype($subtype)[0]['ENTITEIT_NAAM'];
    }

    private function getSubtypes($project_id)
    {
        return $this->subtypeGroupRepository->getAllEntitiesFromProject($project_id);
    }

    private function getSupertype($superType_id)
    {
        return $this->subtypeGroupRepository->getSuperTypeById($superType_id);
    }

    public function addSubtypeGroup($superType, $naamSubtypeGroep, $subtypeType, array $entiteiten): void
    {
        $groupType = $this->getSubtypeTypeArray($subtypeType);
        foreach ($entiteiten as $subtype) {
            $this->subtypeGroupRepository->insertSubtypeGroup($superType, $subtype, $_SESSION['Gebruikersnaam'],
                $naamSubtypeGroep, $groupType[0], $groupType[1]);
        }
    }

    private function getSubtypeTypeArray($subtypeType): array
    {
        switch ($subtypeType) {
            case "complete":
                return array(0, 1);
                break;
            case "mutually":
                return array(1, 0);
                break;
            case "completeMutually":
                return array(1, 1);
                break;
            default:
                return array(0, 0);
                break;
        }
    }
}

?>