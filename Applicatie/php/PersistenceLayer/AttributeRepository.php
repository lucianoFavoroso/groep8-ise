<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 03/06/2019
 * Time: 09:30
 */
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$AR = new AttributeRepository();

class AttributeRepository
{
    public $conn;

    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();
        if(isset($_POST['submit_attribute'] )){
            $pi = 0;
            $mandatory = 0;
            var_dump($_POST);
            if(isset($_POST['pi'] )){
                $pi = 1;
            }
            if(isset($_POST['m'])){
                $mandatory = 1;
            }
           $this->createNewAttribute($_POST['entiteiten'],$_POST['VerbalisatieID'],
               $_SESSION['Gebruikersnaam'], $_POST['AttributeName'], $pi, $mandatory, $_POST['indexStart'], $_POST['indexEind']);
            header("location: ../../details_fact.php?ID={$_POST['VerbalisatieID']}&Project={$_POST['ProjectID']}");
        }
    }



    public function getAllEntitiesInProject($project_ID){
        $stmt =$this->conn->prepare("EXEC usp_getAllEntitiesInProject ?");
        $stmt->execute(array($project_ID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    Public function createNewAttribute($entity_ID,$verbalisatieID ,$user_name, $attribute_name, $pi, $mandatory, $index1, $index2){
        $stmt = $this->conn->prepare("EXEC usp_addAttribute ?,?, ?, ?, ?, ?, ?, ?");
        $stmt->execute(array($entity_ID,
            $verbalisatieID,
            $index1,
            $index2,
            $user_name,
            $attribute_name,
            $pi,
            $mandatory));
    }

    public function getVerbalisation($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getVerbalisation ?");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }


}