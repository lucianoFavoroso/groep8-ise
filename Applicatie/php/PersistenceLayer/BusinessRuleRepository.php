<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 03/06/2019
 * Time: 11:18
 */
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$BRR = new BusinessRuleRepository();

class BusinessRuleRepository
{

    public $conn;

    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();

        if(isset($_POST['submit_Business_Rule'])){
            $this->addNewBusinessRule(intval($_POST['ProjectID']), $_POST['businessRuleName'], $_SESSION['Gebruikersnaam']);
            header("location: ../../overview_business_rules.php?ProjectID={$_POST['ProjectID']}");
        }
    }

    public function getAllBusinessRules($project_ID){
        $stmt =$this->conn->prepare("SELECT BUSINESS_RULE_ZIN FROM BUSINESS_RULE WHERE PROJECT_ID = ?");
        $stmt->execute(array($project_ID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function addNewBusinessRule($project_ID, $businessRuleName, $userName){
        $stmt =$this->conn->prepare("EXEC usp_CreateNewBusinessRule @ProjectID = ?, @BusinessRuleSentence =  ?, @userName = ?");
        $stmt->execute(array($project_ID, $businessRuleName, $userName));
    }



}