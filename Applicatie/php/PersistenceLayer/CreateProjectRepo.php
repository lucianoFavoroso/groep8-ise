<?php


include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";

$ER = new CreateProjectRepo();

class CreateProjectRepo
{
    public $conn;

    public function __construct(){
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();
        var_dump($_POST);
        if(isset($_POST['projectName'])){
            $stmt = $this->conn->prepare("EXEC usp_CreateNewProjectForUser ?,?");
            $stmt->execute(array($_POST['projectName'],$_SESSION['Gebruikersnaam']));
            header("Location: ../../overview_projects.php?");
        }
    }
}