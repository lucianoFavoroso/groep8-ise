<?php

include "./connect.php";
class DetailFactsRepo
{
    public $conn;
    public function __construct()
    {
        $this->conn = connect::getInstance()->getDatabase();
    }

    public function getSubVerbalisations($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getSubverbs ?");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function getEntiteitenBijVerbalisatie($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getEntitiesByVerbalisation ?");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function getRelationsByVerbalisation($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getRelationsByVerbalisation ?
");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

}