<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 29/05/2019
 * Time: 11:00
 */
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$ER = new EntityRepository();


class EntityRepository
{

    public $conn;

    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();
        if(isset($_POST['submit_entity'])){
            $isMatch = 0;
            $isSubtype = 0;
            if(isset($_POST['isMatch'] )){
                $isMatch = 1;
            }
            if(isset($_POST['isSubtype'] )){
                $isSubtype = 1;
            }

            $this->CreateNewEntity($_POST['EntityName'], $_SESSION['Gebruikersnaam'],$_POST['ProjectID'], $_POST['VerbalisatieID'], $isMatch);
            header("location: ../../details_fact.php?ID={$_POST['VerbalisatieID']}&Project={$_POST['ProjectID']}");
        }
        if(isset($_POST['submit_sub_entity'])){

            $this->CreateNewSubEntity($_SESSION['Gebruikersnaam'], $_POST['ProjectID'], $_POST['naamSubtypeEntiteit']);

            $attributes = $_POST['subtype'];
            foreach ($attributes as $attribute){
                $pi = 0;
                $mandatory = 0;
                if(isset($attribute['pi'])){
                    $pi = 1;
                }
                if(isset($attribute['man'] )){
                    $mandatory = 1;
                }
                $this->CreateNewSubEntityAttribute($_SESSION['Gebruikersnaam'], $_POST['ProjectID'], $_POST['naamSubtypeEntiteit'], $attribute['naam'], $pi, $mandatory);
                header("location: ../../overview_entities.php?ProjectID={$_POST['ProjectID']}");
            }

        }
    }


    public function CreateNewEntity($entity_name, $user_name, $project_ID, $verbalisatie_ID, $isMatch){
        $stmt = $this->conn->prepare("EXEC usp_addEntity ?, ?, ?, ?, ?");
        $stmt->execute(array($project_ID, $verbalisatie_ID, $user_name, $entity_name, $isMatch));
    }


    public function CreateNewSubEntity($Gebruikersnaam, $ProjectID, $naamSubTypeEntiteit){
        $stmt = $this->conn->prepare("EXEC usp_CreateNewsubEntity ?, ?, ?");
        $stmt->execute(array($Gebruikersnaam, $ProjectID, $naamSubTypeEntiteit));
    }

    public function CreateNewSubEntityAttribute($Gebruikersnaam, $ProjectID, $naamSubTypeEntiteit, $naam, $pi, $mandatory){
        $stmt = $this->conn->prepare("EXEC usp_CreateNewSubEntityAttribute ?, ?, ?, ?, ?, ?");
        $stmt->execute(array($Gebruikersnaam, $ProjectID, $naamSubTypeEntiteit, $naam, $pi, $mandatory));
    }


    public function getVerbalisation($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getVerbalisations ?");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }
}


