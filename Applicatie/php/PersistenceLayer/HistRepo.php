<?php

include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";

$ER = new HistRepo();
class HistRepo
{
    public $conn;

    public function __construct(){
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();
    }

    public function getHistForProject($projectID){
        $stmt = $this->conn->prepare("EXEC usp_getHistory ?");
        $stmt->execute(array($projectID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
}