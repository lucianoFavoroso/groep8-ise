<?php
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
if(!isset($_SESSION)){
    session_start();
}

$dasd = new InloggenRepo();

class InloggenRepo
{

    public $conn;

    public function __construct()
    {
        $this->conn = connect::getInstance()->getDatabase();
        var_dump($_POST);
        if(isset($_POST['userName']) && isset($_POST['password'])) {
            $this->login($_POST['userName'], $_POST['password']);
        }
    }

    public function login($user,$pass){
        $stmt =$this->conn->prepare("EXEC usp_checkUserCredentials ?,?");
        $stmt->execute(array($user,$pass));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($data[0]['userExists'] == 1){
            session_destroy();
            session_start();
            $_SESSION['Gebruikersnaam'] = $user;
            header("Location: /overview_projects.php");
            exit();
        }else{
            echo "gebruikersnaam of wachtwoord niet correct";
        }
    }
}