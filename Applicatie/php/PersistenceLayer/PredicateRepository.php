<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 27/05/2019
 * Time: 16:41
 */

class PredicateRepository extends Repository
{

    Function getPredicates($projectID){
        return $this->fetch("select * from Users where projectID = :projectID", [
            ':projectID'=> $projectID
        ]);
    }

    public function getAllVerbalisatieInProject($ProjectId){
        $stmt =$this->conn->prepare("EXEC usp_getVerbalisationsInProject ?");
        $stmt->execute(array($ProjectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }
}