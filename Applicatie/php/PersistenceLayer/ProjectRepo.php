<?php

include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$ER = new ProjectRepo();
class ProjectRepo
{
    public $conn;
    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();
        if(isset($_POST['username'])){
            $stmt = $this->conn->prepare("EXEC usp_addUserToProject ?,?,?");
            $stmt->execute(array($_SESSION['Gebruikersnaam'],$_POST['username'],$_POST['projectID']));
            header("Location: ../../details_project.php?Project={$_POST['projectID']}");
        }
    }

    public function getProjectsForUser($gebruikersnaam){
        $stmt =$this->conn->prepare("EXEC usp_getProjectsForUser ?");
        $stmt->execute(array($gebruikersnaam));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

}