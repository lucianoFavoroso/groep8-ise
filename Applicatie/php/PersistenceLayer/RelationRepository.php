<?php
/**
 * Created by IntelliJ IDEA.
 * User: cathelijnedebruijn
 * Date: 03/06/2019
 * Time: 11:28
 */
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$RR = new RelationRepository();

class RelationRepository
{

    public $conn;

    public function __construct()
    {
        if(!isset($_SESSION)){
           session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();

        if(isset($_POST['submit_relation'])){
            print_r($_POST);
            $dependent_primair = 0;
            $dependent_secundair = 0;
            $mandatory_primair = 0;
            $mandatory_secundair = 0;
            if(isset($_POST['dependentPrimair'] )){
                $dependent_primair = 1;
            }
            if(isset($_POST['dependentSecundair'] )){
                $dependent_secundair = 1;
            }
            if(isset($_POST['mandatoryPrimair'] )){
                $mandatory_primair = 1;
            }
            if(isset($_POST['mandatorySecundair'] )){
                $mandatory_secundair = 1;
            }

            $this->createNewRelation($_POST['RelationName'], $_POST['entiteiten'], $_POST['entiteitenSec'], $_POST['MultipliciteitPrimair'], $_POST['MultipliciteitSecundair'], $dependent_primair, $dependent_secundair, $mandatory_primair, $mandatory_secundair, $_SESSION['Gebruikersnaam'],$_POST['ProjectID'], $_POST['VerbalisatieID']);
            header("location: ../../details_fact.php?ID={$_POST['VerbalisatieID']}&Project={$_POST['ProjectID']}");
        }



    }

    public function getAllEntitiesInProject($project_ID){
        $stmt =$this->conn->prepare("EXEC usp_selectAllEntitiesFromProject ?");
        $stmt->execute(array($project_ID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function getVerbalisation($VerbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_getVerbalisations ?");
        $stmt->execute(array($VerbalisatieID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    Public function createNewRelation($relation_name, $entity_name_primair, $entity_name_secundair, $multiplicity_primair, $multiplicity_secundair, $dependent_primair, $dependent_secundair, $mandatory_primair, $mandatory_secundair, $user_name, $projectID, $verbalisationID){

        $stmt = $this->conn->prepare("EXEC usp_benoemenRelatieInVerbalisatie ?,?,?,?,?,?,?");
        $stmt->execute(array($relation_name, $user_name, $verbalisationID, $entity_name_primair, $entity_name_secundair, $dependent_primair, $dependent_secundair, $mandatory_primair, $mandatory_secundair));
    }
}