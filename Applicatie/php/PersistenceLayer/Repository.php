<?php

require_once $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";

abstract class Repository{

    /* @var PDO $connection*/
    private $connection;

    public function __construct(){
        $this->connection = connect::getInstance()->getDatabase();
    }

    public function runQuery($sql, array $parameter){
        $prepared = $this->connection->prepare($sql);
        $prepared->execute($parameter);
        return $prepared;
    }

    public function fetch($sql, array $parameters){
        return $this->runQuery($sql, $parameters)->fetch();
    }

    public function fetchAll($sql, array $parameters){
        return $this->runQuery($sql, $parameters)->fetchAll();
    }
}
?>