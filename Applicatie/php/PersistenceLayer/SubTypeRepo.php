<?php

include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
$AR = new SubTypeRepo();
class SubTypeRepo
{
    public $conn;

    public function __construct()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $this->conn = connect::getInstance()->getDatabase();

    }

    public function getAllEntities($projectID){
        $stmt =$this->conn->prepare("EXEC usp_selectAllEntitiesFromProject ?");
        $stmt->execute(array($projectID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function getSubtypesWithAllEntities($parentID){
        $stmt =$this->conn->prepare("EXEC usp_selectSubtype ?");
        $stmt->execute(array($parentID));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }
}