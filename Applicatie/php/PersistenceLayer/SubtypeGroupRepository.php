<?php

require_once "Repository.php";

class SubtypeGroupRepository extends Repository{

    function getAllEntitiesFromProject($project_id){
        return $this->fetchAll("EXEC usp_selectAllEntitiesFromProjectWithoutParent ?", [
            $project_id
        ]);
    }

    function insertSubtypeGroup($superType, $subtype, $gebruikersnaam, $groepnaam, $mutually_exclusive, $complete){
        return $this->runQuery("EXEC usp_insertSubtypeGroup ?, ?, ?, ?, ?, ?", [
            $superType,
            $subtype,
            $gebruikersnaam,
            $groepnaam,
            $mutually_exclusive,
            $complete
        ]);
    }

    function getSuperTypeById($supertype){
        return $this->fetchAll("EXEC usp_getSupertypeBySupertypeId ?", [
            $supertype
        ]);
    }
}

?>