<?php
require_once "Repository.php";

class UserRepository extends Repository {

    function checkLogin($email, $password){
        $result = $this->getUserData($email);
        if ($this->passVerify($password, $result['password'])) {
            return $result;
        }
        return false;
    }

    function getUserData($email){
        return $this->fetch("select * from Users where email = :email", [
            ':email'=> $email
        ]);
    }

    function passVerify($pass1, $pass2){
        return password_verify($pass1, $pass2);
    }

    /*--REGISTER--*/
    function insertRegisterForm($data){
        return $this->runQuery("INSERT INTO User VALUES (:email, :password, :firstname, :lastname)", [
            ':email'=> $data[0],
            ':password'=> $data[1],
            ':firstname'=> $data[2],
            ':lastname'=> $data[3]
        ]);
    }
}
?>