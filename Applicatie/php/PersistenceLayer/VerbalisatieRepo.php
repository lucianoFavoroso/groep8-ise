<?php
include $_SERVER['DOCUMENT_ROOT' ] . "/connect.php";
if(!isset($_SESSION)){
    session_start();
}

$dasd = new VerbalisatieRepo();
class VerbalisatieRepo
{
    public $conn;

    public function __construct()
    {
        $this->conn = connect::getInstance()->getDatabase();
        if(isset($_POST['verbalisation'])) {
            if (isset($_POST['verbalisation'])) {
                $this->createVerbalisation($_POST['verbalisation'], $_POST['ProjectID']);
            }
            if (isset($_POST['verbalisation2'])) {
                $stmt = $this->conn->prepare("SELECT IDENT_CURRENT('VERBALISATIE') as LAST");
                $stmt->execute();
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $last = $data[0]['LAST'];
                $this->createSubVerbalisation($_POST['verbalisation2'],$last);
            }
            header("location: ../../details_project.php?Project={$_POST['ProjectID']}");
            exit();
        }
    }

    public function createVerbalisation($zin,$projectID){
        $stmt =$this->conn->prepare("EXEC usp_CreateNewVerbalisatieForUserInProject ?,?,?");
        $stmt->execute(array($projectID,$_SESSION['Gebruikersnaam'],$zin));
    }

    public function createSubVerbalisation($zin,$verbalisatieID){
        $stmt =$this->conn->prepare("EXEC usp_CreateNewSubVerbalisation ?,?,?");
        $stmt->execute(array($verbalisatieID,$_SESSION['Gebruikersnaam'],$zin));
   
   
    }

    public function getAllVerbalisatieInProject($ProjectId){
        $stmt =$this->conn->prepare("EXEC usp_getVerbalisationsInProject ?");
        $stmt->execute(array($ProjectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }

    public function getProjectMembersForProject($ProjectId){
        $stmt =$this->conn->prepare("EXEC usp_getProjectMembersForProject ?");
        $stmt->execute(array($ProjectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }else{
            return null;
        }
    }
}
