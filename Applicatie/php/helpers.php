<?php

function loadEnvironment(){

    $envFilePath =  __DIR__ . '/../.env';

    $lines = array_filter(str_replace("\n",'',file($envFilePath)));
    foreach ($lines as $line) {
        putenv($line);
        list($key, $value) = explode('=', $line);
        $_ENV[$key] = $value;
    }
}

function env($key, $default = null)
{
    $value = getenv($key);
    if ($value === false) {
        return $default;
    }
    return trim($value);
}