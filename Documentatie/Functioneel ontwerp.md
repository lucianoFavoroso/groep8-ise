#  Functioneel ontwerp

## Inleiding

### Overall Description

Dit document is het Functioneel Ontwerp voor het ISE-project van projectgroep 8. De opdracht die uitgevoerd moet worden tijdens dit project, betreft het maken van een informatiesysteem en een daarbij horende (demo)applicatie voor de opdrachtgever Chris Scholten. Deze opdracht zal uitgevoerd worden door een groep van vijf studenten van de Hogeschool van Arnhem en Nijmegen uit het tweede jaar. Zij volgen tijdens het project het blok ISE. De groep studenten die de opdracht uitvoert, vormt de projectgroep.  

### Assignment Description

In een professionele setting is het niet praktisch om gebruik te maken van fact based modelleren met behulp van pen en papier; het gebruik van pen en papier past beter bij een workshop. Voor grote projecten zou het handig zijn om een tool te hebben waarmee het hele proces kan worden doorlopen. Deze tool zal dan als resultaat een conceptueel datamodel hebben. Om deze tool te kunnen maken, is het van groot belang om een informatiemodel te hebben die deze tool kan ondersteunen. Voor dit project is de studenten gevraagd een informatiemodel te ontwikkelen waarin alle nodige gegevens opgeslagen kunnen worden.

### Operating Environment

De ontwikkelomgeving die in dit project zal worden gebruikt is Microsoft SQL Server. De taal die hierbij hoort is T-SQL. Deze ontwikkelomgeving is voorgesteld door de opdrachtgever. Om het informatiesysteem te gaan gebruiken is er voor gekozen om een webapplicatie te maken. Deze webapplicatie zal worden gemaakt met de taal PHP. De opmaak zal gemaakt worden met HTML en CSS.

### Design and Implementation Constraints

Tijdens het uitvoeren van dit project, zal het team gebruikmaken van verschillende programmeertalen. Hiervan is T-SQL verplicht voor het informatiesysteem. Daarnaast wordt er PHP gebruikt voor de applicatie in combinatie met HTML en CSS. Ook maakt de projectgroep gebruik van Google Drive, Trello, Bitbucket, en Typora. 

## Overzicht van bedrijfsprocessen

In dit hoofdstuk zullen alle bedrijfsprocessen besproken worden. De bedrijfsprocessen zullen worden weergeven door middel van een Business Process Model and Notation (BPMN). De BPMN is een grafische weergave van de bedrijfsprocessen.

### Stappenplan van het gehele proces.

In deze paragraaf wordt een stappenplan gemaakt waarin in kaart wordt gebracht hoe het gehele analyseproces doorlopen moet worden om tot een CDM te komen. Hieruit wordt duidelijk wat een gebruiker in het echt moet doen, en welke stappen nodig zijn om tot het gewenste resultaat te komen. Er wordt ervan uitgegaan dat de gebruiker het gehele proces in 1 keer goed doorloopt en niet vergeet om bepaalde stappen in te vullen.

1. Gebruiker begint aan het proces
2. De gebruiker geeft aan dat hij/zij een nieuw fact-type (FT) heeft gevonden (bijv. FT-1)
3. De gebruiker schrijft minstens twee verbalisaties op bij dit FT.
4. Indien nog niet alle FT's zijn toegevoegd, ga verder bij stap 2. 
5. De gebruiker selecteert een verbalisatie waar er nog geen variabelen zijn benoemd en onderstreept hier de variabelen.
6. Herhaal stap 5 tot alle verbalisaties zijn onderstreept.
7. De gebruiker sorteert al zijn verbalisaties op basis van het aantal invulplekken (van laag naar hoog).
8. De gebruiker selecteert de eerste verbalisatie en benoemd hier de entiteiten samen met bijbehorende identifier. Vervolgens benoemt de gebruiker de attributen in de verbalisatie. Indien een entiteit al eerder in kaart is gebracht schrijft de gebruiker op dat het een MATCH is, en hoeft de identifier niet vermeld te worden. Indien het gaat om een zwakke entiteit schrijft de gebruiker dit op als een nieuwe entiteit.
9. Indien er sprake is van een relatie in stap 8, schrijft de gebruiker de naam van de relatie op en geeft de gebruiker aan tussen welke twee entiteiten de relatie ligt. Indien van toepassing geeft hij ook aan dat een entiteit dependend is.

### Account aanmaken

Bij het aanmaken van een account, vult de gebruiker zijn gebruikersinformatie in, verzend deze naar het systeem en het systeem controleert of de gebruikersnaam al in gebruik is of niet. Mocht de gebruikersnaam al in gebruik zijn krijgt de gebruiker een melding en kan de gebruiker het opnieuw proberen of het proces beëindigen. Bestaat de gebruikersnaam nog niet, slaat het systeem het account op in de database en krijgt de gebruiker een melding dat het account succesvol is aangemaakt. <!--Referentie naar afbeelding-->

![IMG](Img\BPMN model account aanmaken.png)
<!--Bijschift invoegen-->

### Project aanmaken

Bij het aanmaken van een project, voert een aangemelde gebruiker een projectnaam in. Dit wordt vervolgens opgeslagen in de database, waarna het systeem een melding geeft dat het project succesvol is aangemaakt. Daarnaast wordt de gebruiker geregistreerd als de eigenaar van het aangemaakte project.<!--Referentie naar afbeelding-->

![IMG](Img\BPMN model Project aanmaken.png)<!--Bijschift invoegen-->

### Gebruiker toevoegen aan project

Bij het toevoegen van een gebruiker aan een project, selecteert de eigenaar van het project een bestaande gebruiker, waarna het systeem eerst controleert of de gebruiker echt de eigenaar is. Mocht het zo zijn dat de gebruiker niet de eigenaar is, laat het systeem een melding zien dat de gebruiker niet gemachtigd is en wordt het proces beëindigd. <!--Referentie naar afbeelding-->

![IMG](Img\BPMN model Gebruiker toevoegen aan project.png)

<!--Bijschift invoegen-->

### invoeren verbalisatie, relatie, entiteit en attributen

De volgende bedrijfsprocessen zijn in één BPMN opgenomen:

- Verbalisatie;
- Relatie;
- Enititeit;
- Attributen;
- Subtype;
- Business rules.

Dit is gedaan, omdat deze bedrijfsprocessen een gelijkwaardige proces hebben. De data wordt opgestuurd, gecontroleerd en opgeslagen. Mocht er iets fout gaan dan krijgt de gebruiker een foutmelding en heeft de gebruiker de keus om het opnieuw te proberen of om het proces te beëindigen. <!--Referentie naar afbeelding-->

![IMG](Img\BPMN model Verbalisatie Relatie en entiteit.png)<!--Bijschift invoegen-->

## Use cases

In dit hoofdstuk zullen alle use cases besproken worden. Hiervoor is een use case model gemaakt, die alle use cases bevat. Daarnaast worden de brief descriptions, de user stories en de fully dressed use cases per use case gegeven. 

### User stories

Binnen dit hoofdstuk zullen er user stories opgezet worden voor de acties die een gebruiker met het systeem wilt uitvoeren. Uit deze stories zullen vervolgens use cases of overige functionele eisen ontstaan.

#### Aanmaken account

Als bezoeker wil ik een account aanmaken voor het systeem. Dit stelt mij in staat projecten aan te maken en hierin verbalisaties en analyses te maken.

#### Aanmaken project

Als gebruiker wil ik projecten aanmaken zodat ik verbalisaties kan maken en analyseren. Projecten stellen mij ook in staat om samen met een team aan een grotere opdracht te werken. Het aanmaken van een project maakt mij de eigenaar.

#### Gebruiker toevoegen aan een project

Als eigenaar van het project wil ik teamgenoten toevoegen zodat wij samen aan een groot project kunnen werken.

#### Invoeren verbalisatie

Als lid van een project wil ik verbalisaties toevoegen zodat deze later geanalyseerd kunnen worden.

#### Benoemen entiteiten

Als gebruiker wil ik tijdens de analyse van een verbalisatie de entiteiten die ik herken benoemen. Door entiteiten te identificeren weet ik welke concepten er in het CDM aanwezig zullen zijn. 

#### Benoemen attributen

Als gebruiker wil ik attributen uit mijn verbalisatie halen en deze bij de juist gevonden entiteit neerzetten. Dit stelt mij in staat mijn attributen te koppelen aan gevonden entiteiten.

#### Benoemen relaties

Wanneer het binnen een analyse voorkomt dat er twee entiteiten zijn geïdentificeerd, kan er een relatie tussen zitten. Als gebruiker wil ik graag deze relatie kunnen benoemen. Deze relatie wil ik een naam kunnen geven. Verder wil ik ook aangeven tussen welke twee entiteiten de relatie ligt. Verder moet ik kunnen aangeven welke multipliciteiten er zijn. Door dit te doen, kan ik gemakkelijk een relatie tekenen binnen het CDM.

#### Subtype aanmaken

Als gebruiker wil ik een subtype maken zodat ik deze op een later moment kan toevoegen bij een subtype groep. Als gebruiker wil ik ook in staat zijn om bij dit subtype attributen toe te voegen.

#### Subtype groep aanmaken

Als gebruiker wil ik een subtype groep aanmaken. Aan deze groep wil ik subtypes kunnen toevoegen. Daarnaast wil ik ook aangeven wat voor relatietype er binnen deze specifieke subtype groep is.

#### Controleren model

Als gebruiker wil ik mijn model controleren op correctheid. De reden hiervoor is dat ik fouten uit de analyse kan halen voordat ik het script maak.

#### Generen Powerdesigner script

Wanneer mijn project klaar is wil ik graag automatisch een CDM tekenen in de tool Powerdesigner. Ik wil graag een gemakkelijke manier waarmee ik mijn analysen naar een CDM kan omzetten.

### Use case model

In deze paragraaf wordt het use case model besproken. Het use case model geeft visueel weer welke actoren er binnen het systeem gebruik kunnen maken van de usecases. Op deze manier kan men gemakkelijk zien welke actor toegang heeft tot welke usecase. 

![img](img/UseCase Diagram.png)

### Brief descriptions

In dit hoofdstuk worden alle use cases voorzien van een brief description. 

#### Aanmaken account

Voordat een gebruiker gebruik kan maken van het systeem zal hij zich moeten registreren. De gebruiker geeft aan dat hij zich wil registreren. Het systeem stelt de gebruiker in staat om een account te registreren. Hiervoor moet de gebruiker aangeven welke gebruikersnaam en welk wachtwoord hij wil gebruiken.

#### Aanmaken project

Gebruikers kunnen projecten maken. De gebruiker drukt op de knop om een nieuw project aan te maken en wordt doorverwezen naar een pagina. Hier kan de gebruiker aangeven hoe hij het nieuwe project wil noemen. Nadat het nieuwe project is aangemaakt kan de gebruiker het in zijn project overzicht selecteren. Als de gebruiker dit doet kan hij met het nieuwe project aan de slag gaan. Daarnaast kan de gebruiker die het project heeft aangemaakt later andere gebruikers toevoegen, omdat hij de eigenaar is van het project.

#### Gebruiker toevoegen aan project

De eigenaar, oftewel de gebruiker die een project heeft aangemaakt, van een project kan andere gebruikers toevoegen aan een project. Dit heeft als gevolg dat andere groepsleden ook verbalisatie en analyses kunnen maken. De gebruiker drukt op de knop om andere gebruikers toe te voegen. Als de gebruiker doorverwezen is naar de pagina kan de eigenaar de gebruikersnaam van de andere gebruiker invoeren. Wanneer dit is gedaan kan deze gebruiker ook gebruik maken van het project.

#### Invoeren verbalisatie

De gebruiker heeft de mogelijkheid om verbalisaties binnen het systeem in te voeren. In de project detail pagina kan de gebruiker op de knop voor nieuwe verbalisaties klikken. Wanneer de gebruiker op deze knop klikt is er een een invoerveld aanwezig waar de verbalisatie ingevoerd kan worden. Nadat deze is ingevoerd kan de gebruiker deze verbalisatie kiezen in de project detail pagina. Als de gebruiker op deze verbalisatie klikt kan de verbalisatie geanalyseerd worden.

#### Benoemen entiteiten

Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de entiteiten binnen de verbalisatie benoemen. Zodra de gebruiker, binnen de analyse, op de knop klikt om een entiteit toe te voegen wordt de gebruiker doorverwezen naar een pagina om de naam van de entiteit in te voeren. Als de gebruiker de entiteit heeft ingevoerd heeft zal deze verschijnen op de analyse pagina van de verbalisatie.

#### Benoemen attributen

Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de attributen binnen de verbalisatie benoemen. Als de gebruiker een attribuut wil toevoegen kan hij op de knop klikken om een attribuut toe te voegen. Zodra de pagina geopend is kan de gebruiker de naam van het attribuut invoeren en bij welke entiteit het attribuut hoort. Daarnaast kan de gebruiker ook nog aanvinken of het attribuut de primary identifer is en/of het attribuut mandatory. 

#### Benoemen relaties

Wanneer de gebruiker een verbalisatie wil analyseren kan hij de relaties binnen de verbalisatie benoemen. Wanneer de pagina geladen is kan de gebruiker kiezen tussen welke entiteiten de relatie bestaat en deze een naam geven. Daarnaast kan de gebruiker per entiteit de multipliciteit aangeven en bij één van de entiteiten aanvinken dat deze dependent is.

#### Subtype aanmaken

Voordat de gebruiker een subtype groep zal maken kan hij eerst een subtype maken. Door dit subtype te maken zal deze uiteindelijk aan de subtype groep toegevoegd kunnen worden. Vanuit het tabblad "Overzicht entiteiten en subtypen" kan de gebruiker op de knop klikken om een nieuw subtype entiteit te maken. Nadat dit gebeurd is kan de gebruiker de naam van de entiteit invoeren. Daarnaast kan de gebruiker bij deze entiteit meerdere attributen als dit nodig is.

#### Subtype groep aanmaken

Wanneer de gebruiker een Subtype groep wil maken zal de gebruiker vanuit het tabblad "Overzicht entiteiten en subtypen" dit kunnen doen. De gebruiker klikt bij het supertype op de knop "Subtype toevoegen". Nadat de gebruiker deze knop heeft ingedrukt zal er een pagina openen waar het supertype al genoteerd staat. Daarnaast kan de gebruiker de naam van de groep invoeren en kiezen welke relatie deze groep krijgt. Daarnaast kan de gebruiker meerdere subtype kiezen die bij dit supertype zullen horen.

#### Controleren model

Voordat de gebruiker zijn model gaat genereren kan hij zijn model laten controleren. Hierdoor komen tijdig bepaalde fouten naar voren. Deze kan de gebruiker hierna handmatig weghalen 

#### Genereren PowerDesigner script

Gebruikers kunnen vanuit de gemaakte analyse een export script genereren voor de tool PowerDesigner. Wanneer dit script gedraaid wordt zal er een CDM getekend worden binnen de tool.



### Fully dressed use cases

Binnen deze paragraaf zullen alle use cases uitgewerkt worden naar een fully-dressed versie.

| Use case Invoeren verbalisatie                               |                                   |
| :----------------------------------------------------------- | --------------------------------- |
| **Brief description:** De gebruiker heeft de mogelijkheid om verbalisaties binnen het systeem in te voeren.<br />Deze verbalisaties zullen opgeslagen worden binnen het data systeem. |                                   |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project.<br />**Post condition:** De verbalisatie is opgeslagen binnen het systeem. |                                   |
| **Actor**                                                    | **Systeem**                       |
| 1. Gebruiker voert verbalisatie in.                          |                                   |
|                                                              | 2. Systeem slaat verbalisatie op. |
| 3. Ga terug naar stap 1 wanneer er meer verbalistaties zijn. |                                   |
| **Alternative flows**                                        |                                   |
|                                                              |                                   |



| Use case Benoemen entiteiten                                 |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de entiteiten binnen de verbalisatie benoemen.<br />Deze benoemingen worden opgeslagen binnen het data systeem. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker neemt deel aan het project. De gebruiker is bezig met het analyseren van een verbalisatie.<br />**Post condition:** De benoemde entiteiten zijn opgeslagen binnen het data systeem. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker geeft aan een entiteit te willen benoemen.      |                                                              |
|                                                              | 2. Het systeem geeft de gebruiker de mogelijkheid om de benodigde informatie in te vullen voor het benoemen van een entiteit. |
| 3. Gebruiker geeft aan wat de naam van de entiteit is.       |                                                              |
| 4. Gebruiker geeft aan of de entiteit een MATCH is.          |                                                              |
| 5. Gebruiker geeft aan of de entiteit een subtype is.        |                                                              |
|                                                              | 6. Systeem slaat de entiteit bij de verbalisatie op.         |
| **Alternative flows**                                        |                                                              |
| 6A. [De gebruiker wil nog een entiteit benoemen.]<br />6A1. Ga terug naar stap 1. |                                                              |



| Use case Benoemen attributen                                 |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de attributen binnen de verbalisatie benoemen.<br />Deze benoemingen worden opgeslagen binnen het data systeem. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker is bezig met het analyseren van een verbalisatie. Er is een entiteit binnen de verbalisatie aanwezig.<br />**Post condition:** De attributen zijn opgeslagen binnen het data systeem. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker geeft aan een attribuut te willen benoemen.     |                                                              |
|                                                              | 2. Systeem geeft de mogelijkheid om de betreffende informatie van het attribuut in te voeren. |
| 3. Gebruiker geeft aan wat de naam van het attribuut is.     |                                                              |
| 4. Gebruiker geeft aan of dit attribuut een identifier is.   |                                                              |
| 5. Gebruiker geeft aan bij welke entiteit dit attribuut hoort. |                                                              |
| 6. Gebruiker geeft aan of het attribuut mandatory is.        |                                                              |
| 7. Gebruiker geeft de beginpositie van het attribuut aan.    |                                                              |
| 8. Gebruiker geeft de eindpositie van het attribuut aan.     |                                                              |
|                                                              | 9. Systeem slaat informatie betreft het attribuut op bij de juiste verbalisatie en entiteit. |
| **Alternative flows**                                        |                                                              |
| 9A. [De gebruiker wil meer attributen benoemen.]<br />9A1. Ga terug naar stap 1 |                                                              |



| Use case Benoemen relaties                                   |                                                       |
| :----------------------------------------------------------- | ----------------------------------------------------- |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren kan hij de relaties binnen de verbalisatie benoemen.<br />Deze benoemingen worden opgeslagen binnen het data systeem. |                                                       |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker is bezig met het analyseren van een verbalisatie. Er is minimaal één entiteit aanwezig.<br />**Post condition:** De relaties zijn opgeslagen binnen het data systeem. |                                                       |
| **Actor**                                                    | **Systeem**                                           |
| 1. Gebruiker geeft aan een relatie te benoemen.              |                                                       |
|                                                              | 2. Systeem vraagt de gebruiker om de nodige gegevens. |
| 3. Gebruiker geeft de relatie naam aan.                      |                                                       |
| 4. Gebruiker geeft de primaire entiteit aan.                 |                                                       |
| 5. Gebruiker geeft de secondaire entiteit aan.               |                                                       |
| 6. Gebruiker geeft de multipliciteit van beide entiteiten aan. |                                                       |
| 7. Gebruiker geeft aan welke entiteiten binnen deze relatie dependent zijn. |                                                       |
| 8. Gebruiker geeft aan welke rollen mandatory zijn.          |                                                       |
|                                                              | 9. Systeem slaat de relatie succesvol op.             |
| **Alternative flows**                                        |                                                       |
|                                                              |                                                       |



| Use case subtype aanmaken                                    |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** De gebruiker kan een subtype aanmaken om deze uiteindelijk toe te voegen aan een subtype groep. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker zit in een analyse van een verbalisatie. <br />**Post condition:** Het nieuwe subtype is opgeslagen. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker geeft aan een subtype aan te willen maken.      |                                                              |
|                                                              | 2. Systeem toont de pagina om een subtype te maken.          |
| 3. Gebruiker geeft de entiteit naam aan.                     |                                                              |
|                                                              | 4. Systeem slaat het subtype op binnen het systeem.          |
| **Alternative flows**                                        |                                                              |
| 3A. [De gebruiker wilt een attribuut aan het subtype toevoegen.]<br />3A.1 Gebruiker geeft een attribuut aan. |                                                              |
| 3A.2 Gebruiker geeft aan dat het attribuut de primary identifier is. |                                                              |
| 3A.3 Gebruiker geeft aan dat het attribuut mandatory is.     |                                                              |
| 3B. [Gebruiker wil nog één attributen toevoegen]<br />3B.1 De gebruiker drukt op de knop om een attribuut toe te voegen |                                                              |
|                                                              | 3B.2 Het systeem voegt een nieuwe rij toe waar een attribuut ingevuld kan worden. |
| 3B.3 Ga terug naar stap 3A.1                                 |                                                              |



| Use case subtype groep aanmaken                              |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** De gebruiker kan een subtype groep aanmaken zodat deze uiteindelijk in het CDM zichtbaar zal zijn. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project en bevindt zich in het overzicht van entiteiten en subtypen.  **Post condition:** De nieuwe subtype groep is opgeslagen. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker geeft aan een subtype toe te willen voegen bij een entiteit (supertype). |                                                              |
|                                                              | 2. Het systeem geeft de mogelijkheid om een subtypegroep aan te maken. |
| 3. Gebruiker geeft de subtypegroepnaam aan.                  |                                                              |
| 4. Gebruiker geeft soort relatie aan.                        |                                                              |
| 5. Gebruiker selecteer een subtype.                          |                                                              |
|                                                              | 7. Systeem slaat de subtype groep op binnen het systeem.     |
| **Alternative flows**                                        |                                                              |
| 6A. [Gebruiker wil nog één subtype toevoegen] 6A.1 De gebruiker drukt op de knop om een subtype toe te voegen |                                                              |
|                                                              | 6A.2 Het systeem voegt een nieuwe rij toe waar een subtype gekozen kan worden. |
| 6A.3 Ga terug naar stap 5.                                   |                                                              |



| Use case genereren Powerdesigner export script               |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Brief description:** Gebruikers kunnen vanuit de gemaakte analyse een export script genereren voor de tool PowerDesigner. Wanneer dit script uitgevoerd wordt, zal er een CDM gegenereerd worden op basis van de inhoud van het script. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project.<br />**Post condition:** Het systeem heeft een script gegeneerd waarmee Powerdesigner een CDM kan tekenen. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker selecteert een project.                         |                                                              |
|                                                              | 2. Systeem toont project.                                    |
| 3. Gebruiker geeft aan een script te willen genereren van het geselecteerde project. |                                                              |
|                                                              | 4. Systeem geneert script voor Powerdesigner om een CDM te tekenen. |
| **Alternative flows**                                        |                                                              |
| 4A [Analyse bevat fouten]<br />4A.1 Het systeem geeft een foutmelding. |                                                              |



| Use case Aanmaken account                                    |                                                             |
| :----------------------------------------------------------- | ----------------------------------------------------------- |
| **Brief description:** Voordat een gebruiker aan projecten kan werken moet hij/zij een account hebben. |                                                             |
| **Pre condition:** n.v.t.<br />**Post condition:** De gebruiker is opgeslagen . |                                                             |
| **Actor**                                                    | **Systeem**                                                 |
| 1. Gebruiker voert gewenste gebruikersnaam in.               |                                                             |
|                                                              | 2. Systeem controleert of de gebruikersnaam beschikbaar is. |
| 3. Gebruiker geeft bij beide velden het gewenste wachtwoord aan. |                                                             |
|                                                              | 4. Systeem slaat de gebruiker op.                           |
| **Alternative flows**                                        |                                                             |
| 2A. [Gebruikersnaam is al in gebruik] <br />2A.1 Ga terug naar stap 1. |                                                             |
| 4A. [Wachtwoorden komen niet overeen] <br />4A.1 Het systeem geeft aan dat de wachtwoorden niet overeen komen.<br />4A.2 Ga terug naar stap 3. |                                                             |



| Use case Aanmaken project                                    |                                  |
| :----------------------------------------------------------- | -------------------------------- |
| **Brief description:** Gebruikers kunnen projecten aanmaken. Wanneer de gebruiker een project heeft kan hij/zij hier anderen gebruikers aan toevoegen. Binnen het project kunnen gebruikers verbalisaties maken en analyseren. |                                  |
| **Pre condition:** Gebruiker is ingelogd.<br />**Post condition:** Het project is aangemaakt binnen het systeem. |                                  |
| **Actor**                                                    | **Systeem**                      |
| 1. Gebruiker voert gewenste projectnaam in.                  |                                  |
|                                                              | 2. Systeem slaat het project op. |
| **Alternative flows**                                        |                                  |
| 2A [Gebruiker is al eigenaar van een project met dezelfde naam.]<br />2A.1 Ga terug naar stap 1. |                                  |



| Use case Gebruiker toevoegen aan project                     |                                                |
| :----------------------------------------------------------- | ---------------------------------------------- |
| **Brief description:** Aan projecten kunnen gebruikers toegevoegd worden. De eigenaar van het project kan nieuwe teamleden toevoegen. Toegevoegde gebruikers kunnen verbalisaties toevoegen en analyses binnen het project aanpassen. |                                                |
| **Pre condition:**  Gebruiker heeft een account. Gebruiker is ingelogd. Gebruiker is eigenaar van het project<br />**Post condition:** Er is een gebruiker toegevoegd aan het project. |                                                |
| **Actor**                                                    | **Systeem**                                    |
| 1. Gebruiker voert gewenste gebruikersnaam in.               |                                                |
|                                                              | 2. Systeem voegt gebruiker toe aan het project |
| **Alternative flows**                                        |                                                |
|                                                              |                                                |



## Overige functionele eisen

In dit hoofdstuk zullen functionele eisen worden beschreven. Deze komen niet voor in het hoofdstuk Fully dressed use cases.

### Inloggen

Een van de functionele eisen is: “De gebruiker moet kunnen inloggen met zijn gebruikersnaam en wachtwoord”. Dit is een zodanige standaard en veel voorkomende eis. Om deze reden is deze niet uitgewerkt in een fully dressed use case. Wanneer de gebruikersnaam en/of het wachtwoord niet correct is, zal de gebruiker geen toegang krijgen tot de applicatie en het informatiesysteem.

### Project verwijderen

Een andere eis is: ”De eigenaar van een project kan dat project verwijderen”.  Deze eis is om de volgende redenen niet verwerkt in de use cases. Ten eerste is deze eis niet heel complex. Het fully dressed uitwerken zal dan ook geen grote toegevoegde waarde hebben. Ten tweede is het nog niet bekend of deze eis uitgewerkt gaat worden tijdens dit project.

### Gebruiker uit project verwijderen

De eis: “De eigenaar van een project kan een andere gebruiker, die eerder is toegevoegd aan een project, van dat project afhalen” is niet opgenomen in de use cases om de volgende redenen. Het verwijderen van een gebruiker is een bijzaak ten opzichte van andere eisen. Daarnaast is het en eenvoudige en duidelijke eis. Het uitwerken van deze eis zal dan ook niet heel veel informatie toevoegen.  

### Benoemen business rule

Nog een eis luidt: "De gebruiker kan een business rule invoeren binnen een project". Er is voor gekozen om deze eis niet uit te werken om de volgende reden. Aan de hand van gesprekken met de opdrachtgever bleek dat we een business rule enkel als plain text bij een project moeten opslaan. Dit betekent dat dit enken het inserten van een zin is.

### Controleren model

Een andere eis is: "De gebruiker wil zijn model controleren op fouten.". Deze eis is ook niet verder uitgewerkt omdat dit een knop zal zijn die alleen een lijst van fouten zal laten zien. Deze eis heeft dan ook een groot aandeel in het technische deel van applicatie en niet zo zeer dat van de front-end.

### Invoeren subverbalisatie

Bij een verbalisatie kunnen meerden subverbalisaties ingevoerd worden. Door meerdere subverbalisaties kan het analyseren gemakkelijker worden en is er een mogelijkheid voorbeeld data te genereren.



## Niet functionele eisen

In dit hoofdstuk zullen de niet-functionele eisen beschreven worden. Deze eisen definiëren het gedrag van de applicatie.

### Documentatie

Het gemaakte product moet goed gedocumenteerd worden. Deze documentatie geeft inzicht in de manier waarop de database is gebouwd. Hier zullen de constraints en de technische oplossingen in gedocumenteerd worden. Hieronder valt de analyse, de testscenario's en de constraints waar rekening mee is gehouden. Een goede documentatie zorgt ervoor dat de database makkelijk te onderhouden en overdraagbaar is.

### Het systeem ondersteund de Nederlandse taal

Hetgeen wat de gebruiker ziet is geschreven in de Nederlandse taal.

### Het systeem ondersteund twee gebruikers tegelijk

Binnen het systeem wordt er wat betreft ‘samenwerken' gekeken naar situaties waar 2 personen tegelijk werken. De mogelijke fouten en gevolgen worden beschreven en er wordt beargumenteert waarom er wel of geen oplossing wordt uitgewerkt.

### De veranderingen moeten bijgehouden worden

Binnen het datamodel moet er een mogelijkheid zijn waarmee alle veranderingen bijgehouden worden. De manier waarop dit wordt gedaan is voor de opdrachtgever niet relevant, zolang er een mogelijkheid is om te zien welke veranderingen plaats hebben gevonden.

## Verbalisaties

In dit hoofdstuk worden de in kaart gebrachte verbalisaties geanalyseerd, aan de hand van de methode die geleerd is bij de course DMDD. Door gebruik te maken van deze methode worden de verschillende concepten in kaart gebracht. Ook worden de attributen van een concept bepaald. Tot slot wordt de primary identifier van een concept en de relaties tussen verschillende concepten vastgesteld.

Hieronder is de volledige analyse van de gevonden verbalisaties te vinden.



F-1:

Er is een gebruiker met gebruikersnaam <u>PeterR12</u>.

Er is een gebruiker met gebruikersnaam <u>KlaasT2</u>.

ET: Gebruiker

ID: Att gebruikersnaam

Predicate: Er is een gebruiker met gebruikersnaam [gebruikersnaam]



F-2:

Er is een project met de naam <u>Vélipe Metropole</u>.

Er is een project met de naam <u>Synth Modeling Store.</u>

ET: Project

ID: Att projectnaam

Predicate: Er is een project met de naam [projectnaam]



F-3:

Gebruiker <u>PeterR12</u> heeft als wachtwoord <u>523djw@js</u>.

Gebruiker <u>KlaasT2</u> heeft als wachtwoord <u>258Mei&q!</u>.

ET: Gebruiker MATCH

Att: wachtwoord

Predicate: Gebruiker [gebruikersnaam] heeft als wachtwoord [wachtwoord]



F-4:

<u>PeterR12</u> werkt aan het project <u>Vélipe Metropole.</u>

<u>KlaasT2</u> werkt aan het project <u>Synth Modeling Store</u>.

ET:  Projectbezetting

ID: ET Gebruiker MATCH + Project MATCH

RT Gebruiker_in_Projectbezetting BETWEEN Gebruiker AND Projectbezetting(Dependent)

RT Project_in_Projectbezetting BETWEEN Project AND Projectbezetting(Dependent)

Predicate: [gebruikersnaam] werkt aan het project [projectnaam]



F-5:

De verbalisatie "<u>Er is een fiets met ID 5</u>" is een verbalisatie binnen het project <u>Vélipe Metropole.</u>

De verbalisatie "<u>De fiets met ID 5 staat op Station 5001 op Post 5</u>" is een verbalisatie binnen het project <u>Vélipe Metropole</u>.

ET: Verbalisatie

ID: ET Project MATCH + Att verbalisatie_zin

RT: Verbalisatie_in_Project BETWEEN Project AND Verbalisatie(Dependent)

Predicate: De verbalisatie [verbalisatie_zin] is een verbalisatie binnen het project [projectnaam]



F-6:

Er bestaat een entiteit met de naam "<u>Fiets</u>" in het project <u>Vélipe Metropole</u>.

Er bestaat een entiteit met de naam "<u>Post</u>" in het project <u>Vélipe Metropole</u>.

ET: Entiteit_in_project

ID: ET Project + Att entiteit_naam

RT: Entiteit_in_project BETWEEN Project and Entiteit_in_project (dependent)

Predicate: Er bestaat een entiteit met de naam [entiteit_naam] in het project [projectnaam]



F-7:

De entiteit "<u>Fiets</u>" heeft het attribuut "<u>fietsnummer</u>".

De entiteit "<u>Station</u>" heeft het attribuut "<u>stationsnummer</u>".

ET: Entiteit_in_Project MATCH

ET: Attribuut_in_Verbalisatie MATCH

RT: Attribuut_van_entiteit BETWEEN Entiteit_in_Project AND Attribuut_in_Verbalisatie

Predicate: De entiteit [entiteit_naam] heeft het attribuut [attribuut_naam].



F-8:

<u>PeterR12</u> <u>is eigenaar</u> van het project <u>Vélipe Metropole</u>.

<u>KlaasT2</u> <u>is geen eigenaar</u> van het project <u>Synth Modeling Store</u>.

ET:  Projectbezetting MATCH                

Att: is_eigenaar

Predicate: [gebruikersnaam] [eigenaar] van het project [projectnaam]



F-9:

Bij de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u> is de entiteit "<u>Fiets</u>" in kaart gebracht.

Bij de verbalisatie "<u>De fiets met ID 5 staat op Station 5001 op Post 5</u>" binnen het project <u>Vélipe Metropole</u> is de entiteit "<u>Post</u>" in kaart gebracht.

ET: Entiteit_In_Verbalisatie

ID: ET Entiteit_in_project + ET Verbalisatie

RT: Entiteit_in_verbalisatie BETWEEN Verbalisatie AND Entiteit_in_verbalisatie(Dependent)

RT:  Entiteit_In_Project_In_Entiteit _In_Verbalisatie  BETWEEN Entiteit_In_Project AND Entiteit_in_verbalisatie(Dependent)

Predicate: Bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] is de entiteit [entiteit_naam] in kaart gebracht.



F-10:

Er is een Subtype groep met als groepsnaam "<u>groep 1</u>" en als parent entiteit "<u>Station</u>" uit het project "<u>Vélipe Metropole</u>"

Er is een Subtype groep met als groepsnaam "<u>groep 2</u>" en als parent entiteit "<u>Fiets</u>" uit het project "<u>Vélipe Metropole</u>"

ET: Subtype_groep

ID: ET Entiteit + Att groepsnaam

RT supertype_entiteit BETWEEN Entiteit AND Subtype_groep (dependent)

Predicate: Er is een subtype groep met als groepsnaam [groepsnaam] en als parent entiteit [entiteit_naam] uit het project [project_naam]



F-11:

De entiteit "<u>Fiets</u>" uit het project "<u>Vélipe Metropole</u>" behoort tot een subtype groep met de naam "<u>groep1</u>".

De entiteit "<u>Station</u>" uit het project "<u>Vélipe Metropole</u>" behoort tot een subtype groep met de naam "<u>groep2</u>".

ET: Entiteit MATCH

ET: Subtype_groep MATCH

RT: entiteit_in_groep BETWEEN Entiteit_in_Project AND Subtype_groep

Predicate: De entiteit [entiteit_naam] uit het project [project_naam] behoort tot een subtype groep met de naam [groep_naam].



F-12:

De subtype groep "<u>groep1</u>" met de superklasse "<u>Fiets</u>" <u>is</u> mutually exclusive.

De subtype groep "<u>groep2</u>" met de superklasse "<u>Fiets</u>" <u>is niet</u> mutually exclusive.

ET: Subtype_groep MATCH

Att: mutually_exclusive

Predicate: De subtype groep [groepsnaam] met de superklasse [entiteit_naam] [mutually_exclusive] mutually exclusive.



F-13:

De subtype groep "<u>groep1</u>" met de superklasse "<u>Fiets</u>" <u>is</u> compleet.

De subtype groep "<u>groep2</u>" met de superklasse "<u>Fiets</u>" <u>is niet</u> compleet.

ET: Subtype_groep MATCH

Att: complete

Predicate: De subtype groep [groepsnaam] met de superklasse [entiteit_naam] [complete] compleet.



F-14:

De verbalisatie "<u>Er is een fiets met ID 5</u>"  binnen het project <u>Vélipe Metropole</u> heeft een subverbalisatie "<u>Er is een fiets met ID 7</u>".

De verbalisatie "<u>Er is een station met nummer 2</u>" binnen het project <u>Vélipe Metropole</u> heeft een subverbalisatie "<u>Er is een station met nummer 9</u>".

ET: Subverbalisatie

ID: ET Verbalisatie MATCH + Att subverbalisatie

RT: Subverbalisatie_in_Verbalisatie BETWEEN Verbalisatie AND Subverbalisatie (dependent)

Predicate: De verbalisatie [verbalisatie_zin] binnen het project [projectnaam] heeft een subverbalisatie [subverbalisatie_zin].



F-15:

De entiteit "<u>Fiets</u>" <u>is</u> geïdentificeerd als een MATCH binnen de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u>.

De entiteit "<u>Post</u>" <u>is niet</u> geïdentificeerd als een MATCH binnen de verbalisatie "<u>De fiets met ID 5 staat op Station 5001 op Post 5</u>" binnen het project <u>Vélipe Metropole</u>.

ET: Entiteit_In_Verbalisatie MATCH

Att: is_match

Predicate: De entiteit [entiteit_naam] [is_een_match] geïdentificeerd als een MATCH binnen de verbalisatie [verbalisatie_zin] binnen het project [projectnaam].



F-16:

Bij de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u> is er een invulplek aangegeven met een beginpositie van <u>25</u> en een eindpositie van <u>25</u>.

Bij de verbalisatie "<u>Gebruiker PeterR12 heeft als wachtwoord 523djw@js.</u>" binnen het project <u>ERM factbased repository</u> is er een invulplek aangegeven met een beginpositie van <u>11</u> en een eindpositie van <u>18</u>.

ET: Invulplek

ID: ET Verbalisatie + Att beginpositie + Att eindpositie

RT Invulplek_in_verbalisatie BETWEEN Invulplek (dependent) AND Verbalisatie

Predicate: Bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] is er een invulplek aangegeven met een beginpositie van [beginpositie] en een eindpositie van [eindpositie].



F-17:

Er is binnen de verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project V<u>élipe Metropole</u> een relatie met de naam "<u>Post Bevat Fiets</u>".

Er is binnen de verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> een relatie met de naam "<u>Project_Gebruiker</u>".

ET Relatie

ID: ET Verbalisatie MATCH + Att relatie_naam

RT Relatie_Verbalisatie BETWEEN Relatie (Dependent) AND Verbalisatie

Predicate: Er is binnen de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] een relatie met de naam [relatie_naam]



F-18:

Er is een <u>primaire</u> rol bij de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u>.

Er is een <u>niet primaire</u> rol bij de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository.</u>

ET Rol

ID: ET Relatie + Att is_primair

RT Rol_Van_Relatie BETWEEN Rol (dependent) AND Relatie

Predicate: Er is een [is_primair] rol bij de relatie met de naam [relatie_naam] bij verbalisatie [verbalisatie_zin] binnen het project [projectnaam].



F-19:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> heeft als eerste entiteit <u>Post</u>.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> heeft als eerste entiteit <u>Project</u>.

ET Rol MATCH

ET Entiteit MATCH

RT Begin_Rol BETWEEN Rol AND Entiteit



F-20:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> heeft als tweede entiteit in de rol <u>Post</u>.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> heeft als tweede entiteit in de rol <u>Project</u>.

ET Rol MATCH

ET Entiteit MATCH

RT Eind_Rol BETWEEN Rol AND Entiteit



F-21:

De relatie met de naam "<u>Post_Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> heeft als cardinaliteitstype "<u>One - One</u>"

De relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u></u>" binnen het project <u>ERM factbased repository</u>  heeft als cardinaliteitstype "<u>Many - One</u>".

ET Relatie MATCH

Att cardinaliteitstype

Predicate: De relatie met de naam [relatie_naam] bij verbalisatie [verbalisatie_zin] binnen het project [projectnaam] heet als cardinaliteitstype [cardinaliteitstype] 



F-22:

Het project <u>Vélipe Metropole</u> heeft de business rule "<u>Een pincode bestaat uit 4 nummers.</u>".

Het project <u>Vélipe Metropole</u> heeft de business rule "<u>Een toegangscode bestaat uit 8 nummers</u>".

ET: Project MATCH

ET: Business_rule

ID: ET Project MATCH + Att business_rule

RT: Business_rule_in_Project BETWEEN Project AND Business_rule

Predicate: Het project [project_naam] heeft de business rule [business_rule_zin].



F-23:

Bij de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u> is het attribuut "<u>fietsnummer</u>" in kaart gebracht bij de invulplek met beginpositie van <u>25</u> en een eindpositie van <u>25</u>.

Bij de verbalisatie "<u>Gebruiker PeterR12 heeft als wachtwoord 523djw@js.</u>" binnen het project <u>ERM factbased repository</u> is het attribuut "<u>wachtwoord</u>" in kaart gebracht bij de invulplek met beginpositie van <u>11</u> en een eindpositie van <u>18</u>.

ET: Attribuut_in_verbalisatie

ID: ET Invulplek MATCH + Att attribuut_naam

RT Attribuut_in_Invulplek BETWEEN Invulplek AND Attribuut_in_verbalisatie (Dependent)

Predicate: Bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] is het attribuut [attribuut_naam] in kaart gebracht bij de invulplek met beginpositie van [beginpositie] en een eindpositie van [eindpositie].



F-24:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> heeft een minimale cardinaliteit van <u>0</u>.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> heeft een minimale cardinaliteit van <u>1</u>.

ET Rol MATCH

Att: cardinaliteit_minimum

Predicate: De rol die [is_primair] is in de relatie met de naam [relatie_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] heeft een minimale cardinaliteit van [cardinaliteit_minimum] 



F-25:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> heeft een maximale cardinaliteit van <u>1</u>.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> heeft een maximale cardinaliteit van <u>5</u>.

ET Rol MATCH

Att: cardinaliteit_maximum

Predicate: De rol die [is_primair] is in de relatie met de naam [relatie_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] heeft een maximale cardinaliteit van [cardinaliteit_maximum].



F-26:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> <u>is niet</u> mandatory.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> <u>is niet</u> mandatory</u>.    

ET Rol MATCH

Att: mandatory

Predicate: De rol die [is_primair] is in de relatie met de naam [relatie_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] [mandatory] mandatory.



F-27:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> <u>is niet</u> dependent.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> <u>is niet</u> dependent</u>.    

ET Rol MATCH

Att: dependent

Predicate: De rol die [is_primair] is in de relatie met de naam [relatie_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] [dependent] dependent.



F-28:

De rol die <u>primair</u> is in de relatie met de naam "<u>Post Bevat Fiets</u>" bij verbalisatie "<u>Fiets 5 staat bij Post 3 op Station 1</u>" binnen het project <u>Vélipe Metropole</u> <u>is niet</u> dominant.

De rol die <u>niet primair</u> is in de relatie met de naam "<u>Project_Gebruiker</u>" bij verbalisatie "<u>De gebruiker met de naam PeterR12 neemt deel aan het project ERM factbased repository</u>" binnen het project <u>ERM factbased repository</u> <u>is niet</u> dominant</u>.    

ET Rol MATCH

Att: is_dominant

Predicate: De rol die [is_primair] is in de relatie met de naam [relatie_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] [is_dominant] dominant.



F-29:

Het attribuut "<u>fietsnummer</u>" bij de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u> bij de invulplek met beginpositie van <u>25</u> en een eindpositie van <u>25</u> <u>is</u> onderdeel van de primary identifier.

Het attribuut "<u>isLocked</u>" bij de verbalisatie "<u>Fiets met ID 5 staat op slot</u>" binnen het project <u>Vélipe Metropole</u>  bij de invulplek met beginpositie van <u>11</u> en een eindpositie van <u>18</u> <u>is geen</u> onderdeel van de primary identifier.

ET: Attributen_In_Verbalisatie MATCH

Att: is_primary_identifier

Predicate: Het attribuut [attribuut_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] bij de invulplek met beginpositie van [beginpositie] en een eindpositie van [eindpositie] [is_primary_identifier] onderdeel van de primary identifier.



F-30:

Het attribuut "<u>fietsnummer</u>" bij de verbalisatie "<u>Er is een fiets met ID 5</u>" binnen het project <u>Vélipe Metropole</u> bij de invulplek met beginpositie van <u>25</u> en een eindpositie van <u>25</u> <u>is</u> mandatory

Het attribuut "<u>isLocked</u>" bij de verbalisatie "<u>Fiets met ID 5 staat op slot</u>" binnen het project <u>Vélipe Metropole</u> bij de invulplek met beginpositie van <u>11</u> en een eindpositie van <u>18</u> <u>is niet</u> mandatory.

ET: Attributen_In_Verbalisatie MATCH

Att: is_mandatory

Predicate: Het attribuut [attribuut_naam] bij de verbalisatie [verbalisatie_zin] binnen het project [projectnaam] bij de invulplek met beginpositie van [beginpositie] en een eindpositie van [eindpositie] [is_mandatory] mandatory.



## Conceptueel datamodel

In dit hoofdstuk wordt het conceptuele datamodel besproken. Dit is een belangrijk model binnen de werkwijze die door het ontwikkelteam wordt gehanteerd. In het vorige hoofdstuk zijn de verschillende verbalisaties geanalyseerd. Hieruit zijn een groot aantal concepten, attributen, en relaties in kaart gebracht.

Aan de hand van het resultaat van de analyse is er een conceptueel datamodel (CDM) opgesteld. Hierdoor is er een overzichtelijke weergave gecreëerd, waardoor er eenvoudig te zien is welke concepten er zijn, uit welke attributen deze bestaan en welke relaties er bestaan tussen concepten.

Verder wordt bij de verschillende attributen ook vastgelegd wat voor datatype (of domein) erbij hoort. Ook wordt vastgesteld of het een mandatory attribuut is en of het onderdeel is van de primary identifier.

![img](img/CDM.png)



### Belangrijke keuzes

**Subtypes**

Tijdens het maken van het CDM waren er een aantal problemen met het verwerken van subtypen. Het eerste probleem was hoe we attributen aan een subtype konden koppelen. Hiervoor hebben we nagedacht over een optie om de entiteit 'subtype' direct aan een 'attribuut' te koppelen. Indien we dit zouden doen dan zou dit in een later stadium als gevolg hebben dat er een hoop null-values in de 'attribuut' tabel komen. Een oplossing hiervoor zou kunnen zijn om subtypes te maken bij attribuut. 1 voor attributen voor subtypes, en 1 voor entiteiten. 

Uiteindelijk hebben we ervoor gekozen om subtypes op te nemen als een gewone 'entiteit'. We hebben wel een entiteit voor de subtype_groep. Hierin staan de relatietypes van de subtype groep, en wat de parent-entiteit is. Op deze manier voorkom je null-values, en redundante informatie. Je hebt in dit geval nog wel binnen entiteit null-values, maar dit leek op de beste oplossing.

**Relatie en rollen**

Bij het maken van de relaties werd eerst alles opgeslagen in de entiteit relatie. Na verder onderzoek bleek dat er binnen power-designer gebruik werd gemaakt van rollen (elke relatie bestaat uit 2 specifieke rollen). Dit was op de huidige manier in ons model niet mogelijk, of je hebt voor elke relatie 2 records nodig om beide rollen te kunnen weergeven. 

Aangezien dit voor een hoop redundante informatie gaat zorgen, is ervoor gekozen om een entiteit 'Rol' aan te maken. Elke relatie heeft 2 rollen, die de specifieke rol beschrijven. Op deze manier kun je de rollen makkelijk aanpassen. Ook draagt dit bij aan de eis om een powerdesigner script te kunnen genereren.

### Glossary

| **Concept**              | Betekenis                                                    |
| ------------------------ | ------------------------------------------------------------ |
| Attribuut_in_entiteit    | In dit concept worden alle attributen die in een entiteit behoren. Het attribuut heeft een naam en is een primary_identifier of niet. |
| Gebruiker                | In dit concept worden gebruiker gegevens opgeslagen. In dit geval zijn dit alleen de gebruikersnaam en wachtwoord. |
| Projectbezetting         | Bevat de PI van de gebruiker en het project. Met een attribuut welk de eigenaar van dit project is. |
| Project                  | Bevat de projectnaam van een project.                        |
| Verbalisatie             | Bevat de verbalisatie zin. Dit is de zin die de gebruiker ingevoerd heeft. |
| Entiteit_in_verbalisatie | De gevonden entiteiten binnen een verbalisatie. Deze heeft de PI van Verbalisatie en Entiteit. |
| Entiteit                 | Bevat de informatie betreft entiteit zoals naam.             |
| Relatie                  | Bevat de informatie betreft entiteit zoals naam en tussen welke twee entiteiten deze relatie ligt. |
| Relatie_One_One          | Subtype van Relatie specifiek voor One_On_One relaties. Deze entiteit heeft een relatie met Entiteit voor de dominant role. |



## Rechten
Normaliter bestaan er binnen het functioneel ontwerp een aantal hoofdstukken die te maken hebben met de rechten van een gebruiker. De inhoud van deze hoofdstukken bedraagt het informatiemodel, rechtenstructuur en de CRUD-matrix. 

Er wordt tijdens dit project een multi-user applicatie ontwikkelt waar iedereen dezelfde rechten heeft. Alle gebruikers zijn zogenaamde 'super'-users (dit houdt in dat de gebruikers alle rechten heeft). Hierdoor zullen deze hoofdstukken weinig nuttige informatie bedragen. Dit is de reden dat deze hoofdstukken niet zijn opgenomen.

## Storyboards

In dit hoofdstuk worden alle schermontwerpen getoond en toegelicht. Ook wordt de flow tussen deze schermen besproken.

### Flow

Deze flow-chart geeft weer hoe de gebruiker kan navigeren tussen de schermen. De gele pijlen geven aan bij welk scherm de gebruiker terechtkomt, wanneer de gebruiker op een klikbaar item klikt. Onderstaande afbeelding toont de globale flow door de gehele applicatie.

![img](img/Storyboard.png)

### Account aanmaken

Iedere gebruiker heeft de mogelijkheid om een account aan te maken. De gebruiker voert zijn
gebruikersnaam en wachtwoord in de invoervelden en klikt vervolgens op aanmaken.

![img](img/Account aanmaken.png)

### Inloggen

Gebruikers, die beschikken over een account, kunnen inloggen via de inlogpagina. De gebruiker voert zijn gebruikersnaam en wachtwoord in in de invoervelden en klikt vervolgend op inloggen.

![img](Img\inloggen.png)

### Overzicht Projecten

Wanneer de gebruiker ingelogd is, kan hij een overzicht zien van alle projecten waar hij momenteel aan werkt of lid van is. Door op het plusje te drukken kan de gebruiker een nieuw project toevoegen aan deze lijst.

![project details](Img\project details.png)

### Project details

Een gebruiker kan aan een project feiten toevoegen. Dit kan hij doen door op het plusje knopje te drukken. Daarnaast is er te zien welke gebruikers toegevoegd zijn aan het project.

![overzicht projecten](Img\overzicht projecten.png)

### Feit analyseren

Gebruikers hebben de mogelijkheid een feit te selecteren. De gebruiker zal dan naar de detailpagina van dat bepaalde feit worden genavigeerd. Hier kan hij bepalen welk gedeelte van de analysen hij gaat uitvoeren. Elk van deze knoppen navigeert de gebruiker naar ander scherm.

![Analyse fact](Img\Analyse fact.png)

### Benoemen attribuut

De gebruiker kan binnen dit scherm de attributen benoemen bij een feit
type. Met de checkbox kan de gebruiker aangeven of het gevonden attribuut
onderdeel van de primary indentifier is.

![attribuut benoemen](Img\attribuut benoemen.png)

### Benoemen entiteit

Op dit scherm kan de gebruiker entiteiten aan feiten koppelen. De gebruiker kan aangeven of deze entiteit al eerder benoemd is door de Match checkbox aan te vinken.

![entiteit benoemen](Img\entiteit benoemen.png)

### Benoemen relatie

Wanneer er een relatie aanwezig is tussen twee entiteiten, kan de gebruiker deze relatie benoemen op dit scherm. Hier kan ook de relatienaam aangegeven worden samen met de entiteiten waartussen de relatie bestaat. Verder kan de gebruiker aanvinken, wanneer dit van belang is, welke entiteit dependent is van de andere entiteit. Daarnaast kan er ook aangegeven worden wat de multipliciteit is van deze relatie.

![Relatie benoemen](Img\Relatie benoemen.png)



### Attributen bij entiteit

De gebruikers die bij een
project horen, kunnen de attributen die bij een entiteit horen inzien

![Attributen bij entiteit](Img\Attributen bij entiteit.png)

### Overzicht entiteiten en subtypen

De gebruikers die bij een project horen, kunnen de entiteiten en subtypes inzien. Ook kan de gebruiker op deze pagina een subtype toevoegen.

![Overzicht entiteiten en subtypen](Img\Overzicht entiteiten en subtypen.png)

### Geschiedenis project

De gebruikers die bij een project horen, kunnen de geschiedenis van dat specifieke project inzien.

![Geschiedenis project](Img\Geschiedenis project.png)

### Subtypen toevoegen

De gebruiker die lid is van een project, kan subtypes toevoegen aan een entiteit.

![Subtypes toevoegen](Img\Subtypes toevoegen.png)

### Business rules

De gebruikers die bij een project horen, kunnen een business rule toevoegen aan dat specifiek project.

![Business rules](Img\Business rules.png)

### Verbalisatie

De gebruikers die bij een project horen, kunnen een verbalisatie aan een specifiek
project.

![verbalisatie invoegen](Img\verbalisatie invoegen.png)



## Testplan

### Invoeren verbalisatie

#### Fully-dressed use case

| Use case Invoeren verbalisatie                               |                                   |
| :----------------------------------------------------------- | --------------------------------- |
| **Brief description:** De gebruiker heeft de mogelijkheid om verbalisaties binnen het systeem in te voeren. Deze verbalisaties zullen opgeslagen worden binnen een data systeem. |                                   |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project.<br />**Post condition:** De verbalisatie is opgeslagen binnen het systeem. |                                   |
| **Actor**                                                    | **Systeem**                       |
| 1. Gebruiker voert verbalisatie in.                          |                                   |
|                                                              | 2. Systeem slaat verbalisatie op. |
| 3. Ga terug naar stap 1 wanneer er meer verbalistaties zijn  |                                   |
| **Alternative flows**                                        |                                   |
|                                                              |                                   |

*Tabel  - Fully dressed use case verbalisatie*

#### BPMN

![IMG](Img\BPMN model Verbalisatie Relatie en entiteit.png)*Figuur  - BPMN invoeren verbalisatie, relatie, entiteit en attributen*

#### Testcases

| Testcase 1              |                               |
| :---------------------- | ----------------------------- |
| Functie ontwikkeld door | ISE Groep 8                   |
| Test uitgevoerd door    | Luciano Favoroso              |
| Datum van uitvoering    | 7/06/2019                     |
| Beginsituatie           | Gebruiker zit in een project. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                         | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Invoeren verbalisatie" openen                        | Klik op de knop om een verbalisatie toe te voegen. | De gebruiker wordt doorverwezen naar een pagina waar hij de verbalisatie kan invoeren. | Als verwacht            |
| 2.          | Invoeren verbalisatie                                        | Voer "Er is een fiets met ID 5" in.                | Er staat “Er is een fiets met ID 5” in het invoerveld.       | Als verwacht            |
| 3.          | Extra verbalisatie zin toevoegen                             | Druk op de plus knop.                              | Een extra invoerveld verschijnt onder de eerste verbalisatie. | Als verwacht            |
| 4.          | Extra verbalisatie zin invoeren                              | Voer in het tweede "Er is een fiets met ID 6"      | Er staat "Er is een fiets met ID 6" in het onderste invoerveld. | Als verwacht            |
| 5.          | Verbalisatie opslaan                                         | Klik op de opslaanknop.                            | De projectdetailpagina wordt geopend. De aangemaakte verbalisatie staat in de Fact types-lijst. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                    |                                                              |                         |

*Tabel  - testcase invoeren verbalisatie*



| Testcase 2              |                               |
| :---------------------- | ----------------------------- |
| Functie ontwikkeld door | ISE Groep 8                   |
| Test uitgevoerd door    | Luciano Favoroso              |
| Datum van uitvoering    | 07/06/2019                    |
| Beginsituatie           | Gebruiker zit in een project. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                         | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Invoeren verbalisatie" openen                        | Klik op de knop om een verbalisatie toe te voegen. | De gebruiker wordt doorverwezen naar een pagina waar hij de verbalisatie kan invoeren. | Als verwacht            |
| 2.          | Invoeren verbalisatie                                        | Voer "Er is een fiets met ID 5" in.                | Er staat “Er is een fiets met ID 5” in het invoerveld.       | Als verwacht            |
| 3.          | Extra verbalisatie zin toevoegen                             | Druk op de plus knop.                              | Een extra invoerveld verschijnt onder de eerste verbalisatie. | Als verwacht            |
| 4.          | Extra verbalisatie zin invoeren                              | Voer in het tweede "Er is een fiets met ID 5"      | Er staat "Er is een fiets met ID 5" in het onderste invoerveld. | Als verwacht            |
| 5.          | Verbalisatie opslaan                                         | Klik op de opslaanknop.                            | De gebruiker krijgt een melding dat bepaalde verbalisaties hetzelfde zijn. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                    |                                                              |                         |

*Tabel  - testcase invoeren subverbalisatie*



### Benoemen entiteiten

#### 1. Fully-dressed use case

| Use case Benoemen entiteiten                                 |                                                        |
| :----------------------------------------------------------- | ------------------------------------------------------ |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de entiteiten binnen de verbalisatie benoemen. Deze benoemingen worden opgeslagen binnen het data systeem. |                                                        |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker heeft een verbalisatie geselecteerd. **Post condition:** De benoemde entiteiten zijn opgeslagen binnen het data systeem. |                                                        |
| **Actor**                                                    | **Systeem**                                            |
| 1. Gebruiker selecteert verbalisatie                         |                                                        |
|                                                              | 2. Systeem toont verbalisatie.                         |
| 3. Gebruiker voert entiteit naam in                          |                                                        |
|                                                              | 4. Systeem slaat entiteit naam bij de verbalisatie op. |
| 5. Ga terug naar stap 1 wanneer er meer entiteiten benoemd moeten worden |                                                        |
| **Alternative flows**                                        |                                                        |
| 1A. [Gebruiker selecteert verbalisatie]  1A.1 Einde use case |                                                        |
| 3A.[Gebruiker voert geen entiteit naam in]  3A.1 Einde use case |                                                        |

*Tabel  - Fully dressed use case benoemen entiteit*

#### 2. BPMN

![IMG](Img\BPMN model Verbalisatie Relatie en entiteit.png)

*Figuur  - BPMN invoeren verbalisatie, relatie, entiteit, en attributen*

#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie. <br />Daarnaast heeft een andere verbalisatie binnen <br />het project de entiteit "fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                               | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Benoem entiteit" openen                              | Klik op de knop "Benoem entiteit".                       | De gebruiker wordt doorverwezen naar een pagina waar hij een entiteit kan invoeren. | Als verwacht            |
| 2.          | Invoeren entiteit en match aangeven                          | Typ in het veld "Fiets" en vink de checkbox bij "MATCH". | Verwacht resultaat: “fiets” wordt weergegeven in het invoerveld voor de entiteitnaam en de checkbox is aangevinkt. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                 | De pagina met de analyse wordt geopend en de benoemde entiteit is toegevoegd. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                          |                                                              |                         |

*Tabel  - testcase entiteit invullen*



| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie.  Daarnaast heeft deze verbalisatie binnen  het project de entiteit "fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                               | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Benoem entiteit" openen                              | Klik op de knop "Benoem entiteit".                       | De gebruiker wordt doorverwezen naar een pagina waar hij een entiteit kan invoeren. | Als verwacht            |
| 2.          | Invoeren entiteit en match aangeven                          | Typ in het veld "Fiets" en vink de checkbox bij "MATCH". | Verwacht resultaat: “fiets” wordt weergegeven in het invoerveld voor de entiteitnaam en de checkbox is aangevinkt. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                 | De gebruiker krijgt een melding dat deze verbalisatie al een entiteit met de naam "Fiets" heeft. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                          |                                                              |                         |

*Tabel  - testcase entiteit duplicatie invullen*

### **Benoemen attribuut**

#### 1. Fully-dressed use case

| Use case Benoemen attributen                                 |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren moet hij de attributen binnen de verbalisatie benoemen. Deze benoemingen worden opgeslagen binnen het data systeem. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker heeft een bestaande analyse geselecteerd. **Post condition:** De relaties zijn opgeslagen binnen het data systeem. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker selecteert verbalisatie                         |                                                              |
|                                                              | 2. Systeem toont verbalisatie.                               |
| 3. Gebruiker voert attribuut naam                            |                                                              |
| 4. Gebruiker voert in of dit attribuut een identifier is     |                                                              |
| 5. Gebruiker geeft aan bij welke entiteit dit attribuut hoort |                                                              |
|                                                              | 6. Systeem slaat informatie betreft de attribuut op bij de juiste verbalisatie en entiteit |
| 7. Ga terug naar stap 1 wanneer er meer attributen benoemd moeten worden |                                                              |
| **Alternative flows**                                        |                                                              |
| 1A. [Gebruiker selecteert geen verbalisatie]  1A.1 Einde use case |                                                              |
| 4A.[Gebruiker voert geen attribuut in]  4A.1 Einde use case  |                                                              |

*Tabel  - Fully dressed use case benoemen attribuut*

#### 2. BPMN

![IMG](Img\BPMN model Verbalisatie Relatie en entiteit.png)

*Figuur  - BPMN invoeren verbalisatie, relatie, entiteit en attributen.*

#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie <br />en gekozen verbalisatie heeft een entiteit genaamd "fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "invoeren attribuut" openen                           | Klik op de knop "Benoem attribuut".                          | Gebruiker wordt doorverwezen naar een pagina om een attribuut in te voeren. | Als verwacht            |
| 2.          | Invoeren attribuut                                           | Kies in het entiteit veld "fiets". Typ in het attribuut veld "fiets_ID" en vink beide checkboxen aan. Vul daarnaast in beide index velden "4" in. | In het entiteitveld wordt “fiets” weergegeven. “ID” wordt weergegeven in het attribuutveld. De checkboxes van Primary Identifer en Mandatory zijn aangevinkt. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De pagina met de analyse wordt geopend en de benoemde attribuut is toegevoegd. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase attribuut invullen*



| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie  en gekozen verbalisatie heeft een entiteit genaamd "fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "invoeren attribuut" openen                           | Klik op de knop "Benoem attribuut".                          | Gebruiker wordt doorverwezen naar een pagina om een attribuut in te voeren. | Als verwacht            |
| 2.          | Invoeren attribuut                                           | Kies in het entiteit veld "fiets". Typ in het attribuut veld "fiets_ID" en vink beide checkboxen aan. Vul daarnaast in het eerste indexveld "5" in en in het tweede "4". | In het entiteitveld wordt “fiets” weergegeven. “ID” wordt weergegeven in het attribuutveld. De checkboxes van Primary Identifer en Mandatory zijn aangevinkt. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De gebruiker krijgt een melding dat de attribuutindex niet goed ingevuld is. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase attribuutindex fout*

| Test case 3             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie en gekozen verbalisatie heeft een entiteit genaamd "fiets" met een attribuut "fiets_ID". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "invoeren attribuut" openen                           | Klik op de knop "Benoem attribuut".                          | Gebruiker wordt doorverwezen naar een pagina om een attribuut in te voeren. | Als verwacht            |
| 2.          | Invoeren attribuut                                           | Kies in het entiteit veld "fiets". Typ in het attribuut veld "fiets_ID" en vink beide checkboxen aan. Vul daarnaast in beide index velden "4" in. | In het entiteitveld wordt “fiets” weergegeven. “ID” wordt weergegeven in het attribuutveld. De checkboxes van Primary Identifer en Mandatory zijn aangevinkt. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De gebruiker krijgt een melding dat het attribuut al bestaat binnen deze verbalisatie. | Geen foutmelding.       |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase attribuut duplicatie*

### Benoemen relaties

#### 1. Fully-dressed use case

| Use case Benoemen relaties                                   |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Brief description:** Wanneer de gebruiker een verbalisatie wilt analyseren kan hij de relaties binnen de verbalisatie benoemen. Deze benoemingen worden opgeslagen binnen het data systeem. |                                                              |
| **Pre condition:** De gebruiker is ingelogd. De gebruiker zit in een project. De gebruiker heeft een bestaande analyse geselecteerd. **Post condition:** De relaties zijn opgeslagen binnen het data systeem. |                                                              |
| **Actor**                                                    | **Systeem**                                                  |
| 1. Gebruiker selecteert verbalisatie                         |                                                              |
|                                                              | 2. Systeem toont verbalisatie.                               |
| 3. Gebruiker voert relatie naam in                           |                                                              |
| 4. Gebruiker voert in tussen welke twee entiteiten de relatie is |                                                              |
| 5. Gebruiker geeft aan welke entiteiten binnen deze relatie depended zijn |                                                              |
| 6. Gebruiker geeft de multipliciteit van de relatie aan      |                                                              |
| 7. Gebruiker geeft de dominant role aan wanneer de relatie een op een is. |                                                              |
|                                                              | 8. . Systeem slaat informatie betreft de relatie op bij de juiste verbalisatie |
| **Alternative flows**                                        |                                                              |
| 1A. [Gebruiker selecteert verbalisatie]  1A.1 Einde use case |                                                              |
| 3A.[Gebruiker voert geen entiteit naam in]  3A.1 Einde use case |                                                              |

*Tabel  - Fully dressed use case benoemen relatie*

#### 2. BPMN

![IMG](Img\BPMN model Verbalisatie Relatie en entiteit.png)

*Figuur  - BPMN invoeren verbalisatie, relatie, entiteit en attributen*

#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    | Luciano Favoroso                                             |
| Datum uitvoering        | 06-07-2019                                                   |
| Beginsituatie           | Gebruiker in de analyse van een verbalisatie.<br /> Daarnaast zijn de volgende twee entiteiten <br />aanwezig binnen het project: "fiets" en "post". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Invoeren relatie" openen                             | Klik op de knop "relatie"                                    | Gebruiker wordt doorverwezen naar een pagina om een relatie te benoemen. | Als verwacht            |
| 2.          | Invoeren relatie                                             | Voer in het invulveld voor de relatienaam de waarde "RT_fiets_station_post" in. Kies in het eerste entiteitveld de entiteit "fiets" en in de andere "post". Zet de multipliciteit van "fiets" op "0..1" en van "post" op "0..1". Vink de dependent en Mandatory checkboxen aan van de entiteit "fiets". | Het veld dat de relatienaam toont, heeft de waarde"RT_fiets_station_post". Het eerste entiteitsveld toont “fiets”, en de tweede toont “post”. De dependent checkbox van de entiteit “fiets” is aangevinkt. De multipliciteit van “fiets” staat op “1” en van “post” staat op “1”. | Als verwacht            |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De pagina met de analyse wordt geopend en de gemaakte relatie is toegevoegd. | Als verwacht            |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase relatie beschrijven*



| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Gebruiker bevindt zich in de analyse van een verbalisatie.<br /> Daarnaast zijn de volgende twee entiteiten <br />aanwezig binnen het project: "fiets" en "post". |

| Stap        | Variabele of selectie                                        | Waarde                                                       | Verwacht resultaat                                           | Werkelijk resultaat |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------- |
| 1.          | Pagina "Invoeren relatie" openen                             | Klik op de knop "Relatie"                                    | De gebruiker wordt doorverwezen naar een pagina waar hij de relatie kan benoemen. | Als verwacht        |
| 2.          | Invoeren relatie                                             | Voer in het invulveld voor de relatienaam de waarde "RT_fiets_station_post" in. Kies in het eerste relatieveld de relatie "fiets" en in de andere "post". Vink de dependent checkbox aan van de entiteit "fiets". Zet de multipliciteit van "fiets" op "0..1". | Het veld dat de relatienaam toont, heeft de waarde "RT_fiets_station_post". Het eerste entiteitsveld toont “fiets”, en de tweede toont “post”. De dependent checkbox van de entiteit “fiets” is aangevinkt. De multipliciteit van “fiets” staat op “0..1” en van “post” staat op “  ”. | Als verwacht        |
| 3.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De gebruiker krijgt een melding dat niet alle waarden ingevuld zijn. | Als verwacht        |
| 4.          | Waarden invoeren                                             | Voer de waarde "0..1" in bij de multipliciteit van "post".   | Het veld dat de relatienaam toont, heeft de waarde "RT_fiets_station_post". Het eerste entiteitsveld toont “fiets”, en de tweede toont “post”. De dependent checkbox van de entiteit “fiets” is aangevinkt. De multipliciteit van “fiets” staat op “0..1” en van “post” staat op “0..1”. | Als verwacht        |
| 5.          | Opslaan                                                      | Druk op de opslaan knop.                                     | De pagina met de analyse wordt geopend en de gemaakte relatie is toegevoegd. | Als verwacht        |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                     |

*Tabel  - testcase relatie beschrijven missende waardes*

### Aanmaken account

#### 1. Fully-dressed use case

| Use case Aanmaken account                                    |                                                             |
| :----------------------------------------------------------- | ----------------------------------------------------------- |
| **Brief description:** Voordat een gebruiker aan projecten kan werken moet hij/zij een account hebben. |                                                             |
| **Pre condition:**  **Post condition:** De gebruiker is opgeslagen . |                                                             |
| **Actor**                                                    | **Systeem**                                                 |
| 1. Gebruiker voert gewenste gebruikersnaam in.               |                                                             |
|                                                              | 2. Systeem controleert of de gebruikersnaam beschikbaar is. |
| 3. Gebruiker kiest het gewenste wachtwoord.                  |                                                             |
|                                                              | 4. Systeem slaat de gebruiker op.                           |
| **Alternative flows**                                        |                                                             |
| 2A. [Gebruikersnaam is al in gebruik]  2A.1 Ga terug naar stap 1. |                                                             |

*Tabel  - Fully dressed use case aanmaken account*

#### 2. BPMN

![IMG](Img\BPMN model account aanmaken.png)

*Figuur  - BPMN aanmaken account*

#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum                   |                                                              |
| Beginsituatie           | De gebruiker bevindt zich op het inlogscherm. <br />Daarnaast is er geen gebruiker met de naam "User15". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Account aanmaken" openen                             | Klik op de knop "Account aanmaken"                           | Gebruiker wordt doorverwezen naar een pagina om gegevens in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren gegevens                                            | Vul in het gebruikersnaam veld: "User15" en in de wachtwoordvelden "pass123" | Vul in het gebruikersnaamveld “User15” in en in de wachtwoordvelden “pass123” in. | Hetzelfde als verwacht  |
| 3.          | Registreren                                                  | Druk op de registreer knop.                                  | De gebruiker wordt doorverwezen naar de inlogpagina.         | Hetzelfde als verwacht  |
| 4.          | Inlog gegevens controleren                                   | Voer "User15" in het gebruikersnaam veld en "pass123" in het wachtwoord veld | In het veld voor de gebruikersnaam staat “User15”. De wachtwoordvelden tonen beide “*******”. | Hetzelfde als verwacht  |
| 5.          | Inloggen                                                     | Druk op de knop "Inloggen"                                   | De wordt doorverwezen naar het overzicht scherm.             | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase account aanmaken*



| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich op het inlogscherm <br />en er is al een andere gebruiker met de <br />gebruikersnaam "User15". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Account aanmaken" openen                             | Klik op de knop "Account aanmaken"                           | Gebruiker wordt doorverwezen naar een pagina om gegevens in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren gegevens                                            | Vul in het gebruikersnaam veld: "User15" in en in beide wachtwoordvelden "pass123" | In het veld voor de gebruikersnaam staat “User15”. De wachtwoordvelden tonen beide “*******”. | Hetzelfde als verwacht  |
| 3.          | Registreren                                                  | Druk op de registreer knop.                                  | De gebruiker krijgt een notificatie dat de gebruikersnaam al in gebruik is. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase account aanmaken met gebruikte gebruikersnaam*

| Test case 3             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich op het inlogscherm. <br />Daarnaast is er geen gebruiker met de naam "User15". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Account aanmaken" openen                             | Klik op de knop "Account aanmaken"                           | Gebruiker wordt doorverwezen naar een pagina om gegevens in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren gegevens                                            | Vul in het gebruikersnaam veld: "User15" in, in het eerste wachtwoordveld "pass123" en het tweede "x" | In het veld voor de gebruikersnaam staat “User15”. De wachtwoordvelden tonen beide “***\****”. | Hetzelfde als verwacht  |
| 3.          | Registreren                                                  | Druk op de registreer knop.                                  | De gebruiker krijgt een notificatie dat de wachtwoorden niet overeenkomen. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase account aanmaken wachtwoorden komen niet overeen*

### Aanmaken project

#### 1. Fully-dressed use case

| Use case Aanmaken project                                    |                                  |
| :----------------------------------------------------------- | -------------------------------- |
| **Brief description:** Gebruikers kunnen projecten aanmaken. Wanneer de gebruiker een project heeft kan hij/zij hier anderen gebruikers aan toevoegen. Binnen het project kunnen gebruikers verbalisaties maken en analyseren. |                                  |
| **Pre condition:**  Gebruiker is ingelogd. **Post condition:** Het project is aangemaakt binnen het systeem. |                                  |
| **Actor**                                                    | **Systeem**                      |
| 1. Gebruiker voert gewenste projectnaam in.                  |                                  |
|                                                              | 2. Systeem slaat het project op. |
| **Alternative flows**                                        |                                  |
|                                                              |                                  |

*Tabel  - Fully dressed use case aanmaken project*

#### 2. BPMN

![IMG](Img\BPMN model Project aanmaken.png)

*Figuur  - BPMN aanmaken project*



#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich in het projectoverzicht.<br /> Daarnaast is de gebruiker niet eigenaar van een <br />andere project genaamd "TestProject". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                     | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :--------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Project aanmaken" openen                             | De knop met het plusteken.                     | De gebruiker wordt doorverwezen naar een pagina waar hij de projectnaam kan in vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren projectnaam                                         | Vul in het projectnaam veld: "TestProject" in. | Het veld waar de projectnaam in ingevuld kan worden, toont “TestProject”. | Hetzelfde als verwacht  |
| 3.          | Project aanmaken                                             | Druk op de  knop "Project aanmaken".           | De gebruiker wordt doorverwezen naar het projectoverzicht en het gemaakte project is toegevoegd. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                |                                                              |                         |

*Tabel  - testcase project aanmaken*



| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich in het projectoverzicht <br />en de gebruiker is eigenaar van een project <br />genaamd: "TestProject". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                     | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :--------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Project aanmaken" openen                             | Klik op de knop met het plusteken.             | Gebruiker wordt doorverwezen naar een pagina om de projectnaam in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren projectnaam                                         | Vul in het projectnaam veld: "TestProject" in. | Het veld waar de projectnaam in ingevuld kan worden, toont “TestProject”. | Hetzelfde als verwacht  |
| 3.          | Project aanmaken                                             | Druk op de  knop "Project aanmaken".           | De gebruiker krijgt een notificatie dat de ingevoerde projectnaam al door de gebruiker in gebruik is. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                |                                                              |                         |

*Tabel  - testcase project aanmaken met gebruikte projectnaam*



### Gebruiker toevoegen aan project

#### 1. Fully-dressed use case

| Use case Gebruiker toevoegen aan project                     |                                                |
| :----------------------------------------------------------- | ---------------------------------------------- |
| **Brief description:** Aan projecten kunnen gebruikers toegevoegd worden. De eigenaar van het project kan nieuwe teamleden toevoegen. Toegevoegde gebruikers kunnen verbalisaties toevoegen en analyses binnen het project aanpassen. |                                                |
| **Pre condition:**  Gebruiker heeft een account. Gebruiker is ingelogd. Gebruiker is eigenaar van het project <br />**Post condition:** Er is een gebruiker toegevoegd aan het project. |                                                |
| **Actor**                                                    | **Systeem**                                    |
| 1. Gebruiker voert gewenste gebruikersnaam in.               |                                                |
|                                                              | 2. Systeem voegt gebruiker toe aan het project |
| **Alternative flows**                                        |                                                |
|                                                              |                                                |

*Tabel  - Fully dressed use case gebruiker toevoegen aan project*

#### 2. BPMN

![IMG](Img\BPMN model Gebruiker toevoegen aan project.png)

*Figuur  - BPMN aanmaken project*



#### 3. Test cases

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich in een projectdetailpagina <br />en er bestaat een andere gebruiker met de naam "User16". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                  | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------ | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Gebruiker toevoegen" openen                          | Klik op de knop "Bewerken"                  | Gebruiker wordt doorverwezen naar een pagina om een gebruikersnaam in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren gebruikersnaam                                      | Vul in het gebruikersnaam veld: "User16" in | In het veld waar de projectnaam ingevuld kan worden, wordt “User16” getoond. | Hetzelfde als verwacht  |
| 3.          | Gebruiker toevoegen                                          | Druk op de  knop "Gebruiker toevoegen".     | De gebruiker wordt doorverwezen naar de projectdetailpagina. Hier ziet hij de gebruikersnaam van de toegevoegde gebruiker staan bij “Projectleden”. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                             |                                                              |                         |

*Tabel  - testcase project aanmaken met gebruikte projectnaam*

| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker bevindt zich in een projectdetailpagina  en er bestaat een andere gebruiker met de naam "User16" en is lid van het actuele project. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                  | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------------------------ | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "Gebruiker toevoegen" openen                          | Klik op de knop "Bewerken"                  | Gebruiker wordt doorverwezen naar een pagina om een gebruikersnaam in te vullen. | Hetzelfde als verwacht  |
| 2.          | Invoeren gebruikersnaam                                      | Vul in het gebruikersnaam veld: "User16" in | In het veld waar de projectnaam ingevuld kan worden, wordt “User16” niet getoond. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                             |                                                              |                         |

*Tabel  - testcase gebruiker toevoegen die al toegevoegd is*

### Geschiedenis bekijken

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | Binnen het project zijn er al acties door gebruikers gedaan. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                | **Verwacht resultaat**                                  | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :------------------------ | ------------------------------------------------------- | ----------------------- |
| `1.         | Pagina "Geschiedenis" openen                                 | Gebruiker klikt op actie. | Gebruiker ziet welke veranderingen er doorgevoerd zijn. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                           |                                                         |                         |

*Tabel  - testcase geschiedenis*



### Business rule toevoegen

| Test case 1             |                                 |
| ----------------------- | ------------------------------- |
| Functie ontwikkeld door | ISE Groep 8                     |
| Test uitgevoerd door    |                                 |
| Datum uitvoering        |                                 |
| Beginsituatie           | De gebruiker heeft een project. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| `1.         | Pagina "Business rule toevoegen" openen                      | Gebruiker voert: "Klas grote" in het invoerveld met de tag "naam".<br /><br />In het veld met de naam voert de gebruiker de zin "business rule" de text: "In een klas mogen 42 studenten zitten." in. | De business rule is in een overzicht terug te vinden op de pagina. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase business rule toevoegen*







### Entiteit en subtype bekijken

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker heeft een project. Binnen dit project zijn predicates opgesteld. |

| **Stap**    | **Variabele of selectie**                                    | **Waarde** | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :--------- | ------------------------------------------------------------ | ----------------------- |
| `1.         | Pagina "Overzicht predicate" openen                          |            | De gebruiker ziet een overzicht van alle verbalisaties met de bijbehorende predicates. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |            |                                                              |                         |

*Tabel  - testcase overzicht predicate*



### Subtype entiteit aanmaken

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker zit in het overzicht van de entiteiten en subtype van een project. Dit project heeft de entiteit "Fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "subtype entiteit aanmaken" openen                    |                                                              | De gebruiker krijgt een pagina voor het invullen van een nieuwe entiteit en attributen te zien. | Hetzelfde als verwacht  |
| 2.          | Invoeren entiteit met attributen                             | Voer in het entiteit veld "Elektrisch" en voeg één attribuut toe met de naam "batterijduur". Vink beide primary identifier en mandatory aan. |                                                              | Hetzelfde als verwacht  |
| 3.          | Subtype aanmaken                                             | Druk op de knop opslaan.                                     | De gebruiker wordt doorverwezen naar het overzicht.          | Hetzelfde als verwacht  |
| 4.          | Subtype toevoegen bij de entiteit "Fiets"                    | Druk op de knop "Subtype toevoegen" bij de entiteit Fiets.   | De pagina voor een nieuwe subtype wordt geopend.             | Hetzelfde als verwacht  |
| 5.          | Check of het subtype is aangemaakt                           | Typ de "elektrisch" in in het subtype en kies het subtype "elektrisch". | De subtype elektrisch verschijnt.                            | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase subtype entiteit aanmaken*

| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker zit in het overzicht van de entiteiten en subtype van een project. Dit project heeft de entiteit "Fiets". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "subtype entiteit aanmaken" openen                    |                                                              | De gebruiker krijgt een pagina voor het invullen van een nieuwe entiteit en attributen te zien. | Hetzelfde als verwacht  |
| 2.          | Invoeren entiteit met attributen                             | Voer in het entiteit veld "Fiets" en voeg één attribuut toe met de naam "batterijduur". Vink beide primary identifier en mandatory aan. |                                                              | Hetzelfde als verwacht  |
| 3.          | Subtype aanmaken                                             | Druk op de knop opslaan.                                     | De gebruiker krijgt een melding dat het entiteit al bestaat. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase subtype entiteit bestaat al*

### Subtype groep aanmaken

| Test case 1             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker zit in het overzicht van de entiteiten en subtype van een project. Dit project heeft de entiteit "Fiets". Daarnaast bestaat er een subtype met de naam "Elektrisch". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "subtype toevoegen" openen                            | Druk op de knop "subtype toevoegen" bij de entiteit "Fiets". | De gebruiker ziet de pagina om een subtype groep met subtypen aan te maken. De naam van het supertype is "Fiets". | Hetzelfde als verwacht  |
| 2.          | Subtype informatie invoeren                                  | Voeg in het Subtypegroepnaam veld "groep 1" in en maak de relatie zonder restricties. Voeg daarnaast "Elektrisch" als subtype toe. |                                                              | Hetzelfde als verwacht  |
| 3.          | Subtypegroep opslaan.                                        | Druk op de knop "opslaan".                                   | De gebruiker wordt doorverwezen naar het overzicht en de subtypegroep "Groep 1" staat bij de entiteit "Fiets" met de subtype "Elektrisch". | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase subtype groep aanmaken*

| Test case 2             |                                                              |
| ----------------------- | ------------------------------------------------------------ |
| Functie ontwikkeld door | ISE Groep 8                                                  |
| Test uitgevoerd door    |                                                              |
| Datum uitvoering        |                                                              |
| Beginsituatie           | De gebruiker zit in het overzicht van de entiteiten en subtype van een project. Dit project heeft de entiteit "Fiets" en is supertype van de subtype groep "Groep 1". Daarnaast bestaat er een subtype met de naam "Elektrisch". |

| **Stap**    | **Variabele of selectie**                                    | **Waarde**                                                   | **Verwacht resultaat**                                       | **Werkelijk resultaat** |
| ----------- | ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ | ----------------------- |
| 1.          | Pagina "subtype toevoegen" openen                            | Druk op de knop "subtype toevoegen" bij de entiteit "Fiets". | De gebruiker ziet de pagina om een subtype groep met subtypen aan te maken. De naam van het supertype is "Fiets". | Hetzelfde als verwacht  |
| 2.          | Duplicaat van subtype roepnaam invoeren                      | Voeg in het Subtypegroepnaam veld "groep 1" in en maak de relatie zonder restricties. Voeg daarnaast "Elektrisch" als subtype toe. |                                                              | Hetzelfde als verwacht  |
| 3.          | Duplicaat opslaan                                            | Druk op de knop "opslaan".                                   | De gebruiker krijgt een melding dat deze entiteit al een subtypegroep heeft met dezelfde naam. | Hetzelfde als verwacht  |
| Opmerkingen | <Ruimte   voor eventuele opmerkingen als adviezen t.a.v. gebruiksgemak> |                                                              |                                                              |                         |

*Tabel  - testcase subtype groep bestaat al

