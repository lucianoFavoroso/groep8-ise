# Technisch ontwerp

## Inleiding

### Overall description

Dit document is het Technisch Ontwerp voor het ISE-project van projectgroep 8. De opdracht die uitgevoerd moet worden tijdens dit project, betreft het maken van een informatiesysteem en een daarbij horende (demo)applicatie voor de opdrachtgever Chris Scholten. Deze opdracht zal uitgevoerd worden door een groep van vijf studenten van de Hogeschool van Arnhem en Nijmegen uit het tweede jaar. Zij volgen tijdens het project het blok ISE. De groep studenten die de opdracht uitvoert, vormt de projectgroep.  

### Assignment description

In een professionele setting is het niet praktisch om gebruik te maken van fact based modelleren met behulp van pen en papier; het gebruik van pen en papier past beter bij een workshop. Voor grote projecten zou het handig zijn om een tool te hebben waarmee het hele proces kan worden doorlopen. Deze tool zal dan als resultaat een conceptueel datamodel hebben. Om deze tool te kunnen maken, is het van groot belang om een informatiemodel te hebben die deze tool kan ondersteunen. Voor dit project is de studenten gevraagd een informatiemodel te ontwikkelen waarin alle nodige gegevens opgeslagen kunnen worden.

### Purpose of this document

Binnen dit document worden de technische aspecten van de realisatie van het project besproken. Het document bevat de technische oplossingen voor de functionele eisen. In dit specifieke document wordt allereerst het PDM besproken. Er wordt kort ingegaan op de verschillende concepten. Verder wordt er ook nog ingegaan op de aangebrachte veranderingen tussen het CDM en het PDM. 

Vervolgens wordt de systeemarchitectuur besproken. Hierin wordt besproken hoe de verschillende componenten samenhangen binnen ons project. Indien relevant wordt er ook besproken waarom er voor bepaalde technieken is gekozen.

Hierna wordt de softwarearchitectuur besproken. In dit hoofdstuk is te lezen hoe de software wordt opgebouwd. Verder kan men hier ook lezen welke programmeertalen worden geütiliseerd.

Verder wordt de technische gegevensstructuur besproken. Hier worden de verschillende business rules besproken en geanalyseerd. Indien het niet vanzelfsprekend is hoe het opgelost moet worden, is er een stappenplan bij geleverd. 

In het volgende hoofdstuk wordt de implementatie besproken. Dit is geschreven voor developers die met dit project verder gaan werken. Hier staat per use-case een analyse over wat er moet gebeuren, welke parameters er worden gebruikt, op welke manier de business rules worden bewaakt (CHECK, stored procedure of trigger), op welke scenario's wordt getest, op welke situaties wordt gecontroleerd en wat voor problemen er kunnen plaatsvinden bij door concurrency.

In het hoofdstuk indexes staat beschreven wat er binnen de database met indexes is gedaan. Het hoofdstuk hierna gaat over op welke manier we de niet functionele eisen hebben verwerkt binnen het systeem. 

Ten slotte sluiten we af met een hoofdstuk waar advies instaat voor het volgende ontwikkelteam wat aan de slag gaat met dit project. 

# PDM

 ![IMG](img\pdm.png)

Voor oorsprong van het PDM zie: Funtioneel ontwerp, Conceptueel datamodel.

Hierboven is het PDM te zien. In dit PDM zijn alle tabellen uit de database te zien. Verder kan men ook de attributen, primary keys en datatypen zien. Ten slotte zijn ook de verschillende foreign-key relaties te zien die tussen de tabellen liggen.

### Glossary


| **Tabel**                | **Betekenis**                                                |
| ------------------------ | ------------------------------------------------------------ |
| Verbalisatie             | De opgestelde verbalisatie binnen een project.               |
| Relatie                  | Deze relaties krijgen een naam en kan van het relatietype “many-one”, “many-many” of “one-one” zijn. |
| Rol                      | Een relatie heeft altijd twee rollen. In een relatie worden de primaire en secundaire rol aangeven. deze informatie zal worden opgeslagen in de rollen tabel. |
| Entiteit_in_project      | Bevat alle details van een entiteit binnen een project. Details die onder andere in deze tabel worden opgenomen zijn: of de entiteit is een subtype is, wanneer dit het geval is wordt er vermeld bij welke groepering hij hoort. |
| Attribuut_in_project     | Deze entiteit heeft een relatie met Entiteit_in_project. Door deze relatie kan er opgezocht worden uit welke attributen een entiteit bestaat. Er wordt ook aangegeven of dit attribuut onderdeel is van de Primairy Identifier. |
| Entiteit_in_verbalisatie | Alle entiteiten die gevonden zijn in verbalisaties zijn terug te vinden in deze tabel. |
| Subtype_groep            | Informatie met betrekking tot een subtypegroep is hier terug te vinden. Hierin staat welke entiteit het supertype is, en wat voor soort subtype relaties er binnen deze groep zijn. |
| Gebruiker                | In deze tabel zijn de gegevens van de gebruiker opgeslagen.  |
| Project                  | In deze tabel wordt het ID en de naam van het project opgeslagen. |
| Project_bezetting        | Koppel entiteit tussen gebruiker en project. Hier zullen alle gebruikers bij de des betreffende projecten opgeslagen worden. Ook wordt hier opgeslagen wie de eigenaar is van een project. |
| Businessrule             | In projecten kunnen businessrules worden gedefinieerd. Deze regels beschrijven welke constraints er op bepaalde entiteiten zitten. |
| Subverbalisatie          | Verbalisaties zijn zinnen die opgesteld zijn door de gebruiker. Wanneer de gebruiker wenst om voor een verbalisatie meer voorbeelden op te stellen, is dit mogelijk voor de gebruiker. De verbalisaties worden in deze tabel opgeslagen. |
| Invulplek                | Bij een verbalisatie kunnen bepaalde stukken van de zin onderstreept worden door de gebruiker. Het onderstreepte deel wordt gerepresenteerd door een 'Invulplek' in deze database. Een invulplek bestaat uit een beginpositie en eindpositie (karakterpositie in de verbalisatie) en bevat een verwijzing naar de verbalisatie. |

## Aanpassingen transformatie CDM naar PDM

Bij het transformeren van het CDM naar het PDM zijn er vaak een aantal technische oplossingen die toegepast worden om met bepaalde problemen om te gaan. Dit is ook het geval bij dit PDM. Hieronder wordt per tabel besproken welke veranderingen er zijn gemaakt, en waarom we voor deze verandering hebben gekozen.

Voor meer informatie over het CDM zie: Funtioneel ontwerp, Conceptueel datamodel.

### Geschiedenis

Tijdens het ontwikkelen van het CDM, waren er drie mogelijkheden voor het opslaan van de historie in het datamodel. Deze opties waren onder ander:

1. Elk entiteit dat in aanmerking kan komen voor verandering krijgt de attribuut gebruikers_id erbij en een date-time veld. De gebruikers_id heeft niet een directe relatie met de entiteit gebruiker.
2. Voor elke entiteit worden er schaduwtabellen aangemaakt. Op deze manier heb je een 'copy' van de originele tabel waarin alle geschiedenis wordt bijgehouden. 
3. Er komt een nieuwe entiteit genaamd historie, in deze entiteit worden de veranderingen opgeslagen als plaintext.

Op basis van overleg met de opdrachtgever was er in eerste instantie voor optie 1 gekozen. Tijdens het ontwikkelen van de database en het implementeren van de constraints ontstonden er een aantal problemen. Hierdoor werd het implementeren van de use-case een stuk ingewikkelder. Dit heeft er bij de eerste paar use-cases ervoor gezorgd dat het 20% langer duurde dan de ingeschatte tijd.

Nadat we hier achter zijn gekomen hebben we naar alternatieven gekeken. Binnen SSMS is er een mogelijkheid om de history bij te houden via een manier die door Microsoft wordt geleverd. Hier hebben we even snel naar gekeken en het op onze database toegepast.

Het was voor ons aan het begin onduidelijk hoe we hiermee de geschiedenis succesvol op konden halen. Dit bleek lastiger dan verwacht.

Terwijl 1 persoon bezig was met de bovenstaande manier, was iemand anders aan optie 2 begonnen (schaduwtabellen). We konden geen problemen verzinnen die plaats vinden bij het gebruik van schaduwtabellen. Daarom is er uiteindelijk gekozen voor schaduwtabellen. Deze zijn aan de hand van codegeneratie gegenereerd voor elke tabel.

### Globaal

Alle entiteiten uit het CDM hebben een copy gekregen in de vorm van een history-tabel. Deze hebben een extra attribuut wat aangeeft of het een insert, update of delete is. Deze tabellen zijn niet opgenomen in het PDM. De reden hiervoor is dat het PDM hierdoor onoverzichtelijk wordt.

### Surrogate keys

Bij de tabel 'Relatie', 'Verbalisatie', 'Entiteit_in_project' en 'Subtype_groep'  is er gebruik gemaakt van een surrogate key. Deze keuze is gemaakt om de child-tabellen overzichtelijk te houden. Als dit niet het geval was dan zou de gehele alternative key van de parent-tabel bij elke row in de tabel van de child-tabel staan. 
## Systeemarchitectuur

Een systeemarchitectuur beschrijft de structuur van een systeem, bestaande uit componenten en de relaties tussen deze componenten. De componenten die het geheel van het systeem vormen, zijn als volgt:

* De webapplicatie;
* Een Serviceprovider;
* Een MS-SQL Database.

De webapplicatie die gerealiseerd is, maakt gebruik van een service die vervolgens data ophaalt uit de database om de webapplicatie functioneel te maken. De acties die een gebruiker uitvoert, worden door middel van een service opgevangen en verwerkt in de database. Het uiteindelijke doel is dat er een CDM-script gegenereerd kan worden voor PowerDesigner.

De software van PowerDesigner wordt niet verwerkt in de systeemarchitectuur. Om de relaties tussen bepaalde applicaties vast te leggen zou er gebruik gemaakt kunnen worden van een applicatie-architectuur. Echter heeft dit bij ons geen meerwaarde, omdat de applicaties los van elkaar functioneren. Zo wordt er een exportscript gegenereerd en is het aan de gebruiker zelf, om deze te importeren in PowerDesigner.

In de volgende afbeelding ziet u de systeemarchitectuur van het systeem en worden de relaties aangegeven.

![Systeemarchitectuur](Img/Systeemarchitectuur.PNG)

### Gebruikte DBMS

Tijdens het interview met de opdrachtgever is gebleken dat er geen voorkeur is voor een ontwikkelomgeving. Door de opdrachtgever is geadviseerd een DBMS te kiezen waar iedereen al ervaring mee heeft. Tijdens de ISE-courses hebben alle teamleden gebruikt gemaakt van Microsoft SQL server (MSSQL). Om deze reden is ervoor gekozen om tijdens dit project gebruik te maken van Microsoft SQL server 2017.

MSSQL biedt op het gebied van concurrency genoeg mogelijkheden om data-integriteit in stand te houden. Zo zijn er vier isolation levels. Met deze levels kan ervoor worden gezorgd dat alle transacties goed in multi-user environments werken. Deze isolation levels kunnen per transactie bepaald worden. Door de mogelijkheid om per transactie aan te geven welk isolation-level nodig is, is er genoeg aanpasbaarheid om de concurrency goed te regelen.

Binnen MSSQL kunnen bepaalde business rules afgevangen worden. Dit kan door gebruikt te maken van declaratieve constraints. Door constraints toe te voegen, hoeft er geen eigen code geschreven te worden. MSSQL bewaakt deze constraints zelf, en hoeven deze niet uitgebreid getest te worden. De performance van deze constraints zullen altijd optimaal zijn.

## Softwarearchitectuur

Een software-architectuur is de structuur van een softwaresysteem, bestaande uit software-elementen en de relaties tussen deze elementen.

De applicatie wordt van scratch gerealiseerd, door middel van de programmeertalen PHP, HTML, JS en CSS. Voor het realiseren van de applicatie wordt er gebruik gemaakt van het"Layer pattern".

Het layer pattern is een van de meest bekende patterns. Het idee is om de code op te splitsen in lagen. Elk laag heeft zijn eigen verantwoordelijkheid en communiceert met een hogere laag.  De volgende lagen worden gebruikt:

- Presentation layer - Laat de informatie zien aan de gebruiker;
- Business layer - Handelt de input van de gebruikers af;
- Persistence layer - Manipuleert de database zoals SQL statements. Dit is de laag die communiceert met de database.

In de volgende afbeelding ziet u de softwarearchitectuur van het "Layer pattern".

![Softwarearchitectuur](Softwarearchitectuur\Softwarearchitectuur.PNG)

## Technische gegevensstructuur

### Analyse business rules

#### Relaties

##### Een relatie moet twee entiteiten bevatten

Om een correct CDM te kunnen maken moet een relatie altijd verbonden zijn tussen twee entiteiten. Deze twee entiteiten mogen dezelfde entiteit zijn of verschillenden. Dit maakt het mogelijk dat een entiteit naar zichzelf kan refereren.

Oplossing: De relatie tussen twee entiteiten, wordt opgeslagen in de entiteit "Relatie". Er moet gecontroleerd worden of beiden velden ingevoerd zijn en alleen entiteiten bevatten die binnen het project bestaan.

##### Relaties zijn altijd many-one, one-many , many-many of one-one

Relaties moeten altijd van het type many-one, many-many of one-one zijn. 

Dit kan opgelost worden een check constraint te zetten op de kolom cardinaliteitstype in de tabel "Relatie". De check constraint moet controleren of de waarde in deze kolom "many-one","one-many","many-many" of "one-one" is.

###### Many-one

Als het voorkomt dat een Rol mandatory is, is de minimale cardinaliteit 1.

Binnen een relatie bestaan twee rollen. Wanneer het cardinaliteitstype "many-to-one" is, is de many kant altijd de primary rol. De one kant is altijd de secundaire rol. Wanneer de rol van het soort primair is, mag deze rol dependent zijn. Indien dit het geval is moet deze rol ook mandatory zijn. Als de rol van het type secondair is, mag deze niet dependent zijn. Echter, kan de secundaire rol wel of niet mandatory zijn.

<u>Stappenplan</u>

Trigger/Stored procedure:

Stap 1: Controleer welk type de relatie is in de "Relatie" tabel.

Stap 2: Zoek welke rol de niet-primaire rol is in de Rol tabel.

Stap 3: Zet de waarde dependent op "false".

Stap 4: Controleer of de rollen van deze relatie mandatory zijn. Wanneer een rol mandatory is, moet het veld cardinaliteit_minimun minimaal de waarde 1 hebben.

Wanneer een insert of update aan deze voorwaarde voldoet mag de insert/update doorgevoerd worden.

###### Many-many

Wanneer het cardinaliteitstype van een relatie "many-many" is moeten de rollen de waarde "false" hebben bij de kolom dependent.

<u>Stappenplan</u>

Stap 1: Controleer of het cardinaliteitstype van deze relatie "many-to-many" is bij de ingevoerde relatie.

Stap 2: Zet bij beiden rollen de waarde dependent op "false".

###### One-one

Wanneer het cardinaliteitstype van een relatie "one-one" is moet er een dominant role aanwezig zijn.

<u>Stappenplan</u>

Stap 1: Controleer of het cardinaliteitstype van deze relatie "one-to-one" is bij de ingevoerde relatie.

Stap 2: Controleer of een van deze twee rollen de dominant is.

Wanneer de relatie een "one-to-one" is en een van de twee rollen dominant is kan de insert/update doorgaan.

#### Eniteiten

##### Entiteiten moeten een primary indentifier hebben

Tijdens de controle of het model correct is moeten alle entiteiten een primary identifier hebben.

<u>Stappenplan</u>

Stap 1: Controleer of alle entiteiten binnen het project minimaal 1 attribuut hebben dat als primary identifier is gemarkeerd.

Wanneer dit het geval is, laat de generatie doorgaan.

##### Entiteit namen binnen een project zijn uniek

Alle entiteitnamen binnen een project moeten uniek zijn.

Plaats een unique constraint op het projectnaam veld in de entiteit Entiteit.

##### Entiteiten die al in het project zijn geindentificeerd moeten is_match true hebben

Wanneer een entiteit al in eerdere verbalisatie geïdentificeerd is, moet de waarde is_match "true" zijn. Als een entiteit nog niet geïdentificeerd is moet de waarde is_match "false" zijn.

Dit probleem kan opgelost worden met een stored procedure en een trigger. Voor de stored procedure zal een insert, delete en update gebouwd moeten worden. Bij een trigger is het mogelijk alle drie de situaties gemakkelijk af te vangen in een enkele trigger. Omdat een trigger het werk kan doen in plaats van drie stored procedures heeft een trigger de voorkeur.

<u>Stappenplan</u>

Stap 1: Controleer of de uitgevoerde operatie een update/insert/delete is.

Wanneer de operatie een update of insert statement is, volg de stappen gemarkeerd met A. Als dit niet het geval is, volg de stappen gemarkeerd met B.

Stap 2: Controleer of deze entiteit al eerder benoemd is binnen een verbalisatie in het huidige project.

Stap 3A: Als deze entiteit al voorkomt zet de is_match waarde op "true"

Stap 3B: Als deze entiteit al eerder benoemd is controleer hoevaak deze entiteit voorkomt

Stap 4B: Als deze entiteit maar 1 keer voorkomt kan de delete doorgaan. Wanneer deze entiteit twee keer of vaker voorkomt geef een foutmelding dat de gebruiker een entiteit of verbalisatie probeert te verwijderen waar anderen entiteiten op matchen.

#### Verbalisaties

**Aantal entiteiten**

Een verbalisatie zonder of met een relatie mag maximaal twee entiteiten bevatten. Wanneer er in een verbalisatie twee relaties zijn moeten er drie entiteiten geïdentificeerd zijn. Verbalisaties mogen maximaal twee relaties bevatten. 

Deze constraint moet niet altijd bewaakt worden. Alleen wanneer de gebruiker wenst het gemaakte werk te controleren, dan zal deze constraint gelden.

<u>Stappenplan</u>

Stap 1: Tel het aantal entiteiten bij elke verbalisatie. Als dit minder dan 1 of meer dan 3 is geef een foutmelding.

Stap 2: Controleer of alle verbalisaties met 3 entiteiten twee relaties hebben. Zo niet geef een foutmelding.

Stap 3: Tel het aantal relaties bij elke verbalisatie. Als dit er meer zijn dan twee geef een foutmelding.

##### Attribuut_in_verbalisatie posities

Bij de entiteit attribuut_in_verbalisatie wordt er aangegeven op welk karakter van de zin het attribuut start en eindigt. De eind positie mag niet groter zijn dan het aantal karakters in de zin. En de begin positie mag niet kleiner zijn dan 0. 

De eindpositie mag niet kleiner zijn dan de ingevoerde beginwaarde.

Voor het invoeren van attributen in verbalisaties wordt een stored procedure gemaakt. Deze constraint kan gemakkelijk verwerkt worden binnen een stored procedure. Daarom is ervoor gekozen om een stored procedure te gebruiken.

<u>Stappenplan</u>

Stap 1: Controleer of de waarde attribuut begin_positie groter is dan 1.

Stap 2: Tel het aantal woorden in de verbalisatie.

Stap 3: Check of de waarde attribuut begin_positie kleiner is dan het aantal woorden in de verbalisatie.

#### Subtypes

Wanneer een entiteit zelf een parent is van een groep subtypes mag hij zelf niet in deze groep voor te komen.

Bij updaten/inserten kan het voorkomen dat de parent van de groep zelf in een subtype groep komt. Dit kan opgelost worden door een stored procedure voor inserten en updaten of door een after insert, update trigger. 

Controleer of de entiteit binnen de subtype groep gemarkeerd met parent niet aanwezig is in zijn eigen subtype groep is. Als dit toch het geval is laat de insert of update niet doorgaan en geef een foutmelding.

#### Projecten

##### Projecten hebben minimaal 1 lid

Elk project moet minimaal 1 lid hebben.

Bij het updaten van leden in een project controleer of er minimaal nog 1 lid in zit. Wanneer dit niet het geval is na de update geef een foutmelding.

##### Projecten hebben altijd een eigenaar

Elk project moet altijd een eigenaar hebben.

Bij het verwijderen van een persoon in een project controleer of dit de eigenaar is. Wanneer dit de eigenaar is en het aantal leden nog boven de 1 is, geef een ander lid de rol als eigenaar. Als de eigenaar de laatste persoon is in een project mag hij zichzelf niet verwijderen.



## Implementatie


Binnen dit hoofdstuk worden de code oplossingen voor de functionele en niet functionele eisen besproken. Voor elke Use-case of overige functionele eis is er aangegeven welke code dit oplost, welke parameters verwacht worden, welke unit tests er zijn voor de code, analyse van concurency problemen en welke stappen deze code zet om deze eis te implementeren.



### Use Case Subtypegroep aanmaken

Voor meer informatie over de functionaliteit van het aanmaken van een subtypegroep zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Subtypegroep aanmaken

Binnen het systeem heeft de gebruiker de mogelijkheid subtypegroepen aan te maken. Een subtypegroep is een groep met entiteiten die gekoppeld worden aan een parent subtype. Deze subtypegroep krijgt ook een relatie met betrekking tot de parent meegegeven. Er mogen meerdere subtypegroepen per supertype toegevoegd worden.

Voor meer informatie over de functionaliteit van het aanmaken van een Subtypegroep zie: Functioneel ontwerp, Hoofstuk: Fully dressed use cases, Use case: subtype groep aanmaken.

**CREATE**

Voor het aanmaken van een subtypegroep binnen een project, worden er parameters meegegeven. Met deze parameters kan er een nieuwe subtypegroep aangemaakt worden.

Voor deze operatie wordt een stored procedure gemaakt waarmee de gebruiker nieuwe subtypegroepen kan aanmaken.

Stored procedure: usp_insertSubtypeGroup

Parameters:

@parent_entiteit int,
@entiteit int,
@gebruikersnaam varchar(100),
@groepsnaam varchar(100),
@mutually_exclusive smallint,
@complete smallint

**Stappen subtypegroep aanmaken**

1. Controleer of de combinatie van de meegegeven supertype @parent_entiteit en @groepsnaam al een groep heeft, als dit het geval is sla de insert van een nieuwe groep over;
2. Bestaat de groep nog niet voeg de groep toe aan de subtypegroep;
3. Haal insert op door middel van de parent_entiteit en sla subtype_id op, deze wordt gebruikt om subtypes toe te voegen aan een groep;
4. Controleer of het subtype een parent van de groep is. Als dit het geval is kan de subtype niet worden toegevoegd aan de groep;
5. Als het subtype een parent is, laat een error zien;
6. Is het subtype geen parent, doe een update in ENTITEIT_IN_PROJECT met de opgehaalde subtype_id.

**UPDATE**

Stored procedure: usp_updateSubtypeGroup

Parameter:	

@subtypegroep_id int,
@gebruikersnaam varchar(100),
@groepsnaam varchar(100),
@mutually_exclusive smallint,
@complete smallint

**Stappen subtypegroep update**

1. Controleer of de meegegeven supertype @parent_entiteit nog geen groep heeft, laat dan een error zien;
2. Update groepsnaam, mutually_exclusive en complete.

**Parameters**

Hieronder worden alle parameters beschreven die in de stored procedure usp_insertSubtypeGroup en usp_updateSubtypeGroup gebruikt worden.

**@parent_entiteit**

De entiteit waar de subtypegroep aan gekoppeld moet worden. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@entiteit**

De entiteit die aan een subtypegroep gekoppeld moet worden. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@gebruikersnaam**

De gebruikersnaam, van de gebruiker die de subtypegroep aanmaakt. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@groepsnaam**

De benaming voor de subtypegroep. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@mutually_exclusive**

De relatie tussen de parent en de subtype. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@complete**

De relatie tussen de parent en de subtype. Deze parameter wordt gebruikt in de stored procedure van de INSERT en UPDATE.

**@subtypegroep_id**

Het id van de subtypegroep die geupdate moet worden. Deze parameter wordt gebruikt in de stored procedure van de UPDATE.

#### Test cases

**Test scenario 1**

De meegegeven subtype, mag niet de supertype zijn van een subtypegroep zijn. Als dit het geval is wordt er een error getoond.

**Test scenario 2**

Er wordt gecontroleerd of de meegegeven supertype al een bestaande groep heeft. Is dit niet het geval wordt er een INSERT gedaan met de nieuwe supertypegroep.

**Test scenario 3**

De meegegeven subtype, mag niet het supertype zijn van een subtypegroep zijn. Als dit niet het geval is wordt er netjes een INSERT gedaan met de subtype bij de bijbehorende subtypegroep.

**Test scenario 4**

Bij het UPDATE van een subtypegroep, wordt er door middel van de meegegeven subtypegroep_id gecontroleerd of de subtypegroep bestaat. Als dit niet het geval is, wordt er een error getoond.

**Test scenario 5**

Bij het UPDATE van een subtypegroep, wordt er door middel van de meegegeven subtypegroep_id gecontroleerd of de subtypegroep bestaat. Als dit het geval is, wordt de update uitgevoerd.



### Use Case Benoemen attribuut

Voor meer informatie over de functionaliteit van het benoemen van een attribuut zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Benoemen attributen.

Binnen het systeem heeft de gebruiker de mogelijkheid attributen te benoemen. Een attribuut is een verzameling van woorden die opgeslagen wordt door middel van de verbalisatie_id, beginpositie en eindpositie. Deze combinatie moet uniek zijn en kan dus niet dubbel voorkomen. Ook is het niet mogelijk een attribuut op te slaan op een positie die al gebruikt wordt.

**Beïnvloede tabellen**

ENTITEIT_IN_PROJECT, ENTITEIT_IN_VERBALISATIE, ATTRIBUUT_IN_VERBALISATIE, VERBALISATIE, INVULPLEK.

**CREATE**

Voor het aanmaken van een attribuut binnen een entiteit, worden er parameters meegegeven.
Met deze parameters kan er een nieuwe attribuut aangemaakt worden. 

Voor deze operatie wordt een stored procedure gemaakt waarmee de gebruiker nieuwe attributen kan toevoegen.

Stored procedure: usp_addAttribute

Parameters: @eniteit_id    INT,
					   @verbalisatie_id int,
					   @beginPos int,
					   @eindPos int, 
                       @gebruikersnaam VARCHAR(100), 
                       @attribuut_naam VARCHAR(500)
					   @is_primary_identifier int,
					   @is_manditory int,
					   @TOTAALAANTALKARAKTERS int

**Stappen attribuut toevoegen**

1. De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten. Ook mag de beginpositie niet boven de eindpositie komen en niet onder 0. 
2. De eindpositie mag niet onder de beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.
3. Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.
4. SELECT om te kijken of er al een attribuut aan de invulplek is gekoppeld. Bij attributen moet er gekeken worden naar de verbalisatie_id in combinatie met de begin positie en eindpositie.
5. SELECT om te kijken of de entiteit_id hetzelfde project_id heeft als de verbalisatie van verbalisatie_id.
6. INSERT INVULPLEK en INSERT ATTRIBUUT_INVERBALISATIE

**UPDATE**

Stored procedure: usp_UpdateVerbalisation

Parameters: @verbalisatie_id int, 
		       		@oldBeginpos int,
                       @oldEindpos int,
                       @newBeginPos int,
                       @newEindPos int, 
                       @gebruikersnaam VARCHAR(100), 
                       @attribuut_naam VARCHAR(500)
					   @is_primary_identifier int,
					   @is_mandatory int,	
					   @TOTAALAANTALKARAKTERS int

**Stappen attribuut update**

1. De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten. Ook mag de beginpositie niet boven de eindpositie komen en niet onder 0. 
2. De eindpositie mag niet onder de beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.
3. Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.
4. SELECT om te kijken of de nieuwe ingevoerde @beginpos of @eindpos al een attribuut heeft. (Except de te update record)
5. Update het record met als ID @verbalisatie_id, @beginpos, @eindpos, attribuut_naam van de tabellen INVULPLEK en ATTRIBUUT_IN_VERBALISATIE

#### **Parameters**

Hieronder worden alle parameters beschreven die in de stored procedure usp_addAttribute en usp_updateAttribute gebruikt worden.

**@TOTAALAANTALKARAKTERS**

Het totaal aantal karakters van de verbalisatie_zin.

**@entiteit_id**

Entiteit_id waar de insert voor nodig is.

**@verbalisatie_id**

Verbalisatie_id waar de update of insert voor nodig is.

**@gebruikersnaam**

Gebruikersnaam van de gebruiker die de verandering doorvoert.

**@beginPos**

Beginpositie van een attribuut.

**@eindPos**

Eindpositie van een attribuut.

**@oldBeginPos**

Oude beginpositie van een attribuut.

**@oldEindPos**

Oude eindpositie van een attribuut.

**@newBeginPos**

Nieuwe beginpositie van een attribuut.

**@newEindPos**

Nieuwe eindpositie van een attribuut.

**@attribuut_naam**

De benaming van de attribuut.

**@is_primary_identifier**

Een boolean om aan te geven of de attribuut een primary identifier is.

**@is_mandatory **

Een boolean om aan te geven of de attribuut mandatory is.

#### **Test cases**

**Test scenario 1**

De beginpositie van een attribuut mag niet boven het aantal karakters van de verbalisatie_zin zitten. Ook mag de beginpositie niet boven de eindpositie komen en niet onder de 0. Is een van deze situaties het geval wordt er een error verwacht.

**Test scenario 2**

De eindpositie van een attribuut mag niet onder de beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie_zin. Is een van deze situaties het geval wordt er een error verwacht.

**Test scenario 3**

Wanneer de primary identifier op true staat, mag mandatory niet false zijn. Is dit wel het geval wordt er een error verwacht.

**Test scenario 4**
Attributen in een verbalisatie moet in combinatie met de beginpositie en eindpositie uniek zijn. Mocht deze attribuut al bestaan, wordt er een error verwacht.

**Test scenario 5**

De geselecteerde entiteit, waar het attribuut aan toegevoegd wordt, moet dezelfde project_id bevatten als de verbalisatie. Is dit niet het geval, wordt er een error verwacht.

**Test scenario 6**
Attributen in een verbalisatie moet in combinatie met de beginpositie en eindpositie uniek zijn. Is dit het geval, wordt er een insert gedaan.

**Test scenario 7**

De nieuwe beginpositie van een attribuut mag niet boven het aantal karakters van de verbalisatie_zin zitten. Ook mag de nieuwe beginpositie niet boven de nieuwe eindpositie komen en niet onder de 0. Is een van deze situaties het geval wordt er een error verwacht.

**Test scenario 8**

De nieuwe eindpositie van een attribuut mag niet onder de nieuwe beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie_zin. Is een van deze situaties het geval wordt er een error verwacht.

**Test scenario 9**

Wanneer de primary identifier op true staat, mag mandatory niet false zijn. Is dit wel het geval wordt er een error verwacht.

**Test scenario 10**
Attributen in een verbalisatie moeten in combinatie met de beginpositie en eindpositie uniek zijn. Bij het update wordt er gekeken of de nieuwe beginpositie en eindpositie beschikbaar zijn voor de update. Is dit niet het geval, wordt er een error verwacht.

**Test scenario 11**
Attributen in een verbalisatie moeten in combinatie met de beginpositie en eindpositie uniek zijn. Bij het update wordt er gekeken of de nieuwe beginpositie en eindpositie beschikbaar zijn voor de update. Is dit het geval, wordt er update uitgevoerd.

#### Concurency

**CREATE**

Bij de create stored procedure wordt er door middel van een IF EXISTS gecontroleerd of een attribuut in combinatie met het beginpositie en eindpositie al bestaat. Daarna wordt er door middel van een IF NOT EXISTS gekeken of de enititeit een ander project_id bevat als de verbalisatie. Wanneer beide gevallen niet waar zijn, wordt er een insert gedaan.

Door het standaard isolation level blijven S-locks tot het einde van een read. Hierdoor is het mogelijk dat een anderen insert tussen de IF EXISTS en insert kan komen. Wanneer dit gebeurd is het mogelijk dat de businessrule overtreden wordt. Dit kan opgelost worden door het isolation level naar repeatable read te zetten. Hierdoor blijven de S-locks staan tot het einde van de transactie. Doordat er een S-Lock aanwezig blijft is het niet meer mogelijk een nieuw record te inserted tussentijds.

**UPDATE**

Voor het updaten geldt het zelfde als bij het creëren.



### Use Case Invoeren verbalisatie

Voor meer informatie over de functionaliteit van het invoeren van een verbalisatie zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Invoeren verbalisatie.

Binnen het systeem heeft de gebruiker de mogelijkheid verbalisaties in te voeren.Een verbalisatie is een zin die in de database wordt opgeslagen als een varchar. Een verbalisatie moet uniek zijn binnen een project.

**Beïnvloede tabellen**

VERBALISATIE, PROJECT.

**CREATE**
Wanneer een nieuwe verbalisatie wordt gecreëerd wordt alleen de tabel Verbalisatie aangepast.
Een verbalisatie moet uniek zijn per project. Deze constraint moet handmatig geprogrammeerd worden.

Voor deze operatie wordt een stored procedure gemaakt waarmee de gebruiker nieuwe verbalisaties kan toevoegen. Voor alle operaties worden stored procedures gemaakt , zodat de database afgesloten kan worden van de buiten wereld. Alleen de stored procedures zullen beschikbaar gesteld worden voor de gebruiker.

Stored procedure: usp_CreateNewVerbalisatieForUserInProject

Parameters: @ProjectID    INT, 
                       @UserName     VARCHAR(20), 
                       @Verbalisatie VARCHAR(500)

Betekenis parameters:

**@ProjectID**

Id van het project waar de nieuwe verbalisatie aan wordt toegevoegd.

**@UserName**

Gebruikersnaam van de gebruiker die de verandering doet.

**@Verbalisatie**

Nieuwe verbalisatie zin die wordt toegevoegd aan het project.

**Stappen verbalisatie**

1. Controleer of de nieuwe verbalisatie nog niet bestaat in dit project
   Als de verbalisatie al bestaat geef een error.

**UPDATE**

Stored procedure: usp_UpdateVerbalisation

Parameters: @VerbalisatieID    INT, 
		      		 @ProjectID	INT,
                       @UserName     VARCHAR(20), 
                       @NewVerbalisatie VARCHAR(500)

Parameter betekenis:

**@VerbalisatieID**

VerbalisatieID waar de update voor nodig is.

**@ProjectID**

ProjectID van het project van deze verbalisatie.

**@UserName**

Gebruikersnaam van de gebruiker die de verandering doet.

**@NewVerbalisatie**

Nieuwe verbalasitie zin.

**Stappen verbalisatie UPDATE**

1. Bij een update moet er gecontroleerd worden of de nieuwe verbalisatie zin nog niet bestaat in dit project.
   Wanneer deze zin al bestaat behoort de database een foutmelding te geven.

2. Controleer of de nieuwe verbalisatie nog niet bestaat in zijn eigen subverbalisaties.
   Wanneer dit het geval is, voer de update uit.

#### **Test cases**

**Test scenario 1**
Verbalisaties in een project moeten uniek zijn. Controleer of de nieuwe verbalisatie nog niet bestaat in dit project.
Als deze nog niet bestaat laat de insert doorgaan.
Verbalisatie moet uniek zijn binnnen het project. Insert een verbalisatie die al bestaat in het project. Hierbij wordt een error verwacht.

**Test scenario 2**
Verbalisaties in een project moeten uniek zijn. Controleer of de nieuwe verbalisatie nog niet bestaat in dit project.
Als deze nog niet bestaat laat de insert doorgaan.
Verbalisatie moet uniek zijn binnnen het project. Insert een andere verbalisatie. Hierbij wordt geen error verwacht.

**Test scenario 3**
Verbalisaties binnen een project moeten uniek zijn.
Tijdens het updaten mag de nieuwe verbalisatie nog niet bestaan bij het project.
Hier wordt een goede situatie getest waar de nieuwe verbalisatie nog niet voorkomt bij een van de subverbalisaties, en nog steeds uniek is binnen het project.

**Test scenario 4**
Verbalisaties binnen een project moeten uniek zijn.
Tijdens het updaten mag de nieuwe verbalisatie nog niet bestaan bij het project.
Hier wordt een foute situatie gestest waar de nieuwe waarde van de verbalisatie al bestaat binnen het project. Hierbij wordt een error verwacht.

**Test scenario 5**
Een parent verbalisatie mag niet bestaan als subverbalisatie.
Tijdens het updaten mag de nieuwe verbalisatie niet opgenomen zijn als subverbalisatie bij deze parent.
Hier wordt een foute situatie getest waar de nieuwe verbalisatie al hetzelfde is als een subverbalisatie van deze verbalisaties.

#### Concurency

**CREATE**

Bij de create stored procedure wordt er doormiddel van een IF EXISTS gecontroleerd of een verbalisatie al bestaat bij dit project. Wanneer dit niet het geval is wordt er een insert gedaan.

Door het standaard isolation level blijven S-locks tot het einde van een read. Hierdoor is het mogelijk dat een anderen insert tussen de IF EXISTS en insert kan komen. Wanneer dit gebeurt is het mogelijk dat de businessrule overtreden wordt. Dit kan opgelost worden door het isolation level naar repeatable read te zetten. Hierdoor blijven de S-locks staan tot het einde van de transactie. Doordat er een S-Lock aanwezig blijft is het niet meer mogelijk een nieuw record te inserten tussentijds.

**UPDATE**

Voor het updaten geldt het zelfde als bij het creëren.



#### Use-case aanmaken account

##### Brief description

Voor meer informatie over de functionaliteit van het aanmaken van een account zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Aanmaken account.

Als bezoeker wil ik een account aanmaken voor het systeem. Dit maakt het voor mij mogelijk deel te nemen aan projecten. Ook kan ik hierdoor gebruik maken van alle bijbehorende functionaliteiten.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol aanmaken van een gebruiker:

**Beïnvloede tabellen**

GEBRUIKER.

##### Acties die plaats vinden

De volgende acties vinden plaats binnen de tabel GEBRUIKER:
SELECT om te kijken of er al een gebruiker bestaat met het ingevoerde gebruikersnaam.
INSERT om het nieuwe record toe te voegen.
SELECT om te kijken of de ingevoerde gebruikersnaam bestaat.

##### Foutafhandeling

De volgende mogelijke fout is getest in deze stored procedure:

1. Er is geen resultaat voor de combinatie van gebruikersnaam en wachtwoord.

**CREATE**

Beïnvloede tabellen: GEBRUIKERS

Stored procedure: usp_addUser

Parameters: @gebruikersnaam  VARCHAR(20), 
                       @wachtwoord     	VARCHAR(100) 

Parameter betekenis:

**@gebruikersnaam**

Gebruikersnaam van de gebruiker, die zich wilt aanmelden in het systeem.

**@wachtwoord**

Het wachtwoord om later in te loggen.

#### Tests

**Test scenario 1**
Als de gebruikersnaam al in de database voorkomt, wordt er een foutmelding weergegeven.

**Test scenario 2**
Als de gebruikersnaam nog niet in de database voorkomt, wordt er een INSERT gedaan.

##### Concurrency

Er vindt in deze transactie enkel een SELECT plaats. De standaard isolation level (READ COMMITED) is hierbij voldoende.



#### Use-case Project aanmaken

Voor meer informatie over de functionaliteit van het aanmaken van een project zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Aanmaken project.

Gebruikers kunnen binnen het systeem projecten aanmaken. Na het aanmaken van een project kan de gebruiker beginnen met zijn project.
Wanneer een gebruiker een project aanmaakt wordt hij automatisch de eigenaar van het project.

Tabellen die beinvloed worden door een project aanmaken: Project en ProjectBezetting.
Bij het aanmaken van een project wordt er een nieuw record in de Project tabel toegevoegd.
Wanneer een gebruiker een project maakt wordt hij gelijk de eigenaar en dus toegevoegd als record in Projectbezetting.

Overige functionele eisen: Project verwijderen
Projecten kunnen verwijderd worden. Alleen eigenaren van een project kunnen het project verwijderen.
Als de eigenaar een project verwijderd zullen de records in de database blijven bestaan maar wordt het gehele project weggeschreven in de hitsory.

**Create**
Voor het aanmaken van een nieuw project wordt een stored procedure aangemaakt. Als parameter wordt de projectnaam en username meegeven.
Met deze parameters kan er een nieuw project aangemaakt worden. Projectnamen moeten per eigenaar uniek zijn. Een gebruiker mag dus niet twee keer het project met dezelfde naam hebben.

Beïnvloede tabellen: Project, Projectbezetting.

Stap 1: Controleer of deze gebruiker al een project heeft met dezelfde naam als @ProjectNaam
		Zo ja geef een foutmelding.
Stap 2: Voer een insert uit op de Project tabel met @ProjectNaam
Stap 3: Voer een insert uit op de tabel ProjectBezetting met @ProjectNaam en @Username
Stap 4: Zet IS_EIGENAAR op 1 voor dit record

Wanneer het blijkt dat een user niet (meer) bestaat wordt er automatisch een error gegeven door MSSQL want er ligt een relatie tussen ProjectBezetting en Gebruiker.

**Concurency**

Binnen de stored procedure usp_CreateNewProjectForUser wordt er gecontroleerd of de gebruiker al een project heeft met deze naam. De controle gebeurd door een IF EXISTS wanneer er uit deze query een resultaat komt wordt er een foutmelding gegeven. 

Hierna zal er een insert plaatsvinden in de project tabel. Door het standaard isolation level te gebruiken worden de S-Locks vrijgegeven na het lezen in de IF EXISTS clause. Er bestaat dan een kans dat er na de IF EXISTS een insert plaats vindt op de Project en ProjectBezetting tabel. 

Deze situatie kan alleen voorkomen wanneer de zelfde gebruiker meerdere connecties heeft met het systeem en hij op het zelfde moment een nieuw project aanmaakt met dezelfde naam. Er is weinig kans dat deze situatie voorkomt en zal dus niet afgevangen worden door het isolation level te verhogen.

**Test cases**

Alle test cases die niet afgevangen worden door referentiële integriteit worden opgenomen.

**Test 1**

usp_CreateNewProjectForUser wordt uitgevoerd. Deze gebruiker heeft nog geen projecten met de nieuwe project naam. Geen foutmelding verwacht.

**Test 2**

usp_CreateNewProjectForUser wordt uitgevoerd. Deze gebruiker heeft al een project met de ingevoerde naam. Error wordt verwacht.

**Test 3**

usp_CreateNewProjectForUser wordt uitgevoerd. Deze gebruiker heeft nog geen projecten met de nieuwe project naam. Maar er bestaat al een project met deze naam in het systeem. Geen foutmelding verwacht.

**Update**
Alleen de eigenaar van een project mag de projectnaam veranderen. De nieuwe projectnaam moet uniek zijn voor deze gebruiker.

Beïnvloede tabellen: Project

Stap 1: Controleer of de @Username eigenaar is van dit project
Stap 2: Controleer of deze gebruiker al een project heeft met dezelfde naam als @NewProjectName
		Zo ja geef een foutmelding.
Stap 3: Update het record met als ID @ProjectID in de tabel Project naar @NewProjectName

**Concurency**

Binnen de stored procedure usp_UpdateProjectName wordt er gecontroleerd of de gebruiker al een project heeft met deze naam. De controlen gebeurd door een IF EXISTS wanneer er uit deze query een resultaat komt wordt er een foutmelding gegeven. 

Hierna zal er een update plaatsvinden in de project tabel. Door het standaard isolation level te gebruiken worden de S-Locks vrijgegeven na het lezen in de IF EXISTS clause. Er bestaat dan een kans dat er na de IF EXISTS een insert/update plaats vindt op de Project en ProjectBezetting tabel. 

Deze situatie kan alleen voorkomen wanneer de zelfde gebruiker meerdere connecties heeft met het systeem en hij een nieuw project aanmaakt met dezelfde naam als de nieuwe naam van het project. Of de gebruiker een ander van zijn eigen projecten update naar de zelfde naam als zijn anderen update. Er is weinig kans dat deze situatie voorkomt en zal dus niet afgevangen worden door het isolation level te verhogen.

**Test cases**

Alle test cases die niet afgevangen worden door referentiële integriteit worden opgenomen.

**Test 1**

usp_UpdateProjectName wordt uitgevoerd. De gebruiker update de projectnaam naar een naam die al bestaat waar hij geen eigenaar van is. Geen foutmelding verwacht.

**Test 2**

usp_UpdateProjectName wordt uitgevoerd. De gebruiker update de projectnaam naar een naam die al bestaat waar hij wel eigenaar van is. Foutmelding verwacht.

**Test 3**

usp_UpdateProjectName wordt uitgevoerd. De gebruiker update de projectnaam naar een naam die nog niet bestaat. Geen foutmelding verwacht.



#### Use-case gebruiker toevoegen aan project

Voor meer informatie over de functionaliteit van het gebruiker toevoegen aan een project aan zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Gebruiker toevoegen aan project.

##### Brief description

Een eigenaar van een project kan andere gebruikers toevoegen aan zijn project. Wanneer deze zijn toegevoegd aan het project kunnen ze meewerken aan het project.

##### Parameters

De SQL code voor deze use-case is te vinden in het volgende SQL bestand: "Stored Procedure toevoegen gebruiker aan project.sql"

De volgende parameters worden meegegeven aan de stored procedure;

@eigenaar, deze parameter heeft een datatype van varchar(20). Deze parameter representeert de gebruiker die de actie uitvoert. 

@toe_te_voegen_gebruiker, deze parameter heeft een datatype van varchar(20). Deze parameter representeert de gebruiker die aan het project wordt toegevoegd. 

@project_id, deze parameter heeft het datatype int. Deze parameter representeert het ID van het project waar de gebruiker aan toegevoegd wordt.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol toevoegen van een gebruiker aan een project:           

-PROJECTBEZETTING

##### Acties die plaats vinden

De volgende acties vinden plaats binnen de tabel PROJECTBEZETTING:
SELECT om te kijken of er een record is waar @toe_te_voegen_gebruiker al aan het project deelneemt met een project_id wat overeenkomt met @project_id.
SELECT om te kijken of de gebruiker met @eigenaar een record in projectbezetting heeft waar 'IS_EIGENAAR' = 1 geldt, en een project_id wat overeenkomt met @project_id.
INSERT om het nieuwe record te inserten.

##### Keuze voor constrainttype

Er is ervoor gekozen om gebruik te maken van een stored procedure bij deze use-case. 

Een groot voordeel van triggers is de mogelijkheid om gemakkelijk meerdere records te kunnen inserten. Dit valt hierdoor weg. Verder heeft deze use-case alleen invloed op het toevoegen van gebruikers aan het project. Voorkomen is beter dan genezen. Daarom is er voor een stored procedure gekozen.

##### Foutafhandeling

Naast het main-succes scenario zijn ook de volgende mogelijke fouten in acht genomen voor deze stored procedure:

1. Er is geen record in PROJECTBEZETTING waar IS_EIGENAAR = 1 voor de eigenaar.
2. Er is al een record voor de toe te voegen gebruiker in PROJECTBEZETTING in combinatie met het project.

##### Testcases

De volgende fouten zijn getest in de testcases:

**Test 1: Alles klopt.** 

Binnen deze test wordt het main-succes scenario getest. Alle informatie klopt hier, en de gebruiker 'LucianoTest' is succesvol aan het project toegevoegd. 

**Test 2: geen eigenaar van het project**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gegeven wanneer de 'eigenaar' niet daadwerkelijk een eigenaar van het project is.

**Test 3: de gebruiker neemt al deel aan het project**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gegeven wanneer de toe te voegen gebruiker al deelneemt aan het project.

##### Concurrency

Er vinden in deze transactie enkel SELECTS plaats, en op het einde één insert. Bij het standaard isolation level (READ COMMITED) kunnen bij al deze selects non-repeatable reads en  phantom reads plaatsvinden. 

Dit probleem vindt plaats omdat de S-locks van de select statements hun S-lock vrijgeven na het lezen van een record. Door het isolation level op REPEATABLE READ te zetten wordt het S-Lock vastgehouden tot het eind van de transactie. Op deze manier kan er geen non-repeatable read plaatsvinden.

Er kan dan nog wel een phantom read plaatsvinden. De kans dat dit gebeurt is echter heel klein. 

De kans dat er een fout plaatsvindt is zeer onwaarschijnlijk, aangezien een eigenaar in de meeste gevallen niet vanuit meerdere connecties op precies hetzelfde moment een gebruiker gaat proberen toe te voegen. Op basis van de bovenstaande informatie wordt er geadviseerd om gebruik te maken van isolation level REPEATABLE READ. 



#### Use-case benoemen entiteit

Voor meer informatie over de functionaliteit van het benoemen van een entiteit zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Benoemen entiteiten.

##### Brief description

Wanneer de gebruiker een verbalisatie aan het analyseren is kan hij ervoor kiezen om een entiteit te benoemen bij deze verbalisatie.

##### Parameters

​    @project_id int: Het id van het project waar de gebruiker mee bezig is.

​	@verbalisatie_id int: Het id van de verbalisatie die de gebruiker aan het analyseren is.

​	@gebruikersnaam varchar(20): De naam van de gebruiker die de entiteit benoemt.

​	@entiteit_naam varchar(50): De naam van de nieuwe entiteit die de gebruiker heeft benoemd.

​	@is_match bit: Een boolean waarin aangegeven wordt of de benoemde entiteit MATCH is. true is MATCH. false is niet MATCH.

##### Test cases

**Test 1: Alles klopt**

​	Hier wordt gekeken of alles onder normale omstandigheden correct geinsert wordt. Geen foutmelding verwacht.

**Test 2: Gebruiker niet in project**

​	Hier wordt gekeken of een gebruiker die niet in het huidige project zit niet een entiteit kan benoemen. Foutmelding verwacht

**Test 3: Zelfde naam entiteit binnen verbalisatie**

​	Hier wordt getest dat er geen entiteit met dezelfde naam in een verbalisatie geinsert kan worden. Foutmelding verwacht.

**Test 4: MATCH zonder andere ID**

​	Hier wordt getest dat wanneer de "is match" parameter true is, dat deze ook echt naar een attribuut verwijst. Foutmelding verwacht.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol benoemen van een entiteit bij een project:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Attribuut_In_Verbalisatie
-Projectbezetting

##### Acties die plaats vinden

Acties binnen Entiteit_In_Project:
-Als de MATCH checkbox is aangevinkt een select doen om te kijken of er een ander entiteit is met een ID binnen dit project.
-Als de MATCH checkbox niet is aangevinkt, insert de nieuwe entiteit anders geen acties.

Acties binnen Entiteit_In_Verbalisatie:
-Select om te kijken of er al een entiteit is binnen de geselecteerde verbalisatie met dezelfde naam.

-Insert de nieuwe entiteit bij de geselecteerde verbalisatie.

##### Keuze voor constrainttype

Er is ervoor gekozen om gebruik te maken van een stored procedure bij deze use-case. 
Dit omdat de handeling makkelijk aangeroepen kan worden.

##### Foutafhandeling

De volgende fouten kunnen optreden bij deze usecase:
1. Er is een andere entiteit met dezelfde naam binnen de huidige verbalisatie.
2. De MATCH checkbox is aangevinkt maar er is geen andere entiteit met dezelfde naam en een ID.

##### Concurrency

Er vinden in deze transactie een paar SELECTS uitgevoerd om bepaalde situaties af te vangen. Er is is geen isolation level gezet dus de standaard zal READ COMMITED zijn. Dit kan Phantom reads veroorzaken.

Als een Phantom read voorkomt zorgt dit ervoor dat er een record verkeerd toegevoegd zal worden. Omdat er iets geinstert zal worden terwijl dit eigenlijk niet meer kan. Om dit te voorkomen zal er het isolation level om REPEATABLE READ gezet kunnen worden. Hierdoor zullen de S-locks de gehele transactie stand houden en zal het akkoord ten alle tijden kloppen.

Echter is de kans dat het bovenstaande in dit project voorkomt heel erg klein. Daarom is er voor gekozen om het niet te implementeren. 

#### Aanpassen entiteit

##### Brief description

De gebruiker kan tijdens de analyse ervoor kiezen om een entiteit aan te passen.

##### Parameters

​	@project_id int: Het id van het project waar de gebruiker mee bezig is.

​	@verbalisatie_id int: Het id van de verbalisatie die de gebruiker aan het analyseren is.

​	@gebruikersnaam varchar(20): De naam van de gebruiker die de entiteit benoemt.

​	@entiteit_id int: Het id van de entiteit die aangepast wordt. 

​	@entiteit_naam varchar(50): De nieuwe of oude naam voor het entiteit.

​	@is_match bit: Een boolean waarin aangegeven wordt of de benoemde entiteit MATCH is. true is MATCH. false is niet MATCH.

##### Test cases

**Test 1: Gebruiker niet in project**

​	Hier wordt getest of dat een gebruiker niet iets kan aanpassen terwijl hij niet een project lid is. Foutmelding verwacht.

**Test 2: Er zijn geen aanpassingen gemaakt**

​	Hier wordt getest of dat een gebruiker of er een error gegooid wordt als er niks aangepast is. Foutmelding verwacht.

**Test 3: Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.**

​	Hier wordt getest of er geen entiteit met dezelfde naam als een andere entiteit binnen de verbalisatie geinsert kan worden. Foutmelding verwacht.

**Test 4: De benoemde entiteit heeft geen primary identifier bij het aanvinken van is_match.**

​	Hier wordt getest als de is match aangevinkt is dat deze ook daadwerkelijk naar een attribuut verwijst. Foutmelding verwacht.

**Test 5: Deze entiteit is alleen berschreven in deze verbalisatie en er zijn geen andere entiteiten met dezelfde nieuwe naam.**

​	Hier wordt getest dat een aanpassing aan de naam alleen de naam van het entiteit aangepast onder de bovenstaande omstandigheden. Geen foutmelding verwacht.

**Test 6: Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die al bestaat.**

​	Hier wordt getest of alle aanpassingen correct gedaan worden over meerdere tabellen onder de bovenstaande omstandigheden. Geen foutmelding verwacht.

**Test 7: Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die nog niet bestaat.**

​	Hier wordt getest of alle aanpassingen correct gedaan worden over meerdere tabellen onder de bovenstaande omstandigheden. Anders hier is dat er een nieuwe entiteit aangepast wordt in plaats van veranderen naar een bestaande entiteit. Geen foutmelding verwacht.

**Test 8: De naam wordt aangepast en er is een rol gekoppeld aan het veranderde entiteit binnen de verbalisatie.**

​	Hier wordt getest of rollen correct aangepast worden wanneer dit nodig is. In dit geval is dit het entiteit id van de rollen. Geen foutmelding verwacht.

**Test 9: De naam wordt aangepast en er is een attribuut gekoppend aan het veranderde entiteit binnen de verbalisatie.**

###### 	Hier wordt getest of attributen correct aangepast wordt wanneer dit nodig is. In dit geval is dit het entiteit id van het attribuut. Geen foutmelding verwacht.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol aanpassen van een entiteit bij een verbalisatie:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Projectbezetting

##### Acties die plaats vinden

Acties binnen Entiteit_In_Project:
-Als de naam aangepast is een select doen om te kijken of de naam al bestaat.

Acties binnen Entiteit_In_Verbalisatie:
-Select om te kijken of er al een entiteit is binnen de geselecteerde verbalisatie met dezelfde naam.

-Als de checkbox aangevinkt is wanneer dat niet zo was moet gekeken worden of er al een entiteit bestaat met een ID.

##### Foutafhandeling

De volgende fouten kunnen optreden bij deze usecase:
1. Gebruiker is niet deel van het huidige project.
2. Er is niks aangepast.
3. Er is een andere entiteit met dezelfde naam binnen de huidige verbalisatie.
4. De MATCH checkbox is aangevinkt maar er is geen andere entiteit met dezelfde naam en een ID.

##### Concurrency

Ook hier worden veel SELECT statements uitgevoerd en zal een isolation level REPEATABLE READ ingevoerd kunnen worden. Daarnaast worden er ook een aantal UPDATES uitgevoerd op meerdere tabellen. Hier kan voorkomen dat een bepaalde record verwijderd kan worden terwijl een andere tabel en naar gaat refereren. Om dit te voorkomen kan de isolation level naar SERIALIZABLE gezet worden om dit te voorkomen.

Echter is de kans dat het bovenstaande in dit project voorkomt heel erg klein. Daarom is er voor gekozen om het niet te implementeren. 

#### Verwijderen entiteit

##### Brief description

Wanneer de gebruiker een verbalisatie aan het analyseren is kan hij ervoor kiezen om een entiteit te verwijderen bij deze verbalisatie.

##### Parameters

​	@project_id int: Het id van het project waar de gebruiker mee bezig is.

​	@verbalisatie_id int: Het id van de verbalisatie die de gebruiker aan het analyseren is.

​	@gebruikersnaam varchar(20): De naam van de gebruiker die de entiteit benoemt.

​	@entiteit_id int: Het id van de entiteit die verwijderd wordt.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol verwijderen van een entiteit bij een verbalisatie:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Attribuut_In_Verbalisatie
-Relatie
-Rol

##### Acties die plaats vinden

Acties binnen Entiteit_In_Project:
-Als deze entiteit alleen in de huidige verbalisatie beschreven is moet deze verwijderd worden.

Acties binnen Entiteit_In_Verbalisatie:
-Verwijder de row met de entiteit met binnen de huidige verbalisatie.

Acties binnen Attribuut_In_Verbalisatie
-Als aanwezig, moet de attribuut verwijdert worden bij de entiteit, bij de huidige verbalisatie

Acties binnen Rol
-Als aanwezig, moeten rollen verwijdert worden bij de entiteit, bij de huidige verbalisatie

##### Keuze voor constrainttype

Er is ervoor gekozen om gebruik te maken van een stored procedure bij deze use-case. 

Dit omdat de handeling makkelijk aangeroepen kan worden. 

##### Foutafhandeling

De volgende fouten kunnen optreden bij deze usecase:

1. Gebruiker is niet deel van het huidige project.

##### Test cases

**Test 1: Gebuiker niet in project**

​	Hier wordt getest of een gebruiker, die niet deel is van het project, geweigerd wordt. Foutmelding verwacht. 

**Test 2: Er zijn geen andere verbalisaties met dezelfde entiteit.**

​	Hier wordt getest of het entiteit in ENTITEIT_IN_PROJECT ook correct verwijderd wordt als er geen andere verbalisatie zijn met de betreffende entiteit. Geen foutmelding verwacht.

**Test 3: Alles wordt verwijdert (Ook rol en attribuut)**

​	Hier wordt getest of alle gerelateerde objecten correct verwijderd wordt.

##### Concurrency

Bij het deleten van een entiteit worden er bijna alleen maar DELETES uitgevoerd, naast één SELECT. Om te voorkomen dat er iets verwijdert wordt door een fout getimed SELECT kan de isolation level naar REPEATABLE READ gezet worden. Daarnaast kan er bij het verwijderen van andere records niks fout gaan.

Echter is de kans dat het bovenstaande in dit project voorkomt heel erg klein. Daarom is er voor gekozen om het niet te implementeren. 

#### Overige functionele eis: business rule toevoegen

Voor meer informatie over de functionaliteit van het benoemen van een relaties zie: Functioneel ontwerp, Hoofdstuk: Overige functionele eisen, Benoemen business rule.

Gebruikers kunnen een business rule toevoegen aan een project. Na het aanmaken van een business rule, kan de gebruiker deze bekijken.

Tabellen die beinvloed worden door de use case business rule toevoegen:

- BUSINESS_RULE: Bij het aanmaken van een business rule, wordt er een nieuw record in de tabel BUSINESS_RULE toegevoegd. Hierbij wordt de gebruiker geregistreerd, zodat de eigenaar van het project alle informatie terug kan vinden op de geschiedenispagina.
- hist_BUSINESS_RULE: Wanneer er een actie in de tabel BUSINESS_RULE plaats vindt, zullen deze veranderde waarden door een trigger in de history tabel van BUSINESS_RULE gezet worden.

Relaties die meldingen kunnen geven:
Wanneer het blijkt dat een user niet (meer) bestaat, wordt er een error getoond door MSSQL. Dit is het gevolg van het feit dat er een relatie is tussen de kolom GEBRUIKERSNAAM in de tabellen BUSINESS_RULE en GEBRUIKER. Daarnaast zal er een error worden weergegeven wanneer er blijkt dat het project niet (meer) bestaat in de tabel PROJECT. De oorzaak hiervan is eveneens een relatie, welke in dit geval ligt tussen de tabellen BUSINESS_RULE en PROJECT.

##### INSERT:

Acties om de stored procedure goed te laten verlopen:

- Controleer of de gebruiker lid is van het project met @PROJECT_ID. Deze informatie wordt uit de tabel PROJECTBEZETTING gehaald.
- Voer een insert statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:

- De gebruiker is geen lid van het project.
- Een business rule bestaat al binnen het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters.

Parameters:

| Parameternaam         | Datatype     | Omschrijving                                                 |
| --------------------- | ------------ | ------------------------------------------------------------ |
| @ProjectID            | int          | Bevat de waarde van het ID van het project waarbinnen de businessrule aan wordt toegevoegd. |
| @BusinessRuleSentence | varchar(300) | Bevat de business rule die toegevoegd moet worden aan een project |
| @userName             | varchar(20)  | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |

Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 

Tests:

- Test scenario 1
  Business rule toevoegen aan een project welke nog niet bestaat.
  Foutmelding: geen foutmelding verwacht
- Test scenario 2
  Business rule toevoegen door een user die niet werkt aan het project .
  Foutmelding: 'De gebruiker is niet lid van het project.'

##### UPDATE:

Acties om de stored procedure goed te laten verlopen:

- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer een update statement uit welke de waarde van het record verandert naar de nieuwe meegegeven waarden.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:

- De gebruiker is geen lid van het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters
- De waarde van de business rule zin bestaat al binnen het project.
- Een gebruiker kan een business rule aanpassen welke niet aangemaakt is door deze gebruiker. Hierbij zal de laaste gebruiker geregistreerd worden in de tabel.

Parameters:

| Parameternaam                 | Datatype     | Omschrijving                                                 |
| ----------------------------- | ------------ | ------------------------------------------------------------ |
| @ProjectID                    | int          | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @BusinessRuleSentence         | varchar(300) | Bevat de business rule die toegevoegd moet worden aan een project |
| @userName                     | varchar(20)  | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |
| @BusinessRuleSentenceOriginal | varchar(300) | Bevat de business rule die aangepast moet worden naar de veranderde business rule |
| @userNameOriginal             | varchar(20)  | Bevat de waarde van de gebruikersnaam van de gebruiker die de originele business rule toegevoegd heeft |

Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 

Tests:

- Test scenario 4
  Business rule updaten door een user die werkt aan het project.
  Foutmelding: geen foutmelding verwacht
- Test scenario 5
  Business rule updaten naar een business rule die al aangemaakt is door deze user binnen dit project.
  Foutmelding: 'De gebruiker heeft reeds een business rule aangemaakt met deze zin.' 
- Test scenario 6
  Business rule toevoegen door een user die niet werkt aan het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'

##### DELETE:

Acties om de stored procedure goed te laten verlopen:

- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer het delete statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:

- De gebruiker is geen lid van het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters
- Een gebruiker kan een business rule aanpassen welke niet aangemaakt is door deze gebruiker. Hierbij zal de laaste gebruiker geregistreerd worden in de tabel.

Parameters:

| Parameternaam         | Datatype     | Omschrijving                                                 |
| --------------------- | ------------ | ------------------------------------------------------------ |
| @ProjectID            | int          | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @BusinessRuleSentence | varchar(300) | Bevat de business rule die toegevoegd moet worden aan een project |
| @userName             | varchar(20)  | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |

Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 

Tests:

- Test scenario 7
  Business rule verwijderen.
  Foutmelding: geen foutmelding verwacht
- Test scenario 8
  Business rule verwijderen door een user die niet werkt aan het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'



#### Use-case Benoemen relatie

Voor meer informatie over de functionaliteit van het benoemen van een relaties zie: Functioneel ontwerp, Hoofdstuk: Fully dressed use cases, Use case: Benoemen relaties.

##### Brief description

Deelnemers van een project hebben de mogelijkheid om een relatie te benoemen in een verbalisatie. Een relatie bestaat uit twee verschillende rollen. Beide rollen bestaan uit een 'entiteit van' en een 'entiteit naar'. Een voorbeeld hiervan is de rol van 'Fiets' naar 'Post'. Op deze manier kan de samenhang tussen entiteiten aangegeven worden.

##### Parameters

De SQL code voor deze use-case is te vinden in het volgende SQL bestand: "stored procedure benoemen relatie.sql". 

De volgende parameters worden meegegeven aan de stored procedure usp_benoemenRelatieInVerbalisatie;

@relatienaam, deze parameter heeft een datatype van varchar(50). Dit is de naam die de gebruiker aan de relatie geeft.

@toevoegende_gebruiker deze parameter heeft een datatype van varchar(20). Dit is de gebruiker die de relatie toevoegt. 

@verbalisatie_id, dit is een int. Dit is het ID van de verbalisatie waarin de relatie is geïdentificeerd.

@entiteit_van, dit is een int. Dit is het ID van de eerste entiteit in de relatie.

@entiteit_naar, dit is een int. Dit is het ID van de tweede entiteit in de relatie.

@dependent_rol_1, dit is een bit. Dit geeft aan of de eerste rol van deze relatie dependent is of niet.

@dependent_rol_2, dit is een bit. Dit geeft aan of de tweede rol van deze relatie dependent is of niet.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol toevoegen van een gebruiker aan een project:

- ROL
- RELATIE
- ENTITEIT_IN_VERBALISATIE
- VERBALISATIE
- PROJECTBEZETTING

##### Acties die plaats vinden

De volgende acties vinden plaats binnen de tabel ROL:

INSERT voor beide rollen van de relatie.



De volgende acties vinden plaats binnen de tabel PROJECTBEZETTING:

SELECT om te kijken of de toevoegende gebruiker aan het project deelneemt.



De volgende acties vinden plaats binnen de tabel VERBALISATIE:

SELECT om het project_id van de verbalisatie te krijgen.



De volgende acties vinden plaats binnen de tabel RELATIE:

SELECT om te kijken of er al een relatie met die naam binnen het project bestaat.

SELECT om te kijken of er 2 of meer relaties in kaart zijn gebracht bij deze verbalisatie.

INSERT van de relatie.



De volgende acties vinden plaats binnen de tabel ENTITEIT_IN_VERBALISATIE:

SELECT om te kijken of de @entiteit_van in de verbalisatie is gevonden.

SELECT om te kijken of de @entiteit_naar in de verbalisatie is gevonden.

##### Keuze voor constrainttype

Binnen het datasysteem is ervoor gekozen om de gebruikers enkel bepaalde acties te laten voldoen. Hierom is besloten om stored procedures te maken, waardoor de gebruiker enkel een bepaald aantal acties kan ondernemen, die vooraf zijn opgesteld.

Verder is voorkomen beter dan genezen. Hierdoor wordt er gebruik gemaakt van een stored procedure.

##### Foutafhandeling

De volgende mogelijke fouten zijn getest in deze stored procedure:

1. Zowel rol 1 als rol 2 zijn dependent
2. De toevoegende gebruiker neemt geen deel aan het project
3. Er bestaat al een relatie met dezelfde naam binnen dit project
4. Er zijn binnen de verbalisatie al 2 of meer relaties in kaart gebracht
5. De eerste entiteit is niet in de verbalisatie geïdentificeerd
6. De tweede entiteit is niet in de verbalisatie geïdentificeerd

##### Testcases

**Test 1: Beide rollen zijn dependent**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer beide rollen als dependent zijn aangegeven.

**Test 2: De gebruiker neemt geen deel aan het project**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de toevoegende gebruiker niet aan het project deelneemt.

**Test 3: Er bestaat al een relatie in het project met die naam**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer er een relatie wordt toegevoegd met een naam die al binnen het project bestaat.

**Test 4: Er zijn al twee relaties geidentificeerd**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer er al twee relaties in een verbalisatie zijn geïdentificeerd.

**Test 5: De eerste entiteit bestaat niet in de verbalisatie**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de eerste entiteit nog niet in de verbalisatie in kaart is gebracht.

**Test 6: De tweede entiteit bestaat niet in de verbalisatie**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de tweede entiteit nog niet in de verbalisatie in kaart is gebracht.

**Test 7: De eerste entiteit is dependent**

Binnen deze test wordt er gekeken of de records succesvol worden geïnsert wanneer alles goed is en de eerste entiteit als dependent is aangegeven.

**Test 8: De tweede entiteit is dependent**

Binnen deze test wordt er gekeken of de records succesvol worden geïnsert wanneer alles goed is en de tweede entiteit als dependent is aangegeven.

**Test 9: Geen entiteit is dependent**

Binnen deze test wordt er gekeken of de records succesvol worden geïnsert wanneer alles goed is en geen entiteit als dependent is aangegeven.

##### Concurrency

Binnen deze stored procedure zijn er een aantal problemen die voor kunnen komen wat betreft concurrency. Op het standaard isolation level (READ COMMITED) kan het voorkomen dat een gebruiker uit een project wordt verwijdert nadat de check heeft plaatsgevonden of hij in het project zit. 

Ook kan het voorkomen dat een entiteit uit een verbalisatie wordt verwijdert nadat de check heeft plaatsgevonden. Op deze manier kunnen er entiteiten in een relatie staan die hier niet behoren te staan.

De bovenstaande problemen zijn op te lossen door het isolation level REPEATABLE READ te gebruiken bij de transactie. De kans dat de bovenstaande problemen optreden is echter niet heel groot. Ook is het risico niet groot, aangezien het heel gemakkelijk op te lossen is.

De kans dat er phantom reads optreden bestaat nog wel. Het kan bijvoorbeeld zo zijn dat er een relatie wordt toegevoegd na de check om te kijken of er 2 of meer relaties in de verbalisatie staan. Hierdoor kan het voorkomen dat er 3+ relaties bij een verbalisatie zijn geïdentificeerd. 

Dit is op te lossen door gebruik te maken van isolation level SERIALIZABLE. De kans dat dit gebeurt is echter niet heel groot. Ook is het risico dat dit meedraagt niet groot. Om dit probleem op te lossen is het enige wat je moet doen 1 relatie verwijderen.

Op basis van de bovenstaande informatie wordt er geadviseerd om gebruik te maken van isolation level READ COMMITED.

##### Uitbreiden met powerdesigner informatie

##### Brief description

Bij een relatie binnen powerdesigner heb je meer informatie nodig om de relatie te beschrijven dan binnen de analyse van een verbalisatie. Hierbij komen een aantal (mogelijke) velden kijken zoals het cardinaliteitstype, cardinaliteit_minimum, cardinaliteit_maximum, mandatory en is_dominant.
Sommige velden zijn enkel toegestaan bij bepaalde waarden van andere velden. 
Een voorbeeld hiervan is dat 'is_dominant' alleen ingevuld mag zijn als het cardinaliteitstype ONE - ONE is.

##### Parameters

De SQL code voor deze use-case is te vinden in het volgende SQL bestand: "stored procedure benoemen relatie.sql". 

De volgende parameters worden meegegeven aan de stored procedure usp_relatieUitbreidenMetPowerDesignerGegevens;

@relatienaam, deze parameter heeft een datatype van varchar(50). Dit is de naam die de gebruiker aan de relatie geeft.

@toevoegende_gebruiker deze parameter heeft een datatype van varchar(20). Dit is de gebruiker die de relatie toevoegt. 

@verbalisatie_id, dit is een int. Dit is het ID van de verbalisatie waarin de relatie is geïdentificeerd.

@entiteit_van, dit is een int. Dit is het ID van de eerste entiteit in de relatie.

@entiteit_naar, dit is een int. Dit is het ID van de tweede entiteit in de relatie.

@dependent_rol_1, dit is een bit. Dit geeft aan of de eerste rol van deze relatie dependent is of niet.

@dependent_rol_2, dit is een bit. Dit geeft aan of de tweede rol van deze relatie dependent is of niet.	

@cardinaliteitstype, dit is een varchar(10). Dit geeft aan wat voor cardinaliteitstype er bij deze relatie hoort. Hier zijn enkel de volgende waarden mogelijk; 'ONE - MANY', 'MANY - ONE', 'MANY - MANY', 'ONE - ONE' en null.

@cardinaliteit_minimum_1, deze parameter heeft een datatype van varchar(6). Dit geeft aan wat de minimum cardinaliteit is voor de eerste rol (bijv. bij een cardinaliteit van 1..8 is 1 de minimum cardinaliteit).

@cardinaliteit_minimum_2, deze parameter heeft een datatype van varchar(6). Dit geeft aan wat de minimum cardinaliteit is voor de tweede rol.

@cardinaliteit_maximum_1, deze parameter heeft een datatype van varchar(6). Deze parameter geeft aan wat de maximum cardinaliteit is voor de eerste rol (bijv. bij een cardinaliteit van 1..8 is 8 de maximum cardinaliteit).

@cardinaliteit_maximum_2, deze parameter heeft een datatype van varchar(6). Deze parameter geeft aan wat de maximum cardinaliteit is voor de tweede rol.

@mandatory_1, deze parameter is een bit. Deze parameter geeft aan of de eerste rol mandatory is. 

@mandatory_2, deze parameter is een bit. Deze parameter geeft aan of de tweede rol mandatory is. 

@is_dominant_1, deze parameter is een bit. Deze parameter geeft aan of de eerste rol dominant is.

@is_dominant_2, deze parameter is een bit. Deze parameter geeft aan of de tweede rol dominant is.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol toevoegen van een gebruiker aan een project:

- ROL
- RELATIE

##### Acties die plaats vinden

Nadat alle checks hebben plaatsgevonden in deze stored procedure, en er geen foutmeldingen zijn gethrowed wordt de stored procedure 'usp_benoemenRelatieInVerbalisatie' uitgevoerd. 

De volgende acties vinden plaats binnen de tabel ROL:

Updaten van de rollen die bij de relatie horen.



De volgende acties vinden plaats binnen de tabel RELATIE:

Update van de relatie om het cardinaliteitstype toe te voegen.

##### Keuze voor constrainttype

De volgende check-constraints zijn geschreven:

**MandatoryRolesRequire1MinimumCardinality**

Deze constraint is van toepassing op de tabel ROL. Het kan niet voorkomen dat er een situatie is waar mandatory = 1 en cardinaliteit_minimum = 0 OF mandatory = 0 en cardinaliteit_minimum >= 1. 

**ValuesOfCardinaliteitsType**

Deze constraint is van toepassing op de tabel RELATIE. Enkel de volgende waarden zijn mogelijk bij cardinaliteitstype: 'ONE - MANY', 'MANY - ONE', 'ONE - ONE', 'MANY - MANY' of null.

**IfRolIsDependentItIsAlsoMandatory**

Deze constraint is van toepassing op de tabel ROL. Het kan hierdoor niet voorkomen dat bij een record geld dat dependent = 1 en mandatory = 0.

**MinimumCardinalityCantBeLowerThan0**

Deze constraint is van toepassing op de tabel ROL. Hierdoor is het niet mogelijk dat cardinaliteit_minimum kleiner is dan 0.

**MaximumCardinalityCantBeLowerThan0**

Deze constraint is van toepassing op de tabel ROL. Hierdoor is het niet mogelijk dat cardinaliteit_maximum kleiner is dan 1.

**MinimumCardinalityCantBeBiggerThanMaximumCardinality**

Deze constraint is van toepassing op de tabel ROL. Hierdoor is het niet mogelijk dat de cardinaliteit_maximum kleiner is dan de cardinaliteit_minimum.

**Stored procedures**

Voor het waarborgen van deze constraints is besloten om gebruik te maken van stored procedures. De reden hiervoor is dat de gebruikers op deze manier enkel toegang krijgen tot specifieke voorgeprogrameerde acties. Op deze manier zorgen wij ervoor dat enkel de nodige data in de database komt. 

Verder is het beter om te voorkomen dan te genezen.

##### Foutafhandeling

De volgende mogelijke fouten zijn getest in deze stored procedure:

1. Rol 1 of rol 2 heeft de waarde null bij een 'ONE - ONE' cardinaliteitstype.
2. Bij een relatie met het type 'ONE - ONE' is de maximum cardinaliteit altijd 1.
3. Bij een relatie met het type 'ONE - MANY' mag de primaire rol niet dependent zijn.
4. Bij een relatie met het type 'MANY - ONE' mag de secundaire rol niet dependent zijn.
5. Bij een relatie met het type 'ONE - MANY' moet de maximale cardinaliteit van de secundaire rol 1 zijn.
6. Bij een relatie met het type 'MANY - ONE' moet de maximale cardinaliteit van de primaire rol 1 zijn. 

##### Testcases

**Test 1: Successcenario waarbij de eerste entiteit dependend is**

Binnen deze test wordt er gekeken of de stored procedure succesvol de gegevens opslaat in de database.

**Test 2: ONE - ONE relatie waar bij is_dominant null is voor entiteit 1**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer is_dominant niet ingevuld is voor rol 1 wanneer het cardinaliteitstype 'ONE - ONE' is.

**Test 3: ONE - ONE relatie waar bij is_dominant null is voor entiteit 2**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer is_dominant niet ingevuld is voor rol 2 wanneer het cardinaliteitstype 'ONE - ONE' is.

**Test 4: ONE - ONE relatie waar bij is_dominant null is voor entiteit 1 en 2**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer is_dominant niet ingevuld is voor rol 1 en rol 2 wanneer het cardinaliteitstype 'ONE - ONE' is.

**Test 5: ONE - ONE relatie waarbij de maximum cardinaliteit van rol 2 groter is dan 1.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de tweede rol een cardinaliteit groter dan 1 heeft bij een relatie met het cardinaliteitstype 'ONE - ONE'.

**Test 6: ONE - ONE Relatie waar de maximum cardinaliteit van rol 1 groter is dan 1.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de tweede rol een cardinaliteit groter dan 1 heeft bij een relatie met het cardinaliteitstype 'ONE - ONE'

**Test 7: ONE - ONE relatie waarbij de maximum cardinaliteit van rol 1 en 2 groter is dan 1.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de eerste rol en de tweede rol een cardinaliteit groter dan 1 hebben bij een relatie met het cardinaliteitstype 'ONE - ONE'

**Test 8: ONE - MANY Relatie waar de primaire rol dependent is.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de primaire rol als dependent is aangegeven wanneer het cardinaliteitstype 'ONE - MANY' is.

**Test 9: MANY - ONE Relatie waar de secundaire rol dependent is.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de secundaire rol als dependent is aangegeven wanneer het cardinaliteitstype 'MANY - ONE' is.

**Test 10: ONE - MANY Relatie waar de maximum cardinaliteit van de secundaire rol groter is dan 1**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de secundaire rol een maximale cardinaliteit groter dan 1 heeft.

**Test 11: MANY - ONE relatie waar de maximale cardinaliteit van de primaire rol groter is dan 1**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de primaire rol een maximale cardinaliteit groter dan 1 heeft.

**Test 12: Er wordt een onjuiste waarde voor cardinaliteitstype in de relatie tabel gezet**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer er een cardinaliteitstype wordt ingevoerd wat niet 'ONE - ONE', 'MANY - ONE', 'ONE - MANY', 'MANY - MANY' of null is.

**Test 13: Een mandatory role heeft een minimum cardinaliteit van 0**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer een rol mandatory is en een minimum cardinaliteit van 0 heeft.

**Test 14: Een niet mandatory role heeft een minimum cardinaliteit van 1 of groter**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer een rol niet mandatory is, en een minimum cardinaliteit van 1 of groter heeft.

**Test 15: Een dependent rol is niet mandatory**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer een rol dependent is maar niet mandatory

**Test 16: Een minimale cardinaliteit mag niet kleiner zijn dan 0**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de minimale cardinaliteit kleiner is dan 0.

**Test 17: Maximale cardinaliteit mag niet kleiner dan 1 zijn.**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de maximale cardinaliteit kleiner is dan 1.

**Test 18: Minimale cardinaliteit mag niet groter zijn dan de maximale cardinaliteit**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gethrowed wanneer de maximale cardinaliteit kleiner is dan de minimale cardinaliteit.

##### Concurrency

Binnen deze stored procedure kunnen er geen problemen plaatsvinden met concurrency (afgezien van de problemen van voorgaande stored procedure). De reden hiervoor is dat alle controles worden gedaan aan de hand van parameters die worden meegegeven. Hierdoor kunnen er geen concurrency fouten optreden.



### Overige functionele eis: invoeren subverbalisatie

Voor meer informatie over de functionaliteit van het invoeren van een subverbalisatie zie: Functioneel ontwerp, Hoofdstuk: Overige functionele eisen, Eis: Invoeren subverbalisatie.

Bij het invoeren van een subverbalisaties mag een subverbalisatie niet hetzelfde mag zijn als de parent verbalisatie. 

Verbalisaties hebben subverbalisaties waar de zin het zelfde is maar de invulplek van de variabelen anders is.Deze subverbalisaties worden opgelsagen in een aparte tabel. Subverbalisaties mogen niet hetzelfde zijn als de parent verbalisatie of het zelfde zijn als een andere subverbalisatie bij deze parent verbalisatie.

Bij het invoeren van een subverbalisaties mag een subverbalisatie niet hetzelfde zijn als de parent verbalisatie. Verder moet Verbalisatie_ID bestaan in de verbalisatie tabel maar dit wordt afgevangen door de relatie tussen deze twee entiteiten.

**CREATE**

Beïnvloede tabellen: Verbalisatie, SubVerbalisatie

Stored procedure: usp_CreateNewSubVerbalisation

Parameters: @VerbalisatieID    INT, 
                       @UserName     VARCHAR(20), 
                       @SubVerbalisatie VARCHAR(500)

Parameter betekenis:

**@VerbalisatieID**

VerbalisatieID van de parent verbalisatie waar de subverbalisatie aan verbonden wordt.

**@UserName**

Gebruikersnaam van de gebruiker die de verandering doet. Dit is relevant voor de history.

**@SubVerbalisatie**

De zin voor de nieuwe subverbalisatie.

Stappen

1. Controleer of de gebruiker in het bijbehorende project zit, indien dit niet het geval is,  geef een foutmelding.
2. Controleer of de ingevoerde subverbalisatie het zelfde is als de parent verbalisatie. 
   Als dit het geval is geef een foutmelding.
3. Controleer of de ingevoerde subverbalisatie bestaat bij deze parent verbalisatie.
   Als dit het geval is geef een foutmelding.
4. Indien geen errors, wordt de subverbalisatie geïnsert.

**UPDATE**
Beïnvloede tabellen: Verbalisatie, SubVerbalisatie

Stored procedure: usp_UpdateSubVerbalisation

Parameters: @VerbalisatieID				INT, 
                       @UserName			VARCHAR(20), 
                       @NewSubVerbalisatie VARCHAR(500),
		       @OldSubVerbalisatie VARCHAR(500)

Parameter betekenis:

**@VerbalisatieID**

VerbalisatieID van de parent verbalisatie waar de subverbalisatie aan verbonden wordt.

**@UserName**

Gebruikersnaam van de gebruiker die de verandering doet. Dit is relevant voor de history.

**@NewSubVerbalisatie**

De zin voor de nieuwe subverbalisatie.

**@OldSubVerbalisatie**

Oude subverbalisatie zin die vervangen wordt met de @NewSubVerbalisatie.

Stappen

1. Controleer of de gebruiker in het bijbehorende project zit, indien dit niet het geval is,  geef een foutmelding.
2. Controleer of de ingevoerde subverbalisatie het zelfde is als de parent verbalisatie. 
   Als dit het geval is geef een foutmelding.
3. Controleer of de ingevoerde subverbalisatie bestaat bij deze parent verbalisatie.
   Als dit het geval is geef een foutmelding.
4. Indien geen errors, wordt de subverbalisatie geüpdate.

#### Tests

**Test scenario 1**
Subverbalisaties bij een verbalisatie moeten uniek zijn en mogen niet hetzelfde zijn als de hoofd verbalisatie.
Fout situatie waar een subverbalisatie hetzelfde is als de parent verbalisatie.

**Test scenario 2**
Subverbalisaties bij een verbalisatie moeten uniek zijn en mogen niet het zelfde zijn als de parent verbalisatie.
Goed situatie waar de subverbalisatie uniek is en niet het zelfde is als de parent.

**Test scenario 3**
SubVerbalisaties bij een parent verbalisatie moeten uniek zijn.
Fout situatie een subverbalisatie bij een parent wordt aangepast naar een bestaande subverbalisatie bij deze parent.

**Test scenario 4**
SubVerbalisaties mogen na een update niet hetzelfde zijn als de parent.
Fout situatie waar na de update een subverbalisatie hetzelfde wordt als de parent verbalisatie.

**Test scenario 5**
SubVerbalisaties bij een parent verbalisatie moeten uniek zijn.
Goed situatie waar na de update de subverbalisatie uniek is bij deze parent en hij niet het zelfde is als de parent.

#### Concurency

**CREATE**

Bij de create stored procedure wordt er doormiddel van een IF EXISTS gecontroleerd of een verbalisatie al bestaat bij dit project. Wanneer dit niet het geval is wordt er een insert gedaan.

Door het standaard isolation level blijven S-locks tot het einde van een read. Hierdoor is het mogelijk dat een anderen insert tussen de IF EXISTS en insert kan komen. Wanneer dit gebeurd is het mogelijk dat de businessrule overtreden wordt. Dit kan opgelost worden door het isolation level naar repeatable read te zetten. Hierdoor blijven de S-locks staan tot het einde van de transactie. Doordat er een S-Lock aanwezig blijft is het niet meer mogelijk een nieuw record te inserted tussentijds.

**UPDATE**

Voor het updaten geldt het zelfde als bij het creëren.



#### Subtype entiteit aanmaken

Gebruikers kunnen een subtype entiteit toevoegen aan een project en hier attributen aan koppelen. Na het aanmaken van deze entiteiten en attributen, kan de gebruiker deze bekijken.



Relaties die meldingen kunnen geven:
Wanneer het blijkt dat een user niet (meer) bestaat, wordt er een error getoond door MSSQL. Dit is het gevolg van het feit dat er een relatie is tussen de kolom GEBRUIKERSNAAM in de tabellen ENTITEIT_IN_PROJECT en GEBRUIKER, en ATTRIBUUT_IN_SUBTYPE_ENTITEIT en GEBRUIKER. Daarnaast zal er een error worden weergegeven wanneer er blijkt dat het project niet (meer) bestaat in de tabel PROJECT. De oorzaak hiervan is eveneens een relatie, welke in dit geval ligt tussen de tabellen ENTITEIT_IN_PROJECT en PROJECT, en ATTRIBUUT_IN_SUBTYPE_ENTITEIT en PROJECT.



Tabellen die beinvloed worden door de use case subtype entiteit toevoegen:
- ENTITEIT_IN_PROJECT: Bij het aanmaken, wijzigen en verwijderen van een subtype entiteit zal deze tabel bewerkt worden.
- ATTRIBUUT_IN_SUBTYPE_ENTITEIT: Bij het aanmaken, wijzigen en verwijderen van een attribuut aan/bij een subtype entiteit zal deze tabel bewerkt worden
- Geschiedenis tabellen va ENTITEIT_IN_PROJECT en ATTRIBUUT_IN_SUBTYPE_ENTITEIT: Wanneer er een actie in de tabel
  ENTITEIT_IN_PROJECT of ATTRIBUUT_IN_SUBTYPE_ENTITEIT plaats vindt, zullen deze veranderde waarden door een trigger in de history tabel van ENTITEIT_IN_PROJECT of ATTRIBUUT_IN_SUBTYPE_ENTITEIT gezet worden.



##### INSERT Entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een entiteit bestaat binnen het project met dezelfde naam
- Voer een insert statement uit op de tabel ENTITEIT_IN_PROJECT om de entiteit toe te voegen.



Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.



Parameters:

| Parameternaam  | Datatype    | Omschrijving                                                 |
| -------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID     | int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName      | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is. |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 1
  Entiteit toevoegen aan een project welke nog niet bestaat.
  Foutmelding: geen foutmelding verwacht
- Test scenario 2
  Entiteit toevoegen door een gebruiker die niet lid is van het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'
- Test scenario 3
  Entiteit toevoegen aan een project welke al door dezelfde user aangemaakt is.
  Foutmelding: 'Er bestaat al een entiteit met deze naam.'



##### INSERT Attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een attribuut in de entiteit bestaat binnen het project met dezelfde naam.
- Controleer of mandatory de waarde 1 heeft, wanneer de primary identifier de waarde 1 heeft.
- Voer een insert statement uit op de tabel ATTRIBUUT_IN_SUBTYPE_ENTITEIT om de attribuut toe te voegen.



Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.



Parameters:
| Parameternaam           | Datatype    | Omschrijving                                                 |
| ----------------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID              | int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName          | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName               | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |
| @subEntityNameAttribute | varchar(50) | Bevat de naam van de gewenste attribuut.                     |
| @primaryIdentifier      | bit         | Bevat de bit waarde of het attribuut deel uitmaakt van de primary identifier |
| @mandatory              | bit         | Bevat de bit waarde of het attribuut mandatory is.           |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 4
  Een attribuut toevoegen, welke nog niet bestaat, aan een entiteit.
  Foutmelding: geen foutmelding verwacht
- Test scenario 5
  Een attribuut toevoegen, welke nog niet bestaat, aan een entiteit, door een gebruiker die niet lid is van het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'
- Test scenario 6
  Attribute toevoegen aan een project welke al aangemaakt is.
  Foutmelding: 'Er bestaat al een attribuut in deze entiteit met deze naam.'



##### UPDATE entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of de gewenste entiteitnaam nog niet gebruikt wordt in het project.
- Voer een update statement uit welke de waarde van het record verandert naar de nieuwe meegegeven waarden.



Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.



Parameters:

| Parameternaam          | Datatype    | Omschrijving                                                 |
| ---------------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID             | Int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName         | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName              | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |
| @subEntityNameOriginal | varchar(50) | Bevat de naam van de entiteit die veranderd moet worden.     |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 7
  Entiteit updaten naar waarden welke nog niet bestaat.
  Foutmelding: geen foutmelding verwacht
- Test scenario 8
  Entiteit updaten door een gebruiker die niet in het project zit.
  Foutmelding: 'De gebruiker is niet lid van het project.'
- Test scenario 9
  Entiteit updaten naar een entiteit met een bestaande naam.
  Foutmelding: 'Er bestaat al een entiteit met deze naam.'



##### UPDATE attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een attribuut in de entiteit bestaat binnen het project met dezelfde naam.
- Controleer of mandatory de waarde 1 heeft, wanneer de primary identifier de waarde 1 heeft.
- Voer een update statement uit op de tabel ATTRIBUUT_IN_SUBTYPE_ENTITEIT om de attribuut toe te voegen.



Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.



Parameters:
| Parameternaam                    | Datatype    | Omschrijving                                                 |
| -------------------------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID                       | Int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName                   | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName                        | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |
| @subEntityNameAttribute          | varchar(50) | Bevat de naam van de gewenste attribuut.                     |
| @primaryIdentifier               | Bit         | Bevat de bit waarde of het attribuut deel uitmaakt van de primary identifier |
| @mandatory                       | Bit         | Bevat de bit waarde of het attribuut mandatory is.           |
| @subEntityNameAttributeOrigineel | varchar(50) | Bevat de naam van de attribuut die veranderd moet worden.    |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 10
  attribuut updaten naar waarden welke nog niet bestaat.
  Foutmelding: geen foutmelding verwacht
- Test scenario 11
  Attribuut updaten door een gebruiker die niet in het project zit.
  Foutmelding: 'De gebruiker is niet lid van het project.'
- Test scenario 12
  Attribuut updaten naar een naam die al bestaat.
  Foutmelding: 'Er bestaat al een attribuut in deze entiteit met de gewenste naam.
- Test scenario 13
  Een attribuut updaten welke wel deel uitmaakt van de primary identifier, maar niet mandatory is.
  Foutmelding: 'Een attribuut dat deel uitmaakt van de primary identifier, moet ook mandatory zijn.'



##### DELETE entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of de entiteit geen attributen meer bevat.
- Voer het delete statement uit



Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- Er zijn nog attributen aanwezig die horen bij de entiteit.



Parameters:

| Parameternaam  | Datatype    | Omschrijving                                                 |
| -------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID     | Int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName      | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 14
  entiteit verwijderen welke voldoet aan alle eisen.
  Foutmelding: geen foutmelding verwacht
- Test scenario 15
  entiteit verwijderen door een gebruiker welke niet deel uit maakt van het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'
- Test scenario 16
  entiteit verwijderen welke nog attributen bevat.
  Foutmelding: 'Deze entiteit bevat nog attributen.'



##### Delete attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID

- Voer het delete statement uit

  


Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.



Parameters:
| Parameternaam           | Datatype    | Omschrijving                                                 |
| ----------------------- | ----------- | ------------------------------------------------------------ |
| @ProjectID              | Int         | Bevat de waarde van het ID van het project waarbinnen de business rule aangemaakt is. |
| @subEntityName          | varchar(50) | Bevat de naam die de entiteit moet krijgen.                  |
| @userName               | varchar(20) | Bevat de waarde van de gebruikersnaam van de gebruiker die ingelogd is |
| @subEntityNameAttribute | varchar(50) | Bevat de naam van de gewenste attribuut.                     |



Concurrency:

Voordat het insert statement plaats vindt, worden een aantal controles uitgevoerd. Deze zijn in de vorm van selects statements binnen de tabel PROJECTBEZETTING en ENTITEIT_IN_PROJECT. Hier kan een phantom plaats vinden, bijvoorbeeld wanneer een gebruiker tijdens het uitvoeren van de stored procedure ook van een project verwijderd wordt. 

Op het standaard isolation level is dit mogelijk, doordat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst deel uit maakte van een project tussentijds verwijderd worden (als die tussen de eerste en tweede check verwijderd wordt van een project). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 



Tests:

- Test scenario 17
  attribuut succesvol verwijderen.
  Foutmelding: geen foutmelding verwacht
- Test scenario 18
  attribuut verwijderen door een gebruiker welke niet deel uit maakt van het project.
  Foutmelding: 'De gebruiker is niet lid van het project.'



#### Overige functionele eis: inloggen account

Voor meer informatie over de functionaliteit van het inloggen van een account zie: Functioneel ontwerp, Hoofdstuk: Overige functionele eisen, Use case: Inloggen.

##### Brief description

Als bezoeker wil ik kunnen inloggen in het systeem. Dit maakt het voor mij mogelijk deel te nemen aan projecten. Ook kan ik hierdoor gebruik maken van alle bijbehorende functionaliteiten.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol inloggen van een gebruiker:

-GEBRUIKER

##### Acties die plaats vinden

De volgende acties vinden plaats binnen de tabel GEBRUIKER:
SELECT om te kijken of het ingevulde wachtwoord overeen komt met het opgeslagen wachtwoord.
RETURN een 1 als de ingevulde wachtwoorden met elkaar overeenkomen.

##### Keuze voor constrainttype

Er is ervoor gekozen om gebruik te maken van een stored procedure bij deze use-case. 

##### Foutafhandeling

De volgende mogelijke fout is getest in deze stored procedure:

1. Er bestaat al een gebruiker met deze gebruikersnaam.

##### Concurrency

Een groot voordeel van triggers is de mogelijkheid om gemakkelijk meerdere records te kunnen inserten. Dit valt hierdoor weg. Verder heeft deze use-case alleen invloed op het toevoegen van een gebruiker. Voorkomen is beter dan genezen. Daarom is er voor een stored procedure gekozen.




#### Overige functionele eis: verwijderen gebruiker uit project

Voor meer informatie over de functionaliteit van het verwijderen van een gebruiker uit een project zie: Functioneel ontwerp, Hoofdstuk: Overige functionele eisen, Use case: Gebruiker uit project verwijderen.

Een eigenaar van een project kan andere gebruikers uit het project verwijderen. Wanneer hij dit doet kunnen ze niet meer aan het project meewerken. Op deze manier kan de eigenaar bepalen wie er aanpassingen kan maken aan een project, en wie niet.

##### Parameters

De SQL code voor deze use-case is te vinden in het volgende SQL bestand: "Stored Procedure toevoegen gebruiker aan project.sql"

De volgende parameters worden meegegeven aan de stored procedure;

@eigenaar, deze parameter heeft een datatype van varchar(20). Deze parameter representeert de gebruiker die de actie uitvoert. 

@te_verwijderen_gebruiker varchar(20), deze parameter heeft een datatype van varchar(20). Deze parameter representeert de gebruiker die uit het project wordt verwijdert. 

@project_id, deze parameter heeft het datatype int. Deze parameter representeert het ID van het project waar de gebruiker aan toegevoegd wordt.

##### Relevante tabellen

De volgende tabellen zijn nodig voor het succesvol verwijderen van een gebruiker uit een project:
-PROJECTBEZETTING

##### Acties die plaats vinden

De volgende acties vinden plaats binnen de tabel PROJECTBEZETTING:
SELECT om te kijken of er een record is waar de gebruiker die als eigenaar 
SELECT om te kijken of er een record is in PROJECTBEZETTING waarbij de eigenaar 'IS_EIGENAAR' = 1 heeft bij het project waar het project_id overeenkomt met @project_id.
DELETE voor het verwijderen van het record.

##### Keuze voor constrainttype

Er is ervoor gekozen om gebruik te maken van een stored procedure bij deze use-case. Bij deze constraint wordt er enkel één enkele gebruiker uit een project verwijdert. Verder zijn er een tweetal controles die plaatsvinden (is @eigenaar de eigenaar van het project, wordt er geen eigenaar verwijdert). 

Het is beter om fouten in de database te voorkomen, ten opzichten van het herstellen. Hierdoor is een trigger een stuk minder goed voor deze constraint.

Op basis van bovenstaande argumentatie is er gekozen voor een stored procedure.

##### Foutafhandeling

Naast het main-succes scenario zijn ook de volgende mogelijke fouten in acht genomen voor deze stored procedure:

1. Er is geen record in PROJECTBEZETTING waar IS_EIGENAAR = 1 voor de eigenaar.
2. De te verwijderen gebruiker is een eigenaar van het project.

##### Testcases

De volgende fouten zijn getest in de testcases:

**Test 1: Een gebruiker wordt succesvol verwijdert** 

Binnen deze test wordt het main-succes scenario getest. Alle informatie klopt hier en de gebruiker 'DaveTest' wordt succesvol uit het project verwijdert.

**Test 2: geen eigenaar van het project**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gegeven wanneer de 'eigenaar' niet daadwerkelijk een eigenaar van het project is.

**Test 3: je mag de eigenaar van een project niet verwijderen**

Binnen deze test wordt er gekeken of er succesvol een foutmelding wordt gegeven wanneer de te verwijderen gebruiker ook eigenaar is van het project.

##### Concurrency

Binnen deze stored procedure vinden er enkel een paar controles plaats. Dit zijn beide selects binnen de tabel PROJECTBEZETTING. Hier zouden in principe non-repeatable reads kunnen plaatsvinden. Dit kan gebeuren wanneer de 'eigenaar' bijvoorbeeld via een update geen eigenaar meer is. 

Op het standaard isolation level is dit mogelijk omdat de S-locks na de read worden vrijgegeven. Op deze manier kan iemand die eerst eigenaar is zichzelf uit het project verwijderen (als die tussen de eerste en tweede check wordt geüpdate om geen eigenaar te zijn). 

Dit is op te lossen door gebruik te maken van isolation level REPEATABLE READ. 

In principe kunnen er geen phantom reads optreden bij deze stored procedure. Deze vinden plaats als er ineens een record verdwijnt of ontstaat bij een van de selects. De kans dat dit gebeurt is echter heel onwaarschijnlijk. Aangezien de eigenaar niet verwijdert kan worden uit een project. Ook is het onwaarschijnlijk dat de stored procedure wordt aangeroepen met ingevulde parameters voor het verwijderen van een gebruiker uit een project die op dat moment nog niet is toegevoegd aan het project. 

Mocht dit probleem vaker voorkomen dan verwacht, dan is dit op te lossen door het isolation level op SERIALIZABLE te zetten.

Op basis van informatie wordt er geadviseerd om gebruik te maken van isolation level REPEATABLE READ voor deze stored procedure.

### Indexes

Het doel van een index is om de tijd van een select-query te verminderen. Een database die ondersteund wordt door goede indices is in staat om specifieke query's efficiënter uit te voeren. Binnen onze database is er ook gekeken naar mogelijke locaties om indices aan te maken. 

Aangezien de database heel modulair is opgebouwd, zijn er geen stukken code gevonden waar een index de performance zou kunnen verbeteren. Bij de meeste select query's wordt er gebruik gemaakt van primary keys in de WHERE clausule. Hiervoor worden automatisch clustered indices aangemaakt. Er is wel naar de mogelijkheid voor indices gekeken, maar er zijn geen query's gevonden waarvoor een nuttige index kan worden aangemaakt.

Ten slotte heeft Chris Scholten ook vermeld dat er geen grote hoeveelheden data opgeslagen gaan worden in het systeem.  Een index zorgt voornamelijk voor een verhoging van de performance bij grote hoeveelheden data. Aangezien dat niet het geval is bij deze database, is ervoor gekozen om er niet meer tijd en moeite in te steken, aangezien het voor een minimale winst van performance gaat zorgen.

## Niet-functionele eisen

De volgende niet-functionele eisen zijn in kaart gebracht in het FO:

- Documentatie
- Het systeem ondersteund de Nederlandse taal
- Het systeem ondersteund twee gebruikers tegelijk

In het Technisch Ontwerp hebben we de database gedocumenteerd zodat de database makkelijk te onderhouden is en overdraagbaar is. In het hoofdstuk implementatie is er bij elke stored procedure een analyse geschreven. Verder zijn de testscenario's  beschreven. Hierin is te zien wat er wordt getest en wat er wordt verwacht. Ten slotte wordt er ook verteld welke constraints worden afgedekt in de stored procedure.

De front-end is geschreven in de Nederlandse taal. Hiermee wordt de niet-functionele eis wat betreft het ondersteunen van de Nederlandse taal afgedekt. 

In het functioneel ontwerp staat bij de eis "Het systeem ondersteund twee gebruikers tegelijk" vermeld dat wij hiervoor zullen kijken naar de mogelijke fouten en gevolgen zullen kijken. Deze informatie is ook terug te vinden in het hoofdstuk implementatie. 

## Technische Realisatie Interface 

Zoals eerder besproken zal de interface gerealiseerd worden door middel van PHP, HTML, CSS en het front-end framework Bootstrap. Het is een rudimentaire interface waarin de gebruiker de aangemaakte stored procedures kan aanroepen. Momenteel is er nog geen functionaliteit om de stored procedures, die gegevens updaten, aan te roepen vanuit de front-end.

Alle front-end code is terug te vinden in de map applicatie.

## Advies voor verdere teams

Gedurende dit project is er door het ontwikkelteam ervoor gekozen om een data-driven applicatie te ontwikkelen. De reden hiervoor is dat hiermee gemakkelijk aangetoond kan worden dat de database werkt, en dat alle informatie succesvol opgeslagen kan worden.

Hierdoor zijn er wel een aantal keuzes gemaakt die de implementatie makkelijker hebben gemaakt, maar die niet aan het traditionele analyse-proces voldoen. Indien er een applicatie gemaakt gaat worden om het analyse-proces te digitaliseren zullen er een aantal veranderingen aangebracht moeten worden aan het huidige project.

Allereerst, is er nog geen mogelijkheid om een datatype aan attributen toe te voegen. Dit kan op meerdere manieren opgelost worden. Er kan een extra kolom worden toegevoegd aan attribuut_in_project waarin het datatype wordt toegevoegd. Dit is echter geen nette oplossing omdat een datatype uit meerdere onderdelen kan bestaan (lengte, datatype, precision etc.). Om dit op te lossen is het mogelijk om in het FO gebruik te maken van subtypes. Hierdoor krijg je veel minder met null-values te maken in de database.

Verder hebben we ook bij het benoemen van een relatie een 'gecombineerde' stored procedure. De stored procedure die de extra informatie die powerdesigner nodig heeft in de database zet, voert ook de stored procedure uit die de informatie van het benoemen op papier in de database zet, uit. Deze stored procedures zullen uit elkaar gehaald moeten worden, zodat ze apart uitgevoerd kunnen worden. 

Binnen de stored procedure voor het toevoegen van een attribuut wordt ook gelijk de 'Invulplek' in de database gezet. In het analyse proces is het in kaart brengen van invulplekken de eerste stap. Dit moet nog uit deze stored procedure geïsoleerd worden. In de huidige database is het hierdoor nog niet mogelijk om invulplekken aan te geven (zonder dat je gelijk een attribuut benoemt).



