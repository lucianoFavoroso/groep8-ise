USE FactBasedAnalysis
CREATE LOGIN BasicUser WITH PASSWORD = 'BasicB*tch123!';
CREATE USER BasicUser FOR LOGIN BasicUser;  

IF EXISTS(select * from sys.procedures where name=N'giveProcPremToBasicUser')
	BEGIN
		DROP PROC giveProcPremToBasicUser
	END
GO
GO
CREATE PROC giveProcPremToBasicUser
AS
BEGIN
DECLARE @query varchar(MAX);
SET @query = ' ';
SELECT @query = @query + 'GRANT EXECUTE ON OBJECT::FactBasedAnalysis.dbo.'+
name + ' TO BasicUser ' from sys.procedures where name like 'usp_%';
EXEC sp_sqlexec @query;
END
GO
GO
EXEC giveProcPremToBasicUser
