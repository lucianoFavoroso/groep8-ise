/*
Stored procedure: Aanmaken account
Use case: Een account aanmaken

Als bezoeker wil ik een account aanmaken voor het systeem. 
Dit maakt het voor mij mogelijk deel te nemen aan projecten. 
Ook kan ik hierdoor gebruik maken van alle bijbehorende functionaliteiten.

De volgende tabellen zijn nodig voor het succesvol toevoegen van een account
-Gebruiker

Acties binnen de tabel GEBRUIKER:
-Select om te kijken of er al een record is voor de toe te voegen gebruiker.
-Inserten van het nieuwe record

Type constraint: Stored Procedure
Waarom: Er is ervoor gekozen om gebruik te maken van een stored 
procedure bij deze use-case. Een groot voordeel van triggers is de
mogelijkheid om gemakkelijk meerdere records te kunnen inserten.
Dit valt hierdoor weg. Verder heeft deze use-case alleen invloed op
het toevoegen van een gebruiker. Voorkomen is beter dan genezen. 
Daarom is er voor een stored procedure gekozen.

Binnen deze usecase onderneemt de gebruiker 1 stap; Hij geeft zijn gebruikersnaam 
en wachtwoord op waarmee hij zich wil aanmelden in het systeem.

De volgende fouten kunnen optreden bij deze usecase:
1. Er bestaat al een gebruiker met deze gebruikersnaam.
*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_addUser')
	BEGIN
		DROP PROC usp_addUser
	END
GO
CREATE PROCEDURE usp_addUser --Insert een record in projectbezetting.
	@gebruikersnaam varchar(100),
	@wachtwoord varchar(100)
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
		IF exists(SELECT 1 FROM GEBRUIKER where GEBRUIKERSNAAM = @gebruikersnaam)
		BEGIN
			RAISERROR('Deze gebruiker bestaat al', 16, 1)
		END

		INSERT INTO GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD) 
		VALUES (@gebruikersnaam, @wachtwoord)
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Gebruiker bestaat al

-Gebruiker is aangemaakt
*/

EXEC tSQLt.NewTestClass 'toevoegenGebruiker';
GO
/* ================= 
    Gebruiker bestaat al!
==================*/
CREATE PROCEDURE [toevoegenGebruiker].[test = gebruiker bestaat al]  
AS
BEGIN
--Assemble

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'DitIsEenMoeilijkeWachtwoord'), ('LucianoTest', '12345')
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Deze gebruiker bestaat al'

--Act
    exec  usp_addUser 'BonnoTest', 'LucianoTest'

END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[toevoegenGebruiker].[test = gebruiker bestaat al]'
GO

/* ================= 
    Gebruiker is aangemaakt
==================*/
CREATE PROCEDURE [toevoegenGebruiker].[test = gebruiker is aangemaakt]  
AS
BEGIN
--Assemble
	
	IF OBJECT_ID('[toevoegenGebruiker].[Expected]','Table') IS NOT NULL
	DROP TABLE [toevoegenGebruiker].[Expected]

	SELECT TOP 0 * 
	INTO toevoegenGebruiker.Expected --snelle manier om een structuur te "kopi�ren"
	FROM GEBRUIKER;

	INSERT INTO toevoegenGebruiker.Expected(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef')

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
		  
--Act
exec  usp_addUser 'BonnoTest', 'abcdef'

--Assert
EXEC [tSQLt].[AssertEqualsTable] 'toevoegenGebruiker.Expected', 'dbo.GEBRUIKER'
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[toevoegenGebruiker].[test = gebruiker is aangemaakt]'
GO