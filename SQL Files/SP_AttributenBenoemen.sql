/*
Stored procedure: Attributen benoemen
Use case: Benoemen attributen

Als gebruiker wil ik attributen uit mijn verbalisatie halen en deze bij de juist
gevonden entiteit neerzetten. Dit stelt mij in staat mijn attributen te koppelen 
aan gevonden entiteiten.

Create
Voor het aanmaken van een attribuut binnen een entiteit, worden er parameters meegegeven.
Met deze parameters kan er een nieuwe attribuut aangemaakt worden. 

De volgende tabellen zijn nodig voor het succesvol benoemen van een attribuut:
-ATTRIBUUT_IN_VERBALISATIE
-INVULPLEK
-ENTITEIT_IN_PROJECT
-VERBALISATIE

Acties binnen de tabel ATTRIBUUT_IN_VERBALISATIE:
Stap 1: De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten, 
		ook mag de beginpositie niet boven de eindpositie komen en niet onder 0.
Stap 2: De eindpositie mag niet onder het beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.
Stap 3: Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.
Stap 4: SELECT om te kijken of er al een attribuut aan de invulplek is gekoppeld. Bij
		attributen moet er gekeken worden naar de verbalisatie_id in combinatie met de 
		begin positie en eindpositie.
Stap 5: SELECT om te kijken of de entiteit_id hetzelfde project_id heeft als de verbalisatie
		van verbalisatie_id.
Stap 6: INSERT INVULPLEK en ATTRIBUUT_INVERBALISATIE

Update
Stap 1: De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten, 
		ook mag de beginpositie niet boven de eindpositie komen en niet onder 0.
Stap 2: De eindpositie mag niet onder het beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.
Stap 3: Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.
Stap 4: SELECT om te kijken of de nieuwe ingevoerde @beginpos of @eindpos al een attribuut heeft. (Except de te update record)
Stap 5: Update het record met als ID @verbalisatie_id, @beginpos, @eindpos, attribuut_naam van de tabellen INVULPLEK en ATTRIBUUT_IN_VERBALISATIE

De volgende tabellen zijn nodig voor het succesvol updaten van een attribuut:
-ATTRIBUUT_IN_VERBALISATIE
-INVULPLEK
-VERBALISATIE

Delete
Beinvloede tabellen: ATTRIBUUT_IN_VERBALISATIE en INVULPLEK.

Read
Beinvloede tabellen: ATTRIBUUT_IN_VERBALISATIE
*/

/*==================
CREATE
===================*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_addAttribute')
	BEGIN
		DROP PROC usp_addAttribute
	END
GO
CREATE PROCEDURE usp_addAttribute --Insert een record in Attribuut_in_verbalisatie.
	@eniteit_id int,
	@verbalisatie_id int,
	@beginpos int,
	@eindpos int,
	@gebruikersnaam varchar(100),
	@attribuut_naam varchar(100),
	@is_primary_identifier bit,
	@is_mandatory bit
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY

		DECLARE @TOTAALAANTALKARAKTERS INT

		SELECT @TOTAALAANTALKARAKTERS = LEN(VERBALISATIE_ZIN) from VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id

		/*De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten, 
		ook mag de beginpositie niet boven de eindpositie komen en niet onder 0*/
		IF(@beginpos < 1 or @beginpos > @TOTAALAANTALKARAKTERS or @beginpos > @eindpos)
		BEGIN
			RAISERROR('De beginpositie is onjuist', 16, 1)
		END
		
		/*De eindpositie mag niet onder het beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.*/
		IF(@eindpos < @beginpos or @eindpos > @TOTAALAANTALKARAKTERS)
		BEGIN
			RAISERROR('De eindpositie is onjuist', 16, 1)
		END

		/*Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.*/
		IF(@is_primary_identifier = 1 and @is_mandatory = 0)
		BEGIN
			RAISERROR('Wanneer dependent true is, dan moet mandatory ook true zijn', 16, 1)
		END
			
		/*SELECT om te kijken of er al een attribuut aan de invulplek is gekoppeld. Bij
		attributen moet er gekeken worden naar de verbalisatie_id in combinatie met de 
		begin positie en eindpositie*/
		IF exists(SELECT * FROM ATTRIBUUT_IN_VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id and ((@beginpos between BEGINPOSITIE and EINDPOSITIE) or (@eindpos between BEGINPOSITIE and EINDPOSITIE)))
		BEGIN
			RAISERROR('Er bestaat al een attribuut op de door u ingevulde beginpositie en eindpositie', 16, 1)
		END

		/*SELECT om te kijken of de entiteit_id hetzelfde project_id heeft als de verbalisatie
		van verbalisatie_id.*/
		IF not exists(SELECT 1 FROM ENTITEIT_IN_PROJECT where ENTITEIT_ID = @eniteit_id and PROJECT_ID IN(select PROJECT_ID from VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id))
		BEGIN
			RAISERROR('De door u ingevulde entiteit, bevindt zich niet in dit project', 16, 1)
		END

		/*INSERT INVULPLEK*/
		INSERT INTO INVULPLEK(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE) 
		VALUES (@verbalisatie_id, @beginpos, @eindpos)		
		
		/*INSERT ATTRIBUUT_INVERBALISATIE*/
		INSERT INTO ATTRIBUUT_IN_VERBALISATIE(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE, ATTRIBUUT_NAAM, 
		GEBRUIKERSNAAM, ENTITEIT_ID, IS_PRIMARY_IDENTIFIER, IS_MANDATORY)
		VALUES (@verbalisatie_id, @beginpos, @eindpos, @attribuut_naam, @gebruikersnaam,
		@eniteit_id, @is_primary_identifier, @is_mandatory)

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*==================
UPDATE
===================*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_updateAttribute')
	BEGIN
		DROP PROC usp_updateAttribute
	END
GO
CREATE PROCEDURE usp_updateAttribute --Update een record in Attribuut_in_verbalisatie.
	@verbalisatie_id int,
	@oldBeginpos int,
	@oldEindpos int,
	@newBeginPos int,
	@newEindPos int,
	@gebruikersnaam varchar(100),
	@attribuut_naam varchar(100),
	@is_primary_identifier bit,
	@is_mandatory bit
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
		
		DECLARE @TOTAALAANTALKARAKTERS INT

		SELECT @TOTAALAANTALKARAKTERS = LEN(VERBALISATIE_ZIN) from VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id

		/*De beginpositie mag niet boven het totaal aantal karakters van de verbalisatie_zin zitten, 
		ook mag de beginpositie niet boven de eindpositie komen en niet onder 0*/
		IF(@newBeginPos < 1 or @newBeginPos > @TOTAALAANTALKARAKTERS or @newBeginPos > @newEindPos)
		BEGIN
			RAISERROR('De beginpositie is onjuist', 16, 1)
		END
		
		/*De eindpositie mag niet onder de beginpositie zitten en niet boven het totaal aantal karakters van de verbalisatie.*/
		IF(@newEindPos < @newBeginPos or @newEindPos > @TOTAALAANTALKARAKTERS)
		BEGIN
			RAISERROR('De eindpositie is onjuist', 16, 1)
		END

		/*Wanneer de primary identifier op true staat, moet is_mandatory ook op true staan, anders geef een error.*/
		IF(@is_primary_identifier = 1 and @is_mandatory = 0)
		BEGIN
			RAISERROR('Wanneer dependent true is, dan moet mandatory ook true zijn', 16, 1)
		END
		
		/*SELECT om te kijken of de nieuwe ingevoerde @beginpos of @eindpos al 
		een attribuut heeft. (Except de te update record)*/
		IF exists(
		SELECT * FROM ATTRIBUUT_IN_VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id and @newBeginPos 
		between BEGINPOSITIE and EINDPOSITIE or @newEindPos between BEGINPOSITIE and EINDPOSITIE
		except
		SELECT * from ATTRIBUUT_IN_VERBALISATIE where VERBALISATIE_ID = @verbalisatie_id and 
		BEGINPOSITIE = @oldBeginpos and EINDPOSITIE = @oldEindpos
		)
		BEGIN
			RAISERROR('Er bestaat al een attribuut op de door u ingevulde beginpositie en eindpositie', 16, 1)
		END

		/*Update het record met als ID @verbalisatie_id, @beginpos, @eindpos, attribuut_naam in de tabellen INVULPLEK en ATTRIBUUT_IN_VERBALISATIE*/
		UPDATE INVULPLEK SET BEGINPOSITIE = @newBeginPos, EINDPOSITIE = @newEindPos
		where VERBALISATIE_ID = @verbalisatie_id and BEGINPOSITIE = @oldBeginpos and EINDPOSITIE = @oldEindpos
		
		/*UPDATE ATTRIBUUT_INVERBALISATIE*/
		UPDATE ATTRIBUUT_IN_VERBALISATIE SET BEGINPOSITIE = @newBeginPos, EINDPOSITIE = @newEindPos,
		ATTRIBUUT_NAAM = @attribuut_naam, GEBRUIKERSNAAM = @gebruikersnaam, 
		IS_PRIMARY_IDENTIFIER = @is_primary_identifier, IS_MANDATORY = @is_mandatory
		where VERBALISATIE_ID = @verbalisatie_id and BEGINPOSITIE = @oldBeginpos and EINDPOSITIE = @oldEindpos
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*===========
UNIT-TESTS CREATE:
Er zullen tests aangemaakt worden voor de volgende gevallen:

-Beginpositie is onjuist

-Eindpositie is onjuist

-Wanneer dependent true is, dan moet mandatory ook true zijn

-Er bestaat al een attribuut op de ingevoerde beginpositie en eindpositie

-De ingevoerde entiteit bevindt zich niet in het project

-INSERT INVULPLEK en ATTRIBUUT_IN_VERBALISATIE
==============*/
EXEC tSQLt.NewTestClass 'toevoegenAttribuut';
GO
/* ================= 
	Test scenario 1: Beginpositie is onjuist
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = de beginpositie is onjuist]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De beginpositie is onjuist'

	exec usp_addAttribute 2, 2, 26, 24, 'DaveQ', 'fiets_id', 1, 1

	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = de beginpositie is onjuist]'
GO

/* ================= 
	Test scenario 2: Eindpositie is onjuist
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = de eindpositie is onjuist]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De eindpositie is onjuist'

	exec usp_addAttribute 2, 2, 26, 46, 'DaveQ', 'fiets_id', 1, 1

	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = de eindpositie is onjuist]'
GO

/* ================= 
	Test scenario 3: Wanneer dependent true is, dan moet mandatory ook true zijn
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = mandatory moet true zijn]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Wanneer dependent true is, dan moet mandatory ook true zijn'

	exec usp_addAttribute 2, 2, 26, 41, 'DaveQ', 'fiets_id', 1, 0

	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = mandatory moet true zijn]'
GO

/* ================= 
    Test scenario 4: Er bestaat al een attribuut op de ingevoerde beginpositie en eindpositie
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = attribuut bestaat al]  
AS
BEGIN
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE';

	INSERT INTO dbo.ATTRIBUUT_IN_VERBALISATIE(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE, ATTRIBUUT_NAAM,
	GEBRUIKERSNAAM, ENTITEIT_ID, IS_PRIMARY_IDENTIFIER, IS_MANDATORY)
	VALUES(2, 23, 24, 'fiets_id', 'DaveQ', 2, 1, 1)
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er bestaat al een attribuut op de door u ingevulde beginpositie en eindpositie'

	exec  usp_addAttribute 2, 2, 24, 24, 'DaveQ', 'fiets_id', 1, 1
	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = attribuut bestaat al]'
GO

/* ================= 
	Test scenario 5: entiteit bevindt zich niet in dit project
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = entiteit bevindt zich niet in dit project]  
AS
BEGIN
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE';

	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT';
	
	EXEC tSQLt.FakeTable 'dbo', 'VERBALISATIE';

	INSERT INTO dbo.ATTRIBUUT_IN_VERBALISATIE(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE, ATTRIBUUT_NAAM,
	GEBRUIKERSNAAM, ENTITEIT_ID, IS_PRIMARY_IDENTIFIER, IS_MANDATORY)
	VALUES(4, 12, 13, 'post_id', 'DaveQ', 2, 1, 1)

	INSERT INTO dbo.ENTITEIT_IN_PROJECT(ENTITEIT_ID, SUBTYPE_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES(2, null, 2, 'DaveQ', 'FIETS')

	INSERT INTO dbo.VERBALISATIE(VERBALISATIE_ID, PROJECT_ID, GEBRUIKERSNAAM, VERBALISATIE_ZIN)
	VALUES(4, 1, 'DaveQ', 'Fiets met ID 5 heeft batterijlading van 50%')
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De door u ingevulde entiteit, bevindt zich niet in dit project'

	exec usp_addAttribute 2, 4, 24, 24, 'DaveQ', 'fiets_id', 1, 1

	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = entiteit bevindt zich niet in dit project]'
GO

/* ================= 
    Test scenario 6: Attribuut is aangemaakt
==================*/
CREATE PROCEDURE toevoegenAttribuut.[test = Attribuut is aangemaakt]  
AS
BEGIN
	IF OBJECT_ID('[toevoegenAttribuut].[Expected]','Table') IS NOT NULL
	DROP TABLE [toevoegenAttribuut].[Expected]

	SELECT TOP 0 * 
	INTO toevoegenAttribuut.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ATTRIBUUT_IN_VERBALISATIE;

	INSERT INTO toevoegenAttribuut.Expected(GEBRUIKERSNAAM, ATTRIBUUT_NAAM, ENTITEIT_ID,
	VERBALISATIE_ID, IS_PRIMARY_IDENTIFIER, BEGINPOSITIE, EINDPOSITIE, IS_MANDATORY)
	VALUES('DaveQ', 'fiets_id', 2, 2, 1, 1, 12, 1)

	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE';
			  
	exec usp_addAttribute 2, 2, 1, 12, 'DaveQ', 'fiets_id', 1, 1

	EXEC [tSQLt].[AssertEqualsTable] 'toevoegenAttribuut.Expected', 'dbo.ATTRIBUUT_IN_VERBALISATIE'
	END

	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = Attribuut is aangemaakt]'
GO

/*===========
UNIT-TESTS UPDATE:
Er zullen tests aangemaakt worden voor de volgende gevallen:

-Beginpositie is onjuist

-Eindpositie is onjuist

-Wanneer dependent true is, dan moet mandatory ook true zijn

-Er bestaat al een attribuut op de ingevoerde beginpositie en eindpositie(Except de te update record)

-UPDATE INVULPLEK en ATTRIBUUT_IN_VERBALISATIE
==============*/
EXEC tSQLt.NewTestClass 'updatenAttribuut';
GO

/* ================= 
	Test scenario 7: Beginpositie is onjuist
==================*/
CREATE PROCEDURE updatenAttribuut.[test = de beginpositie is onjuist]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De beginpositie is onjuist'

	exec usp_updateAttribute 2, 1, 12, 26, 24, 'DaveQ', 'fiets_id', 1, 1

	END

	GO	
	EXEC [tSQLt].[Run] '[updatenAttribuut].[test = de beginpositie is onjuist]'
GO

/* ================= 
	Test scenario 8: Eindpositie is onjuist
==================*/
CREATE PROCEDURE updatenAttribuut.[test = de eindpositie is onjuist]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De eindpositie is onjuist'

	exec usp_updateAttribute 2, 1, 12, 26, 46, 'DaveQ', 'fiets_id', 1, 1

	END

	GO	
	EXEC [tSQLt].[Run] '[updatenAttribuut].[test = de eindpositie is onjuist]'
GO

/* ================= 
	Test scenario 9: Wanneer dependent true is, dan moet mandatory ook true zijn
==================*/
CREATE PROCEDURE updatenAttribuut.[test = mandatory moet true zijn]  
AS
BEGIN
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Wanneer dependent true is, dan moet mandatory ook true zijn'

	exec usp_updateAttribute 2, 1, 12, 26, 41, 'DaveQ', 'fiets_id', 1, 0

	END

	GO	
	EXEC [tSQLt].[Run] '[updatenAttribuut].[test = mandatory moet true zijn]'
GO

/* ================= 
    Test scenario 10: Er bestaat al een attribuut op de ingevoerde beginpositie en eindpositie
==================*/
CREATE PROCEDURE updatenAttribuut.[test = attribuut bestaat al]  
AS
BEGIN
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE';

	INSERT INTO dbo.ATTRIBUUT_IN_VERBALISATIE(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE, ATTRIBUUT_NAAM,
	GEBRUIKERSNAAM, ENTITEIT_ID, IS_PRIMARY_IDENTIFIER, IS_MANDATORY)
	VALUES(2, 23, 24, 'fiets_id', 'DaveQ', 2, 1, 1), (2, 1, 12, 'post_id', 'DaveQ', 4, 1, 1)
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er bestaat al een attribuut op de door u ingevulde beginpositie en eindpositie'

	exec  usp_updateAttribute 2, 23, 24, 11, 12, 'DaveQ', 'fiets_id', 1, 1

	END
	GO	
	EXEC [tSQLt].[Run] '[toevoegenAttribuut].[test = attribuut bestaat al]'
GO

/* ================= 
    Test scenario 11: Attribuut is geupdate
==================*/
CREATE PROCEDURE updatenAttribuut.[test = Attribuut is geupdate]  
AS
BEGIN
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE';

	INSERT INTO dbo.ATTRIBUUT_IN_VERBALISATIE(VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE, ATTRIBUUT_NAAM,
	GEBRUIKERSNAAM, ENTITEIT_ID, IS_PRIMARY_IDENTIFIER, IS_MANDATORY)
	VALUES(2, 23, 24, 'fiets_id', 'DaveQ', 2, 1, 1)

	IF OBJECT_ID('[updatenAttribuut].[Expected]','Table') IS NOT NULL
	DROP TABLE updatenAttribuut.[Expected]

	SELECT TOP 0 * 
	INTO updatenAttribuut.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ATTRIBUUT_IN_VERBALISATIE;

	INSERT INTO updatenAttribuut.Expected(GEBRUIKERSNAAM, ATTRIBUUT_NAAM, ENTITEIT_ID,
	VERBALISATIE_ID, IS_PRIMARY_IDENTIFIER, BEGINPOSITIE, EINDPOSITIE, IS_MANDATORY)
	VALUES('BonnoTest', 'post_id', 2, 2, 1, 23, 24, 1)
			  
	exec  usp_updateAttribute 2, 23, 24, 23, 24, 'BonnoTest', 'post_id', 1, 1

	EXEC [tSQLt].[AssertEqualsTable] 'updatenAttribuut.Expected', 'dbo.ATTRIBUUT_IN_VERBALISATIE'
	END

	GO	
	EXEC [tSQLt].[Run] '[updatenAttribuut].[test = Attribuut is geupdate]'
GO