/*
Stored procedure: Inloggen
Overige functionele eis: Gebruiker wilt inloggen

Als bezoeker wil ik kunnen inloggen in het systeem.

De volgende tabellen zijn nodig voor het succesvol inloggen van een 
gebruiker:
-Gebruiker

Acties binnen GEBRUIKER:
-Controleer of gebruikersnaam bestaat in de database
-Controleer of wachtwoorden overeenkomen met elkaar
-Als de credentials met elkaar overeen komen return true

Type constraint: Stored Procedure
Waarom: Er is ervoor gekozen om gebruik te maken van een stored 
procedure bij deze use-case. 

Het is enkel nodig om te controleren of de credentials met elkaar 
overeenkomen. Een groot voordeel van triggers is de mogelijkheid
om gemakkelijk meerdere records te kunnen inserten. 
Dit valt hierdoor weg. Daarom is er voor een stored procedure 
gekozen.

Binnen deze usecase onderneemt de gebruiker 1 stap; Hij geeft zijn 
gebruikersnaam en wachtwoord op waarmee hij zich wil aanmelden in 
het systeem

De volgende fouten kunnen optreden bij deze usecase:
1. Er is geen resultaat voor de combinatie van gebruikersnaam en wachtwoord.
*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_checkUserCredentials')
	BEGIN
		DROP PROC usp_checkUserCredentials
	END
GO
CREATE PROCEDURE usp_checkUserCredentials --Insert een record in projectbezetting.
	@gebruikersnaam varchar(100),
	@wachtwoord varchar(100)
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		DECLARE @CREDENTIALS BIT

		IF NOT EXISTS(SELECT 1 FROM GEBRUIKER where GEBRUIKERSNAAM = @gebruikersnaam)
		BEGIN
			RAISERROR('Er bestaat geen account met deze gebruikersnaam.', 16, 1)
		END

		IF NOT EXISTS(SELECT 1 FROM GEBRUIKER WHERE GEBRUIKERSNAAM = @gebruikersnaam AND WACHTWOORD = @wachtwoord)
		BEGIN
			RAISERROR('Het ingevoerde wachtwoord is onjuist.', 16, 1)
		END

		select count(*) as userExists from GEBRUIKER where GEBRUIKERSNAAM = @gebruikersnaam and WACHTWOORD = @wachtwoord

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Er is geen record van de ingevoerde gebruikersnaam

-Het ingevoerde wachtwoord komt niet overeen met het wachtwoord uit de database

-De gebruikersnaam en wachtwoord komen met elkaar overeen(Gebruiker is ingelogd)
*/

EXEC tSQLt.NewTestClass 'inloggenGebruiker';
GO

/* ================= 
    Gebruiker bestaat niet
==================*/
CREATE PROCEDURE inloggenGebruiker.[test = gebruiker bestaat niet]  
AS
BEGIN
--Assemble

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'DitIsEenMoeilijkeWachtwoord'), ('LucianoTest', '12345')
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er bestaat geen account met deze gebruikersnaam.'

--Act
    exec  usp_checkUserCredentials 'DaveTest', '1234'

END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[inloggenGebruiker].[test = gebruiker bestaat niet]'
GO

/* =================
	Wachtwoorden komen niet met elkaar overeen 
==================*/
CREATE PROCEDURE inloggenGebruiker.[test = foutieve wachtwoord]  
AS
BEGIN
--Assemble

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'DitIsEenMoeilijkeWachtwoord'), ('LucianoTest', '12345')
		  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Het ingevoerde wachtwoord is onjuist.'

--Act
    exec  usp_checkUserCredentials 'BonnoTest', '1234'

END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[inloggenGebruiker].[test = foutieve wachtwoord]'
GO

/* =================
	Gebruiker is ingelogd
==================*/
CREATE PROCEDURE inloggenGebruiker.[test = ingelogd]  
AS
BEGIN
--Assemble

	DECLARE @result INT
	DECLARE @expected INT;

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('DaveQ', 'qweasdzxc')

	SELECT @result = count(*) FROM dbo.GEBRUIKER where GEBRUIKERSNAAM = 'DaveQ' and WACHTWOORD = 'qweasdzxc'

	SET @expected = 1;
		  
--Act
exec  usp_checkUserCredentials  'DaveQ', 'qweasdzxc'

--Assert
EXEC [tSQLt].[AssertEquals] @expected, @result
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[inloggenGebruiker].[test = ingelogd]'
GO