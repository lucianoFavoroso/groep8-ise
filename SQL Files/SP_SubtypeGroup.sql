/*
Eniteit mag niet in een subtype groep zitten wanneer hij zelf de parent is
De parent wordt meegegeven door middel van een parameter.

Stored procedure: Subtypegroep aanmaken
Use case: Subtypegroep aanmaken

Als gebruiker wil ik subtypes uit mijn project halen en deze bij de juiste
groepering neerzetten. Dit steld mij in staat subtypes te koppelen aan een
groep van subtypes.

Create
Voor het aanmaken van een subtypegroep binnen een project, worden er parameters meegegeven.
Met deze parameters kan er een nieuwe subtypegroep aangemaakt worden.

De volgende tabellen zijn nodig voor het succesvol benoemen van een subtypegroep:
-ENTITEIT_IN_PROJECT
-SUBTYPE_GROEP

Acties binnen de tabel SUBTYPE_GROEP:
Stap 1: Controleer of de combinatie van de meegegeven supertype @parent_entiteit en 
		@groepsnaam al een groep heeft, als dit het geval is sla de insert van een 
		nieuwe groep over.
Stap 2: Bestaat de groep nog niet voeg de groep toe aan de subtypegroep.
Stap 3: Haal insert op door middel van de parent_entiteit en sla subtype_id op
Stap 4: Controleer of de subtype een parent van de groep is, als dit het 
		geval is kan de subtype niet worden toegevoegd aan de groep.
Stap 5: Als de subtype een parent is, laat een error zien
Stap 6: Is de subtype geen parent, doe een update in ENTITEIT_IN_PROJECT 
		met de opgehaalde subtype_id

Update
Stap 1: Controleer of de meegegeven supertype @parent_entiteit nog geen groep heeft,
		laat dan een error zien.
Stap 2: Update groepsnaam, mutually_exclusive en complete

De volgende tabellen zijn nodig voor het succesvol updaten van een Subtypegroep:
-SUBTYPE_GROEP

Delete
Beinvloede tabellen: ENTITEIT_IN_PROJECT, SUBTYPE_GROEP

Read
Beinvloede tabellen: ENTITEIT_IN_PROJECT, SUBTYPE_GROEP
*/

/*====================
CREATE
======================*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_insertSubtypeGroup')
	BEGIN
		DROP PROC usp_insertSubtypeGroup
	END
GO
CREATE PROCEDURE usp_insertSubtypeGroup --Insert een record in SUBTYPE_GROUP
	@parent_entiteit int,
	@entiteit int,
	@gebruikersnaam varchar(100),
	@groepsnaam varchar(100),
	@mutually_exclusive smallint,
	@complete smallint
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		/*
		Stap 1: Controleer of de meegegeven supertype @parent_entiteit al een groep heeft, 
		als dit het geval is sla de insert van een nieuwe groep over.
		Stap 2: Bestaat de groep nog niet voeg de groep toe aan de subtypegroep.
		*/
		IF NOT EXISTS(SELECT 1 FROM SUBTYPE_GROEP WHERE PARENT_ENTITEIT = @parent_entiteit and GROEPSNAAM = @groepsnaam)
		BEGIN
			INSERT INTO SUBTYPE_GROEP(PARENT_ENTITEIT, GEBRUIKERSNAAM, GROEPSNAAM, MUTUALLY_EXCLUSIVE, COMPLETE) 
			VALUES (@parent_entiteit, @gebruikersnaam, @groepsnaam, @mutually_exclusive, @complete)
		END

		/*
		Stap 3: Haal insert op door middel van de parent_entiteit en sla subtype_id op
		*/
		DECLARE @SUBTYPEID int
		SELECT @SUBTYPEID = SUBTYPE_ID FROM SUBTYPE_GROEP WHERE PARENT_ENTITEIT = @parent_entiteit and GROEPSNAAM = @groepsnaam

		/*
		Stap 4: Controleer of de @entiteit een parent van de groep is, als dit het 
				geval is kan de subtype niet worden toegevoegd aan de groep.
		Stap 5: Als de subtype een parent is, laat een error zien
		*/
		IF NOT EXISTS(SELECT ENTITEIT_ID from ENTITEIT_IN_PROJECT where ENTITEIT_ID = @entiteit
		EXCEPT SELECT ENTITEIT_ID from ENTITEIT_IN_PROJECT where ENTITEIT_ID in(
		SELECT PARENT_ENTITEIT as ENTITEIT_ID from SUBTYPE_GROEP where PARENT_ENTITEIT = @parent_entiteit))
		BEGIN
			RAISERROR('De meegegeven entiteit, is een subtype parent', 16, 1)
		END

		/*
		Stap 6: Is de subtype geen parent, doe een update in ENTITEIT_IN_PROJECT 
		met de opgehaalde subtype_id
		*/
		UPDATE ENTITEIT_IN_PROJECT SET SUBTYPE_ID = @SUBTYPEID where ENTITEIT_ID = @entiteit
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*====================
UPDATE
======================*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_updateSubtypeGroup')
	BEGIN
		DROP PROC usp_updateSubtypeGroup
	END
GO
CREATE PROCEDURE usp_updateSubtypeGroup --Update subtypegroep
	@subtypegroep_id int,
	@gebruikersnaam varchar(100),
	@groepsnaam varchar(100),
	@mutually_exclusive smallint,
	@complete smallint
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		/*
		Stap 1: Controleer of de meegegeven supertype @parent_entiteit nog geen groep heeft,
		laat dan een error zien.
		*/
		IF NOT EXISTS(SELECT 1 FROM SUBTYPE_GROEP WHERE SUBTYPE_ID = @subtypegroep_id)
		BEGIN
			RAISERROR('De suptypegroep bestaat nog niet', 16, 1)
		END

		/*
		Stap 2: Update gebruikersnaam, groepsnaam, mutually_exclusive en complete
		*/
		UPDATE SUBTYPE_GROEP SET GEBRUIKERSNAAM = @gebruikersnaam, GROEPSNAAM = @groepsnaam, MUTUALLY_EXCLUSIVE = @mutually_exclusive, COMPLETE = @complete where SUBTYPE_ID = @subtypegroep_id
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*===========
UNIT-TESTS CREATE:
Er zullen tests aangemaakt worden voor de volgende gevallen:

-De meegegeven subtype is de parent

-Maakt een nieuwe subtype_groep aan

-Voegt subtype_id toe aan ENTITIET_IN_PROJECT
==============*/
EXEC tSQLt.NewTestClass 'addSubtypeGroup';
GO

/* ================= 
	Test scenario 1: De meegegeven subtype is de parent
==================*/
CREATE PROCEDURE [addSubtypeGroup].[test = subtype is de parent]  
AS
BEGIN
	EXEC tSQLt.FakeTable 'dbo', 'SUBTYPE_GROEP';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT';

	INSERT INTO dbo.ENTITEIT_IN_PROJECT(ENTITEIT_ID, SUBTYPE_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
	VALUES (1, null, 1, 'Dave', 'EL_Fiets')

	EXEC tSQLt.ExpectException @ExpectedMessage = 'De meegegeven entiteit, is een subtype parent'
	
	exec usp_insertSubtypeGroup 1,1, 'Dave', 'Groep', 1, 1
	END

	GO	
	EXEC [tSQLt].[Run] '[addSubtypeGroup].[test = subtype is de parent]'
GO

/* ================= 
	Test scenario 2: Maakt een nieuwe subtype_groep aan
==================*/
CREATE PROCEDURE [addSubtypeGroup].[test = maak nieuwe subtype_groep aan]  
AS
BEGIN
	IF OBJECT_ID('[addSubtypeGroup].[Expected]','Table') IS NOT NULL
	DROP TABLE [addSubtypeGroup].[Expected]

	SELECT TOP 0 * 
	INTO addSubtypeGroup.Expected --snelle manier om een structuur te "kopi�ren"
	FROM SUBTYPE_GROEP;

	SET IDENTITY_INSERT addSubtypeGroup.Expected ON

	INSERT INTO addSubtypeGroup.Expected(SUBTYPE_ID, PARENT_ENTITEIT, GEBRUIKERSNAAM, GROEPSNAAM, MUTUALLY_EXCLUSIVE, COMPLETE)
    VALUES (1, 4, 'Dave', 'Groep', 1, 1)

	EXEC tSQLt.FakeTable 'dbo', 'SUBTYPE_GROEP', @IDENTITY = 1;

	exec usp_insertSubtypeGroup 4, 2, 'Dave', 'Groep', 1, 1

	EXEC [tSQLt].[AssertEqualsTable] 'addSubtypeGroup.Expected', 'dbo.SUBTYPE_GROEP'
	END

	GO	
	EXEC [tSQLt].[Run] '[addSubtypeGroup].[test = maak nieuwe subtype_groep aan]'
GO

/* ================= 
	Test scenario 3: Subtype toevoegen aan bestaande groep
==================*/
CREATE PROCEDURE [addSubtypeGroup].[test = subtype toevoegen]  
AS
BEGIN
	IF OBJECT_ID('[addSubtypeGroup].[Expected]','Table') IS NOT NULL
	DROP TABLE [addSubtypeGroup].[Expected]

	SELECT TOP 0 * 
	INTO addSubtypeGroup.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ENTITEIT_IN_PROJECT;

	EXEC tSQLt.FakeTable 'dbo', 'SUBTYPE_GROEP', @IDENTITY = 1;
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT';

	SET IDENTITY_INSERT addSubtypeGroup.Expected ON

	INSERT INTO addSubtypeGroup.Expected(ENTITEIT_ID, SUBTYPE_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
    VALUES (2, 1, 1, 'Dave', 'FIETS')

	INSERT INTO dbo.ENTITEIT_IN_PROJECT(ENTITEIT_ID, SUBTYPE_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
    VALUES (2, null, 1, 'Dave', 'FIETS')

	exec usp_insertSubtypeGroup 1, 2, 'Dave', 'FIETS', 1, 1

	EXEC [tSQLt].[AssertEqualsTable] 'addSubtypeGroup.Expected', 'dbo.ENTITEIT_IN_PROJECT'
	END

	GO	
	EXEC [tSQLt].[Run] '[addSubtypeGroup].[test = subtype toevoegen]'
GO


/*===========
UNIT-TESTS UPDATE:
Er zullen tests aangemaakt worden voor de volgende gevallen:

-De subtypegroep bestaat niet

-Update subtypegroep
==============*/
EXEC tSQLt.NewTestClass 'updateSubtypeGroup';
GO

/* ================= 
	Test scenario 4: De subtypegroep bestaat niet
==================*/
CREATE PROCEDURE [updateSubtypeGroup].[test = de subtypegroep bestaat niet]  
AS
BEGIN

	EXEC tSQLt.FakeTable 'dbo', 'SUBTYPE_GROEP';

	EXEC tSQLt.ExpectException @ExpectedMessage = 'De suptypegroep bestaat nog niet'

	exec usp_updateSubtypeGroup 1, 'Dave', 'ET_FIETS', 1, 0

	END

	GO	
	EXEC [tSQLt].[Run] '[updateSubtypeGroup].[test = de subtypegroep bestaat niet]'
GO

/* ================= 
	Test scenario 5: Subtype geupdate
==================*/
CREATE PROCEDURE [updateSubtypeGroup].[test = subtype geupdate]  
AS
BEGIN
	IF OBJECT_ID('[updateSubtypeGroup].[Expected]','Table') IS NOT NULL
	DROP TABLE [updateSubtypeGroup].[Expected]

	SELECT TOP 0 * 
	INTO updateSubtypeGroup.Expected --snelle manier om een structuur te "kopi�ren"
	FROM SUBTYPE_GROEP;

	SET IDENTITY_INSERT updateSubtypeGroup.Expected ON

	EXEC tSQLt.FakeTable 'dbo', 'SUBTYPE_GROEP', @IDENTITY = 1;

	INSERT INTO updateSubtypeGroup.Expected(SUBTYPE_ID, PARENT_ENTITEIT, GEBRUIKERSNAAM, GROEPSNAAM, MUTUALLY_EXCLUSIVE, COMPLETE)
    VALUES (1, 5, 'Bonno', 'FIETS', 1, 0)

	INSERT INTO dbo.SUBTYPE_GROEP(PARENT_ENTITEIT, GEBRUIKERSNAAM, GROEPSNAAM, MUTUALLY_EXCLUSIVE, COMPLETE)
	values(5, 'Dave', 'ET_FIETS', 1, 1)
		
	exec usp_updateSubtypeGroup 1, 'Bonno', 'FIETS', 1, 0

	EXEC [tSQLt].[AssertEqualsTable] 'updateSubtypeGroup.Expected', 'dbo.SUBTYPE_GROEP'
	END

	GO	
	EXEC [tSQLt].[Run] '[updateSubtypeGroup].[test = subtype geupdate]'
GO