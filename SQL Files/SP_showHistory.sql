/*
Overige functionele eis: Overzicht history
History wordt bijgehouden voor de gehele database doormiddel van schaduw tabbelen.
Per project moet het mogelijk zijn een history op te vragen van alle veranderingen binnen dit project.
Het overzicht is gesorteerd op datum zodat de meest recenten veranderingen bovenaan staan.
*/

use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getHistory')
	BEGIN
		DROP PROC usp_getHistory
	END
GO
CREATE PROCEDURE usp_getHistory
	@projectID INT
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
		SELECT AV.GEBRUIKERSNAAM,AV.changeTime,AV.ATTRIBUUT_NAAM as newValue,
		(SELECT TOP(1) AV2.ATTRIBUUT_NAAM FROM hist_ATTRIBUUT_IN_VERBALISATIE AV2 WHERE AV2.changeTime < AV.changeTime
		AND AV2.VERBALISATIE_ID = AV.VERBALISATIE_ID ORDER BY AV2.changeTime DESC) as oldValue,
		AV.actie, 'Attribuut' as tableName
		FROM hist_ATTRIBUUT_IN_VERBALISATIE AV INNER JOIN hist_VERBALISATIE V ON AV.VERBALISATIE_ID = V.VERBALISATIE_ID
		AND V.PROJECT_ID = @projectID
		UNION ALL
		SELECT HV.GEBRUIKERSNAAM,HV.changeTime,HV.VERBALISATIE_ZIN as newValue,
		(SELECT TOP(1) HV2.VERBALISATIE_ZIN as oldValue FROM hist_VERBALISATIE HV2 WHERE HV2.changeTime<HV.changeTime
		AND HV2.VERBALISATIE_ID = HV.VERBALISATIE_ID AND HV2.PROJECT_ID = @projectID ORDER BY HV2.changeTime DESC) as oldValue,
		HV.actie,'Verbalisatie' as tableName
		FROM hist_VERBALISATIE HV
		WHERE HV.PROJECT_ID = @projectID
		UNION ALL
		SELECT V.GEBRUIKERSNAAM,V.changeTime,V.ENTITEIT_NAAM as newValue,
	    (SELECT TOP(1) V2.ENTITEIT_NAAM FROM hist_ENTITEIT_IN_PROJECT V2 
		WHERE V2.changeTime<V.changeTime
		AND V2.PROJECT_ID = V.PROJECT_ID ORDER BY V2.changeTime DESC) as oldValue,V.actie, 'Entiteit' as tableName
		FROM hist_ENTITEIT_IN_PROJECT V WHERE V.PROJECT_ID = @projectID
		ORDER BY changeTime DESC
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
