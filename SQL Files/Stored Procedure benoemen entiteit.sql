/*
Stored procedure: Benoemen entiteit van verbalisatie (insert)
Use case: Benoemen entiteit

Wanneer de gebruiker een verbalisatie aan het analyseren is kan hij ervoor kiezen om een entiteit te benoemen bij deze verbalisatie.

De volgende tabellen zijn nodig voor het succesvol benoemen van een entiteit bij een verbalisatie:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Attribuut_In_Verbalisatie
-Projectbezetting

Acties binnen Entiteit_In_Project:
-Als de MATCH checkbox is aangevinkt een select doen om te kijken of er een ander entiteit is met een ID binnen dit project.
-Als de MATCH checkbox niet is aangevinkt, insert de nieuwe entiteit anders geen acties.

Acties binnen Entiteit_In_Verbalisatie:
-Select om te kijken of er al een entiteit is binnen de geselecteerde verbalisatie met dezelfde naam.
-Insert de nieuwe entiteit bij de geselecteerde verbalisatie.

Type constraint: Stored Procedure
Waarom: Dit omdat de handeling makkelijk aangeroepen kan worden.

De gebruiker onderneemt 1 stap binnen deze usecase. Dat is het benoemen van de entiteit en daarnaast aangeven welke of dit een MATCH en/of subtype is.

De volgende fouten kunnen optreden bij deze usecase:
1. Er is een andere entiteit met dezelfde naam binnen de huidige verbalisatie.
2. De MATCH checkbox is aangevinkt maar er is geen andere entiteit met dezelfde naam en een ID.

De volgorde waarin op deze problemen gecontroleerd gaan worden is als volgt:
1, 2.
*/
use FactBasedAnalysis
GO
IF EXISTS(SELECT * FROM sys.procedures WHERE name=N'usp_addEntity')
	BEGIN
		DROP PROC usp_addEntity
	END
GO
CREATE PROCEDURE usp_addEntity
	@project_id int,
	@verbalisatie_id int,
	@gebruikersnaam varchar(20),
	@entiteit_naam varchar(50),
	@is_match bit
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
	IF NOT EXISTS (SELECT 1 FROM PROJECTBEZETTING WHERE GEBRUIKERSNAAM = @gebruikersnaam AND PROJECT_ID = @project_id)
	BEGIN
		RAISERROR('De gebruiker maakt geen deel uit van dit project.', 16, 1)
	END

	IF exists(
		SELECT 1
		FROM ENTITEIT_IN_PROJECT EP inner join ENTITEIT_IN_VERBALISATIE EV
		ON EP.ENTITEIT_ID = EV.ENTITEIT_ID
		WHERE EP.ENTITEIT_NAAM = @entiteit_naam and EV.VERBALISATIE_ID = @verbalisatie_id
	)
	BEGIN
		RAISERROR('Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.', 16, 1)
	END

	IF @is_match = 1 IF NOT EXISTS(
		SELECT 1
		FROM ENTITEIT_IN_PROJECT EP inner join ATTRIBUUT_IN_VERBALISATIE AV
		ON EP.ENTITEIT_ID = AV.ENTITEIT_ID
		WHERE EP.ENTITEIT_NAAM = @entiteit_naam and AV.IS_PRIMARY_IDENTIFIER = 1 and EP.PROJECT_ID = @project_id
	)
	BEGIN
		RAISERROR('De benoemde entiteit heeft geen primary identifer.', 16, 1)
	END

	IF EXISTS (SELECT 1 FROM ENTITEIT_IN_PROJECT WHERE PROJECT_ID = @project_id AND ENTITEIT_NAAM = @entiteit_naam) 
	BEGIN
		INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH) VALUES
		((SELECT ENTITEIT_ID FROM ENTITEIT_IN_PROJECT WHERE PROJECT_ID = @project_id AND ENTITEIT_NAAM = @entiteit_naam), @verbalisatie_id, @gebruikersnaam, @is_match)
	END
	ELSE
	BEGIN
		INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES
		(@project_id, @gebruikersnaam, @entiteit_naam)

		INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH) VALUES
		((SELECT ENTITEIT_ID FROM ENTITEIT_IN_PROJECT WHERE PROJECT_ID = @project_id AND ENTITEIT_NAAM = @entiteit_naam), @verbalisatie_id, @gebruikersnaam, @is_match)
	END

	--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Alles klopt

-Gebruiker is niet deel van het huidige project.

-Er is een andere entiteit met dezelfde naam binnen de huidige verbalisatie.

-MATCH is aangevinkt ondanks dat er een entiteit is met dezelfde naam zonder ID.
*/

EXEC tSQLt.NewTestClass 'benoemenEntiteit';
GO
/* ================= 
    Alles klopt
==================*/
CREATE PROCEDURE [benoemenEntiteit].[test = alles klopt]
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT  

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_addEntity 1, 1, 'JacquesTest', 'Entiteit1', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[benoemenEntiteit].[test = alles klopt]'
GO

/* ================= 
	Gebuiker niet in project
==================*/
CREATE PROCEDURE [benoemenEntiteit].[test = gebruiker niet in project]
AS
BEGIN
--Assemble
	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

	--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De gebruiker maakt geen deel uit van dit project.'

	--Act
	exec usp_addEntity 1, 1, 'JacquesFout', 'Entiteit1', 0

END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[benoemenEntiteit].[test = gebruiker niet in project]'
GO

/* ==================================
Zelfde naam entiteit binnen verbalisatie
===================================*/
CREATE PROCEDURE [benoemenEntiteit].[test = zelfde naam]  
AS
BEGIN
--Assmble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ENTITEIT_IN_VERBALISATIE(ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.';

--Act
	exec usp_addEntity 1, 1, 'JacquesTest', 'Entiteit1', 0
END

GO
EXEC [tSQLt].[Run] '[benoemenEntiteit].[test = zelfde naam]'
GO

/* ==================================
MATCH zonder andere ID
===================================*/
CREATE PROCEDURE [benoemenEntiteit].[test = MATCH zonder ID]  
AS
BEGIN
--Assmble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ENTITEIT_IN_VERBALISATIE(ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 2, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ATTRIBUUT_IN_VERBALISATIE(ENTITEIT_ID, GEBRUIKERSNAAM, ATTRIBUUT_NAAM, VERBALISATIE_ID, IS_PRIMARY_IDENTIFIER, BEGINPOSITIE, EINDPOSITIE)
	VALUES (1, 'JacquesTest', 'AttribuutTest', 2, 0, 5, 5)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De benoemde entiteit heeft geen primary identifer.';

--Act
	exec usp_addEntity 1, 1, 'JacquesTest', 'Entiteit2', 1
END

GO
EXEC [tSQLt].[Run] '[benoemenEntiteit].[test = MATCH zonder ID]'
GO



/*
Stored procedure: Bijwerken van entiteit (Update)
Use case: Benoemen entiteit

De gebruiker kan tijdens de analyse ervoor kiezen om een entiteit aan te passen.

De volgende tabellen zijn nodig voor het succesvol aanpassen van een entiteit bij een verbalisatie:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Projectbezetting

Acties binnen Entiteit_In_Project:
-Als de naam aangepast is een select doen om te kijken of de naam al bestaat.

Acties binnen Entiteit_In_Verbalisatie:
-Select om te kijken of er al een entiteit is binnen de geselecteerde verbalisatie met dezelfde naam.
-Als de checkbox aangevinkt is wanneer dat niet zo was moet gekeken worden of er al een entiteit bestaat met een ID.

Type constraint: Stored Procedure
Waarom: Dit omdat de handeling makkelijk aangeroepen kan worden. 

De gebruiker onderneemt 1 stap binnen deze usecase. Dat is het benoemen van de entiteit en daarnaast aangeven welke of dit een MATCH en/of subtype is.

De volgende fouten kunnen optreden bij deze usecase:
1. Gebruiker is niet deel van het huidige project.
2. Er is niks aangepast.
3. Er is een andere entiteit met dezelfde naam binnen de huidige verbalisatie.
4. De MATCH checkbox is aangevinkt maar er is geen andere entiteit met dezelfde naam en een ID.

De volgorde waarin op deze problemen gecontroleerd gaan worden is als volgt:
1, 2, 3.
*/
use FactBasedAnalysis
GO
IF EXISTS(SELECT * FROM sys.procedures WHERE name=N'usp_alterEntity')
	BEGIN
		DROP PROC usp_alterEntity
	END
GO
CREATE PROCEDURE usp_alterEntity
	@project_id int,
	@verbalisatie_id int,
	@gebruikersnaam varchar(20),
	@entiteit_id int,
	@entiteit_naam varchar(50),
	@is_match bit
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY

	IF NOT EXISTS (SELECT 1 FROM PROJECTBEZETTING WHERE GEBRUIKERSNAAM = @gebruikersnaam AND PROJECT_ID = @project_id)
	BEGIN
		RAISERROR('De gebruiker maakt geen deel uit van dit project.', 16, 1)
	END

	DECLARE @current_entiteit_naam varchar(50)
	DECLARE @current_is_match bit
	DECLARE @new_entiteit_id int

	SET @current_entiteit_naam = (
		SELECT entiteit_naam 
		FROM ENTITEIT_IN_PROJECT
		WHERE ENTITEIT_ID = @entiteit_id)
	
	SET @current_is_match = (
		SELECT IS_MATCH
		FROM ENTITEIT_IN_VERBALISATIE
		WHERE ENTITEIT_ID = @entiteit_id 
		AND VERBALISATIE_ID = @verbalisatie_id)
		
	IF (@current_entiteit_naam = @entiteit_naam) AND (@current_is_match	= @is_match)
	BEGIN 	
		RAISERROR('Er zijn geen aanpassingen gemaakt.', 16, 1)
	END	
		
	IF @current_entiteit_naam != @entiteit_naam	
	BEGIN				 
		IF exists(
			SELECT 1
			FROM ENTITEIT_IN_PROJECT EP inner join ENTITEIT_IN_VERBALISATIE EV
			ON EP.ENTITEIT_ID = EV.ENTITEIT_ID
			WHERE EP.ENTITEIT_NAAM = @entiteit_naam and EV.VERBALISATIE_ID = @verbalisatie_id
		)
		BEGIN
			RAISERROR('Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.', 16, 1)
		END
	END

	IF @current_is_match != @is_match
	BEGIN
		IF @is_match = 1 AND not exists(
			SELECT 1
			FROM ENTITEIT_IN_PROJECT EP inner join ATTRIBUUT_IN_VERBALISATIE AV
			ON EP.ENTITEIT_ID = AV.ENTITEIT_ID
			WHERE EP.ENTITEIT_NAAM = @entiteit_naam and AV.IS_PRIMARY_IDENTIFIER = 1 and EP.PROJECT_ID = @project_id
		)
		BEGIN
			RAISERROR('De benoemde entiteit heeft geen primary identifer.', 16, 1)
		END
	END

	IF @current_is_match != @is_match
	BEGIN
		UPDATE ENTITEIT_IN_VERBALISATIE SET IS_MATCH = @is_match, GEBRUIKERSNAAM = @gebruikersnaam
	END
	
	IF @current_entiteit_naam != @entiteit_naam	
	BEGIN
		IF ((SELECT COUNT(*) FROM ENTITEIT_IN_VERBALISATIE WHERE ENTITEIT_ID = @entiteit_id) = 1 AND NOT exists (SELECT 1 FROM ENTITEIT_IN_PROJECT WHERE ENTITEIT_NAAM = @entiteit_naam AND PROJECT_ID = @project_id))
		BEGIN
			UPDATE ENTITEIT_IN_PROJECT SET ENTITEIT_NAAM = @entiteit_naam, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_ID = @entiteit_id
		END
		ELSE
		BEGIN
			IF exists (SELECT 1 FROM ENTITEIT_IN_PROJECT WHERE ENTITEIT_NAAM = @entiteit_naam AND PROJECT_ID = @project_id)
			BEGIN
				SELECT @new_entiteit_id = ENTITEIT_ID FROM ENTITEIT_IN_PROJECT WHERE ENTITEIT_NAAM = @entiteit_naam AND PROJECT_ID = @project_id
				UPDATE ENTITEIT_IN_VERBALISATIE SET ENTITEIT_ID = @new_entiteit_id, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_ID = @entiteit_id AND VERBALISATIE_ID = @verbalisatie_id
			END
			ELSE
			BEGIN
				INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
				VALUES (@project_id, @gebruikersnaam, @entiteit_naam)

				SET @new_entiteit_id = IDENT_CURRENT('ENTITEIT_IN_PROJECT')
				UPDATE ENTITEIT_IN_VERBALISATIE SET ENTITEIT_ID = @new_entiteit_id, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_ID = @entiteit_id AND VERBALISATIE_ID = @verbalisatie_id
			END
			
			IF exists (SELECT 1 FROM ROL RO INNER JOIN RELATIE RE on RO.RELATIE_ID = RE.RELATIE_ID WHERE (ENTITEIT_VAN = @entiteit_id or ENTITEIT_NAAR = @entiteit_id) and RE.VERBALISATIE_ID = @verbalisatie_id)
			BEGIN
				UPDATE ROL SET ENTITEIT_VAN = @new_entiteit_id, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_VAN = @entiteit_id AND RELATIE_ID IN (SELECT RELATIE_ID FROM RELATIE WHERE VERBALISATIE_ID = @verbalisatie_id)
				UPDATE ROL SET ENTITEIT_NAAR = @new_entiteit_id, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_NAAR = @entiteit_id AND RELATIE_ID IN (SELECT RELATIE_ID FROM RELATIE WHERE VERBALISATIE_ID = @verbalisatie_id)
			END

			IF exists (SELECT 1 FROM ATTRIBUUT_IN_VERBALISATIE WHERE ENTITEIT_ID = @entiteit_id AND VERBALISATIE_ID = @verbalisatie_id)
			BEGIN
				UPDATE ATTRIBUUT_IN_VERBALISATIE SET ENTITEIT_ID = @new_entiteit_id, GEBRUIKERSNAAM = @gebruikersnaam WHERE ENTITEIT_ID = @entiteit_id AND VERBALISATIE_ID = @verbalisatie_id
			END
		END
	END

	--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-De gebruiker is niet deel van het huidige project.

-Er zijn geen aanpassingen gemaakt.

-Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.

-De benoemde entiteit heeft geen primary identifier bij het aanvinken van is_match.

-Deze entiteit is alleen berschreven in deze verbalisatie en er zijn geen andere entiteiten met dezelfde nieuwe naam.

-Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die al bestaat.

-Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die nog niet bestaat.

-De naam wordt aangepast en er is een rol gekoppeld aan het veranderde entiteit binnen de verbalisatie.

-De naam wordt aangepast en er is een attribuut gekoppend aan het veranderde entiteit binnen de verbalisatie.
*/

EXEC tSQLt.NewTestClass 'aanpassenEntiteit';
GO

/* ================= 
Gebuiker niet in project
==================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = gebruiker niet in project]
AS
BEGIN
--Assemble
	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

	--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De gebruiker maakt geen deel uit van dit project.'

	--Act
	exec usp_addEntity 1, 1, 'JacquesFout', 'Entiteit1', 0

END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = gebruiker niet in project]'
GO

/* ==================================
Er zijn geen aanpassingen gemaakt.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = geen aanpassingen]  
AS
BEGIN
--Assmble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ENTITEIT_IN_VERBALISATIE(ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er zijn geen aanpassingen gemaakt.';

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit1', 0
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = geen aanpassingen]'
GO

/* ==================================
Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = duplicatie naam in verbalisatie]  
AS
BEGIN
--Assmble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ENTITEIT_IN_VERBALISATIE(ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (2, 1, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er is al een andere entiteit met dezelfde naam binnen deze verbalisatie.';

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 2, 'Entiteit1', 1
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = duplicatie naam in verbalisatie]'
GO

/* ==================================
De benoemde entiteit heeft geen primary identifier bij het aanvinken van is_match.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = is_match niet gevonden]  
AS
BEGIN
--Assmble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	INSERT INTO ENTITEIT_IN_PROJECT(PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	INSERT INTO ENTITEIT_IN_VERBALISATIE(ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 2, 'JacquesTest', 0), (2, 1, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De benoemde entiteit heeft geen primary identifer.';

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 2, 'Entiteit1', 1
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = is_match niet gevonden]'
GO

/* ==================================
Deze entiteit is alleen berschreven in deze verbalisatie en er zijn geen andere entiteiten met dezelfde nieuwe naam.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = enige entiteit naam aanpassen]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1') 

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 1)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit2', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = enige entiteit naam aanpassen]'
GO

/* ==================================
Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die al bestaat.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = naam van meerdere verbalisatie veranderd naar een naam die al bestaat in een project.]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (2, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit2', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = naam van meerdere verbalisatie veranderd naar een naam die al bestaat in een project.]'
GO

/* ==================================
Deze entiteit is beschreven in andere verbalisaties en de naam is veranderd naar een naam die nog niet bestaat.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = naam van meerdere verbalisatie veranderd naar een naam die niet bestaat in een project.]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (2, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit2', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = naam van meerdere verbalisatie veranderd naar een naam die niet bestaat in een project.]'
GO

/* ==================================
De naam wordt aangepast en er is een rol gekoppeld aan het veranderde entiteit binnen de verbalisatie.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = De naam wordt aangepast en er is een rol gekoppeld aan het veranderde entiteit binnen de verbalisatie.]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2'), (1, 'JacquesTest', 'Entiteit3')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (2, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	--RELATIE
	EXEC tSQLt.FakeTable 'dbo', 'RELATIE', @Identity = 1;

	INSERT INTO RELATIE (VERBALISATIE_ID, RELATIE_NAAM)
	VALUES (1, 'testRelatie')

	--ROL
	EXEC tSQLt.FakeTable 'dbo', 'ROL', @Identity = 1; 

	SELECT * INTO expectedRol FROM ROL

	INSERT INTO ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID)
	VALUES (1, 2, 1), (2, 1, 1)

	INSERT INTO expectedRol (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM)
	VALUES (3, 2, 1, 'JacquesTest'), (2, 3, 1, 'JacquesTest')

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit3', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedRol', 'dbo.ROL'
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = De naam wordt aangepast en er is een rol gekoppeld aan het veranderde entiteit binnen de verbalisatie.]'
GO

/* ==================================
De naam wordt aangepast en er is een attribuut gekoppend aan het veranderde entiteit binnen de verbalisatie.
===================================*/
CREATE PROCEDURE [aanpassenEntiteit].[test = De naam wordt aangepast en er is een attribuut gekoppend aan het veranderde entiteit binnen de verbalisatie.]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1'), (1, 'JacquesTest', 'Entiteit2')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (2, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedAttribuutInVerbalisatie FROM Attribuut_IN_VERBALISATIE

	INSERT INTO ATTRIBUUT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID)
	VALUES (1, 1)

	INSERT INTO expectedAttribuutInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM)
	VALUES (2, 1, 'JacquesTest')

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_alterEntity 1, 1, 'JacquesTest', 1, 'Entiteit2', 0

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedAttribuutInVerbalisatie', 'dbo.ATTRIBUUT_IN_VERBALISATIE'
END

GO
EXEC [tSQLt].[Run] '[aanpassenEntiteit].[test = De naam wordt aangepast en er is een attribuut gekoppend aan het veranderde entiteit binnen de verbalisatie.]'
GO

/*
Stored procedure: Benoemen entiteit van verbalisatie (Delete)
Use case: Benoemen entiteit

Wanneer de gebruiker een verbalisatie aan het analyseren is kan hij ervoor kiezen om een entiteit te verwijderen bij deze verbalisatie.

De volgende tabellen zijn nodig voor het succesvol verwijderen van een entiteit bij een verbalisatie:
-Entiteit_In_Project
-Entiteit_In_Verbalisatie
-Attribuut_In_Verbalisatie
-Relatie
-Rol

Acties binnen Entiteit_In_Project:
-Als deze entiteit alleen in de huidige verbalisatie beschreven is moet deze verwijderd worden.

Acties binnen Entiteit_In_Verbalisatie:
-Verwijder de row met de entiteit met binnen de huidige verbalisatie.

Acties binnen Attribuut_In_Verbalisatie
-Als aanwezig, moet de attribuut verwijdert worden bij de entiteit, bij de huidige verbalisatie

Acties binnen Rol
-Als aanwezig, moeten rollen verwijdert worden bij de entiteit, bij de huidige verbalisatie

Type constraint: Stored Procedure
Waarom: Dit omdat de handeling makkelijk aangeroepen kan worden. 

De gebruiker onderneemt 1 stap binnen deze usecase. Dat is aangeven welke entiteit hij wil verwijderen.
*/
use FactBasedAnalysis
GO
IF EXISTS(SELECT * FROM sys.procedures WHERE name=N'usp_deleteEntity')
	BEGIN
		DROP PROC usp_deleteEntity
	END
GO
CREATE PROCEDURE usp_deleteEntity
	@project_id int,
	@verbalisatie_id int,
	@gebruikersnaam varchar(20),
	@entiteit_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY

	IF NOT EXISTS (SELECT 1 FROM PROJECTBEZETTING WHERE GEBRUIKERSNAAM = @gebruikersnaam AND PROJECT_ID = @project_id)
	BEGIN
		RAISERROR('De gebruiker maakt geen deel uit van dit project.', 16, 1)
	END

	IF EXISTS (SELECT 1 FROM RELATIE WHERE VERBALISATIE_ID = @verbalisatie_id)
	BEGIN
		DECLARE @relatie_id int
		SELECT @relatie_id = RELATIE_ID FROM RELATIE WHERE VERBALISATIE_ID = @verbalisatie_id
		DELETE FROM ROL WHERE RELATIE_ID = @relatie_id
		DELETE FROM RELATIE WHERE RELATIE_ID = @relatie_id
	END

	IF EXISTS (SELECT 1 FROM ATTRIBUUT_IN_VERBALISATIE WHERE VERBALISATIE_ID = @verbalisatie_id)
	BEGIN
		DELETE FROM ATTRIBUUT_IN_VERBALISATIE WHERE VERBALISATIE_ID = @verbalisatie_id
	END

	IF (SELECT COUNT(*) FROM ENTITEIT_IN_VERBALISATIE WHERE ENTITEIT_ID = @entiteit_id) = 1
	BEGIN
		DELETE FROM ENTITEIT_IN_PROJECT WHERE ENTITEIT_ID = @entiteit_id AND PROJECT_ID = @project_id
	END

	DELETE FROM ENTITEIT_IN_VERBALISATIE WHERE ENTITEIT_ID = @entiteit_id AND VERBALISATIE_ID = @verbalisatie_id

	--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Er zijn geen andere verbalisaties met dezelfde entiteit.

-Alles wordt verwijderd. (Ook rol en attribuut)
*/

EXEC tSQLt.NewTestClass 'deleteEntiteit';
GO

/* ================= 
	Gebuiker niet in project
==================*/
CREATE PROCEDURE [deleteEntiteit].[test = gebruiker niet in project]
AS
BEGIN
--Assemble
	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

	--Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De gebruiker maakt geen deel uit van dit project.'

	--Act
	exec usp_addEntity 1, 1, 'JacquesFout', 'Entiteit1', 0

END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[deleteEntiteit].[test = gebruiker niet in project]'
GO

/* ==================================
Er zijn geen andere verbalisaties met dezelfde entiteit.
===================================*/
CREATE PROCEDURE [deleteEntiteit].[test = Er zijn geen andere verbalisaties met dezelfde entiteit]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	--ROL
	EXEC tSQLt.FakeTable 'dbo', 'ROL', @Identity = 1;
	
	--RELATIE
	EXEC tSQLt.FakeTable 'dbo', 'RELATIE', @Identity = 1;

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_deleteEntity 1, 1, 'JacquesTest', 1

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
END

GO
EXEC [tSQLt].[Run] '[deleteEntiteit].[test = Er zijn geen andere verbalisaties met dezelfde entiteit]'
GO

/* ==================================
Alles wordt verwijderd (Ook rol en attribuut)
===================================*/
CREATE PROCEDURE [deleteEntiteit].[test = Alles wordt verwijderd]  
AS
BEGIN
--Assemble
	--ENTITEIT_IN_PROJECT
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_PROJECT', @Identity = 1;

	SELECT * INTO expectedEntiteitInProject FROM ENTITEIT_IN_PROJECT 
	
	INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1')
	
	INSERT INTO expectedEntiteitInProject (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) 
	VALUES (1, 'JacquesTest', 'Entiteit1') 

	--ENTITEIT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedEntiteitInVerbalisatie FROM ENTITEIT_IN_VERBALISATIE

	INSERT INTO ENTITEIT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 1, 'JacquesTest', 0), (1, 2, 'JacquesTest', 0)

	INSERT INTO expectedEntiteitInVerbalisatie (ENTITEIT_ID, VERBALISATIE_ID, GEBRUIKERSNAAM, IS_MATCH)
	VALUES (1, 2, 'JacquesTest', 0)

	--ATTRIBUUT_IN_VERBALISATIE
	EXEC tSQLt.FakeTable 'dbo', 'ATTRIBUUT_IN_VERBALISATIE', @Identity = 1;

	SELECT * INTO expectedAttribuutInVerbalisatie FROM ATTRIBUUT_IN_VERBALISATIE

	INSERT INTO ATTRIBUUT_IN_VERBALISATIE (ENTITEIT_ID, VERBALISATIE_ID)
	VALUES (1, 1)

	--ROL
	EXEC tSQLt.FakeTable 'dbo', 'ROL', @Identity = 1;

	SELECT * INTO expectedRol FROM ROL

	INSERT INTO ROL (RELATIE_ID, ENTITEIT_VAN, ENTITEIT_NAAR)
	VALUES (1, 1, 2), (1, 2, 1)

	--RELATIE
	EXEC tSQLt.FakeTable 'dbo', 'RELATIE', @Identity = 1;

	SELECT * INTO expectedRelatie FROM Relatie

	INSERT INTO RELATIE (VERBALISATIE_ID)
	VALUES (1)

	--PROJECTBEZETTING
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING', @Identity = 1;

	INSERT INTO PROJECTBEZETTING (PROJECT_ID, GEBRUIKERSNAAM) 
	VALUES (1, 'JacquesTest')

--Act
	exec usp_deleteEntity 1, 1, 'JacquesTest', 1

--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInProject', 'dbo.ENTITEIT_IN_PROJECT'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedEntiteitInVerbalisatie', 'dbo.ENTITEIT_IN_VERBALISATIE'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedAttribuutInVerbalisatie', 'dbo.ATTRIBUUT_IN_VERBALISATIE'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedRol', 'dbo.ROL'
	EXEC [tSQLt].[AssertEqualsTable] 'expectedRelatie', 'dbo.RELATIE'
END
GO
EXEC [tSQLt].[Run] '[deleteEntiteit].[test = Alles wordt verwijderd]'
GO