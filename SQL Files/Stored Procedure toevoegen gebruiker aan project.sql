/*
Stored procedure: Toevoegen gebruiker aan project (insert)
Use case: Gebruiker toevoegen aan project

Een eigenaar van een project kan andere gebruikers toevoegen aan zijn project. 
Wanneer deze zijn toegevoegd aan het project kunnen ze meewerken aan het project.

De volgende tabellen zijn nodig voor het succesvol toevoegen van een gebruiker aan een project:
-Projectbezetting

Acties binnen projectbezetting:
-Select om te kijken of er al een record is voor de toe te voegen gebruiker in combinatie met het project.
-Select om te kijken of de toevoegende gebruiker 'is_eigenaar' = true heeft.
-Inserten van het nieuwe record

Type constraint: Stored Procedure
Waarom: Er is geen situatie waarbinnen een gebruiker meerdere gebruikers tegelijk kan toevoegen aan een project. 
Een groot voordeel van triggers is de mogelijkheid om gemakkelijk meerdere records te kunnen inserten. Dit valt hierdoor weg.
Verder heeft deze usecase alleen invloed op het toevoegen van gebruikers aan het project. Voorkomen is beter dan genezen. Hierom is er gekozen voor een stored procedure.

De eigenaar van een project onderneemt 1 stap binnen deze usecase. Dat is aangeven welke gebruiker hij wilt toevoegen aan het project.

De volgende fouten kunnen optreden bij deze usecase:
1. Er is geen record in projectbezetting waar de 'eigenaar' de waarde true heeft voor is_eigenaar.
2. Er is al een record voor de toe te voegen gebruiker in projectbezetting bij het project.
	

De volgorde waarin op deze problemen gecontroleerd gaat worden is als volgt;
1, 2.
*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_addUserToProject')
	BEGIN
		DROP PROC usp_addUserToProject
	END
GO
CREATE PROCEDURE usp_addUserToProject --Insert een record in projectbezetting.
	@eigenaar varchar(20),
	@toe_te_voegen_gebruiker varchar(20),
	@project_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
		IF not exists(
			SELECT 1
			FROM PROJECTBEZETTING
			WHERE GEBRUIKERSNAAM = @eigenaar AND IS_EIGENAAR = 1 AND PROJECT_ID = @project_id
		)
		BEGIN
			RAISERROR('Je bent geen eigenaar van dit project.', 16, 1)
		END

		IF exists(
			SELECT 1 
			FROM PROJECTBEZETTING
			WHERE GEBRUIKERSNAAM = @toe_te_voegen_gebruiker AND @project_id = project_id
		)
		BEGIN
			RAISERROR('Deze gebruiker neemt al deel aan het project.', 16, 1)
		END

		INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
		(@toe_te_voegen_gebruiker, @project_id, 0)
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO



/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig. De tests zullen uitgevoerd worden op basis van de volgorde in de stored procedure.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Er is geen record waar de gebruiker is_eigenaar = true

-De gebruiker die toegevoegd wordt neemt al deel aan het project.

-Alles klopt
*/

EXEC tSQLt.NewTestClass 'toevoegenGebruikerAanProject';
GO
/* ================= 
    Alles klopt
==================*/
CREATE PROCEDURE [toevoegenGebruikerAanProject].[test = alles klopt]  
AS
BEGIN
--Assemble
	IF OBJECT_ID('[toevoegenGebruikerAanProject].[Expected]','Table') IS NOT NULL
	DROP TABLE [toevoegenGebruikerAanProject].[Expected]

	SELECT TOP 0 * 
	INTO toevoegenGebruikerAanProject.Expected --snelle manier om een structuur te "kopi�ren"
	FROM PROJECTBEZETTING;

	INSERT INTO toevoegenGebruikerAanProject.Expected(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR)
	VALUES('BonnoTest', 1, 1), ('LucianoTest', 1, 0)

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 1)
		  
--Act
exec  usp_addUserToProject  'BonnoTest', 'LucianoTest', 1

--Assert
EXEC [tSQLt].[AssertEqualsTable] 'toevoegenGebruikerAanProject.Expected', 'dbo.PROJECTBEZETTING'
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[toevoegenGebruikerAanProject].[test = alles klopt]'
GO

/* ==================================
Geen 'eigenaar' van het project.
===================================*/
CREATE PROCEDURE [toevoegenGebruikerAanProject].[test = geen eigenaar van het project]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 0)
		  
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Je bent geen eigenaar van dit project.'

	--Act
	exec  usp_addUserToProject  'BonnoTest', 'LucianoTest', 1
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[toevoegenGebruikerAanProject].[test = geen eigenaar van het project]'
GO

/* ==================================
De gebruiker neemt al deel aan project
===================================*/
CREATE PROCEDURE [toevoegenGebruikerAanProject].[test = de gebruiker neemt al deel aan het project]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 1),
	('LucianoTest', 1, 1)
		  
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Deze gebruiker neemt al deel aan het project.'

	--Act
	exec  usp_addUserToProject  'BonnoTest', 'LucianoTest', 1
END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[toevoegenGebruikerAanProject].[test = de gebruiker neemt al deel aan het project]'
GO

--Runt alle unittests
EXEC [tSQLt].[Run] '[toevoegenGebruikerAanProject]'
GO

