/*
Stored procedure: Gebruiker verwijderen uit het project.
Use case: N.v.t.

Een eigenaar van een project kan andere gebruikers verwijderen uit zijn project. 
Wanneer een gebruiker wordt verwijdert kan hij niet meer meewerken aan het project. 

De volgende tabellen zijn nodig voor het succesvol toevoegen van een gebruiker aan een project:
-Projectbezetting

Acties binnen projectbezetting:
-Select om te kijken of de eigenaar niet wordt verwijdert. 

Type constraint: Stored Procedure
Waarom: Er is geen situatie waarbinnen een gebruiker meerdere gebruikers tegelijk kan verwijderen uit een project. 
Een groot voordeel van triggers is de mogelijkheid om gemakkelijk meerdere records te kunnen inserten/updaten/deleten. Dit valt hierdoor weg.
Verder heeft deze usecase alleen invloed op het verwijderen van gebruikers uit het project. Voorkomen is beter dan genezen. Hierom is er gekozen voor een stored procedure.

De eigenaar van een project onderneemt 1 stap binnen deze usecase. Dat is aangeven welke gebruiker hij uit het project wilt verwijderen.

De volgende fouten kunnen optreden bij deze usecase:
1. De 'eigenaar' is geen eigenaar van het project.
2. De gebruiker die verwijdert wordt is de eigenaar.

De volgorde waarin op deze problemen gecontroleerd gaat worden is als volgt;
1, 2.
*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_removeUserFromProject')
	BEGIN
		DROP PROC usp_removeUserFromProject
	END
GO
CREATE PROCEDURE usp_removeUserFromProject 
	@eigenaar varchar(20),
	@te_verwijderen_gebruiker varchar(20),
	@project_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
		IF not exists(
			SELECT 1
			FROM PROJECTBEZETTING
			WHERE GEBRUIKERSNAAM = @eigenaar AND IS_EIGENAAR = 1 and PROJECT_ID = @project_id
		)
		BEGIN
			RAISERROR('Je bent geen eigenaar van dit project.', 16, 1)
		END

		IF exists(
			SELECT 1 
			FROM PROJECTBEZETTING
			WHERE GEBRUIKERSNAAM = @te_verwijderen_gebruiker AND IS_EIGENAAR = 1 AND PROJECT_ID = @project_id
		)
		BEGIN
			RAISERROR('Je mag de eigenaar van het project niet verwijderen.', 16, 1)
		END
		delete from projectbezetting where gebruikersnaam = @te_verwijderen_gebruiker AND PROJECT_ID = @project_id
	
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

/*
Voor het unit-testen van deze situatie zijn er een aantal tests nodig.

Er zullen tests aangemaakt worden voor de volgende gevallen:

-Er wordt succesvol een gebruiker verwijdert.

-De 'eigenaar' is helemaal geen eigenaar van het project

-De eigenaar van een project wordt verwijdert. 


-Alles klopt
*/

EXEC tSQLt.NewTestClass 'verwijderenGebruikerUitProject';
GO
/* ================= 
    Alles klopt
==================*/
CREATE PROCEDURE [verwijderenGebruikerUitProject].[test = Een gebruiker wordt succesvol verwijdert]  
AS
BEGIN
--Assemble
	IF OBJECT_ID('[verwijderenGebruikerUitProject].[Expected]','Table') IS NOT NULL
	DROP TABLE [verwijderenGebruikerUitProject].[Expected]

	SELECT TOP 0 * 
	INTO verwijderenGebruikerUitProject.Expected --snelle manier om een structuur te "kopi�ren"
	FROM PROJECTBEZETTING;

	INSERT INTO verwijderenGebruikerUitProject.Expected(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR)
	VALUES('BonnoTest', 1, 1), ('LucianoTest', 1, 0)

	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef'),
		  ('DaveTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 1), ('LucianoTest', 1, 0), ('DaveTest', 1, 0)
		  
--Act
exec  usp_removeUserFromProject  'BonnoTest', 'DaveTest', 1

--Assert
EXEC [tSQLt].[AssertEqualsTable] 'verwijderenGebruikerUitProject.Expected', 'dbo.PROJECTBEZETTING'
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[verwijderenGebruikerUitProject].[test = Een gebruiker wordt succesvol verwijdert]'
GO

/* ==================================
Geen 'eigenaar' van het project.
===================================*/
CREATE PROCEDURE [verwijderenGebruikerUitProject].[test = geen eigenaar van het project]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 0)
		  
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Je bent geen eigenaar van dit project.'

	--Act
	exec  usp_removeUserFromProject  'BonnoTest', 'LucianoTest', 1
END

GO	
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[verwijderenGebruikerUitProject].[test = geen eigenaar van het project]'
GO

/* ==================================
De gebruiker die verwijdert wordt is een eigenaar
===================================*/
CREATE PROCEDURE [verwijderenGebruikerUitProject].[test = je mag de eigenaar van een project niet verwijderen]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'GEBRUIKER';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECT';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';

	INSERT INTO dbo.GEBRUIKER(GEBRUIKERSNAAM, WACHTWOORD)
	VALUES('BonnoTest', 'abcdef'),
		  ('LucianoTest', 'abcdef')

	INSERT INTO dbo.PROJECT(PROJECT_ID, PROJECTNAAM)
	VALUES(1, 'TestProject')

	INSERT INTO dbo.PROJECTBEZETTING(GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('BonnoTest', 1, 1),
	('LucianoTest', 1, 0)
		  
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Je mag de eigenaar van het project niet verwijderen.'

	--Act
	exec  usp_removeUserFromProject  'BonnoTest', 'BonnoTest', 1
END

GO
--Runt deze specifieke unittest
EXEC [tSQLt].[Run] '[verwijderenGebruikerUitProject].[test = je mag de eigenaar van een project niet verwijderen]'
GO

--Runt alle unittests
EXEC [tSQLt].[Run] '[verwijderenGebruikerUitProject]'
GO

