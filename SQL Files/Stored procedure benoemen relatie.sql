/*
Stored procedure: Benoemen van een relatie in een verbalisatie
Use case: Benoemen relatie

Bij het analyseren van verbalisaties kan het voorkomen dat er sprake is van een relatie tussen 1 of 2 entiteiten.
Wanneer dit het geval is dan moet de gebruiker deze relatie kunnen benoemen. 
Hiervoor dient hij aan te geven wat de naam van de relatie is. 
Verder moet de gebruiker ook aan geven tussen welke twee entiteiten een relatie ligt.
Het is ook mogelijk dat een van de twee entiteiten dependent is. Dit moet succesvol opgeslagen worden.

De volgende tabellen zijn nodig voor het succesvol toevoegen van een relatie.
-Rol
-Relatie
-Entiteit_in_project
-Verbalisatie

Acties binnen Relatie:
-Select om te controleren of er al een relatie met die naam is binnen het project.
-Select om te controleren of er al 2 relaties in een verbalisatie zijn.
-Inserten van de relatie

Acties binnen Rol:
-Inserten van de twee verschillende rollen (max 2 per relatie, afgevangen door PK).

Acties binnen Entiteit_in_verbalisatie:
-Select om te controleren of het een entiteit in de verbalisatie is ge�dentificeerd.

Acties binnen verbalisatie: 
-Controleren of er niet al een relatie is met die naam binnen het project

Acties binnen projectbezetting:
-Kijken of de gebruiker deelneemt aan het project

Type constraint: Stored Procedure
Waarom: Bij het invoeren van een relatie wordt er maar 1 relatie per keer benoemd. Hierdoor valt een groot voordeel van triggers weg.
Verder is het ook beter om te voorkomen dan te genezen. Daarom is er gekozen voor een stored procedure. 

Binnen deze stored procedure wordt er naar de volgende fouten gekeken:
-De gebruiker probeert een relatie te benoemen bij een verbalisatie waar al 3 relaties bestaan.
-De gebruiker probeert een relatie te benoemen met een relatienaam die in dat project al is gebruikt.
-De gebruiker probeert een relatie te benoemen met entiteiten uit een ander project dat het project van de verbalisatie.
-De gebruiker neemt niet deel an het project.
-Beide rollen zijn dependent.
*/
use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_benoemenRelatieInVerbalisatie')
	BEGIN
		DROP PROC usp_benoemenRelatieInVerbalisatie
	END
GO
CREATE PROCEDURE usp_benoemenRelatieInVerbalisatie --Insert een record in projectbezetting.
	@relatienaam varchar(50),
	@toevoegende_gebruiker varchar(20),
	@verbalisatie_id int,
	@entiteit_van int,
	@entiteit_naar int,
	@dependent_rol_1 bit,
	@dependent_rol_2 bit
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
	  IF @dependent_rol_1 = 1 AND @dependent_rol_2 = 1
	  RAISERROR('Het is niet mogelijk dat beide rollen dependent zijn.', 16, 1)

	  IF not exists(
					SELECT 1 --Kijken of de gebruiker deelneemt aan het project
					FROM PROJECTBEZETTING 
					WHERE GEBRUIKERSNAAM = @toevoegende_gebruiker AND 
					PROJECT_ID = (
							  select PROJECT_ID 
							  from VERBALISATIE 
							  where VERBALISATIE_ID = @verbalisatie_id
							  )
					)
					RAISERROR('Je werkt niet mee aan dit project en mag geen relatie benoemen', 16, 1)

	  IF exists(
				SELECT 1  --Kijken of er al een relatie is met dezelfde naam in dit project
				FROM RELATIE r join VERBALISATIE v on r.VERBALISATIE_ID = v.VERBALISATIE_ID 
				WHERE project_id = (
									SELECT project_id 
									FROM verbalisatie 
									WHERE verbalisatie_id = @verbalisatie_id
									) 
				and relatie_naam = @relatienaam  
				)
				RAISERROR('Er bestaat al een relatie met deze naam binnen het project.', 16, 1)

	  IF exists(
				SELECT 1  --Kijken of er al 2 of meer relaties zijn.
				FROM RELATIE 
				WHERE VERBALISATIE_ID = @verbalisatie_id 
				GROUP BY VERBALISATIE_ID 
				HAVING count(verbalisatie_id)>1
	            )
				RAISERROR('Er zijn al 2 relaties bij deze verbalisatie in kaart gebracht.', 16, 1)

	  IF not exists(
					SELECT 1 --Kijken of entiteit_van in hetzelfde project zit als de verbalisatie.
					FROM ENTITEIT_IN_VERBALISATIE 
					WHERE verbalisatie_id = @verbalisatie_id AND ENTITEIT_ID = @entiteit_van
				    )
					RAISERROR('De eerste entiteit is niet in de verbalisatie gevonden', 16, 1)

	  IF not exists(
SELECT 1 --Kijken of entiteit_van in hetzelfde project zit als de verbalisatie.
					FROM ENTITEIT_IN_VERBALISATIE 
					WHERE verbalisatie_id = @verbalisatie_id AND ENTITEIT_ID = @entiteit_naar
					)
					RAISERROR('De tweede entiteit is niet in de verbalisatie gevonden', 16, 1)

		INSERT INTO RELATIE (GEBRUIKERSNAAM, VERBALISATIE_ID, RELATIE_NAAM) VALUES 
		(@toevoegende_gebruiker, @verbalisatie_id, @relatienaam)
	
		INSERT INTO ROL(ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, DEPENDENT)
		VALUES (@entiteit_van, @entiteit_naar,  IDENT_CURRENT('RELATIE'), @toevoegende_gebruiker, 1, @dependent_rol_1),
			   (@entiteit_naar, @entiteit_van,  IDENT_CURRENT('RELATIE'), @toevoegende_gebruiker, 0, @dependent_rol_2)

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

EXEC tSQLt.NewTestClass 'benoemenRelatieInVerbalisatie';
GO



/*=========================
Beide rollen zijn dependent
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = beide rollen zijn dependent]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST')
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Het is niet mogelijk dat beide rollen dependent zijn.'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie  'DitIsEenRelatie', 'Bonno123', 1, 1, 2, 1, 1
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = beide rollen zijn dependent]'
GO

/*=========================
De gebruiker neemt niet deel aan het project
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = de gebruiker neemt geen deel aan het project]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1, @Defaults = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST')
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Je werkt niet mee aan dit project en mag geen relatie benoemen'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie  'DitIsEenRelatie', 'Jacques123', 1, 1, 2, 0, 0
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = de gebruiker neemt geen deel aan het project]'
GO


/*=========================
Er bestaat al een relatie met die naam
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = Er bestaat al een relatie in het project met die naam]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1, @Defaults = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST')
	insert into RELATIE VALUES ('Bonno123', 2, 'RelatienaamIsAlBezetInHetProject', null)
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er bestaat al een relatie met deze naam binnen het project.'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'RelatienaamIsAlBezetInHetProject', 'Bonno123', 1, 1, 2, 0, 0
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = Er bestaat al een relatie in het project met die naam]'

GO


/*=========================
Er zijn al twee relaties in deze verbalisatie gevonden
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = Er zijn al twee relaties geidentificeerd]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1, @Defaults = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1, @Defaults = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST')
	insert into RELATIE VALUES ('Bonno123', 1, 'RandomNaam1', null), ('Bonno123', 1, 'RandomNaam2', null)
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Er zijn al 2 relaties bij deze verbalisatie in kaart gebracht.'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 1, 2, 0, 0
	
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = Er zijn al twee relaties geidentificeerd]'

GO

/*=========================
De eerste entiteit is niet in de verbalisatie ge�dentificeerd
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = De eerste entiteit bestaat niet in de verbalisatie]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
	
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De eerste entiteit is niet in de verbalisatie gevonden'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 3, 2, 0, 0
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = De eerste entiteit bestaat niet in de verbalisatie]'

GO


/*=========================
De tweede entiteit is niet in de relatie ge�dentificeerd
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = De tweede entiteit bestaat niet in de verbalisatie]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
	
    	
	--Assert	  
	EXEC tSQLt.ExpectException @ExpectedMessage = 'De tweede entiteit is niet in de verbalisatie gevonden'

	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 2, 3, 0, 0
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = De tweede entiteit bestaat niet in de verbalisatie]'

GO


/*=========================
De eerste entiteit is dependent 
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = de eerste rol is dependent]  
AS
BEGIN
--Assemble
	IF OBJECT_ID('[benoemenRelatieInVerbalisatie].[Expected]','Table') IS NOT NULL
	DROP TABLE [benoemenRelatieInVerbalisatie].[Expected]

	SELECT TOP 0 * 
	INTO benoemenRelatieInVerbalisatie.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ROL;

	INSERT INTO benoemenRelatieInVerbalisatie.Expected(ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, DEPENDENT)
	VALUES (1, 2, 1, 'Bonno123', 1, 1), (2, 1, 1, 'Bonno123', 0, 0)

	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
	
    	  
	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0

	--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'benoemenRelatieInVerbalisatie.Expected', 'dbo.ROL'
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = de eerste rol is dependent]'


GO
/*=========================
	
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = de tweede rol is dependent]  
AS
BEGIN
--Assemble
	IF OBJECT_ID('[benoemenRelatieInVerbalisatie].[Expected]','Table') IS NOT NULL
	DROP TABLE [benoemenRelatieInVerbalisatie].[Expected]

	SELECT TOP 0 * 
	INTO benoemenRelatieInVerbalisatie.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ROL;

	INSERT INTO benoemenRelatieInVerbalisatie.Expected(ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, DEPENDENT)
	VALUES (1, 2, 1, 'Bonno123', 1, 0), (2, 1, 1, 'Bonno123', 0, 1)

	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
	
    	  
	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 1, 2, 0, 1

	--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'benoemenRelatieInVerbalisatie.Expected', 'dbo.ROL'
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = de tweede rol is dependent]'

GO

/*=========================
Geen entiteit is dependent 
=========================*/
CREATE PROCEDURE [benoemenRelatieInVerbalisatie].[test = geen rol is dependent]  
AS
BEGIN
--Assemble
	IF OBJECT_ID('[benoemenRelatieInVerbalisatie].[Expected]','Table') IS NOT NULL
	DROP TABLE [benoemenRelatieInVerbalisatie].[Expected]

	SELECT TOP 0 * 
	INTO benoemenRelatieInVerbalisatie.Expected --snelle manier om een structuur te "kopi�ren"
	FROM ROL;

	INSERT INTO benoemenRelatieInVerbalisatie.Expected(ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, DEPENDENT, MANDATORY)
	VALUES (1, 2, 1, 'Bonno123', 1, 0, null), (2, 1, 1, 'Bonno123', 0, 0, null)

	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1;

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
	
    	  
	--Act
	EXEC  usp_benoemenRelatieInVerbalisatie 'Relatie123', 'Bonno123', 1, 1, 2, 0, 0

	--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'benoemenRelatieInVerbalisatie.Expected', 'dbo.ROL'
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie].[test = geen rol is dependent]'

EXEC [tSQLt].[Run] '[benoemenRelatieInVerbalisatie]'

/*
Stored procedure: Uitbreiden van een relatie met informatie voor powerdesigner
Use case: Benoemen relatie

Bij een relatie binnen powerdesigner heb je meer data nodig om de relatie te beschrijven dan binnen de analyse van een verbalisatie.
Hierbij komen een aantal (mogelijke) velden kijken zoals het cardinaliteitstype, cardinaliteit (minimum en maximum), mandatory en is_dominant.
Sommige velden zijn enkel toegestaan bij bepaalde waarden van andere velden. 
Een voorbeeld hiervan is dat 'is_dominant' alleen ingevuld mag zijn als het cardinaliteitstype ONE - ONE is.

De volgende tabellen worden be�nvloed bij het uitbreiden van een relatie:

Relatie:
-Het updaten van een relatie om het cardinaliteitstype aan te passen

Rol:
-Het toevoegen van een cardinaliteit
-Toevoegen van een waarde van 0 of 1 voor mandatory.
-Toevoegen van een waarde voor cardinaliteit_minimum voor beide rollen
-Toevoegen van een waarde voor cardinaliteit_maximum voor beide rollen
-Toevoegen van een waarde van 0 of 1 voor is_dominant in het geval dat het cardinaliteitstype ONE - ONE is.

Er moet met de volgende punten rekening gehouden worden:
Indien de cardinaliteit kleiner dan 0 is, dan behoort er een foutmelding te komen.
De minimale cardinaliteit van een rol kan niet groter zijn dan de maximale cardinaliteit van dezelfde rol.
Indien een relatie het cardinaliteitstype ONE - ONE heeft dan moet is_dominant bij de bijbehorende rollen NOT NULL zijn. 
Indien een relatie het cardinaliteitstype ONE - ONE heeft dan heeft het attribuut cardinaliteit_maximum maximaal een waarde van 1.
Indien een rol de waarde mandatory = 1 heeft, dan is de minimum cardinaliteit 1.
Indien een rol de waarde cardinaliteit_minimum = 1 heeft, dan moet gelden mandatory = 1.
Indien een relatie het cardinaliteitstype ONE - MANY heeft dan mag de rol met de waarde is_primair = 1 niet dependent zijn. 
Indien een relatie het cardinaliteitstype MANY - ONE heeft dan mag de rol met de waarde is_primair = 0 niet dependent zijn.
Indien een relatie het cardinaliteitstype ONE - MANY heeft dan mag de rol met de waarde is_primair = 0 bij cardinaliteit_maximum maximaal een waarde van 1 hebben.
Indien een relatie het cardinaliteitstype MANY - ONE heeft dan mag de rol met de waarde is_primair = 1 bij cardinaliteit_maximum maximaal een waarde van 1 hebben. 
Indien een rol de waarde is_dependend = 1 heeft, dan moet is_mandatory ook 1 zijn.

Daar waar mogelijk wordt er gebruik gemaakt van check constraints omdat declaratieve constraints de voorkeur hebben. 
Voor de rest wordt er gebruik gemaakt van een stored procedure. 
Binnen deze stored procedure wordt er ook gebruik gemaakt van de stored procedure usp_benoemenRelatieInVerbalisatie.
Dit is enkel van belang binnen onze applicatie, omdat deze stappen samen uitgevoerd worden. 
Als er een applicatie komt die niet data driven is, maar aan de hand van het daadwerkelijke proces verloopt, dan moeten er hier een paar aanpassingen plaatsvinden.
In dat geval moet er een relationship_id meegegeven worden in plaats van dat er gebruik wordt gemaakt van 'IDENT_CURRENT('RELATIE')'
*/

GO

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name ='MandatoryRolesRequire1MinimumCardinality')
BEGIN
Alter TABLE ROL DROP CONSTRAINT MandatoryRolesRequire1MinimumCardinality
END
ALTER TABLE ROL
add constraint MandatoryRolesRequire1MinimumCardinality
check(not ((MANDATORY = 1 AND CARDINALITEIT_MINIMUM = 0) OR 
		   (MANDATORY = 0 AND CARDINALITEIT_MINIMUM >= 1)))
GO

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name = 'ValuesOfCardinaliteitsType')
BEGIN
Alter TABLE RELATIE DROP CONSTRAINT ValuesOfCardinaliteitstype
END
ALTER TABLE RELATIE
add constraint ValuesOfCardinaliteitstype
check(cardinaliteitstype = 'ONE - MANY' or 
	  cardinaliteitstype = 'MANY - MANY' or 
	  cardinaliteitstype = 'MANY - ONE' or 
	  cardinaliteitstype = 'ONE - ONE' or 
	  cardinaliteitstype is null)

GO

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name = 'IfRolIsDependentItIsAlsoMandatory')
BEGIN
Alter TABLE ROL DROP CONSTRAINT IfRolIsDependentItIsAlsoMandatory
END
ALTER TABLE ROL
add constraint IfRolIsDependentItIsAlsoMandatory
check(not(DEPENDENT = 1 AND MANDATORY = 0))

GO

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name = 'MinimumCardinalityCantBeLowerThan0')
BEGIN
Alter TABLE ROL DROP CONSTRAINT MinimumCardinalityCantBeLowerThan0
END
ALTER TABLE ROL
add constraint MinimumCardinalityCantBeLowerThan0
check(not(CARDINALITEIT_MINIMUM < 0))

GO	

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name = 'MaximumCardinalityCantBeLowerThan0')
BEGIN
Alter TABLE ROL DROP CONSTRAINT MaximumCardinalityCantBeLowerThan0
END
ALTER TABLE ROL
add constraint MaximumCardinalityCantBeLowerThan0
check(not(CARDINALITEIT_MAXIMUM < 1))

GO

IF EXISTS(select * from sys.objects where type_desc = 'CHECK_CONSTRAINT' AND name = 'MinimumCardinalityCantBeBiggerThanMaximumCardinality')
BEGIN
Alter TABLE ROL DROP CONSTRAINT MinimumCardinalityCantBeBiggerThanMaximumCardinality
END
ALTER TABLE ROL
add constraint MinimumCardinalityCantBeBiggerThanMaximumCardinality
check(not(CARDINALITEIT_MINIMUM > CARDINALITEIT_MAXIMUM))

GO 

IF EXISTS(select * from sys.procedures where name=N'usp_relatieUitbreidenMetPowerDesignerGegevens')
	BEGIN
		DROP PROC usp_relatieUitbreidenMetPowerDesignerGegevens
	END
GO
CREATE PROCEDURE usp_relatieUitbreidenMetPowerDesignerGegevens --Insert een record in projectbezetting.
	@relatienaam varchar(50),
	@toevoegende_gebruiker varchar(20),
	@verbalisatie_id int,
	@entiteit_van int,
	@entiteit_naar int,
	@dependent_rol_1 bit,
	@dependent_rol_2 bit,
	@cardinaliteitstype varchar(10),
	@cardinaliteit_minimum_1 varchar(6),
	@cardinaliteit_minimum_2 varchar(6),
	@cardinaliteit_maximum_1 varchar(6),
	@cardinaliteit_maximum_2 varchar(6),
	@mandatory_1 bit,
	@mandatory_2 bit,
	@is_dominant_1 bit,
	@is_dominant_2 bit

AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
		IF @cardinaliteitstype = 'ONE - ONE' AND (@is_dominant_1 is null OR @is_dominant_2 is null)
			BEGIN
				RAISERROR('Bij het cardinaliteitstype ONE - ONE moet bij beide is_dominant ingevuld zijn.', 16, 1)
			END
		IF @cardinaliteitstype = 'ONE - ONE' AND (@cardinaliteit_maximum_1 != 1 OR @cardinaliteit_maximum_2 != 1) 
			BEGIN
				RAISERROR('Bij het cardinaliteitstype ONE - ONE is de maximum cardinaliteit bij een rol altijd 1', 16, 1)
			END
		IF (@cardinaliteitstype = 'ONE - MANY' AND @dependent_rol_1 = 1) 
			BEGIN
				RAISERROR('Bij een ONE - MANY relationship mag de primaire rol niet dependent zijn.', 16, 1)
			END
		IF (@cardinaliteitstype = 'MANY - ONE' AND @dependent_rol_2 = 1) 
			BEGIN
				RAISERROR('Bij een MANY - ONE relationship mag de secundaire rol niet dependent zijn.', 16, 1)
			END
		IF (@cardinaliteitstype = 'ONE - MANY' AND not (@cardinaliteit_maximum_2 = 1))
			BEGIN
				RAISERROR('Bij een cardinaliteitstype van ONE - MANY moet de maximale cardinaliteit van de secundaire rol 1 zijn.', 16, 1)
			END
		IF (@cardinaliteitstype = 'MANY - ONE' AND not (@cardinaliteit_maximum_1 = 1))
			BEGIN
				RAISERROR('Bij een cardinaliteitstype van MANY - ONE moet de maximale cardinaliteit van de primaire rol 1 zijn.', 16, 1)
			END
		EXEC  usp_benoemenRelatieInVerbalisatie @relatienaam, @toevoegende_gebruiker, @verbalisatie_id, @entiteit_van, @entiteit_naar, @dependent_rol_1, @dependent_rol_2
		update RELATIE set CARDINALITEITSTYPE = @cardinaliteitstype where RELATIE_ID = IDENT_CURRENT('RELATIE')
		update ROL set CARDINALITEIT_MINIMUM = @cardinaliteit_minimum_1, CARDINALITEIT_MAXIMUM = @cardinaliteit_maximum_1, MANDATORY = @mandatory_1, IS_DOMINANT = @is_dominant_1 where RELATIE_ID = IDENT_CURRENT('RELATIE') AND IS_PRIMAIRE_ROL = 1
		update ROL set CARDINALITEIT_MINIMUM = @cardinaliteit_minimum_2, CARDINALITEIT_MAXIMUM = @cardinaliteit_maximum_2, MANDATORY = @mandatory_2, IS_DOMINANT = @is_dominant_2 where RELATIE_ID = IDENT_CURRENT('RELATIE') AND IS_PRIMAIRE_ROL = 0

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

EXEC tSQLt.NewTestClass 'benoemenRelatieViaPowerdesigner';
GO


/*=========================
Successcenario
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = successcenario waarbij de eerste entiteit dependend is]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	IF OBJECT_ID('[benoemenRelatieViaPowerdesigner].[ExpectedRol]','Table') IS NOT NULL
	DROP TABLE [benoemenRelatieViaPowerdesigner].[ExpectedRol]
	SELECT TOP 0 * 
	INTO benoemenRelatieViaPowerdesigner.ExpectedRol --snelle manier om een structuur te "kopi�ren"
	FROM ROL;

	IF OBJECT_ID('[benoemenRelatieViaPowerdesigner].[ExpectedRelatie]','Table') IS NOT NULL
	DROP TABLE [benoemenRelatieViaPowerdesigner].[ExpectedRelatie]
	SELECT TOP 0 * 
	INTO benoemenRelatieViaPowerdesigner.ExpectedRelatie --snelle manier om een structuur te "kopi�ren"
	FROM RELATIE;

	INSERT INTO benoemenRelatieViaPowerdesigner.ExpectedRelatie (GEBRUIKERSNAAM, VERBALISATIE_ID, RELATIE_NAAM, CARDINALITEITSTYPE) VALUES 
	('Bonno123', 1, 'Relatie123', 'MANY - ONE')


	INSERT INTO benoemenRelatieViaPowerdesigner.ExpectedRol(ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT)
	VALUES (1, 2, 1, 'Bonno123', 1, 1, 1, 1, 1, null), (2, 1, 1, 'Bonno123', 0, 0, 4, 0, 0, null)

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'MANY - ONE', 1, 0, 1, 4, 1, 0, null, null
	

	--Assert
	EXEC [tSQLt].[AssertEqualsTable] 'benoemenRelatieViaPowerdesigner.ExpectedRelatie', 'dbo.RELATIE'
	EXEC [tSQLt].[AssertEqualsTable] 'benoemenRelatieViaPowerdesigner.ExpectedRol', 'dbo.ROL'
END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = successcenario waarbij de eerste entiteit dependend is]'

GO


/*=========================
ONE - ONE relatie waar is_dominant niet is ingevuld voor entiteit 1.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 1]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE moet bij beide is_dominant ingevuld zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 0, 1, 4, 1, 0, null, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 1]'

GO

/*=========================
ONE - ONE relatie waar is_dominant niet is ingevuld voor entiteit 2.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 2]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE moet bij beide is_dominant ingevuld zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 0, 1, 4, 1, 0, 1, null

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 2]'

GO

/*=========================
ONE - ONE relatie waar is_dominant niet is ingevuld voor beide entiteiten
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 1 en 2]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE moet bij beide is_dominant ingevuld zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 0, 1, 4, 1, 0, null, null

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waar bij is_dominant null is voor entiteit 1 en 2]'

GO

/*=========================
ONE - ONE Relatie waar de maximum cardinaliteit van rol 2 groter is dan 1.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 1 groter is dan 1.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE is de maximum cardinaliteit bij een rol altijd 1'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 1, 1, 4, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 1 groter is dan 1.]'

GO

/*=========================
ONE - ONE Relatie waar de maximum cardinaliteit van rol 1 groter is dan 1.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 2 groter is dan 1.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE is de maximum cardinaliteit bij een rol altijd 1'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 1, 2, 1, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 2 groter is dan 1.]'

GO

/*=========================
ONE - ONE Relatie waar de maximum cardinaliteit van rol 1 en 2 groter is dan 1.
=========================*/
create PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 1 en 2 groter is dan 1.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij het cardinaliteitstype ONE - ONE is de maximum cardinaliteit bij een rol altijd 1'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - ONE', 1, 1, 2, 4, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - ONE relatie waarbij de maximum cardinaliteit van rol 1 en 2 groter is dan 1.]'

GO


/*=========================
ONE - MANY Relatie waar de primaire rol dependent is.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - MANY Relatie waar de primaire rol dependent is.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij een ONE - MANY relationship mag de primaire rol niet dependent zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 1, 0, 'ONE - MANY', 2, 2, 1, 4, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - MANY Relatie waar de primaire rol dependent is.]'

GO


/*=========================
MANY - ONE Relatie waar de secundaire rol dependent is.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = MANY - ONE Relatie waar de secundaire rol dependent is.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij een MANY - ONE relationship mag de secundaire rol niet dependent zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 0, 1, 'MANY - ONE', 2, 2, 1, 4, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = MANY - ONE Relatie waar de secundaire rol dependent is.]'

GO

/*=========================
ONE - MANY Relatie waar de maximum cardinaliteit van rol 2 groter is dan 1
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = ONE - MANY Relatie waar de maximum cardinaliteit van rol 2 groter is dan 1]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij een cardinaliteitstype van ONE - MANY moet de maximale cardinaliteit van de secundaire rol 1 zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 0, 0, 'ONE - MANY', 0, 0, 1, 4, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = ONE - MANY Relatie waar de maximum cardinaliteit van rol 2 groter is dan 1]'

GO


/*=========================
MANY - ONE relatie waar de maximale cardinaliteit van de primaire rol groter is dan 1.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = MANY - ONE relatie waar de maximale cardinaliteit van de primaire rol groter is dan 1]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Bij een cardinaliteitstype van MANY - ONE moet de maximale cardinaliteit van de primaire rol 1 zijn.'
		  
	--Act
	EXEC usp_relatieUitbreidenMetPowerDesignerGegevens 'Relatie123', 'Bonno123', 1, 1, 2, 0, 0, 'MANY - ONE', 0, 0, 2, 1, 1, 0, 0, 1

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = MANY - ONE relatie waar de maximale cardinaliteit van de primaire rol groter is dan 1]'

GO

/*=========================
Er wordt een onjuiste waarde voor cardinaliteitstype in de relatie tabel gezet
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Er wordt een onjuiste waarde voor cardinaliteitstype in de relatie tabel gezet]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	insert into VERBALISATIE values (1, 'Bonno123', 'Er is een fiets met ID 5'), (1, 'Bonno123', 'Hey dit is een andere verbalisatie')
	insert into ENTITEIT_IN_VERBALISATIE values ('Bonno123', 1, 1, 0), ('Bonno123', 2, 1, 0), ('Bonno123', 3, 2, 0)
	insert into PROJECTBEZETTING values ('Bonno123', 1, 1), ('Luciano123', 1, 0), ('Jacques123', 2, 0)
	insert into ENTITEIT_IN_PROJECT VALUES (null, 1, 'Bonno123', 'FIETS'), (null, 1, 'Bonno123', 'POST'), (null, 2, 'Bonno123', 'FIETS')
    	  
	EXEC tSQLt.ApplyConstraint @TableName ='dbo.RELATIE', @ConstraintName = 'ValuesOfCardinaliteitstype'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "ValuesOfCardinaliteitstype". The conflict occurred in database "FactBasedAnalysis", table "dbo.RELATIE", column ''CARDINALITEITSTYPE''.'
		  
	--Act
	insert into RELATIE (GEBRUIKERSNAAM, VERBALISATIE_ID, RELATIE_NAAM, CARDINALITEITSTYPE) VALUES
	('Bonno123', 1, 'Relatienaam', 'Test')

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Er wordt een onjuiste waarde voor cardinaliteitstype in de relatie tabel gezet]'

GO


	  	  
/*=========================
Een mandatory role heeft een cardinaliteit van 0
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Een mandatory role heeft een minimum cardinaliteit van 0]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'MandatoryRolesRequire1MinimumCardinality'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "MandatoryRolesRequire1MinimumCardinality". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL".'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, 0, 1, 1, 1, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Een mandatory role heeft een minimum cardinaliteit van 0]'

GO

  	  
/*=========================
Een niet mandatory role heeft een minimum cardinaliteit van 1 of groter.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Een niet mandatory role heeft een minimum cardinaliteit van 1 of groter]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'MandatoryRolesRequire1MinimumCardinality'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "MandatoryRolesRequire1MinimumCardinality". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL".'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, 1, 1, 0, 0, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Een niet mandatory role heeft een minimum cardinaliteit van 1 of groter]'

GO

/*=========================
Een dependent rol is niet mandatory
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Een dependent rol is niet mandatory]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'IfRolIsDependentItIsAlsoMandatory'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "IfRolIsDependentItIsAlsoMandatory". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL".'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, 1, 1, 0, 1, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Een dependent rol is niet mandatory]'

GO

/*=========================
Minimale cardinaliteit mag niet kleiner zijn dan 0.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Een minimale cardinaliteit mag niet kleiner zijn dan 0]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'MinimumCardinalityCantBeLowerThan0'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "MinimumCardinalityCantBeLowerThan0". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL", column ''CARDINALITEIT_MINIMUM''.'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, -1, 1, 0, 1, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Een minimale cardinaliteit mag niet kleiner zijn dan 0]'


GO
/*=========================
Maximale cardinaliteit mag niet kleiner dan 1 zijn.
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Maximale cardinaliteit mag niet kleiner dan 1 zijn.]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'MaximumCardinalityCantBeLowerThan0'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "MaximumCardinalityCantBeLowerThan0". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL", column ''CARDINALITEIT_MAXIMUM''.'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, 0, 0, 0, 1, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Maximale cardinaliteit mag niet kleiner dan 1 zijn.]'

GO

/*=========================
Minimale cardinaliteit mag niet groter zijn dan de maximale cardinaliteit
=========================*/
CREATE PROCEDURE [benoemenRelatieViaPowerdesigner].[test = Minimale cardinaliteit mag niet groter zijn dan de maximale cardinaliteit]  
AS
BEGIN
--Assemble
	EXEC tSQLt.FakeTable 'dbo', 'ROL';
	EXEC tSQLt.FakeTable 'dbo', 'PROJECTBEZETTING';
	EXEC tSQLt.FakeTable 'dbo', 'ENTITEIT_IN_VERBALISATIE';
	EXEC tSQLt.FakeTable
           'dbo.VERBALISATIE',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.ENTITEIT_IN_PROJECT',@Identity = 1;
	EXEC tSQLt.FakeTable 
           'dbo.RELATIE', @Identity = 1

	EXEC tSQLt.ApplyConstraint @TableName ='dbo.ROL', @ConstraintName = 'MinimumCardinalityCantBeBiggerThanMaximumCardinality'

    --Assert
	EXEC tSQLt.ExpectException @ExpectedMessage = 'The INSERT statement conflicted with the CHECK constraint "MinimumCardinalityCantBeBiggerThanMaximumCardinality". The conflict occurred in database "FactBasedAnalysis", table "dbo.ROL".'
		  
	--Act
	insert into ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, 
					 IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(1, 2, 1, 'Bonno123', 1, 2, 1, 0, 1, 0)
	

END

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner].[test = Minimale cardinaliteit mag niet groter zijn dan de maximale cardinaliteit]'

GO

EXEC [tSQLt].[Run] '[benoemenRelatieViaPowerdesigner]'
