/*
Use Case Business rule toevoegen

Gebruikers kunnen een business rule toevoegen aan een project. Na het aanmaken van een business rule, kan de gebruiker
deze bekijken.

Relaties die meldingen kunnen geven:
Wanneer het blijkt dat een user niet (meer) bestaat, wordt er een error getoond door MSSQL. Dit is het gevolg van het
feit dat er een relatie is tussen de kolom GEBRUIKERSNAAM in de tabellen BUSINESS_RULE en GEBRUIKER. Daarnaast zal er een
error worden weergegeven wanneer er blijkt dat het project niet (meer) bestaat in de tabel PROJECT. De oorzaak hiervan
is eveneens een relatie, welke in dit geval ligt tussen de tabellen BUSINESS_RULE en PROJECT.

Tabellen die beinvloed worden door de use case business rule toevoegen:
- BUSINESS_RULE: Bij het aanmaken van een business rule, wordt er een nieuw record in de tabel BUSINESS_RULE toegevoegd.
  Hierbij wordt de gebruiker geregistreerd, zodat de eigenaar van het project alle informatie terug kan vinden in op de
  geschiedenispagina
- hist_BUSINESS_RULE: Wanneer er een actie in de tabel BUSINESS_RULE plaats vindt, zullen deze veranderde waarden door
  een trigger in de history tabel van BUSINESS_RULE gezet worden.


INSERT:

Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer een insert statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @BusinessRuleSentence             datatype: varchar(300)    Bevat de business rule die toegevoegd moet worden aan een
                                                                project
- @userName                         datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is


UPDATE:

Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer een update statement uit welke de waarde van het record verandert naar de nieuwe meegegeven waarden.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters
- Een gebruiker kan een business rule aanpassen welke niet aangemaakt is door deze gebruiker. Hierbij zal de laaste
  gebruiker geregistreerd worden in de tabel.

Parameters:
- @ProjectID                        datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @BusinessRuleSentence             datatype: varchar(300)    Bevat de business rule die toegevoegd moet worden aan een
                                                                project
- @userName                         datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is
- @BusinessRuleSentenceOriginal     datatype: varchar(300)    Bevat de business rule die aangepast moet worden naar de
                                                                veranderde business rule
- @userNameOriginal                 datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                de originele business rule toegevoegd heeft



DELETE:

- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer het delete statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- De ingevulde business rule bestaat uit meer dan 300 karakters
- Een gebruiker kan een business rule aanpassen welke niet aangemaakt is door deze gebruiker. Hierbij zal de laaste
  gebruiker geregistreerd worden in de tabel.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @BusinessRuleSentence             datatype: varchar(300)    Bevat de business rule die toegevoegd moet worden aan een
                                                                project
- @userName                         datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is


 */


-- CREATE STORED PROCEDURE
GO
IF EXISTS(select * from sys.procedures where name='usp_CreateNewBusinessRule')
BEGIN
DROP PROC usp_CreateNewBusinessRule
END
GO
CREATE PROCEDURE usp_CreateNewBusinessRule
  @ProjectID int,
  @BusinessRuleSentence varchar(300),
  @userName varchar(20)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

      /*
      Stappen voor insert stored procedure
        Controleer of de gebruiker lid is van het project met @PROJECT_ID
        Voer een insert statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen
	    */

        IF EXISTS
            ( SELECT 1
              FROM PROJECTBEZETTING
              WHERE GEBRUIKERSNAAM = @userName
              AND PROJECT_ID = @ProjectID
            )
            BEGIN
            IF NOT EXISTS
                  ( SELECT 1
                    FROM BUSINESS_RULE
                    WHERE GEBRUIKERSNAAM = @userName
                    AND PROJECT_ID = @ProjectID
                    AND BUSINESS_RULE_ZIN = @BusinessRuleSentence
                  )
                  BEGIN
                  INSERT INTO BUSINESS_RULE (GEBRUIKERSNAAM, BUSINESS_RULE_ZIN, PROJECT_ID)
                  VALUES (@userName, @BusinessRuleSentence, @ProjectID)
                  END
            ELSE
              BEGIN
                ;THROW 50001, 'De gebruiker heeft deze business rule reeds aangemaakt.', 1;
              END
            END
        ELSE
          BEGIN
              ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
          END

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO



-- UPDATE STORED PROCEDURE

GO
IF EXISTS(select * from sys.procedures where name='usp_UpdateBusinessRule')
BEGIN
DROP PROC usp_UpdateBusinessRule
END
GO
CREATE PROCEDURE usp_UpdateBusinessRule
  @ProjectID int,
  @BusinessRuleSentence varchar(300),
  @userName varchar(20),
  @BusinessRuleSentenceOriginal varchar(300),
  @userNameOriginal varchar(20)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	    /*
	    Stappen voor update stored procedure:
        - Controleer of de gebruiker lid is van het project met @PROJECT_ID
        - Voer een update statement uit welke de waarde van het record verandert naar de nieuwe meegegeven waarden.
	    */

       IF EXISTS
          ( SELECT 1
            FROM PROJECTBEZETTING
            WHERE GEBRUIKERSNAAM = @userName
            AND PROJECT_ID = @ProjectID
          )
          BEGIN
              IF NOT EXISTS
                  ( SELECT 1
                    FROM BUSINESS_RULE
                    WHERE GEBRUIKERSNAAM = @userName
                    AND PROJECT_ID = @ProjectID
                    AND BUSINESS_RULE_ZIN = @BusinessRuleSentence
                  )
                  BEGIN
                    UPDATE BUSINESS_RULE
                    SET GEBRUIKERSNAAM = @userName, BUSINESS_RULE_ZIN = @BusinessRuleSentence
                    WHERE PROJECT_ID = @ProjectID
                    AND BUSINESS_RULE_ZIN = @BusinessRuleSentenceOriginal
                    AND GEBRUIKERSNAAM = @userNameOriginal
                  END
              ELSE
                BEGIN
                  ;THROW 50001, 'De gebruiker heeft reeds een business rule aangemaakt met deze zin.', 1;
                END
          END
        ELSE
          BEGIN
              ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
          END

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO



-- DELETE STORED PROCEDURE

GO
IF EXISTS(select * from sys.procedures where name='usp_DeleteBusinessRule')
BEGIN
DROP PROC usp_DeleteBusinessRule
END
GO
CREATE PROCEDURE usp_DeleteBusinessRule
  @ProjectID int,
  @BusinessRuleSentence varchar(300),
  @userName varchar(20)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	    /*
	    Stappen voor delete stored procedure:
        - Controleer of de gebruiker lid is van het project met @PROJECT_ID
        - Voer het delete statement uit op de tabel BUSINESS_RULE om de business rule toe te voegen.
	    */

       IF EXISTS
          ( SELECT 1
            FROM PROJECTBEZETTING
            WHERE GEBRUIKERSNAAM = @userName
            AND PROJECT_ID = @ProjectID
          )
          BEGIN
            DELETE FROM BUSINESS_RULE
            WHERE PROJECT_ID = @ProjectID
              AND BUSINESS_RULE_ZIN = @BusinessRuleSentence
              AND GEBRUIKERSNAAM = @userName
          END
        ELSE
          BEGIN
              ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
          END

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO


EXEC tSQLt.NewTestClass 'TestClassBusinessRule';
GO

/*
Test scenario 1
Business rule toevoegen aan een project welke nog niet bestaat.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User adds a business rule successfully]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)


		EXEC tSQLt.ExpectNoException

		EXEC usp_CreateNewBusinessRule
		  @ProjectID = 1,
		  @BusinessRuleSentence = 'This is an example business rule',
		  @userName = 'TestUser'

    END;
GO

/*
Test scenario 2
Business rule toevoegen aan een project welke al door dezelfde user aangemaakt is.
Foutmelding: 'De gebruiker heeft deze business rule reeds aangemaakt.'
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User adds a business rule, which already exists. Error will be shown]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES ('TestUser','This is an example business rule',1)


		EXEC tSQLt.ExpectException
      @ExpectedMessage = 'De gebruiker heeft deze business rule reeds aangemaakt.';

		EXEC usp_CreateNewBusinessRule
		  @ProjectID = 1,
		  @BusinessRuleSentence = 'This is an example business rule',
		  @userName = 'TestUser'

    END;
GO

/*
Test scenario 3
Business rule toevoegen door een user die niet werkt aan het project .
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User adds a business rule to a project that he's not assigned to. Error will be shown]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)


		EXEC tSQLt.ExpectException
      @ExpectedMessage = 'De gebruiker is niet lid van het project.';

		EXEC usp_CreateNewBusinessRule
		  @ProjectID = 2,
		  @BusinessRuleSentence = 'This is an example business rule',
		  @userName = 'TestUser'

    END;
GO

/*
Test scenario 4
Business rule updaten door een user die werkt aan het project.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User updates a business rule successfully. No error expected.]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES ('TestUser','This is an example business rule',1)


		EXEC tSQLt.ExpectNoException


		EXEC usp_UpdateBusinessRule
		  @ProjectID = 1,
		  @BusinessRuleSentence = 'This is an example business rule part two',
		  @userName = 'TestUser',
		  @BusinessRuleSentenceOriginal = 'This is an example business rule',
		  @userNameOriginal = 'TestUser'

    END;
GO

/*
Test scenario 5
Business rule updaten naar een business rule die al aangemaakt is door deze user binnen dit project.
Foutmelding: 'De gebruiker heeft reeds een business rule aangemaakt met deze zin.'
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User updates a business rule to a value that already exists. An error will be shown.]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES
		('TestUser','This is an example business rule',1),
		('TestUser','This is an example business rule part two',1)


EXEC tSQLt.ExpectException
      @ExpectedMessage = 'De gebruiker heeft reeds een business rule aangemaakt met deze zin.';

		EXEC usp_UpdateBusinessRule
		  @ProjectID = 1,
		  @BusinessRuleSentence = 'This is an example business rule part two',
		  @userName = 'TestUser',
		  @BusinessRuleSentenceOriginal = 'This is an example business rule',
		  @userNameOriginal = 'TestUser'

    END;
GO

/*
Test scenario 6
Business rule toevoegen door een user die niet werkt aan het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User updates a business rule in a project that he's not assigned to. Error will be shown]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES('TestUser','This is an example business rule',1)


		EXEC tSQLt.ExpectException
      @ExpectedMessage = 'De gebruiker is niet lid van het project.';

		EXEC usp_UpdateBusinessRule
		  @ProjectID = 2,
		  @BusinessRuleSentence = 'This is an example business rule part two',
		  @userName = 'TestUser',
		  @BusinessRuleSentenceOriginal = 'This is an example business rule',
		  @userNameOriginal = 'TestUser'

    END;
GO


/*
Test scenario 7
Business rule verwijderen.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassBusinessRule.[test = User deletes a business rule successfully. No error expected.]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES ('TestUser','This is an example business rule',1)


		EXEC tSQLt.ExpectNoException


		EXEC usp_DeleteBusinessRule
		  @ProjectID = 1,
		  @BusinessRuleSentence = 'This is an example business rule',
		  @userName = 'TestUser'

    END;
GO

/*
Test scenario 8
Business rule verwijderen door een user die niet werkt aan het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassBusinessRule.[test = A business rule is deleted by a user that is not assigned to the project. Error will be shown.]
AS
    BEGIN

    EXEC tSQLt.FakeTable
             'dbo.BUSINESS_RULE',@Identity = 1, @Defaults = 1;
    EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.GEBRUIKER',@Defaults = 1;


    INSERT INTO GEBRUIKER VALUES('TestUser','TestPassword')

		INSERT INTO PROJECT VALUES('TestProject')

		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1)

		INSERT INTO BUSINESS_RULE VALUES ('TestUser','This is an example business rule',1)


		EXEC tSQLt.ExpectException
      @ExpectedMessage = 'De gebruiker is niet lid van het project.';


		EXEC usp_DeleteBusinessRule
		  @ProjectID = 2,
		  @BusinessRuleSentence = 'This is an example business rule',
		  @userName = 'TestUser'

    END;
GO


---------------------
EXEC [tSQLt].[Run] 'TestClassBusinessRule'
