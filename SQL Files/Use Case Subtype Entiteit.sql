/*
Use Case subtype entiteit toevoegen

Gebruikers kunnen een subtype entiteit toevoegen aan een project en hier attributen aan koppelen. Na het aanmaken van
deze entiteiten en attributen, kan de gebruiker deze bekijken.

Relaties die meldingen kunnen geven:
Wanneer het blijkt dat een user niet (meer) bestaat, wordt er een error getoond door MSSQL. Dit is het gevolg van het
feit dat er een relatie is tussen de kolom GEBRUIKERSNAAM in de tabellen ENTITEIT_IN_PROJECT en GEBRUIKER, en
ATTRIBUUT_IN_SUBTYPE_ENTITEIT en GEBRUIKER. Daarnaast zal er een error worden weergegeven wanneer er blijkt dat het
project niet (meer) bestaat in de tabel PROJECT. De oorzaak hiervan is eveneens een relatie, welke in dit geval ligt
tussen de tabellen ENTITEIT_IN_PROJECT en PROJECT, en ATTRIBUUT_IN_SUBTYPE_ENTITEIT en PROJECT.

Tabellen die beinvloed worden door de use case subtype entiteit toevoegen:
- ENTITEIT_IN_PROJECT: Bij het aanmaken, wijzigen en verwijderen van een subtype entiteit zal deze tabel bewerkt worden.
- ATTRIBUUT_IN_SUBTYPE_ENTITEIT: Bij het aanmaken, wijzigen en verwijderen van een attribuut aan/bij een subtype entiteit
  zal deze tabel bewerkt worden
- Geschiedenis tabellen va ENTITEIT_IN_PROJECT en ATTRIBUUT_IN_SUBTYPE_ENTITEIT: Wanneer er een actie in de tabel
  ENTITEIT_IN_PROJECT of ATTRIBUUT_IN_SUBTYPE_ENTITEIT plaats vindt, zullen deze veranderde waarden door een trigger in
  de history tabel van ENTITEIT_IN_PROJECT of ATTRIBUUT_IN_SUBTYPE_ENTITEIT gezet worden.


INSERT:


Entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een entiteit bestaat binnen het project met dezelfde naam
- Voer een insert statement uit op de tabel ENTITEIT_IN_PROJECT om de entiteit toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is

Attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een attribuut in de entiteit bestaat binnen het project met dezelfde naam.
- Controleer of mandatory de waarde 1 heeft, wanneer de primary identifier de waarde 1 heeft.
- Voer een insert statement uit op de tabel ATTRIBUUT_IN_SUBTYPE_ENTITEIT om de attribuut toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is
- @subEntityNameAttribute            datatype: varchar(50)     Bevat de naam van de gewenste attribuut.
- @primaryIdentifier                 datatype: bit             Bevat de bit waarde of het attribuut deel uitmaakt van
                                                                de primary identifier
- @mandatory                         datatype: bit             Bevat de bit waarde of het attribuut mandatory is.



UPDATE:


Entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of de gewenste entiteitnaam nog niet gebruikt wordt in het project.
- Voer een update statement uit welke de waarde van het record verandert naar de nieuwe meegegeven waarden.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is
- @subEntityNameOriginal             datatype: varchar(50)     Bevat de naam van de entiteit die veranderd moet worden.


Attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of er al een attribuut in de entiteit bestaat binnen het project met dezelfde naam.
- Controleer of mandatory de waarde 1 heeft, wanneer de primary identifier de waarde 1 heeft.
- Voer een update statement uit op de tabel ATTRIBUUT_IN_SUBTYPE_ENTITEIT om de attribuut toe te voegen.

Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.
- Er bestaat al een entiteit met deze naam.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is
- @subEntityNameAttribute            datatype: varchar(50)     Bevat de naam van de gewenste attribuut.
- @primaryIdentifier                 datatype: bit             Bevat de bit waarde of het attribuut deel uitmaakt van
                                                                de primary identifier
- @mandatory                         datatype: bit             Bevat de bit waarde of het attribuut mandatory is.
- @subEntityNameAttributeOrigineel   datatype: varchar(50)     Bevat de naam van de attribuut die veranderd moet worden.


DELETE:


Entiteit:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Controleer of de entiteit geen attributen meer bevat.
- Voer het delete statement uit

Problemen/situaties die voor kunnen komen bij het uitvoeren van de update stored procedure:
- De gebruiker is geen lid van het project.
- Er zijn nog attributen aanwezig die horen bij de entiteit.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is


Attribuut:


Acties om de stored procedure goed te laten verlopen:
- Controleer of de gebruiker lid is van het project met @PROJECT_ID
- Voer het delete statement uit


Problemen/situaties die voor kunnen komen bij het uitvoeren van de insert stored procedure:
- De gebruiker is geen lid van het project.

Parameters:
- @ProjectID                         datatype: int             Bevat de waarde van het ID van het project waarbinnen de
                                                                business rule aangemaakt is.
- @subEntityName                     datatype: varchar(50)     Bevat de naam die de entiteit moet krijgen.
- @userName                          datatype: varchar(20)     Bevat de waarde van de gebruikersnaam van de gebruiker die
                                                                ingelogd is
- @subEntityNameAttribute            datatype: varchar(50)     Bevat de naam van de gewenste attribuut.



 */


use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name='usp_CreateNewsubEntity')
	BEGIN
		DROP PROC usp_CreateNewsubEntity
	END
GO

CREATE PROCEDURE usp_CreateNewsubEntity @userName VARCHAR(20),
                                        @ProjectID INT,
                                        @subEntityName VARCHAR(50)
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY


  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN
      IF NOT EXISTS
        (SELECT 1
         FROM ENTITEIT_IN_PROJECT
         WHERE GEBRUIKERSNAAM = @userName
           AND PROJECT_ID = @ProjectID
           AND ENTITEIT_NAAM = @subEntityName
        )
        BEGIN
          INSERT INTO ENTITEIT_IN_PROJECT (GEBRUIKERSNAAM, ENTITEIT_NAAM, PROJECT_ID)
          VALUES (@userName, @subEntityName, @ProjectID)
        END
      ELSE
        BEGIN
          ;THROW 50001, 'Er bestaat al een entiteit met deze naam.', 1;
        END
    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
go


IF EXISTS(select * from sys.procedures where name='usp_CreateNewSubEntityAttribute')
	BEGIN
		DROP PROC usp_CreateNewSubEntityAttribute
	END
GO

CREATE PROCEDURE usp_CreateNewSubEntityAttribute @userName VARCHAR(20),
                                                 @ProjectID INT,
                                                 @subEntityName VARCHAR(50),
                                                 @subEntityNameAttribute VARCHAR(50),
                                                 @primaryIdentifier BIT,
                                                 @mandatory BIT
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY


  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN

      DECLARE @entity_id int

      SET @entity_id =
          (SELECT ENTITEIT_ID
           FROM ENTITEIT_IN_PROJECT
           WHERE PROJECT_ID = @ProjectID
             AND ENTITEIT_NAAM = @subEntityName
          )
      IF NOT EXISTS
        (SELECT 1
         FROM ATTRIBUUT_IN_SUBTYPE_ENTITEIT
         WHERE PROJECT_ID = @ProjectID
           AND ENTITEIT_ID = @entity_id
           AND ATTRIBUUT_NAAM = @subEntityNameAttribute
        )
        IF ((@primaryIdentifier = 1 AND @mandatory = 1) OR (@primaryIdentifier = 0 AND @mandatory = 1) OR
            (@primaryIdentifier = 0 AND @mandatory = 0))
          BEGIN
            INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (GEBRUIKERSNAAM, ATTRIBUUT_NAAM, PROJECT_ID,
                                                       IS_PRIMARY_IDENTIFIER, IS_MANDATORY, ENTITEIT_ID)
            VALUES (@userName, @subEntityNameAttribute, @ProjectID, @primaryIdentifier, @mandatory, @entity_id)
          END
        ELSE
          BEGIN
            ;THROW 50001, 'Een attribuut dat deel uitmaakt van de primary identifier, moet ook mandatory zijn.', 1;
          END
      ELSE
        BEGIN
          ;THROW 50001, 'Er bestaat al een attribuut in deze entiteit met deze naam.', 1;
        END
    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
GO


IF EXISTS(select * from sys.procedures where name='usp_UpdatesubEntity')
	BEGIN
		DROP PROC usp_UpdatesubEntity
	END
GO

CREATE PROCEDURE usp_UpdatesubEntity @userName VARCHAR(20),
                                     @ProjectID INT,
                                     @subEntityName VARCHAR(50),
                                     @subEntityNameOriginal VARCHAR(50)
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY

  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN
      IF NOT EXISTS
        (SELECT 1
         FROM ENTITEIT_IN_PROJECT
         WHERE GEBRUIKERSNAAM = @userName
           AND PROJECT_ID = @ProjectID
           AND ENTITEIT_NAAM = @subEntityName
        )
        BEGIN
          UPDATE ENTITEIT_IN_PROJECT
          SET GEBRUIKERSNAAM = @userName,
              ENTITEIT_NAAM  = @subEntityName,
              PROJECT_ID     = @ProjectID
          WHERE PROJECT_ID = @ProjectID
            AND ENTITEIT_NAAM = @subEntityNameOriginal
        END
      ELSE
        BEGIN
          ;THROW 50001, 'Er bestaat al een entiteit met deze naam.', 1;
        END
    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
go



IF EXISTS(select * from sys.procedures where name='usp_UpdateSubEntityAttribute')
	BEGIN
		DROP PROC usp_UpdateSubEntityAttribute
	END
GO

CREATE PROCEDURE usp_UpdateSubEntityAttribute @userName VARCHAR(20),
                                              @ProjectID INT,
                                              @subEntityName VARCHAR(50),
                                              @subEntityNameAttribute VARCHAR(50),
                                              @subEntityNameAttributeOrigineel VARCHAR(50),
                                              @primaryIdentifier BIT,
                                              @mandatory BIT
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY

  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN

      DECLARE @entity_id int

      SET @entity_id =
          (SELECT ENTITEIT_ID
           FROM ENTITEIT_IN_PROJECT
           WHERE PROJECT_ID = @ProjectID
             AND ENTITEIT_NAAM = @subEntityName
          )
      IF NOT EXISTS
        (SELECT 1
         FROM ATTRIBUUT_IN_SUBTYPE_ENTITEIT
         WHERE PROJECT_ID = @ProjectID
           AND ENTITEIT_ID = @entity_id
           AND ATTRIBUUT_NAAM = @subEntityNameAttribute
        )
        IF ((@primaryIdentifier = 1 AND @mandatory = 1) OR (@primaryIdentifier = 0 AND @mandatory = 1) OR
            (@primaryIdentifier = 0 AND @mandatory = 0))
          BEGIN
            UPDATE ATTRIBUUT_IN_SUBTYPE_ENTITEIT
            SET GEBRUIKERSNAAM        = @userName,
                ATTRIBUUT_NAAM        = @subEntityNameAttribute,
                IS_PRIMARY_IDENTIFIER = @primaryIdentifier,
                IS_MANDATORY          = @mandatory
            WHERE ATTRIBUUT_NAAM = @subEntityNameAttributeOrigineel
              AND ENTITEIT_ID = @entity_id
              AND PROJECT_ID = @ProjectID
          END
        ELSE
          BEGIN
            ;THROW 50001, 'Een attribuut dat deel uitmaakt van de primary identifier, moet ook mandatory zijn.', 1;
          END
      ELSE
        BEGIN
          ;THROW 50001, 'Er bestaat al een attribuut in deze entiteit met de gewenste naam.', 1;
        END
    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
GO


IF EXISTS(select * from sys.procedures where name='usp_DeleteSubEntity')
	BEGIN
		DROP PROC usp_DeleteSubEntity
	END
GO

CREATE PROCEDURE usp_DeleteSubEntity @userName VARCHAR(20),
                                     @ProjectID INT,
                                     @subEntityName VARCHAR(50)
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY

  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN
      DECLARE @entity_id int

      SET @entity_id =
          (SELECT ENTITEIT_ID
           FROM ENTITEIT_IN_PROJECT
           WHERE PROJECT_ID = @ProjectID
             AND ENTITEIT_NAAM = @subEntityName
          )
      IF NOT EXISTS
        (SELECT 1
         FROM ATTRIBUUT_IN_SUBTYPE_ENTITEIT
         WHERE PROJECT_ID = @ProjectID
           AND ENTITEIT_ID = @entity_id
        )
        BEGIN
          DELETE
          FROM ENTITEIT_IN_PROJECT
          WHERE PROJECT_ID = @ProjectID
            AND ENTITEIT_ID = @entity_id
        END
      ELSE
        BEGIN
          ;THROW 50001, 'Deze entiteit bevat nog attributen.', 1;
        END
    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
go


IF EXISTS(select * from sys.procedures where name='usp_DeleteSubEntityAttribute')
	BEGIN
		DROP PROC usp_DeleteSubEntityAttribute
	END
GO

CREATE PROCEDURE usp_DeleteSubEntityAttribute @userName VARCHAR(20),
                                              @ProjectID INT,
                                              @subEntityName VARCHAR(50),
                                              @subEntityNameAttribute VARCHAR(50)
AS
SET NOCOUNT ON;
SET XACT_ABORT OFF;
  --check if there is already an tran running if yes set savepoint else start own
DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF @TranCounter > 0
  SAVE TRANSACTION ProcedureSave;
ELSE
  BEGIN TRANSACTION
  ;
BEGIN TRY

  IF EXISTS
    (SELECT 1
     FROM PROJECTBEZETTING
     WHERE GEBRUIKERSNAAM = @userName
       AND PROJECT_ID = @ProjectID
    )
    BEGIN

      DECLARE @entity_id int

      SET @entity_id =
          (SELECT ENTITEIT_ID
           FROM ENTITEIT_IN_PROJECT
           WHERE PROJECT_ID = @ProjectID
             AND ENTITEIT_NAAM = @subEntityName
          )

      DELETE
      FROM ATTRIBUUT_IN_SUBTYPE_ENTITEIT
      WHERE PROJECT_ID = @ProjectID
        AND ENTITEIT_ID = @entity_id
        AND ATTRIBUUT_NAAM = @subEntityNameAttribute

    END
  ELSE
    BEGIN
      ;THROW 50001, 'De gebruiker is niet lid van het project.', 1;
    END

  --If there are no errors the tran must be commited else jump to catch block to close tran
  IF @TranCounter = 0
    AND XACT_STATE() = 1
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @TranCounter = 0
    BEGIN
      --started own tran so rollback it
      IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;
    END;
  ELSE
    BEGIN
      --not started own tran rollback to save point
      IF XACT_STATE() <> -1
        ROLLBACK TRANSACTION ProcedureSave;
    END;
  THROW;
END CATCH;
GO



EXEC tSQLt.NewTestClass 'TestClassSubEntity';
GO

/*
Test scenario 1
Entiteit toevoegen aan een project welke nog niet bestaat.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an entity successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)


  EXEC tSQLt.ExpectNoException

  EXEC usp_CreateNewsubEntity 'TestUser', 1, 'Test'

END;
GO

/*
Test scenario 2
Entiteit toevoegen door een gebruiker die niet lid is van het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an entity, but the user is no member of the project. Error will be shown]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_CreateNewsubEntity 'TestUser', 2, 'Test'

END;
GO

/*
Test scenario 3
Entiteit toevoegen aan een project welke al door dezelfde user aangemaakt is.
Foutmelding: 'Er bestaat al een entiteit met deze naam.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an entity, which already exists. Error will be shown]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (project_id, gebruikersnaam, entiteit_naam) VALUES (1, 'TestUser', 'Test')


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Er bestaat al een entiteit met deze naam.';

  EXEC usp_CreateNewsubEntity 'TestUser', 1, 'Test'

END;
GO


/*
Test scenario 4
Een attribuut toevoegen, welke nog niet bestaat, aan een entiteit.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an attribute successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')


  EXEC tSQLt.ExpectNoException

  EXEC usp_CreateNewSubEntityAttribute 'TestUser', 1, 'Test', 'testAttribute', 1, 1

END;
GO

/*
Test scenario 5
Een attribuut toevoegen, welke nog niet bestaat, aan een entiteit, door een gebruiker die niet lid is van het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an attribute, but the user is no member of the project. Error will be shown]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_CreateNewSubEntityAttribute 'TestUser1', 1, 'Test', 'testAttribute', 1, 1

END;
GO


/*
Test scenario 6
Attribute toevoegen aan een project welke al aangemaakt is.
Foutmelding: 'Er bestaat al een attribuut in deze entiteit met deze naam.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User adds an attribute, which already exists. Error will be shown]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT', @IDENTITY =1;
  EXEC tSQLt.FakeTable
       'dbo.PROJECT', @IDENTITY =1;
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING', @IDENTITY =1;
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT', @IDENTITY =1;


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECTNAAM) VALUES ('TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (project_id, gebruikersnaam, entiteit_naam) VALUES (1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)

  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Er bestaat al een attribuut in deze entiteit met deze naam.';

  EXEC usp_CreateNewSubEntityAttribute 'TestUser', 1, 'Test', 'testAttribute', 1, 1

END;
GO

/*
Test scenario 7
Entiteit updaten naar waarden welke nog niet bestaat.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an entity successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)

  EXEC tSQLt.ExpectNoException

  EXEC usp_UpdatesubEntity @UserName='TestUser', @ProjectID = 1, @subEntityName = 'testAttribute1',
       @subEntityNameOriginal = 'testAttribute'


END;
GO



/*
Test scenario 8
Entiteit updaten door een gebruiker die niet in het project zit.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an entity. The user is not a member of the project.]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')

  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_UpdatesubEntity @UserName='TestUser1', @ProjectID = 1, @subEntityName = 'testAttribute1',
       @subEntityNameOriginal = 'testAttribute'


END;
GO


/*
Test scenario 9
Entiteit updaten naar een entiteit met een bestaande naam.
Foutmelding: 'Er bestaat al een entiteit met deze naam.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an entity to a name that already exists.]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test'),
         (1, 1, 'TestUser', 'Test1')

  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Er bestaat al een entiteit met deze naam.';

  EXEC usp_UpdatesubEntity @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test1',
       @subEntityNameOriginal = 'Test'


END;
GO

/*
Test scenario 10
attribuut updaten naar waarden welke nog niet bestaat.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an attribute successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)

  EXEC tSQLt.ExpectNoException

  EXEC usp_UpdateSubEntityAttribute @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute1',
       @subEntityNameAttributeOrigineel = 'testAttribute', @primaryIdentifier = 1, @mandatory = 1


END;
GO



/*
Test scenario 11
Attribuut updaten door een gebruiker die niet in het project zit.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an attribute. The user is not a member of the project.]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_UpdateSubEntityAttribute @UserName='TestUser1', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute1',
       @subEntityNameAttributeOrigineel = 'testAttribute', @primaryIdentifier = 1, @mandatory = 1


END;
GO


/*
Test scenario 12
Attribuut updaten naar een naam die al bestaat.
Foutmelding: 'Er bestaat al een attribuut in deze entiteit met de gewenste naam.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an attribute to a name that already exists.]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1),
         ('TestUser', 'testAttribute1', 1, 1, 1, 1)

  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Er bestaat al een attribuut in deze entiteit met de gewenste naam.';

  EXEC usp_UpdateSubEntityAttribute @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute1', @subEntityNameAttributeOrigineel = 'testAttribute',
       @primaryIdentifier = 1, @mandatory = 1


END;
GO


/*
Test scenario 13
Een attribuut updaten welke wel deel uitmaakt van de primary identifier, maar niet mandatory is.
Foutmelding: 'Een attribuut dat deel uitmaakt van de primary identifier, moet ook mandatory zijn.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User updates an attribute. Primary identifier is 1, mandatory is 0]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)

  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Een attribuut dat deel uitmaakt van de primary identifier, moet ook mandatory zijn.';

  EXEC usp_UpdateSubEntityAttribute @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute1', @subEntityNameAttributeOrigineel = 'testAttribute',
       @primaryIdentifier = 1, @mandatory = 0


END;
GO

/*
Test scenario 14
entiteit verwijderen welke voldoet aan alle eisen.
Foutmelding: geen foutmelding verwacht
*/
CREATE PROCEDURE TestClassSubEntity.[test = User deletes an entity successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')


  EXEC tSQLt.ExpectNoException

  EXEC usp_DeleteSubEntity @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test'


END;
GO


/*
Test scenario 15
entiteit verwijderen door een gebruiker welke niet deel uit maakt van het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
*/
CREATE PROCEDURE TestClassSubEntity.[test = User deletes an entity, but is not a member of the project]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES (1, 'TestUser', 'Test')


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_DeleteSubEntity @UserName='TestUser1', @ProjectID = 1, @subEntityName = 'Test'


END;
GO


/*
Test scenario 16
entiteit verwijderen welke nog attributen bevat.
Foutmelding: 'Deze entiteit bevat nog attributen.'
 */
CREATE PROCEDURE TestClassSubEntity.[test = User deletes an entity, but there are still attributes assigned to the entity]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'Deze entiteit bevat nog attributen.';

  EXEC usp_DeleteSubEntity @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test'


END;
GO


/*
Test scenario 17
attribuut succesvol verwijderen.
Foutmelding: geen foutmelding verwacht
 */
CREATE PROCEDURE TestClassSubEntity.[test = User deletes an attribute successfully]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)


  EXEC tSQLt.ExpectNoException

  EXEC usp_DeleteSubEntityAttribute @UserName='TestUser', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute'


END;
GO



/*
Test scenario 18
attribuut verwijderen door een gebruiker welke niet deel uit maakt van het project.
Foutmelding: 'De gebruiker is niet lid van het project.'
 */
CREATE PROCEDURE TestClassSubEntity.[test = User deletes an attribute. The user is not a member is of the project.]
AS
BEGIN

  EXEC tSQLt.FakeTable
       'dbo.ENTITEIT_IN_PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECT';
  EXEC tSQLt.FakeTable
       'dbo.PROJECTBEZETTING';
  EXEC tSQLt.FakeTable
       'dbo.GEBRUIKER';
  EXEC tSQLt.FakeTable
       'dbo.ATTRIBUUT_IN_SUBTYPE_ENTITEIT';


  INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES ('TestUser', 'TestPassword')

  INSERT INTO PROJECT (PROJECT_ID, PROJECTNAAM) VALUES (1, 'TestProject')

  INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES ('TestUser', 1, 1)

  INSERT INTO ENTITEIT_IN_PROJECT (ENTITEIT_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM)
  VALUES (1, 1, 'TestUser', 'Test')

  INSERT INTO ATTRIBUUT_IN_SUBTYPE_ENTITEIT (gebruikersnaam, attribuut_naam, project_id, is_primary_identifier,
                                             is_mandatory, entiteit_id)
  VALUES ('TestUser', 'testAttribute', 1, 1, 1, 1)


  EXEC tSQLt.ExpectException
       @ExpectedMessage = 'De gebruiker is niet lid van het project.';

  EXEC usp_DeleteSubEntityAttribute @UserName='TestUser1', @ProjectID = 1, @subEntityName = 'Test',
       @subEntityNameAttribute = 'testAttribute'


END;
GO


EXEC [tSQLt].[Run] 'TestClassSubEntity'
