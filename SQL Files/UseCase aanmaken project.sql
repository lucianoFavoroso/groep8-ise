
/*
Use Case Aanmaken project

Gebruikers kunnen binnen het systeem projecten aanmaken. Na het aanmaken van een project kan de gebruiker beginnen met zijn project.
Wanneer een gebruiker een project aanmaakt wordt hij automatisch de eigenaar van het project.

Tabellen die beinvloed worden door een project aanmaken: Project en ProjectBezetting.
Bij het aanmaken van een project wordt er een nieuw record in de Project tabel toegevoegd.
Wanneer een gebruiker een project maakt wordt hij gelijk de eigenaar en dus toegevoegd als record in ProjectBezetting.

Overige functionele eisen: Project verwijderen
Projecten kunnen verwijdert worden. Alleen de eigenaar van een project kan het project verwijderen.
Als de eigenaar een project verwijdert zullen de records in de database blijven bestaan maar wordt het gehele project weggeschreven in de history.

Create
Voor het aanmaken van een nieuw project wordt een stored procedure aangemaakt. Als parameter wordt de projectnaam en username meegeven.
Met deze parameters kan er een nieuw project aangemaakt worden. Projectnamen moeten per eigenaar uniek zijn. Een gebruiker mag dus niet twee keer het project met dezelfde naam hebben.

Beinvloede tabellen: Project, Projectbezetting.

Stap 1: Controleer of deze gebruiker al eigenaar is van een project met dezelfde naam als @ProjectNaam
		Zo ja geef een foutmelding.
Stap 2: Voer een insert uit op de Project tabel met @ProjectNaam
Stap 3: Voer een insert uit op de tabel ProjectBezetting met @ProjectNaam en @Username
Stap 4: Zet IS_EIGENAAR op 1 voor dit record

Wanneer het blijkt dat een user niet (meer) bestaat wordt er automatisch een error gegeven door MSSQL want er ligt een relatie tussen ProjectBezetting en Gebruiker.

Update
Alleen de eigenaar van een project mag de projectnaam veranderen.

Stap 1: Controleer of de @Username eigenaar is van dit project
Stap 2: Controleer of deze gebruiker al een project heeft met dezelfde naam als @NewProjectName
		Zo ja geef een foutmelding.
Stap 3: Update het record met als ID @ProjectID in de tabel Project naar @NewProjectName

Beinvloede tabellen: Project

Delete
Beinvloede tabellen: Project, Projectbezetting, Verbalisatie... etc en history tabellen.

Read
Beinvloede tabellen: Project

*/

--Create stored procedure
GO
IF EXISTS(select * from sys.procedures where name=N'usp_CreateNewProjectForUser')
BEGIN
DROP PROC usp_CreateNewProjectForUser
END
GO
CREATE PROCEDURE usp_CreateNewProjectForUser @ProjectNaam VARCHAR(50), 
                                            @UserName    VARCHAR(20)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

/*
	Stap 1: Controleer of deze gebruiker al een project heeft met dezelfde naam als @ProjectNaam
		Zo ja geef een foutmelding.
	Stap 2: Voer een insert uit op de Project tabel met @ProjectNaam
	Stap 3: Voer een insert uit op de tabel ProjectBezetting met @ProjectNaam en @Username
	*/
        IF EXISTS
        (
            SELECT 1
            FROM PROJECT P
                 INNER JOIN PROJECTBEZETTING PB ON P.PROJECT_ID = PB.PROJECT_ID
            WHERE PB.GEBRUIKERSNAAM = @UserName
                  AND PB.IS_EIGENAAR = 1
                  AND P.PROJECTNAAM = @ProjectNaam
        )
        BEGIN
                ;THROW 50001, 'Gebruiker heeft al een project met deze naam waar hij eigenaar van is', 1;
        END;
        INSERT INTO PROJECT (PROJECTNAAM)
        VALUES(@ProjectNaam);
        INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM,PROJECT_ID,IS_EIGENAAR)
        VALUES
        (@UserName, 
         IDENT_CURRENT('PROJECT'), 
         1
        );

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO

--Update stored procedure
GO
IF EXISTS(select * from sys.procedures where name=N'usp_UpdateProjectName')
BEGIN
DROP PROC usp_UpdateProjectName
END
GO
CREATE PROCEDURE usp_UpdateProjectName @ProjectID      INT, 
                                       @NewProjectName VARCHAR(50), 
                                       @UserName       VARCHAR(20)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	/*
	Stap 1: Controleer of de @Username eigenaar is van dit project
	Stap 2: Controleer of deze gebruiker al een project heeft met dezelfde naam als @NewProjectName
			Zo ja geef een foutmelding.
	Stap 3: Update het record met als ID @ProjectID in de tabel Project naar @NewProjectName
	*/

        --Check of de nieuwe projectnaam al bestaat bij deze user
        IF EXISTS
        (
            SELECT P.PROJECTNAAM
            FROM PROJECT P
                 INNER JOIN PROJECTBEZETTING PB ON P.PROJECT_ID = PB.PROJECT_ID
            WHERE PB.GEBRUIKERSNAAM = @UserName
                  AND PB.IS_EIGENAAR = 1
                  AND P.PROJECTNAAM = @NewProjectName
        )
            BEGIN
                ;THROW 50001, 'Gebruiker heeft al een project met deze naam waar hij eigenaar van is', 1;
        END;

		UPDATE PROJECT SET PROJECTNAAM = @NewProjectName

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO

EXEC tSQLt.NewTestClass 'TestCRUDProject';
GO

--Test scenario 1
--Gebruiker heeft al een project met deze naam Foutmelding wordt verwacht.
CREATE PROCEDURE TestCRUDProject.[test = Gebruiker heeft al een project met deze naam bij het aanmaken]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		   
		INSERT INTO GEBRUIKER VALUES('TestGebruiker','TestPassword')
			
        EXEC tSQLt.ExpectException 
             @ExpectedMessage = 'Gebruiker heeft al een project met deze naam waar hij eigenaar van is';

		EXEC usp_CreateNewProjectForUser 'TestGebruiker', 'test'

		EXEC usp_CreateNewProjectForUser 'TestGebruiker', 'test'
    END;
GO

--Test scenario 2
/*
Situatie:

�De gebruiker heeft een project met de naam �Project 1'. De gebruiker probeert een project aan te maken dat ook de naam �Project 1' heeft.
Verwachte resultaat:
Foutmelding
*/
CREATE PROCEDURE TestCRUDProject.[test = Gebruiker heeft nog geen project met deze naam dus insert moet doorgaan op project en projectbezetting tabellen]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		SELECT * INTO ExpectedProject FROM dbo.PROJECT
		SELECT * INTO ExpectedProjectBezetting FROM dbo.PROJECTBEZETTING
		
		INSERT INTO ExpectedProject VALUES ('test')
		INSERT INTO ExpectedProjectBezetting VALUES ('TestGebruiker',1,1)
		
		INSERT INTO GEBRUIKER VALUES('TestGebruiker','TestPassword')

		EXEC usp_CreateNewProjectForUser 'test', 'TestGebruiker'

		EXEC tSQLt.AssertEqualsTable ExpectedProject, PROJECT
		EXEC tSQLt.AssertEqualsTable ExpectedProjectBezetting, PROJECTBEZETTING

    END;
GO

--Test scenario 3
/*
Maar er bestaat al een project met deze naam waar een andere gebruiker de eigenaar is
*/
CREATE PROCEDURE TestCRUDProject.[test = Gebruiker heeft al een project met deze naam bij het updaten]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		INSERT INTO GEBRUIKER VALUES('TestGebruiker','TestPassword')
				
		INSERT INTO PROJECT VALUES('TestProject')
		
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker',1,1)


        EXEC tSQLt.ExpectException 
             @ExpectedMessage = 'Gebruiker heeft al een project met deze naam waar hij eigenaar van is';

		EXEC usp_UpdateProjectName 1,'TestProject','TestGebruiker'
    END;
GO

--Test scenario 4
--Naam van project wordt aangepast en deze naam bestaat nog niet bij deze gebruiker. Geen error verwacht.
CREATE PROCEDURE TestCRUDProject.[test = Naam van project wordt aangepast en deze naam bestaat nog niet bij deze gebruiker]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		INSERT INTO GEBRUIKER VALUES('TestGebruiker','TestPassword')
		INSERT INTO PROJECT VALUES('TestProject')
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker',1,1)

		EXEC usp_UpdateProjectName 1,'ProjectJantje','TestGebruiker'
    END;
GO

--Test scenario 5
--Gebruiker update ene projectnaam naar een niet bestaand project. Geen error verwacht
CREATE PROCEDURE TestCRUDProject.[test = Gebruiker update een projectnaam naar een niet bestaand project]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		--Test gebruiker 1
		INSERT INTO GEBRUIKER VALUES('TestGebruiker1','TestPassword')
		INSERT INTO PROJECT VALUES('TestProject')
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker1',1,1)

		--Test gebruiker 2
		INSERT INTO GEBRUIKER VALUES('TestGebruiker2','TestPassword')
		INSERT INTO PROJECT VALUES('TestProject')
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker2',2,1)

		EXEC usp_UpdateProjectName 1,'ProjectJantje','TestGebruiker'
    END;
GO

--Test scenario 6
--Gebruiker maakt project aan met dezelfde naam als een bestaand project waar hij zelf geen eigenaar van is maar wel lid van is. Geen error verwacht
CREATE PROCEDURE TestCRUDProject.[test = Gebruiker maakt project aan met dezelfde naam als een bestaand project waar hij lid van is maar geen eigenaar]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.PROJECTBEZETTING',@Defaults = 1;
		EXEC tSQLt.FakeTable 
             'dbo.GEBRUIKER',@Defaults = 1;

		--Test gebruiker 1
		INSERT INTO GEBRUIKER VALUES('TestGebruiker1','TestPassword')
		INSERT INTO PROJECT VALUES('TestProject')
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker1',1,1)

		--Test gebruiker 2
		INSERT INTO GEBRUIKER VALUES('TestGebruiker2','TestPassword')
		INSERT INTO PROJECT VALUES('TestProject')
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker2',2,1)
		--Voeg testgebruiker 2 toe aan project van test gebruiker 1 met dezelfde project naam
		INSERT INTO PROJECTBEZETTING VALUES('TestGebruiker2',1,0)


		EXEC usp_UpdateProjectName 1,'ProjectJantje','TestGebruiker'
    END;
GO

EXEC [tSQLt].[Run] 'TestCRUDProject'
