/*
Use case Invoeren subverbalisatie

Verbalisaties hebben subverbalisaties waar de zin het zelfde is maar de invul plekken van variablen anders is.
Deze subverbalisaties worden opgelsagen in een aparte tabel.
Subverbalisaties mogen niet het zelfde zijn als de parent verbalisatie of het zelfde zijn als een andere subverbalisatie bij deze parent verbalisatie.

Bij het invoeren van een subverbalisaties is een constraint dat een subverbalisatie niet het zelfde mag zijn als de parent verbalisatie.
Verder moet Verbalisatie_ID bestaan in de verbalisatie tabel maar dit wordt afgevangen door de relatie tussen deze twee entiteiten.

CREATE
Beinvloede tabellen: Verbalisatie en SubVerbalisatie.

Stappen voor Subverbalisatie.
1. Controleer of de ingevoerde subverbalisatie het zelfde is als de parent verbalisatie. 
Als dit het geval is geef een foutmelding.
2. Controleer of de ingevoerde subverbalisatie nog niet bestaat bij deze parent verbalisatie.
Als dit niet het geval is geef een foutmelding.

UPDATE
Beinvloede tabellen: Verbalisatie, SubVerbalisatie

Stappen
1. Controleer of de ingevoerde subverbalisatie het zelfde is als de parent verbalisatie. 
Als dit het geval is geef een foutmelding.
2. Controleer of de ingevoerde subverbalisatie nog niet bestaat bij deze parent verbalisatie.
Als dit niet het geval is geef een foutmelding.
*/

--Create new subverbalisation
IF EXISTS(select * from sys.procedures where name=N'usp_CreateNewSubVerbalisation')
BEGIN
DROP PROC usp_CreateNewSubVerbalisation
END
GO
CREATE PROCEDURE usp_CreateNewSubVerbalisation @VerbalisatieID    INT, 
                                               @UserName     VARCHAR(20), 
                                               @SubVerbalisatie VARCHAR(500)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
	--Controlen of deze user in het project werkzaam is. Moet standaard.
	IF NOT EXISTS(
	SELECT 1
	FROM PROJECTBEZETTING PB
	WHERE GEBRUIKERSNAAM = @UserName
	)
	BEGIN
		;THROW 50069,'Gebruiker niet werkzaam in het project',1;
	END 

	/*
	Stap 1: Controleer of de subverbalisatie niet het zelfde is als de parent verbalisatie.
	Als die bestaat geef een error.
	*/
        IF EXISTS
        (
		SELECT V.VERBALISATIE_ZIN
		FROM VERBALISATIE V
		WHERE 
		V.VERBALISATIE_ID = @VerbalisatieID
		AND
		V.VERBALISATIE_ZIN = @SubVerbalisatie
        )
        BEGIN
                ;THROW 50001, 'SubVerbalisatie komt overeen met de parent verbalisatie.', 1;
        END;

		--Controleer of de subverbalisatie al bestaat bij deze hoofdverbalisatie
		IF EXISTS(
		SELECT SV.SUBVERBALISATIE_ZIN
		FROM SUBVERBALISATIE SV
		WHERE SV.SUBVERBALISATIE_ZIN = @SubVerbalisatie AND SV.VERBALISATIE_ID = @VerbalisatieID
		)
		BEGIN
                ;THROW 50001, 'SubVerbalisatie bestaat al bij deze parent.', 1;
        END;

		INSERT INTO SUBVERBALISATIE (GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN,VERBALISATIE_ID) 
		VALUES (@UserName,@SubVerbalisatie,@VerbalisatieID)

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO

--update subverbalisatie
GO
IF EXISTS(select * from sys.procedures where name=N'usp_UpdateSubVerbalisation')
BEGIN
DROP PROC usp_UpdateSubVerbalisation
END
GO
CREATE PROCEDURE usp_UpdateSubVerbalisation	   @VerbalisatieID				INT, 
                                               @UserName			VARCHAR(20), 
                                               @NewSubVerbalisatie VARCHAR(500),
											   @OldSubVerbalisatie VARCHAR(500)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

		--Controlen of deze user in het project werkzaam is. Moet standaard.
	IF NOT EXISTS(
	SELECT *
	FROM SUBVERBALISATIE SV INNER JOIN VERBALISATIE V ON V.VERBALISATIE_ID = SV.VERBALISATIE_ID
	INNER JOIN PROJECTBEZETTING PB on V.PROJECT_ID = PB.PROJECT_ID
	WHERE PB.GEBRUIKERSNAAM = @UserName
	)
	BEGIN
		;THROW 50069,'Gebruiker niet werkzaam in het project',1;
	END 
	--Stap 1
        IF EXISTS
        (
            SELECT SV.SUBVERBALISATIE_ZIN
			FROM SUBVERBALISATIE SV
			WHERE SUBVERBALISATIE_ZIN = @NewSubVerbalisatie
        )
		BEGIN
			;THROW 500003,'Update kan niet omdat er al een subverbalisatie bestaat met dezelfde zin.',1
		END

		--Stap 2:
		IF EXISTS
		(
		SELECT 1
		FROM VERBALISATIE V
		WHERE V.VERBALISATIE_ZIN = @NewSubVerbalisatie
		)
		BEGIN
			;THROW 500004,'Update kan niet omdat de nieuwe subverbalisatie het zelfde is als de parent.',1
		END

		UPDATE SUBVERBALISATIE SET SUBVERBALISATIE_ZIN = @NewSubVerbalisatie
		, GEBRUIKERSNAAM = @UserName
		WHERE SUBVERBALISATIE_ZIN = @OldSubVerbalisatie AND VERBALISATIE_ID = @VerbalisatieID

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO

EXEC tSQLt.NewTestClass 'TestCRUDInvoerenSubVerbalisatie';
GO

/*
Test clauses SubVerbalisaties Create
*/
--Test scenario 1
--Subverbalisaties bij een verbalisatie moeten uniek zijn en mogen niet het zelfde zijn als de parent verbalisatie.
--Fout situatie waar een subverbalisatie het zelfde is als de parent verbalisatie.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Subverbalisaties binnen een project moeten uniek zijn en mogen niet het zelfde zijn als de parent. ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE(GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN,VERBALISATIE_ID) VALUES('TestUser','Er is een fiets',1);
		EXEC tSQLt.ExpectException 
             @ExpectedMessage = 'SubVerbalisatie komt overeen met de parent verbalisatie.';
		EXEC usp_CreateNewSubVerbalisation 1,'TestUser','Er is een fiets';
    END;
GO
--Test scenario 2
--Subverbalisaties bij een verbalisatie moeten uniek zijn en mogen niet het zelfde zijn als de parent verbalisatie.
--Fout situatie waar de subverbalisatie het zelfde is als de parent.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Subverbalisatie bij hoofdverbalisatie is het zelfde. ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
				EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
				INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE(GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN,VERBALISATIE_ID) VALUES('TestUser','Er is een auto',1);
		EXEC tSQLt.ExpectException 
             @ExpectedMessage = 'SubVerbalisatie bestaat al bij deze parent.';
		EXEC usp_CreateNewSubVerbalisation 1,'TestUser','Er is een auto';
    END;
GO
--Test scenario 3
--Subverbalisaties bij een verbalisatie moeten uniek zijn en mogen niet het zelfde zijn als de parent verbalisatie.
--Goed situatie waar de subverbalisatie uniek is en niet het zelfde is als de parent.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Subverbalisatie komt nog niet voor bij de hoofdverbalisatie]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		   		EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
				INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES (1,'TestUser','Er is een auto')

		SELECT * INTO ExpectedSubVerbalisatie FROM dbo.SUBVERBALISATIE
		INSERT INTO ExpectedSubVerbalisatie (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES (1,'TestUser','Er is een vliegtuig')

		EXEC usp_CreateNewSubVerbalisation 1,'TestUser','Er is een vliegtuig';
		EXEC tSQLt.AssertEqualsTable ExpectedSubVerbalisatie, SUBVERBALISATIE

    END;
GO

/*
Test clauses SubVerbalisatie UPDATE
*/
--Test scenario 4
--SubVerbalisaties bij een parent verbalisatie moeten uniek zijn.
--Fout situatie een subverbalisatie bij een parent wordt aangepast naar een bestaande subverbalisatie bij deze parent.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Tijdens het updaten mag de nieuwe subverbalisatie nog niet bestaan bij deze parent. ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		   		EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
				INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een auto');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een krokodil');

		EXEC tSQLt.ExpectException 'Update kan niet omdat er al een subverbalisatie bestaat met dezelfde zin.'

		EXEC usp_UpdateSubVerbalisation 1,'TestUser','Er is een krokodil','Er is een auto'
    END;
GO
--Test scenario 5
--SubVerbalisaties mogen na een update niet hetzelfde zijn als de parent.
--Fout situatie waar na de update een subverbalisatie hetzelfde wordt als de parent verbalisatie.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Tijdens het updaten mag de nieuwe subverbalisatie niet het zelfde zijn als de parent. ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		   		EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
				INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een auto');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een krokodil');

		EXEC tSQLt.ExpectException 'Update kan niet omdat de nieuwe subverbalisatie het zelfde is als de parent.'

		EXEC usp_UpdateSubVerbalisation 1,'TestUser','Er is een fiets','Er is een auto'
    END;
GO
--Test scenario 6
--SubVerbalisaties bij een parent verbalisatie moeten uniek zijn.
--Goed situatie waar na de update de subverbalisatie uniek is bij deze parent en hij niet het zelfde is als de parent.
CREATE PROCEDURE TestCRUDInvoerenSubVerbalisatie.[test = Tijdens het updaten mag de nieuwe subverbalisatie nog niet bestaan bij deze parent.]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		   		EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
				INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');

		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een auto');

		SELECT * INTO ExpectedSubVerbalisatie FROM dbo.SUBVERBALISATIE
		UPDATE ExpectedSubVerbalisatie SET SUBVERBALISATIE_ZIN = 'Er is een krokodil' WHERE SUBVERBALISATIE_ZIN = 'Er is een auto'

		EXEC usp_UpdateSubVerbalisation 1,'TestUser','Er is een krokodil','Er is een auto'

		EXEC tSQLt.AssertEqualsTable ExpectedSUBVerbalisatie, SUBVERBALISATIE
    END;
GO

EXEC [tSQLt].[Run] 'TestCRUDInvoerenSubVerbalisatie'