/*
Use case Invoeren verbalisatie

Binnen het systeem heeft de gebruiker de mogelijkheid verbalisaties in te voeren.
Een verbalisatie is een zin en is opgeslagen als een varchar. 
Elke verbalisatie is opgeslagen per project en moet uniek zijn elk project.

CREATE
Wanneer een nieuwe verbalisatie wordt gecreerd wordt alleen de tabel Verbalisatie aangepast.
Een verbalisatie moet uniek zijn per project. Deze constraint moet handmatig uitgeprogrameerd worden.

Voor deze operatie wordt een stored procedure gemaakt waarmee de gebruiker nieuwe verbalisaties kan toevoegen.

Stappen verbalisatie
Controleer of de nieuwe verbalisatie nog niet bestaat in dit project
Als de verbalisatie al bestaat geef een error.

UPDATE
Bij een update moet er gecontroleerd worden of de nieuwe verbalisatie zin nog niet bestaat in dit project.
Wanneer deze zin al bestaat geef een foutmelding.
Controleer of de nieuwe verbalisatie nog niet bestaat in zijn eigen subverbalisaties.
Wanneer dit allemaal goed is voor een update uit.

DELETE

READ

*/

--Create stored procedure Verbalisatie
--Per project moet de verbalisatie uniek zijn
IF EXISTS(select * from sys.procedures where name=N'usp_CreateNewVerbalisatieForUserInProject')
BEGIN
DROP PROC usp_CreateNewVerbalisatieForUserInProject
END
GO
CREATE PROCEDURE usp_CreateNewVerbalisatieForUserInProject @ProjectID    INT, 
                                                           @UserName     VARCHAR(20), 
                                                           @Verbalisatie VARCHAR(500)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
	IF NOT EXISTS(
	SELECT 1
	FROM PROJECTBEZETTING PB WHERE PB.PROJECT_ID = @ProjectID AND PB.GEBRUIKERSNAAM = @UserName
	)
	BEGIN
		;THROW 50069,'Gebruiker zit niet in dit project',1;
	END

	/*
	Stap 1: Controleer of deze verbalisatie al bestaat in dit project en rekeninghoudend met history.
	Als deze verbalisatie al bestaat geef een foutmelding.
	*/

        IF EXISTS
        (
            SELECT V.VERBALISATIE_ZIN
            FROM VERBALISATIE V
            WHERE V.VERBALISATIE_ZIN = @Verbalisatie
			AND
			V.PROJECT_ID = @ProjectID
        )
            BEGIN
                ;THROW 50001, 'Verbalisatie bestaat al in dit project.', 1;
			END;
        INSERT INTO VERBALISATIE
        (PROJECT_ID, 
         GEBRUIKERSNAAM, 
         VERBALISATIE_ZIN
        )
        VALUES
        (@ProjectID, 
         @UserName, 
         @Verbalisatie
        );

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO


--update verbalisation
GO
IF EXISTS(select * from sys.procedures where name=N'usp_UpdateVerbalisation')
BEGIN
DROP PROC usp_UpdateVerbalisation
END
GO
CREATE PROCEDURE usp_UpdateVerbalisation	   @VerbalisatieID    INT, 
											   @ProjectID	INT,
                                               @UserName     VARCHAR(20), 
                                               @NewVerbalisatie VARCHAR(500)
AS
     SET NOCOUNT ON;
     SET XACT_ABORT OFF;
     --check if there is already an tran running if yes set savepoint else start own
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
         ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	IF NOT EXISTS(
	SELECT 1
	FROM PROJECTBEZETTING PB WHERE PB.PROJECT_ID = @ProjectID AND PB.GEBRUIKERSNAAM = @UserName
	)
	BEGIN
		;THROW 50069,'Gebruiker zit niet in dit project',1;
	END
	/*
	Stap 1: Controleer of de nieuwe verbalisatie nog niet bestaat binnen dit project.
	Stap 2: Controleer of de nieuwe verbalisatie nog niet bestaat als subverbalisatie.
	*/
	--Stap 1
        IF EXISTS
        (
            SELECT V.VERBALISATIE_ZIN
            FROM VERBALISATIE V
            WHERE V.VERBALISATIE_ZIN = @NewVerbalisatie
			AND
			V.PROJECT_ID = @ProjectID
        )
		BEGIN
			;THROW 500003,'Update kan niet omdat er al een verbalisatie bestaat met dezelfde zin',1
		END

		--Stap 2:
		IF EXISTS
		(
		SELECT 1
		FROM VERBALISATIE V
		WHERE
		@NewVerbalisatie IN (
		SELECT SV.SUBVERBALISATIE_ZIN
		FROM SUBVERBALISATIE SV
		WHERE SV.VERBALISATIE_ID = @VerbalisatieID
		)
		)
		BEGIN
			;THROW 500004,'Update kan niet omdat er al een subverbalisatie is met dezelfde zin als de nieuwe verbalisatie.',1
		END

		UPDATE VERBALISATIE SET VERBALISATIE_ZIN = @NewVerbalisatie
		,GEBRUIKERSNAAM = @UserName WHERE VERBALISATIE_ID = @VerbalisatieID

        --If there are no errors the tran must be commited else jump to catch block to close tran
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @TranCounter = 0
            BEGIN
                --started own tran so rollback it
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
        END;
            ELSE
            BEGIN
                --not started own tran rollback to save point
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
        END;
        THROW;
    END CATCH;
GO


EXEC tSQLt.NewTestClass 'TestCRUDInvoerenVerbalisatie';
GO
/*
Test clauses Verbalisatie CREATE
*/
--Test scenario 1
--Verbalisaties in een project moeten uniek zijn. Controleer of de nieuwe verbalisatie nog niet bestaat in dit project.
--Als deze nog niet bestaat laat de insert doorgaan.
--Verbalisatie moet uniek zijn binnnen het project. Insert zelfde verbalisatie in project Error Verwacht.
CREATE PROCEDURE TestCRUDInvoerenVerbalisatie.[test = Verbalisatie moet uniek zijn binnnen het project. Error]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
					EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
			
        EXEC tSQLt.ExpectException 
             @ExpectedMessage = 'Verbalisatie bestaat al in dit project.';

		EXEC usp_CreateNewVerbalisatieForUserInProject 1,'TestUser','Er is een fiets';
    END;
GO
--Test scenario 2
--Verbalisaties in een project moeten uniek zijn. Controleer of de nieuwe verbalisatie nog niet bestaat in dit project.
--Als deze nog niet bestaat laat de insert doorgaan.
--Verbalisatie moet uniek zijn binnnen het project. Insert anderen verbalisatie geen error verwacht.
CREATE PROCEDURE TestCRUDInvoerenVerbalisatie.[test = Verbalisatie moet uniek zijn binnnen het project.]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
								EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');

		SELECT * INTO ExpectedVerbalisatie FROM dbo.VERBALISATIE
		INSERT INTO ExpectedVerbalisatie (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES (1,'TestUser','Er is een auto')

		EXEC usp_CreateNewVerbalisatieForUserInProject 1,'TestUser','Er is een auto';
		EXEC tSQLt.AssertEqualsTable ExpectedVerbalisatie, VERBALISATIE

    END;
GO

/*
Test clauses Verbalisatie UPDATE
*/
--Test scenario 3
--Verbalisaties binnen een project moeten uniek zijn.
--Tijdens het updaten mag de nieuwe verbalisatie nog niet bestaan bij het project.
--Goed situatie waar de nieuwe waarde van verbalisatie nog niet voorkomt bij een van de childeren. En deze nog steeds uniek is binnen het project.
CREATE PROCEDURE TestCRUDInvoerenVerbalisatie.[test = UPDATE Verbalisatie bestaat nog niet binnen project]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
								EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');

		SELECT * INTO ExpectedVerbalisatie FROM dbo.VERBALISATIE
		UPDATE ExpectedVerbalisatie SET VERBALISATIE_ZIN = 'Er is een vliegtuig' WHERE VERBALISATIE_ID = 1

		EXEC usp_UpdateVerbalisation 1,1,'TestUser','Er is een vliegtuig'
		EXEC tSQLt.AssertEqualsTable ExpectedVerbalisatie, VERBALISATIE
    END;
GO
--Test scenario 4
--Verbalisaties binnen een project moeten uniek zijn.
--Tijdens het updaten mag de nieuwe verbalisatie nog niet bestaan bij het project.
--Fout sitautie nieuwe waarde van verbalisatie bestaat al binnen dit project.
CREATE PROCEDURE TestCRUDInvoerenVerbalisatie.[test = UPDATE Verbalisatie bestaat al binnen project.ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
								EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');

		EXEC tSQLt.ExpectException 'Update kan niet omdat er al een verbalisatie bestaat met dezelfde zin'

		EXEC usp_UpdateVerbalisation 1,1,'TestUser','Er is een fiets'

    END;
GO
--Test scenario 5
--Een parent verbalisatie mag niet bestaan als subverbalisatie.
--Tijdens het updaten mag de nieuwe verbalisatie niet opgenomen zijn als subverbalisatie bij deze parent.
--Fout situatie nieuwe verbalisatie bestaat al in dit project.
CREATE PROCEDURE TestCRUDInvoerenVerbalisatie.[test = Tijdens het updaten mag de nieuwe verbalisatie niet opgenomen zijn als subverbalisatie bij deze parent. ERROR]
AS
    BEGIN
        -------Assemble
        EXEC tSQLt.FakeTable
             'dbo.PROJECT',@Identity = 1, @Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.VERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
           'dbo.SUBVERBALISATIE',@Identity = 1,@Defaults = 1;
		EXEC tSQLt.FakeTable 
			'dbo.GEBRUIKER',@Identity = 1,@Defaults = 1;
								EXEC tSQLt.FakeTable
             'dbo.PROJECTBEZETTING',@Identity = 1, @Defaults = 1;

		INSERT INTO PROJECT VALUES ('TestProject');
		INSERT INTO GEBRUIKER VALUES ('TestUser','TestPass');
		INSERT INTO PROJECTBEZETTING VALUES('TestUser',1,1);
		INSERT INTO VERBALISATIE (PROJECT_ID,GEBRUIKERSNAAM,VERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een fiets');
		INSERT INTO SUBVERBALISATIE (VERBALISATIE_ID,GEBRUIKERSNAAM,SUBVERBALISATIE_ZIN) VALUES(1,'TestUser','Er is een auto');

		EXEC tSQLt.ExpectException 'Update kan niet omdat er al een subverbalisatie is met dezelfde zin als de nieuwe verbalisatie.'

		EXEC usp_UpdateVerbalisation 1,1,'TestUser','Er is een auto'
    END;
GO

EXEC [tSQLt].[Run] 'TestCRUDInvoerenVerbalisatie'
