use master
go
IF EXISTS(select * from sys.databases where name=N'FactBasedAnalysis')
DROP DATABASE FactBasedAnalysis
GO
CREATE DATABASE FactBasedAnalysis
GO
use FactBasedAnalysis

	/*==============================================================*/
	/* DBMS name:      Microsoft SQL Server 2008                    */
	/* Created on:     17-5-2019 11:22:56                           */
	/*==============================================================*/

	/*==============================================================*/
	/* Table: INVULPLEK                                             */
	/*==============================================================*/
	create table INVULPLEK (
	   VERBALISATIE_ID       int                  not null,
	   BEGINPOSITIE          int                  not null,
	   EINDPOSITIE           int                  not null
	   constraint PK_INVULPLEK primary key (VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE)
	)
	go

	/*==============================================================*/
	/* Table: ATTRIBUUT_IN_VERBALISATIE                             */
	/*==============================================================*/
	create table ATTRIBUUT_IN_VERBALISATIE (
	   GEBRUIKERSNAAM        varchar(20)          not null,
	   ATTRIBUUT_NAAM        varchar(50)          not null,
	   ENTITEIT_ID           int                  not null,
	   VERBALISATIE_ID       int                  not null,
	   IS_PRIMARY_IDENTIFIER bit                  not null,
	   BEGINPOSITIE          int                  not null,
	   EINDPOSITIE           int                  not null,
	   IS_MANDATORY          bit                  null,
	   constraint PK_ATTRIBUUT_IN_VERBALISATIE primary key (ATTRIBUUT_NAAM, BEGINPOSITIE, EINDPOSITIE, VERBALISATIE_ID)
	)
	go

	/*==============================================================*/
	/* Index: ATTRIBUUT_IN_VERBALISATIE_FK                          */
	/*==============================================================*/
	create index ATTRIBUUT_IN_VERBALISATIE_FK on ATTRIBUUT_IN_VERBALISATIE (
	VERBALISATIE_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: ATTRIBUUT_VAN_ENTITEIT_FK                             */
	/*==============================================================*/
	create index ATTRIBUUT_VAN_ENTITEIT_FK on ATTRIBUUT_IN_VERBALISATIE (
	ENTITEIT_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_ATTRIBUUT_IN_VERBALISATIE_FK                */
	/*==============================================================*/
	create index GEBRUIKER_ATTRIBUUT_IN_VERBALISATIE_FK on ATTRIBUUT_IN_VERBALISATIE (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: BUSINESS_RULE                                         */
	/*==============================================================*/
	create table BUSINESS_RULE (
	   GEBRUIKERSNAAM       varchar(20)          not null,
	   BUSINESS_RULE_ZIN    varchar(300)         not null,
	   PROJECT_ID            int                  not null,
	   constraint PK_BUSINESS_RULE primary key  (BUSINESS_RULE_ZIN, PROJECT_ID)
	)
	go

	/*==============================================================*/
	/* Index: BUSINESS_RULE_IN_PROJECT_FK                           */
	/*==============================================================*/
	create index BUSINESS_RULE_IN_PROJECT_FK on BUSINESS_RULE (
	PROJECT_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_BUSINESS_RULE_FK                         */
	/*==============================================================*/
	create index GEBRUIKER_IN_BUSINESS_RULE_FK on BUSINESS_RULE (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: ENTITEIT_IN_PROJECT                                   */
	/*==============================================================*/
	create table ENTITEIT_IN_PROJECT (
	   ENTITEIT_ID          int  identity(1,1)          not null,
	   SUBTYPE_ID           int                         null,
	   PROJECT_ID            int                         not null,
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   ENTITEIT_NAAM        varchar(50)                 not null,
	   constraint PK_ENTITEIT_IN_PROJECT primary key (ENTITEIT_ID),
	   constraint AK_ALTERNATIVE_KEY_1_ENTITEIT unique (PROJECT_ID, ENTITEIT_NAAM)
	)
	go

	/*==============================================================*/
	/* Index: ENTITEIT_IN_GROEP_FK                                  */
	/*==============================================================*/
	create index ENTITEIT_IN_GROEP_FK on ENTITEIT_IN_PROJECT (
	SUBTYPE_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: ENTITEIT_IN_PROJECT_FK                                */
	/*==============================================================*/
	create index ENTITEIT_IN_PROJECT_FK on ENTITEIT_IN_PROJECT (
	PROJECT_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_ENITEIT_IN_PROJECT_FK                    */
	/*==============================================================*/
	create index GEBRUIKER_IN_ENITEIT_IN_PROJECT_FK on ENTITEIT_IN_PROJECT (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: ENTITEIT_IN_VERBALISATIE                              */
	/*==============================================================*/
	create table ENTITEIT_IN_VERBALISATIE (
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   ENTITEIT_ID          int                         not null,
	   VERBALISATIE_ID      int                         not null,
	   IS_MATCH             bit                         not null,
	   constraint PK_ENTITEIT_IN_VERBALISATIE primary key  (ENTITEIT_ID, VERBALISATIE_ID)
	)
	go

	/*==============================================================*/
	/* Index: ENTITEIT_IN_ENTITEIT_IN_VERBALISATIE_FK               */
	/*==============================================================*/
	create index ENTITEIT_IN_ENTITEIT_IN_VERBALISATIE_FK on ENTITEIT_IN_VERBALISATIE (
	VERBALISATIE_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: ENTITEIT_IN_PROJECT_IN_VERBALISATIE_FK                */
	/*==============================================================*/
	create index ENTITEIT_IN_PROJECT_IN_VERBALISATIE_FK on ENTITEIT_IN_VERBALISATIE (
	ENTITEIT_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_ENTITEIT_IN_VERBALISATIE_FK                 */
	/*==============================================================*/
	create index GEBRUIKER_ENTITEIT_IN_VERBALISATIE_FK on ENTITEIT_IN_VERBALISATIE (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: GEBRUIKER                                             */
	/*==============================================================*/
	create table GEBRUIKER (
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   WACHTWOORD           varchar(150)                not null,
	   constraint PK_GEBRUIKER primary key  (GEBRUIKERSNAAM)
	)
	go

	/*==============================================================*/
	/* Table: PROJECT                                               */
	/*==============================================================*/
	create table PROJECT (
	   PROJECT_ID		    int identity(1,1)           not null,
	   PROJECTNAAM          varchar(50)                 not null,
	   constraint PK_PROJECT primary key  (PROJECT_ID)
	)
	go

	/*==============================================================*/
	/* Table: PROJECTBEZETTING                                      */
	/*==============================================================*/
	create table PROJECTBEZETTING (
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   PROJECT_ID            int                         not null,
	   IS_EIGENAAR          bit                         not null,
	   constraint PK_PROJECTBEZETTING primary key (GEBRUIKERSNAAM, PROJECT_ID)
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_PROJECT_FK                               */
	/*==============================================================*/
	create index GEBRUIKER_IN_PROJECT_FK on PROJECTBEZETTING (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Index: PROJECT_IN_PROJECTBEZZETING_FK                        */
	/*==============================================================*/
	create index PROJECT_IN_PROJECTBEZZETING_FK on PROJECTBEZETTING (
	PROJECT_ID ASC
	)
	go

	/*==============================================================*/
	/* Table: RELATIE                                               */
	/*==============================================================*/
	create table RELATIE (
	   RELATIE_ID           int identity(1,1)           not null,
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   VERBALISATIE_ID      int                         not null,
	   RELATIE_NAAM         varchar(50)                 not null,
	   CARDINALITEITSTYPE   varchar(10)                 null,
	   constraint PK_RELATIE primary key  (RELATIE_ID),
	   constraint AK_ALTERNATIVE_KEY_1_RELATIE unique (VERBALISATIE_ID, RELATIE_NAAM)
	)
	go

	/*==============================================================*/
	/* Index: RELATIE_PROJECT_FK                                    */
	/*==============================================================*/
	create index RELATIE_PROJECT_FK on RELATIE (
	VERBALISATIE_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_RELATIE_FK                               */
	/*==============================================================*/
	create index GEBRUIKER_IN_RELATIE_FK on RELATIE (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: ROL                                                   */
	/*==============================================================*/
	create table ROL (
	   ENTITEIT_VAN         int                  not null,
	   ENTITEIT_NAAR        int                  not null,
	   RELATIE_ID           int                  not null,
	   GEBRUIKERSNAAM       varchar(20)          not null,
	   IS_PRIMAIRE_ROL      bit                  not null,
	   CARDINALITEIT_MINIMUM varchar(6)          null,
	   CARDINALITEIT_MAXIMUM varchar(6)          null,
	   MANDATORY            bit                  null,
	   DEPENDENT            bit                  not null,
	   IS_DOMINANT          bit                  null,
	   constraint PK_ROL primary key (RELATIE_ID, IS_PRIMAIRE_ROL)
	)
	go

	/*==============================================================*/
	/* Index: EIND_ROL_FK                                           */
	/*==============================================================*/
	create index EIND_ROL_FK on ROL (
	ENTITEIT_VAN ASC
	)
	go

	/*==============================================================*/
	/* Index: BEGIN_ROL_FK                                          */
	/*==============================================================*/
	create index BEGIN_ROL_FK on ROL (
	ENTITEIT_NAAR ASC
	)
	go

	/*==============================================================*/
	/* Index: ROL_VAN_RELATIE_FK                                    */
	/*==============================================================*/
	create index ROL_VAN_RELATIE_FK on ROL (
	RELATIE_ID ASC
	)
	go

	/*==============================================================*/
	/* Table: SUBTYPE_GROEP                                         */
	/*==============================================================*/
	create table SUBTYPE_GROEP (
	   SUBTYPE_ID           int identity(1,1)           not null,
	   PARENT_ENTITEIT      int                         not null,
	   GEBRUIKERSNAAM       varchar(20)                 not null,
	   GROEPSNAAM           varchar(50)                 not null,
	   MUTUALLY_EXCLUSIVE   bit                         not null,
	   COMPLETE             bit                         not null,
	   constraint PK_SUBTYPE_GROEP primary key (SUBTYPE_ID),
	   constraint AK_ALTERNATIVE_KEY_1_SUBTYPE_ unique (GROEPSNAAM, PARENT_ENTITEIT)
	)
	go

	/*==============================================================*/
	/* Index: SUPERTYPE_ENTITEIT_FK                                 */
	/*==============================================================*/
	create index SUPERTYPE_ENTITEIT_FK on SUBTYPE_GROEP (
	PARENT_ENTITEIT ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_SUBTYPE_GROEP_FK                         */
	/*==============================================================*/
	create index GEBRUIKER_IN_SUBTYPE_GROEP_FK on SUBTYPE_GROEP (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: SUBVERBALISATIE                                       */
	/*==============================================================*/
	create table SUBVERBALISATIE (
	   GEBRUIKERSNAAM       varchar(20)          not null,
	   SUBVERBALISATIE_ZIN  varchar(500)         not null,
	   VERBALISATIE_ID      int                  not null,
	   constraint PK_SUBVERBALISATIE primary key nonclustered (SUBVERBALISATIE_ZIN, VERBALISATIE_ID)
	)
	go

	/*==============================================================*/
	/* Index: SUBVERBALISATIE_IN_VERBALISATIE_FK                    */
	/*==============================================================*/
	create index SUBVERBALISATIE_IN_VERBALISATIE_FK on SUBVERBALISATIE (
	VERBALISATIE_ID ASC
	)
	go

	/*==============================================================*/
	/* Index: GEBRUIKER_IN_SUBVERBALISATIE_FK                       */
	/*==============================================================*/
	create index GEBRUIKER_IN_SUBVERBALISATIE_FK on SUBVERBALISATIE (
	GEBRUIKERSNAAM ASC
	)
	go

	/*==============================================================*/
	/* Table: VERBALISATIE                                          */
	/*==============================================================*/
	create table VERBALISATIE (
	   VERBALISATIE_ID      int identity(1,1)       not null,
	   PROJECT_ID            int                     not null,
	   GEBRUIKERSNAAM       varchar(20)             not null,
	   VERBALISATIE_ZIN     varchar(500)            not null,
	   constraint PK_VERBALISATIE primary key (VERBALISATIE_ID),
	   constraint AK_ALTERNATIVE_KEY_1_VERBALIS unique (PROJECT_ID, VERBALISATIE_ZIN)
	)
	go

	/*==============================================================*/
	/* Table: ATTRIBUUT_IN_SUBTYPE_ENTITEIT                         */
	/*==============================================================*/
	create table ATTRIBUUT_IN_SUBTYPE_ENTITEIT(
		GEBRUIKERSNAAM 					varchar(20) 	not null
			constraint ATTRIBUUT_IN_SUBTYPE_ENTITEIT___fk_gebruikersnaam references GEBRUIKER,
		ATTRIBUUT_NAAM 					varchar(50) 	not null,
		PROJECT_ID 							int 					not null
			constraint ATTRIBUUT_IN_SUBTYPE_ENTITEIT_PROJECT__fk_project_id references PROJECT,
		IS_PRIMARY_IDENTIFIER 	bit 					not null,
		IS_MANDATORY 						bit,
		ENTITEIT_ID 						int 					not null,
			constraint ATTRIBUUT_IN_SUBTYPE_ENTITEIT_pk primary key nonclustered (ATTRIBUUT_NAAM, PROJECT_ID, ENTITEIT_ID)
	)
	go

	
	create index VERBALISATIE_IN_PROJECT_FK on VERBALISATIE (
	PROJECT_ID ASC
	)
	go


	create index GEBRUIKER_VERBALISATIE_FK on VERBALISATIE (
	GEBRUIKERSNAAM ASC
	)
	go

	create index INVULPLEK_VERBALISATIE on INVULPLEK(
	VERBALISATIE_ID asc
	)
	go

		
	alter table INVULPLEK
	   add constraint FK_Invulplek_Verbalisatie foreign key (VERBALISATIE_ID)
	      references VERBALISATIE (VERBALISATIE_ID)
		   
	alter table ATTRIBUUT_IN_VERBALISATIE
	   add constraint FK_AttribuutInVerbalisatie_Invulplek foreign key (VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE)
		  references INVULPLEK (VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE)
	go

	alter table ATTRIBUUT_IN_VERBALISATIE
	   add constraint FK_AttribuutInVerbalisatie_EntiteitInProject foreign key (ENTITEIT_ID)
		  references ENTITEIT_IN_PROJECT (ENTITEIT_ID)
	go

	alter table ATTRIBUUT_IN_VERBALISATIE
	   add constraint FK_AttribuutInVerbalisatie_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table BUSINESS_RULE
	   add constraint FK_BusinessRule_Project foreign key (PROJECT_ID)
		  references PROJECT (PROJECT_ID)
	go

	alter table BUSINESS_RULE
	   add constraint FK_BusinessRule_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table ENTITEIT_IN_PROJECT
	   add constraint FK_EntiteitInProject_SubtypeGroep foreign key (SUBTYPE_ID)
		  references SUBTYPE_GROEP (SUBTYPE_ID)
	go

	alter table ENTITEIT_IN_PROJECT
	   add constraint FK_EntiteitInProject_Project foreign key (PROJECT_ID)
		  references PROJECT (PROJECT_ID)
	go

	alter table ENTITEIT_IN_PROJECT
	   add constraint FK_EntiteitInProject_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table ENTITEIT_IN_VERBALISATIE
	   add constraint FK_EntiteitInVerbalisatie_Verbalisatie foreign key (VERBALISATIE_ID)
		  references VERBALISATIE (VERBALISATIE_ID)
	go

	alter table ENTITEIT_IN_VERBALISATIE
	   add constraint FK_EntiteitInVerbalisatie_EntiteitInProject foreign key (ENTITEIT_ID)
		  references ENTITEIT_IN_PROJECT (ENTITEIT_ID)
	go

	alter table ENTITEIT_IN_VERBALISATIE
	   add constraint FK_EntiteitInVerbalisatie_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table PROJECTBEZETTING
	   add constraint FK_Projectbezetting_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table PROJECTBEZETTING
	   add constraint FK_Projectbezetting_Project foreign key (PROJECT_ID)
		  references PROJECT (PROJECT_ID)
	go

	alter table RELATIE
	   add constraint FK_Relatie_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table RELATIE
	   add constraint FK_Relatie_Verbalisatie foreign key (VERBALISATIE_ID)
		  references VERBALISATIE (VERBALISATIE_ID)
	go

	alter table ROL
	   add constraint FK_Rol_EersteEntiteit foreign key (ENTITEIT_NAAR)
		  references ENTITEIT_IN_PROJECT (ENTITEIT_ID)
	go

	alter table ROL
	   add constraint FK_Rol_TweedeEntiteit foreign key (ENTITEIT_VAN)
		  references ENTITEIT_IN_PROJECT (ENTITEIT_ID)
	go

	alter table ROL
	   add constraint FK_Rol_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table ROL
	   add constraint FK_Rol_Relatie foreign key (RELATIE_ID)
		  references RELATIE (RELATIE_ID)
	go

	alter table SUBTYPE_GROEP
	   add constraint FK_SubtypeGroep_Gebruik foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table SUBTYPE_GROEP
	   add constraint FK_SubtypeGroep_EntiteitInProject foreign key (PARENT_ENTITEIT)
		  references ENTITEIT_IN_PROJECT (ENTITEIT_ID)
	go

	alter table SUBVERBALISATIE
	   add constraint FK_Subverbalisatie_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table SUBVERBALISATIE
	   add constraint FK_Subverbalisatie_Verbalisatie foreign key (VERBALISATIE_ID)
		  references VERBALISATIE (VERBALISATIE_ID)
	go

	alter table VERBALISATIE
	   add constraint FK_Verbalisatie_Gebruiker foreign key (GEBRUIKERSNAAM)
		  references GEBRUIKER (GEBRUIKERSNAAM)
	go

	alter table VERBALISATIE
	   add constraint FK_Verbalisatie_Project foreign key (PROJECT_ID)
		  references PROJECT (PROJECT_ID)
	go

	CREATE PROC usp_generateHistTrigger
	(
	@tableName varchar(100)
	)
	AS
	BEGIN
	DECLARE @query varchar(max) = ' ';
	SELECT @query = @query + 'CREATE TRIGGER histTrigger'+@tableName+' ON '+@tableName+' AFTER INSERT,UPDATE AS BEGIN INSERT INTO hist_'+@tableName+' ('
	SELECT @query = @query + collomn.COLUMN_NAME +', '
	FROM [INFORMATION_SCHEMA].[COLUMNS] collomn
	WHERE collomn.TABLE_NAME = @tableName
	SELECT @query = @query + 'actie) SELECT *, (IIF (EXISTS(select * from deleted), ''U'', ''I'') )  FROM inserted END'
	exec sp_sqlexec @query
	END
	GO

	GO
	CREATE PROC usp_generateHistTriggerDelete
	(
	@tableName varchar(100)
	)
	AS
	BEGIN
	DECLARE @query varchar(max) = ' ';
	SELECT @query = @query + 'CREATE TRIGGER histTriggerDelete'+@tableName+' ON '+@tableName+' AFTER DELETE AS BEGIN INSERT INTO hist_'+@tableName+' ('
	SELECT @query = @query + collomn.COLUMN_NAME +', '
	FROM [INFORMATION_SCHEMA].[COLUMNS] collomn
	WHERE collomn.TABLE_NAME = @tableName
	SELECT @query = @query + 'actie) SELECT *,''D'' FROM deleted END'
	SELECT @query
	exec sp_sqlexec @query
	END
	GO
	/*
	Genereert een history table voor de orginele tabel met naam @naam
	De tabel en data types zijn het zelfde maar er wordt een extra collomn met timestamp toegevoegd.
	Deze nieuwe collomn moet opgenomen worden in de primary key.
	*/
	CREATE PROC usp_generateHistTables
	(
	@tableName varchar(100)
	)
	AS
	BEGIN
	DECLARE @query nvarchar(max) = ' ' 
	SELECT @query = @query + 'CREATE TABLE hist_'+@tableName+'('
	SELECT @query = @query + collData.COLUMN_NAME + ' ' + collData.DATA_TYPE 
	+IIF(collData.DATA_TYPE = 'varchar',
	'('+CAST(collData.CHARACTER_MAXIMUM_LENGTH as varchar)+') ','')
	+IIF(collData.DATA_TYPE = 'numeric', '('
	+CAST(collData.NUMERIC_PRECISION as varchar)+','+CAST(collData.NUMERIC_SCALE as varchar)+')','')
	+IIF(collData.IS_NULLABLE = 'NO',' NOT NULL ,',' NULL ,')
	FROM [INFORMATION_SCHEMA].[COLUMNS] collData
	WHERE collData.[TABLE_NAME] = @tableName

	SELECT @query = @query + 'changeTime datetime DEFAULT CURRENT_TIMESTAMP  not null,'

	SELECT @query = @query + 'actie varchar(1) not null,'

	SELECT @query = @query + 'PRIMARY KEY ('

	SELECT @query = @query +
	coll.COLUMN_NAME + ','
	FROM [INFORMATION_SCHEMA].CONSTRAINT_COLUMN_USAGE coll
	INNER JOIN [INFORMATION_SCHEMA].TABLE_CONSTRAINTS constraintData on coll.TABLE_NAME = constraintData.TABLE_NAME
	AND coll.CONSTRAINT_NAME = constraintData.CONSTRAINT_NAME
	AND constraintData.CONSTRAINT_TYPE = 'PRIMARY KEY'
	WHERE coll.[TABLE_NAME] = @tableName
	SELECT @query = @query+'changeTime))'

	EXEC sp_sqlexec @query;
	EXEC usp_generateHistTrigger @tableName;
	END
	GO

	/*
	Procedure die voor alle tabellen in de database zonder de tSQLt tabellen de history tabellen en triggers genereert
	*/
	CREATE PROC usp_generateTriggersAndHistTablesForDatabase
	AS
	BEGIN
	DECLARE @query varchar(max) = ' ';
	SELECT @query = @query + 'EXEC usp_generateHistTables '''+TABLE_NAME+''';
	EXEC usp_generateHistTriggerDelete'''+TABLE_NAME+''';'
	FROM [INFORMATION_SCHEMA].TABLES WHERE TABLE_NAME NOT LIKE 'hist_%' AND TABLE_SCHEMA != 'tSQLt'
	SELECT @query
	EXEC sp_sqlexec @query;
	END
	GO

	EXEC usp_generateTriggersAndHistTablesForDatabase
	GO