use FactBasedAnalysis --Or other name of database
	/*==============================================================*/
	/* Table: GEBRUIKER                                             */
	/*==============================================================*/
	INSERT INTO GEBRUIKER (GEBRUIKERSNAAM, WACHTWOORD) VALUES 
	('JacquesD',     '123456789'), 
	('LucianoF',     'qwertyuiop'), 
	('DaveQ',        'qweasdzxc'), 
	('CathelijneDB', 'qazwsxedc'), 
	('BonnoV',       'asdfghjkl')
	GO
	
	/*==============================================================*/
	/* Table: PROJECT                                               */
	/*==============================================================*/
	INSERT INTO PROJECT (PROJECTNAAM) VALUES
	('V�lipe Metropole'),
	('Fact based analysis'),
	('Synth Modeling Store')
	GO


	/*==============================================================*/
	/* Table: PROJECTBEZETTING                                      */
	/*==============================================================*/
	INSERT INTO PROJECTBEZETTING (GEBRUIKERSNAAM, PROJECT_ID, IS_EIGENAAR) VALUES
	('JacquesD',     2, 1),
	('JacquesD',     1, 0),
	('BonnoV',       1, 0), 
	('BonnoV',       2, 0),
	('CathelijneDB', 1, 1),
	('CathelijneDB', 2, 0),
	('DaveQ',        3, 1),
	('DaveQ',        2, 0),
	('LucianoF',     1, 0),
	('LucianoF',     2, 0)
	GO

	/*==============================================================*/
	/* Table: VERBALISATIE                                          */
	/*==============================================================*/
	INSERT INTO VERBALISATIE(PROJECT_ID, GEBRUIKERSNAAM, VERBALISATIE_ZIN) VALUES
	(1, 'JacquesD',     'Er is een fiets met ID 5'),
	(1, 'DaveQ',        'Er is een Post met ID 4 op station met ID 2'),
	(1, 'CathelijneDB', 'Er is een station met ID 5'),
	(1, 'DaveQ',        'Fiets met ID 2 staat op Post met ID 4 op station met ID 2'),
	(1, 'BonnoV',       'Fiets met ID 5 heeft een batterijlading van 50%')
	GO

	/*==============================================================*/
	/* Table: ENTITEIT_IN_PROJECT                                   */
	/*==============================================================*/
	INSERT INTO ENTITEIT_IN_PROJECT(SUBTYPE_ID, PROJECT_ID, GEBRUIKERSNAAM, ENTITEIT_NAAM) VALUES 
	(null, 1, 'JacquesD', 'FIETS'),
	(null, 1, 'LucianoF', 'STATION'),
	(null, 1, 'BonnoV',   'POST'),
	(null, 1, 'BonnoV',   'E_FIETS')
	GO

	/*==============================================================*/
	/* Table: ENTITEIT_IN_VERBALISATIE                              */
	/*==============================================================*/
	INSERT INTO ENTITEIT_IN_VERBALISATIE (GEBRUIKERSNAAM, ENTITEIT_ID, VERBALISATIE_ID, IS_MATCH) VALUES
	('JacquesD',     1, 1, 0),
	('LucianoF',     2, 2, 0),
	('BonnoV',       2, 3, 1),
	('BonnoV',       3, 3, 0),
	('BonnoV',       1, 4, 1),
	('CathelijneDB', 3, 4, 1),
	('BonnoV',       4, 5, 0)
	GO
	
	/*==============================================================*/
	/* Table: INVULPLEK                                             */
	/*==============================================================*/
	INSERT INTO INVULPLEK (VERBALISATIE_ID, BEGINPOSITIE, EINDPOSITIE) VALUES
	(1, 24, 24),
	(2, 26, 26),
	(3, 43, 43), 
	(3, 23, 23),
	(4, 14, 14),
	(4, 37, 37),
	(4, 57, 57),
	(5, 14, 14),
	(5, 45, 46)


	/*==============================================================*/
	/* Table: ATTRIBUUT_IN_VERBALISATIE                             */
	/*==============================================================*/
	INSERT INTO ATTRIBUUT_IN_VERBALISATIE (GEBRUIKERSNAAM, ATTRIBUUT_NAAM, ENTITEIT_ID, VERBALISATIE_ID, IS_PRIMARY_IDENTIFIER, BEGINPOSITIE, EINDPOSITIE, IS_MANDATORY) VALUES
	('CathelijneDB', 'Fiets_ID',       1, 1, 1, 24, 24, 1),
	('CathelijneDB', 'Station_ID',     2, 2, 1, 26, 26, 1),
	('JacquesD',     'Station_ID',     3, 3, 1, 43, 43, 1),
    ('JacquesD',     'Post_ID',        3, 3, 1, 23, 23, 1),
    ('DaveQ',        'Post_ID',        3, 4, 1, 37, 37, 1),
	('DaveQ',        'Station_ID',     3, 4, 1, 57, 57, 1),
    ('JacquesD',     'Fiets_ID',       1, 4, 1, 14, 14, 1),
	('JacquesD',     'Fiets_ID',       1, 5, 1, 14, 14, 1),
    ('JacquesD',     'Battery_charge', 1, 5, 0, 45, 46, null)
	GO
		
	/*==============================================================*/
	/* Table: SUBVERBALISATIE                                       */
	/*==============================================================*/
	INSERT INTO SUBVERBALISATIE(GEBRUIKERSNAAM, SUBVERBALISATIE_ZIN, VERBALISATIE_ID) VALUES
	('JacquesD',     'Er is een fiets met ID 9',                                   1),
	('CathelijneDB', 'Er is een station met ID 2',                                 2),
	('DaveQ',        'Er is een Post met ID 5 op station met ID 2',                3),
	('DaveQ',        'Fiets met ID 7 staat op Post met ID 1 op station met ID 3',  4),
	('LucianoF',     'Fiets met ID 4 heeft een batterijlading van 80%',            5)
	
	GO	

	/*==============================================================*/
	/* Table: SUBTYPE_GROEP                                         */
	/*==============================================================*/
	INSERT INTO SUBTYPE_GROEP (PARENT_ENTITEIT, GEBRUIKERSNAAM, GROEPSNAAM, MUTUALLY_EXCLUSIVE, COMPLETE) VALUES
	(1, 'BonnoV', 'GROEP1', 0, 0)
	GO
	
	UPDATE ENTITEIT_IN_PROJECT set SUBTYPE_ID = 1 where ENTITEIT_ID = 4
	GO

	/*==============================================================*/
	/* Table: BUSINESS_RULE                                         */
	/*==============================================================*/
	INSERT INTO BUSINESS_RULE (GEBRUIKERSNAAM, BUSINESS_RULE_ZIN, PROJECT_ID) VALUES
	('BonnoV', 'De batterijlading van een elektrische fiets moet tussen de 0 en 100 liggen', 1),
	('JacquesD', 'Er kan op ieder moment van tijd maximaal 1 fiets bij 1 post staan',        1)
	GO

	/*==============================================================*/
	/* Table: RELATIE                                               */
	/*==============================================================*/
	INSERT INTO RELATIE (GEBRUIKERSNAAM, VERBALISATIE_ID, RELATIE_NAAM, CARDINALITEITSTYPE) VALUES
	('LucianoF', 3, 'POST_STATION', 'ONE - MANY'),
	('LucianoF', 4, 'POST_FIETS',   'ONE - ONE')
	GO

	/*==============================================================*/
	/* Table: ROL                                                   */
	/*==============================================================*/
	INSERT INTO ROL (ENTITEIT_VAN, ENTITEIT_NAAR, RELATIE_ID, GEBRUIKERSNAAM, IS_PRIMAIRE_ROL, CARDINALITEIT_MINIMUM, CARDINALITEIT_MAXIMUM, MANDATORY, DEPENDENT, IS_DOMINANT) VALUES
	(3, 2, 1, 'LucianoF', 1, 1, 1, 1, 1, null), 
	(2, 3, 1, 'LucianoF', 0, 1, 70, 1, 0, null),
	(3, 1, 2, 'LucianoF', 1, 0, 1,  0, 0, 1),
	(1, 3, 2, 'LucianoF', 0, 0, 1,  0, 0, 0)
	
	use master


