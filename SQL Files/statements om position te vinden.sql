	DECLARE @verbalisatie nvarchar(max) = 'Fiets met ID 5 heeft een batterijlading van 50%'
	DECLARE @begin_characters table (pos int)
	DECLARE @zoekterm nvarchar(max) = '%5%'
	DECLARE @pos int
	DECLARE @oldpos int
	SELECT @oldpos=0
	SELECT @pos=patindex(@zoekterm,@verbalisatie) 
	WHILE @pos > 0 and @oldpos<>@pos
	BEGIN
		INSERT INTO @begin_characters VALUES (@pos)
		SELECT @oldpos=@pos
		SELECT @pos=patindex(@zoekterm,Substring(@verbalisatie,@pos + 1,len(@verbalisatie))) + @pos
	END
	SELECT * FROM @begin_characters