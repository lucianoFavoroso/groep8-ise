use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_selectSubtype')
	BEGIN
		DROP PROC usp_selectSubtype
	END
GO
CREATE PROCEDURE usp_selectSubtype
	@parent_entiteit int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

	SELECT EIP.ENTITEIT_NAAM,SG.GROEPSNAAM,SG.MUTUALLY_EXCLUSIVE,SG.COMPLETE FROM SUBTYPE_GROEP SG INNER JOIN ENTITEIT_IN_PROJECT EIP on SG.SUBTYPE_ID = EIP.SUBTYPE_ID
	WHERE SG.PARENT_ENTITEIT = @parent_entiteit
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
