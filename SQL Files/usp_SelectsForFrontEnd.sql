use FactBasedAnalysis
GO
IF EXISTS(select * from sys.procedures where name=N'usp_selectAllEntitiesFromProject')
	BEGIN
		DROP PROC usp_selectAllEntitiesFromProject
	END
GO
CREATE PROCEDURE usp_selectAllEntitiesFromProject 
	@project_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		select * from ENTITEIT_IN_PROJECT where PROJECT_ID = @project_id

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getAllEntitiesInProject')
	BEGIN
		DROP PROC usp_getAllEntitiesInProject
	END
GO
CREATE PROCEDURE usp_getAllEntitiesInProject 
	@project_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		SELECT ENTITEIT_NAAM,ENTITEIT_ID FROM ENTITEIT_IN_PROJECT WHERE PROJECT_ID = @project_id

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getVerbalisation')
	BEGIN
		DROP PROC usp_getVerbalisation
	END
GO
CREATE PROCEDURE usp_getVerbalisation 
	@verbalisatie_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		SELECT * FROM VERBALISATIE V INNER JOIN 
        SUBVERBALISATIE SV on v.VERBALISATIE_ID = sv.VERBALISATIE_ID
        WHERE V.VERBALISATIE_ID = @verbalisatie_id
		
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getSubverbs')
	BEGIN
		DROP PROC usp_getSubverbs
	END
GO
CREATE PROCEDURE usp_getSubverbs 
	@verbalisatie_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		SELECT * FROM VERBALISATIE V LEFT JOIN 
        SUBVERBALISATIE SV on v.VERBALISATIE_ID = sv.VERBALISATIE_ID
        WHERE V.VERBALISATIE_ID = @verbalisatie_id
		
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getEntitiesByVerbalisation')
	BEGIN
		DROP PROC usp_getEntitiesByVerbalisation
	END
GO
CREATE PROCEDURE usp_getEntitiesByVerbalisation 
	@verbalisatie_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

SELECT DISTINCT EIP.ENTITEIT_NAAM,AIV.ATTRIBUUT_NAAM,AIV.IS_PRIMARY_IDENTIFIER FROM ENTITEIT_IN_VERBALISATIE EIV 
INNER JOIN ENTITEIT_IN_PROJECT EIP on EIP.ENTITEIT_ID = EIV.ENTITEIT_ID
LEFT JOIN ATTRIBUUT_IN_VERBALISATIE AIV on AIV.ENTITEIT_ID = EIV.ENTITEIT_ID
WHERE EIV.VERBALISATIE_ID = @verbalisatie_id 
		
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO

GO
IF EXISTS(select * from sys.procedures where name=N'usp_getVerbalisations')
	BEGIN
		DROP PROC usp_getVerbalisations
	END
GO
CREATE PROCEDURE usp_getVerbalisations 
	@verbalisatie_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
SELECT * FROM VERBALISATIE V INNER JOIN 
        SUBVERBALISATIE SV on v.VERBALISATIE_ID = sv.VERBALISATIE_ID
        WHERE V.VERBALISATIE_ID = @verbalisatie_id
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getVerbalisationsInProject')
	BEGIN
		DROP PROC usp_getVerbalisationsInProject
	END
GO
CREATE PROCEDURE usp_getVerbalisationsInProject 
	@projectID int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
SELECT * FROM VERBALISATIE WHERE PROJECT_ID = @projectID
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getProjectsForUser')
	BEGIN
		DROP PROC usp_getProjectsForUser
	END
GO
CREATE PROCEDURE usp_getProjectsForUser 
	@Username varchar(500)
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
SELECT PROJECTNAAM,P.PROJECT_ID FROM PROJECT P INNER JOIN PROJECTBEZETTING PB on P.PROJECT_ID = PB.PROJECT_ID WHERE GEBRUIKERSNAAM = @Username
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getProjectMembersForProject')
	BEGIN
		DROP PROC usp_getProjectMembersForProject
	END
GO
GO
CREATE PROCEDURE usp_getProjectMembersForProject 
	@project_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
SELECT * FROM PROJECTBEZETTING WHERE PROJECT_ID = @project_id
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getSupertypeBySupertypeId')
	BEGIN
		DROP PROC usp_getSupertypeBySupertypeId
	END
GO
CREATE PROCEDURE usp_getSupertypeBySupertypeId 
	@supertype_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY

		select * from ENTITEIT_IN_PROJECT where ENTITEIT_ID = @supertype_id

		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
GO
IF EXISTS(select * from sys.procedures where name=N'usp_getRelationsByVerbalisation')
	BEGIN
		DROP PROC usp_getRelationsByVerbalisation
	END
GO
GO
CREATE PROCEDURE usp_getRelationsByVerbalisation 
	@verbalisatie_id int
AS
	SET NOCOUNT ON 
	SET XACT_ABORT OFF
	--check if there is already an tran running if yes set savepoint else start own
	DECLARE @TranCounter INT;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
	BEGIN TRY
SELECT REL.RELATIE_NAAM,EIP.ENTITEIT_NAAM as VAN,EIP2.ENTITEIT_NAAM as NAAR,REL.CARDINALITEITSTYPE FROM RELATIE REL
INNER JOIN ROL R on REL.RELATIE_ID = R.RELATIE_ID
INNER JOIN ENTITEIT_IN_PROJECT EIP on R.ENTITEIT_VAN = EIP.ENTITEIT_ID
INNER JOIN ENTITEIT_IN_PROJECT EIP2 on R.ENTITEIT_NAAR = EIP2.ENTITEIT_ID
WHERE REL.VERBALISATIE_ID = @verbalisatie_id
		--If there are no errors the tran must be commited else jump to catch block to close tran
		IF @TranCounter = 0 AND XACT_STATE() = 1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @TranCounter = 0 
			BEGIN
				--started own tran so rollback it
				IF XACT_STATE() = 1 ROLLBACK TRANSACTION;
			END;
		ELSE
			BEGIN
			--not started own tran rollback to save point
				IF XACT_STATE() <> -1 ROLLBACK TRANSACTION ProcedureSave;
			END;	
		THROW
	END CATCH
GO
